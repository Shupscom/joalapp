import 'package:dio/dio.dart';

import 'http_utils.dart';

class ErrorInterceptor extends Interceptor {
  @override
  Future onError(DioError err) async {
    err = await HttpUtils.buildErrorResponse(err);
    return err;
  }
}