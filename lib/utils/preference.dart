
import 'package:joalapp/model/user_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserPreferences {
  Future<bool> saveUser(UserModel user) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.setString("firstName", user.firstName);
    prefs.setString("lastName", user.lastName);
    prefs.setString("email", user.email);
    prefs.setString("height", user.height);
    prefs.setString("weight", user.weight);
    prefs.setString("bio", user.bio);
    prefs.setString("dob", user.dob);
    prefs.setString("city", user.city);
    prefs.setString("countryOfOrigin", user.countryOfOrigin);
    prefs.setString("gender", user.gender);
    prefs.setString("weightGoal", user.weightGoal);
    prefs.setString("activityLevel", user.activityLevel);


    return prefs.commit();
  }

  Future<UserModel> getUser() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    String firstName = prefs.getString("firstName");
    String lastName = prefs.getString("lastName");
    String email = prefs.getString("email");
    String height = prefs.getString("height");
    String weight = prefs.getString("weight");
    String bio = prefs.getString("bio");
    String dob = prefs.getString("dob");
    String city = prefs.getString("city");
    String countryOfOrigin = prefs.getString("countryOfOrigin");
    String gender = prefs.getString("gender");
    String weightGoal = prefs.getString("weightGoal");
    String activityLevel = prefs.getString("activityLevel");


    return UserModel(
        firstName: firstName,
        lastName: lastName,
        email: email,
        height: height,
        weight: weight,
        bio: bio,
        dob: dob,
        city: city,
        countryOfOrigin: countryOfOrigin,
        gender: gender,
        weightGoal: weightGoal,
       activityLevel: activityLevel
    );
  }

  void removeUser() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove("email");
  }
  Future<String> getToken(args) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");
    return token;
  }
}