import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/model/response_data.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../attachment.dart';
import 'connection_utils.dart';
import 'http_errro_string.dart';

class HttpUtils {
  static final BaseOptions options = new BaseOptions(
    connectTimeout: 85800,
    receiveTimeout: 19250,
  );
  static Dio getInstance() {
    Dio dio = Dio(options)
      ..interceptors.add(PrettyDioLogger(
          requestBody: true,
          requestHeader: true,
          responseBody: true,
          responseHeader: false,
          compact: false,
          error: true,
          maxWidth: 90,
      ),
      );
      dio.interceptors.add(
        InterceptorsWrapper(
            onRequest: (RequestOptions requestOptions) async {
          dio.interceptors.requestLock.lock();
             SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
            var token = sharedPreferences.getString("token");
            print("http");
            print(token);
          if (token != null) {
            dio.options.headers[HttpHeaders.authorizationHeader] =
                'Bearer ' + token;
            print(dio.options.headers);
          }
          dio.interceptors.requestLock.unlock();
          return requestOptions;
        },
//          onError: (DioError error) async {
//
//
//
////            SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
////            var token = sharedPreferences.getString("access_token");
////            var refreshToken = sharedPreferences.getString("refreshToken");
////            print(token);
//            if(error.response?.statusCode == 401){
//              dio.interceptors.requestLock.lock();
//              dio.interceptors.responseLock.lock();
//              RequestOptions options = error.response.request;
////                var newToken = await accountRepository.refreshToken(token, refreshToken);
////              options.headers["Authorization"] = "Bearer " + newToken;
//              dio.interceptors.requestLock.unlock();
//              dio.interceptors.responseLock.unlock();
//              return dio.request(options.path, options: options);
//            }
//            else if(error.response?.statusCode == 451){
////              await accountRepository.deleteToken();
////              await accountRepository.logout();
////              UserPreferences().removeUser();
//             App.attachmentNavKey.currentState
//            .pushNamedAndRemoveUntil('/signin', (_) => false);
//            }else if(error.response?.statusCode == 0){
////             await accountRepository.deleteToken();
////              await accountRepository.logout();
////             await _userRepository.deleteAllUser();
////              UserPreferences().removeUser();
//             App.attachmentNavKey.currentState
//                 .pushNamedAndRemoveUntil('/signin', (_) => false);
//            }
//            return error;
//          }
        ),
      );
    return dio;
  }

  static Future<DioError> buildErrorResponse(DioError err) async {
    print(err.type);
    print(err.response);
    switch (err.type) {
      case DioErrorType.CONNECT_TIMEOUT:
        if (await ConnectionUtils.getActiveStatus()) {
          err.response =
              Response(data: HttpErrorStrings.CONNECTION_TIMEOUT_ACTIVE);
        } else {
          err.response =
              Response(data: HttpErrorStrings.CONNECTION_TIMEOUT_NOT_ACTIVE);
        }
        break;
      case DioErrorType.SEND_TIMEOUT:
        err.response = Response(data: HttpErrorStrings.SEND_TIMEOUT);
        break;
      case DioErrorType.RECEIVE_TIMEOUT:
        err.response = Response(data: HttpErrorStrings.RECEIVE_TIMEOUT);
        break;
      case DioErrorType.RESPONSE:
        if (err.response.statusCode == HttpStatus.badRequest) {
         var value = json.decode(err.response.data);
          err.response = Response(data: value['data']);
        }
        else {
          err.response = Response(data: HttpErrorStrings.BAD_RESPONSE);
        }
        break;
      case DioErrorType.CANCEL:
        err.response = Response(data: HttpErrorStrings.OPERATION_CANCELLED);
        break;
      case DioErrorType.DEFAULT:
        if (!await ConnectionUtils.getActiveStatus()) {
          err.response = Response(data: HttpErrorStrings.DEFAULT);
        }
        else {
          err.response = Response(data: HttpErrorStrings.BAD_RESPONSE);
        }
        break;
      default:
        err.response = Response(data: HttpErrorStrings.UNKNOWN);
        break;
    }
    return err;
  }
}
