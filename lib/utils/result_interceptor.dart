import 'dart:io';
import 'package:dio/dio.dart';
import 'package:joalapp/model/response_data.dart';

class ResultInterceptor extends Interceptor {
  static const BAD_REQUEST_STATUS_MESSAGE = "BAD REQUEST";

  @override
  Future onResponse(Response response) async {
    if (response.statusCode == HttpStatus.ok) {
      ResponseData responseData = ResponseData.fromJson(response.data);
      if (responseData.success) {
        response.data = responseData.data;
        return response;
      } else {
        String message;
        if (responseData.message != null) {
          message = responseData.message;
        } else {
          message = responseData.data;
        }
        return new DioError(
            request: response.request,
            response: Response(
                data: message,
                request: response.request,
                statusCode: HttpStatus.badRequest,
                statusMessage: BAD_REQUEST_STATUS_MESSAGE),
            type: DioErrorType.RESPONSE,
            error: message);
      }
    }
  }
}
