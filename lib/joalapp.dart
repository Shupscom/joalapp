import 'package:flutter/material.dart';
import 'package:joalapp/utils/theme.dart';
import 'package:provider/provider.dart';

import 'attachment.dart';
import 'model/dark_theme.dart';
class JaolApp extends StatefulWidget {
  @override
  _JaolAppState createState() => _JaolAppState();
}

class _JaolAppState extends State<JaolApp> {
  DarkThemeProvider themeChangeProvider = new DarkThemeProvider();

  @override
  void initState() {
    getCurrentAppTheme();
    super.initState();

  }

  void getCurrentAppTheme() async {
    themeChangeProvider.darkTheme =
    await themeChangeProvider.darkThemePreference.getTheme();
  }
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) {
        return themeChangeProvider;
      },
      child: Consumer<DarkThemeProvider>(
      builder: (BuildContext context, value, Widget child) {
       return  MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: Styles.themeData(themeChangeProvider.darkTheme, context),
          title: "JaolApp",
          initialRoute: '/',
          onGenerateRoute: _getRoute,
        );
      }
      ),
    );
  }
}

Route<dynamic> _getRoute(RouteSettings settings) {
  Widget builder = Container();
  bool fullscreenDialog = false;
  switch (settings.name) {
    case '/':
      builder = Scaffold(
        body: App(),
      );
      fullscreenDialog = false;
      break;
    default:
      throw Exception('JaolApp: Invalid route: ${settings.name}');
      break;
  }
  return MaterialPageRoute(
    builder: (_) => builder,
    fullscreenDialog: fullscreenDialog,
    settings: settings,
  );
}