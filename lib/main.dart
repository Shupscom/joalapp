import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/joalapp.dart';
import 'package:joalapp/model/dark_theme.dart';
import 'package:joalapp/repository/subscription_repository.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'bloc/authentication/authenticate_bloc.dart';
import 'bloc/authentication/authenticate_event.dart';

class SimpleBlocDelegate extends BlocDelegate {
  @override
  void onEvent(Bloc bloc, Object event) {
    print(event);
    super.onEvent(bloc, event);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    print(transition);
    super.onTransition(bloc, transition);
  }

  @override
  void onError(Bloc bloc, Object error, StackTrace stackTrace) {
    print(error);
    super.onError(bloc, error, stackTrace);
  }
}
void main() {
  BlocSupervisor.delegate = SimpleBlocDelegate();
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);
  SharedPreferences.setMockInitialValues({});
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider<DarkThemeProvider>(
          create: (BuildContext context) => DarkThemeProvider(),
        ),
        FutureProvider(create: (_) => SubscriptionRepository.getSubcriptionActive(), initialData: "Loading",)
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider<AuthenticationBloc>(
              create: (context){
                return AuthenticationBloc()..add(AppStarted());
              }),
        ],
        child: JaolApp(),
      ),
    ),

  );
}


