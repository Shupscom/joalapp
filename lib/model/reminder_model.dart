class ReminderModel {
  String sId;
  String type;
  int frequency;
  CreatedBy createdBy;
  int hour;
  int minute;
  List<int> selectedDays;
  String time;
  bool isActive;
  bool isRecurring;
  String createdAt;
  String updatedAt;

  ReminderModel(
      {this.sId,
        this.type,
        this.frequency,
        this.createdBy,
        this.hour,
        this.minute,
        this.selectedDays,
        this.time,
        this.isActive,
        this.isRecurring,
        this.createdAt,
        this.updatedAt});

  ReminderModel.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    type = json['type'];
    frequency = json['frequency'];
    createdBy = json['createdBy'] != null
        ? new CreatedBy.fromJson(json['createdBy'])
        : null;
    hour = json['hour'];
    minute = json['minute'];
    selectedDays = json['selectedDays'].cast<int>();
    time = json['time'];
    isActive = json['isActive'];
    isRecurring = json['isRecurring'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['type'] = this.type;
    data['frequency'] = this.frequency;
    if (this.createdBy != null) {
      data['createdBy'] = this.createdBy.toJson();
    }
    data['hour'] = this.hour;
    data['minute'] = this.minute;
    data['selectedDays'] = this.selectedDays;
    data['time'] = this.time;
    data['isActive'] = this.isActive;
    data['isRecurring'] = this.isRecurring;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    return data;
  }
}

class CreatedBy {
  int height;
  int weight;
  double heightInMeters;
  int weightInKG;
  String sId;
  String email;
  String dob;
  String firstName;
  String lastName;
  String fullName;
  Bmi bmi;
  int age;
  String id;

  CreatedBy(
      {this.height,
        this.weight,
        this.heightInMeters,
        this.weightInKG,
        this.sId,
        this.email,
        this.dob,
        this.firstName,
        this.lastName,
        this.fullName,
        this.bmi,
        this.age,
        this.id});

  CreatedBy.fromJson(Map<String, dynamic> json) {
    height = json['height'];
    weight = json['weight'];
    heightInMeters = json['heightInMeters'];
    weightInKG = json['weightInKG'];
    sId = json['_id'];
    email = json['email'];
    dob = json['dob'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    fullName = json['fullName'];
    bmi = json['bmi'] != null ? new Bmi.fromJson(json['bmi']) : null;
    age = json['age'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['height'] = this.height;
    data['weight'] = this.weight;
    data['heightInMeters'] = this.heightInMeters;
    data['weightInKG'] = this.weightInKG;
    data['_id'] = this.sId;
    data['email'] = this.email;
    data['dob'] = this.dob;
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['fullName'] = this.fullName;
    if (this.bmi != null) {
      data['bmi'] = this.bmi.toJson();
    }
    data['age'] = this.age;
    data['id'] = this.id;
    return data;
  }
}

class Bmi {
  double value;
  String weight;

  Bmi({this.value, this.weight});

  Bmi.fromJson(Map<String, dynamic> json) {
    value = json['value'];
    weight = json['weight'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['value'] = this.value;
    data['weight'] = this.weight;
    return data;
  }
}
