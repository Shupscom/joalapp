class NuggetModel {
  String id;
  String body;
  String category;
  String imgSrc;
  List<String> tags;

  NuggetModel({this.body, this.tags});

  NuggetModel.fromJson(Map<String, dynamic> json) {
    body = json['_id'];
    body = json['body'];
    category = json['category'];
    imgSrc = json['imgSrc'];
    tags = json['tags'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.id;
    data['body'] = this.body;
    data['category'] = this.category;
    data['tags'] = this.tags;
    data['imgSrc'] = this.imgSrc;
    return data;
  }
}