class MealModel {
  String name;
  List<String> components;

  MealModel({this.name, this.components});

  MealModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    components = json['components'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['components'] = this.components;
    return data;
  }
}