import 'package:joalapp/model/response_data.dart';

class DataModel {
  String message;
  ResponseData responseData;

  DataModel({this.message, this.responseData});

  DataModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    responseData = json['responseData'] != null ? new ResponseData.fromJson(json['responseData']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    if (this.responseData != null) {
      data['responseData'] = this.responseData.toJson();
    }
    return data;
  }
}