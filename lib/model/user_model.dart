class UserModel {
  String firstName;
  String lastName;
  String email;
  String password;
  String conPassword;
  String height;
  String weight;
  String bio;
  String dob;
  String city;
  String countryOfOrigin;
  String countryOfResisdence;
  String gender;
  String weightGoal;
  String activityLevel;

  UserModel(
      {this.firstName,
        this.lastName,
        this.email,
        this.password,
        this.conPassword,
        this.height,
        this.weight,
        this.bio,
        this.dob,
        this.city,
        this.countryOfOrigin,
        this.countryOfResisdence,
        this.gender,
        this.weightGoal,
        this.activityLevel});

  UserModel.fromJson(Map<String, dynamic> json) {
    firstName = json['firstName'];
    lastName = json['lastName'];
    email = json['email'];
    password = json['password'];
    conPassword = json['conPassword'];
    height = json['height'];
    weight = json['weight'];
    bio = json['bio'];
    dob = json['dob'];
    city = json['city'];
    countryOfOrigin = json['countryOfOrigin'];
    countryOfResisdence = json['countryOfResisdence'];
    gender = json['gender'];
    weightGoal = json['weightGoal'];
    activityLevel = json['activityLevel'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['email'] = this.email;
    data['password'] = this.password;
    data['conPassword'] = this.conPassword;
    data['height'] = this.height;
    data['weight'] = this.weight;
    data['bio'] = this.bio;
    data['dob'] = this.dob;
    data['city'] = this.city;
    data['countryOfOrigin'] = this.countryOfOrigin;
    data['countryOfResisdence'] = this.countryOfResisdence;
    data['gender'] = this.gender;
    data['weightGoal'] = this.weightGoal;
    data['activityLevel'] = this.activityLevel;
    return data;
  }
}

