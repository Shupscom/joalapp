class PlanModel {
  String name;
  List<Days> days;

  PlanModel({this.name, this.days});

  PlanModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    if (json['days'] != null) {
      days = new List<Days>();
      json['days'].forEach((v) {
        days.add(new Days.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    if (this.days != null) {
      data['days'] = this.days.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Days {
  int day;
  String breakfast;
  String lunch;
  String dinner;
  String bSnack;
  String lSnack;
  String dSnack;

  Days(
      {this.day,
        this.breakfast,
        this.lunch,
        this.dinner,
        this.bSnack,
        this.lSnack,
        this.dSnack});

  Days.fromJson(Map<String, dynamic> json) {
    day = json['day'];
    breakfast = json['breakfast'];
    lunch = json['lunch'];
    dinner = json['dinner'];
    bSnack = json['BSnack'];
    lSnack = json['LSnack'];
    dSnack = json['DSnack'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['day'] = this.day;
    data['breakfast'] = this.breakfast;
    data['lunch'] = this.lunch;
    data['dinner'] = this.dinner;
    data['BSnack'] = this.bSnack;
    data['LSnack'] = this.lSnack;
    data['DSnack'] = this.dSnack;
    return data;
  }
}