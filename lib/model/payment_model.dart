class PaymentModel {
  String currency;
  String plan;
  String channel;
  String cardNumber;
  String cvv;
  int expiryMonth;
  int expiryYear;

  PaymentModel({this.currency,
    this.plan,
    this.channel,
    this.cardNumber,
    this.cvv,
    this.expiryMonth,
    this.expiryYear});

  PaymentModel.fromJson(Map<String, dynamic> json) {
    currency = json['currency'];
    plan = json['plan'];
    channel = json['channel'];
    cardNumber = json['cardNumber'];
    cvv = json['cvv'];
    expiryMonth = json['expiryMonth'];
    expiryYear = json['expiryYear'];
  }
}