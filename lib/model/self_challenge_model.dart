
class SelfChallengeModel {
  String challengeGroup;
  String startDate;
  String endDate;

  SelfChallengeModel({this.challengeGroup, this.startDate, this.endDate});

  SelfChallengeModel.fromJson(Map<String, dynamic> json) {
    challengeGroup = json['challengeGroup'];
    startDate = json['startDate'];
    endDate = json['endDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['challengeGroup'] = this.challengeGroup;
    data['startDate'] = this.startDate;
    data['endDate'] = this.endDate;
    return data;
  }
}