class  SuperMealModel{
  String sId;
  String name;
  Nutrients nutrients;
  String imgSrc;
  bool isActive;
  List<String> categories;
  List<String> groups;
  String createdAt;
  String updatedAt;

  SuperMealModel({this.sId,
        this.name,
        this.nutrients,
        this.imgSrc,
        this.isActive,
        this.categories,
        this.groups,
        this.createdAt,
        this.updatedAt});

  SuperMealModel.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    name = json['name'];
    nutrients = json['nutrients'] != null
        ? new Nutrients.fromJson(json['nutrients'])
        : null;
    imgSrc = json['imgSrc'];
    isActive = json['isActive'];
    categories = json['categories'].cast<String>();
    groups = json['groups'].cast<String>();
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['name'] = this.name;
    if (this.nutrients != null) {
      data['nutrients'] = this.nutrients.toJson();
    }
    data['imgSrc'] = this.imgSrc;
    data['isActive'] = this.isActive;
    data['categories'] = this.categories;
    data['groups'] = this.groups;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    return data;
  }
}

class Nutrients {
  String sId;
  double carbs;
  double proteins;
  double fats;
  double calories;

  Nutrients({this.sId, this.carbs, this.proteins, this.fats, this.calories});

  Nutrients.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    carbs = double.parse(json['carbs'].toString());
    proteins = double.parse(json['proteins'].toString());
    fats = double.parse(json['fats'].toString());
    calories = double.parse(json['calories'].toString());
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['carbs'] = this.carbs;
    data['proteins'] = this.proteins;
    data['fats'] = this.fats;
    data['calories'] = this.calories;
    return data;
  }
}