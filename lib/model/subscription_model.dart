class SubscriptionModel {
  String sId;
  String name;
  String code;
  int frequency;
  String frequencyType;
  String createdBy;
  String description;
  bool isActive;
  List<Prices> prices;

  SubscriptionModel(
      {this.sId,
        this.name,
        this.code,
        this.frequency,
        this.frequencyType,
        this.createdBy,
        this.description,
        this.isActive,
        this.prices});

  SubscriptionModel.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    name = json['name'];
    code = json['code'];
    frequency = json['frequency'];
    frequencyType = json['frequencyType'];
    createdBy = json['createdBy'];
    description = json['description'];
    isActive = json['isActive'];
    if (json['prices'] != null) {
      prices = new List<Prices>();
      json['prices'].forEach((v) {
        prices.add(new Prices.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['name'] = this.name;
    data['code'] = this.code;
    data['frequency'] = this.frequency;
    data['frequencyType'] = this.frequencyType;
    data['createdBy'] = this.createdBy;
    data['description'] = this.description;
    data['isActive'] = this.isActive;
    if (this.prices != null) {
      data['prices'] = this.prices.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Prices {
  Currency currency;
   double amount;

  Prices({this.currency, this.amount});

  Prices.fromJson(Map<String, dynamic> json) {
    currency = json['currency'] != null
        ? new Currency.fromJson(json['currency'])
        : null;
    amount = double.parse(json['amount'].toString());
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.currency != null) {
      data['currency'] = this.currency.toJson();
    }
    data['amount'] = this.amount;
    return data;
  }
}

class Currency {
  String sId;
  String name;
  String code;

  Currency({this.sId, this.name, this.code});

  Currency.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    name = json['name'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['name'] = this.name;
    data['code'] = this.code;
    return data;
  }
}