

class ChallengeModel {
  String sId;
  ChallengeGroup challengeGroup;
  String startDate;
  String endDate;
  bool isActive;
  String status;
  String createdAt;
  String updatedAt;

  ChallengeModel(
      {this.sId,
        this.challengeGroup,
        this.startDate,
        this.endDate,
        this.isActive,
        this.status,
        this.createdAt,
        this.updatedAt});

  ChallengeModel.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    challengeGroup = json['challengeGroup'] != null
        ? new ChallengeGroup.fromJson(json['challengeGroup'])
        : null;
    startDate = json['startDate'];
    endDate = json['endDate'];
    isActive = json['isActive'];
    status = json['status'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    if (this.challengeGroup != null) {
      data['challengeGroup'] = this.challengeGroup.toJson();
    }
    data['startDate'] = this.startDate;
    data['endDate'] = this.endDate;
    data['isActive'] = this.isActive;
    data['status'] = this.status;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    return data;
  }
}

class ChallengeGroup {
  String sId;
  String name;
  String createdAt;
  String updatedAt;

  ChallengeGroup({this.sId, this.name, this.createdAt, this.updatedAt});

  ChallengeGroup.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    name = json['name'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['name'] = this.name;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    return data;
  }
}