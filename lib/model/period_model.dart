class Period {
  PeriodPlan breakfast;
  PeriodPlan bSnack;
  PeriodPlan lunch;
  PeriodPlan lSnack;
  PeriodPlan dinner;
  PeriodPlan dSnack;

  Period(
      {this.breakfast,
        this.bSnack,
        this.lunch,
        this.lSnack,
        this.dinner,
        this.dSnack});

  Period.fromJson(Map<String, dynamic> json) {
    breakfast = json['breakfast'] != null
        ? new PeriodPlan.fromJson(json['breakfast'])
        : null;
    bSnack =
    json['BSnack'] != null ? new PeriodPlan.fromJson(json['BSnack']) : null;
    lunch =
    json['lunch'] != null ? new PeriodPlan.fromJson(json['lunch']) : null;
    lSnack =
    json['LSnack'] != null ? new PeriodPlan.fromJson(json['LSnack']) : null;
    dinner =
    json['dinner'] != null ? new PeriodPlan.fromJson(json['dinner']) : null;
    dSnack =
    json['DSnack'] != null ? new PeriodPlan.fromJson(json['DSnack']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.breakfast != null) {
      data['breakfast'] = this.breakfast.toJson();
    }
    if (this.bSnack != null) {
      data['BSnack'] = this.bSnack.toJson();
    }
    if (this.lunch != null) {
      data['lunch'] = this.lunch.toJson();
    }
    if (this.lSnack != null) {
      data['LSnack'] = this.lSnack.toJson();
    }
    if (this.dinner != null) {
      data['dinner'] = this.dinner.toJson();
    }
    if (this.dSnack != null) {
      data['DSnack'] = this.dSnack.toJson();
    }
    return data;
  }
}

class PeriodPlan {
  List<String> components;

  PeriodPlan({this.components});

  PeriodPlan.fromJson(Map<String, dynamic> json) {
    components = json['components'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['components'] = this.components;
    return data;
  }
}