import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/screens/dietManager/create_meal_plan_screen.dart';
import 'package:joalapp/screens/dietManager/recommended_meal_screen.dart';
import 'package:joalapp/screens/landing_screen.dart';
import 'package:joalapp/screens/nuggets/all_nuggets_page.dart';
import 'package:joalapp/screens/nuggets/single_nuggets_screen.dart';
import 'package:joalapp/screens/onboarding/activity_screen.dart';
import 'package:joalapp/screens/onboarding/date_screen.dart';
import 'package:joalapp/screens/onboarding/email_signin_page.dart';
import 'package:joalapp/screens/onboarding/email_signin_screen.dart';
import 'package:joalapp/screens/onboarding/email_signup_page.dart';
import 'package:joalapp/screens/onboarding/gender_page.dart';
import 'package:joalapp/screens/onboarding/gender_screen.dart';
import 'package:joalapp/screens/onboarding/height_screen.dart';
import 'package:joalapp/screens/onboarding/location_screen.dart';
import 'package:joalapp/screens/onboarding/password_recovery.dart';
import 'package:joalapp/screens/onboarding/password_recovery_page.dart';
import 'package:joalapp/screens/onboarding/weight_goal_screen.dart';
import 'package:joalapp/screens/onboarding/weight_screen.dart';
import 'package:joalapp/screens/profile/edit_profile_screen.dart';
import 'package:joalapp/screens/profile/how_it_works_screen.dart';
import 'package:joalapp/screens/profile/metric_page.dart';
import 'package:joalapp/screens/profile/metric_screen.dart';
import 'package:joalapp/screens/profile/privacy_screen.dart';
import 'package:joalapp/screens/profile/profile_screen.dart';
import 'package:joalapp/screens/profile/subscription_screen.dart';
import 'package:joalapp/screens/profile/subscription_upgrade_page.dart';
import 'package:joalapp/screens/profile/subscription_upgrade_screen.dart';
import 'package:joalapp/screens/signup_page.dart';
import 'package:joalapp/screens/signup_screen.dart';
import 'package:joalapp/screens/splash_screen.dart';
import 'package:joalapp/screens/statistics/pick_challenge_screen.dart';
import 'package:joalapp/screens/statistics/statistics_screen.dart';
import 'package:joalapp/screens/subscription/payment_successful_screen.dart';
import 'package:joalapp/screens/subscription/subscription_plan_screen.dart';
import 'package:joalapp/screens/superMeals/add_super_meal_screen.dart';
import 'package:joalapp/screens/superMeals/super_meal_filter_screen.dart';
import 'package:joalapp/screens/superMeals/super_meal_page.dart';
import 'package:joalapp/screens/superMeals/super_meal_screen.dart';
import 'package:joalapp/screens/superMeals/supermeal_details.dart';
import 'package:joalapp/screens/water_tracker/day_end_screen.dart';
import 'package:joalapp/screens/water_tracker/set_reminder_page.dart';
import 'package:joalapp/screens/water_tracker/set_reminder_screen.dart';
import 'package:joalapp/screens/water_tracker/water_tracker_page.dart';
import 'package:joalapp/screens/water_tracker/water_tracker_screen.dart';

import 'bloc/authentication/authenticate_bloc.dart';
import 'bloc/authentication/authenticate_state.dart';

class App extends StatefulWidget {
  static final GlobalKey<NavigatorState> attachmentNavKey =
      GlobalKey<NavigatorState>();

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> with WidgetsBindingObserver {
  @override
  void initState() {
    // TODO: implement initState
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthenticationBloc, AuthenticationState>(
      listener: (BuildContext context, AuthenticationState state) {
        if (state is AuthenticationStateTimeOut) {
//          _showTimeOutDialog(context);
        } else if (state is AuthenticationStateShowOnBoarding) {
          App.attachmentNavKey.currentState
              .pushNamedAndRemoveUntil('/welcomeScreen', (_) => false);
        } else if (state is AuthenticationAuthenticated) {
//          App.attachmentNavKey.currentState
//              .pushNamedAndRemoveUntil('/profileScreen', (_) => false,);
        App.attachmentNavKey.currentState.pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => ProfileScreen(userData: state.userData)) ,
                (route) => false);
        }
        else if (state is AuthenticationUnauthenticated) {
          App.attachmentNavKey.currentState
              .pushNamedAndRemoveUntil('/emailsignInScreen', (_) => false);
        } else if (state is AuthenticationLoading) {
          App.attachmentNavKey.currentState.pushNamedAndRemoveUntil(
            '/loading', (_) => false,
          );
        }


      },
      child: WillPopScope(
        onWillPop: () async {
          return !await App.attachmentNavKey.currentState.maybePop();
        },
        child: Navigator(
          key: App.attachmentNavKey,
          initialRoute: 'attachment',
          onGenerateRoute: _onGenerateRoute,
//          observers: [observer],
        ),
      ),
    );
  }

  Route _onGenerateRoute(RouteSettings settings) {
    Widget builder = Container();
    bool fullscreenDialog = false;
    switch (settings.name) {
      case 'attachment':
        builder = _Content();
        break;
      case '/welcomeScreen':
        builder = LandingScreen();
        fullscreenDialog = true;
        break;
      case '/signupScreen':
        builder = SignUpPage();
        fullscreenDialog = true;
        break;
      case '/emailsignUpScreen':
        builder = RegisterSignUpPage();
        break;
      case '/emailsignInScreen':
        builder = EmailSignInPage();
        break;
      case '/passwordRecovery':
        builder = PasswordRecoveryPage();
        break;
      case '/dateScreen':
        builder = DateBirthScreen();
        break;
      case '/heightScreen':
        builder = HeightScreen();
        break;
      case '/weightScreen':
        builder = WeightScreen();
        break;
      case '/activityScreen':
        builder = ActivityLevelScreen();
        break;
      case '/weightgoalScreen':
        builder = WeightGoalScreen();
        break;
      case '/locationScreen':
        builder = LocationScreen();
        break;
      case '/profileScreen':
        builder = ProfileScreen();
        break;
      case '/editrProfileScreen':
        builder = EditProfileScreen();
        break;
      case '/metricScreen':
        builder = MetricPage();
        break;
      case '/howScreen':
        builder = HowScreen();
        break;
      case '/subscriptionScreen':
        builder = SubscriptionScreen();
        break;
      case '/subscriptionUpgradeScreen':
        builder = SubscriptionUpgradePage();
        break;
      case '/privacyScreen':
        builder = PrivacyScreen();
        break;
      case '/allNuggetScreen':
        builder = AllNuggetsPage();
        break;
      case '/singleNuggetScreen':
        builder = SingleNuggetScreen();
        break;
      case '/subscriptionPlanScreen':
        builder = SubscriptionPlanScreeen();
        break;
      case '/paymentSucessfulScreen':
        builder = PaymentSucessfulScreen();
        break;
      case '/superMealScreen':
        builder = SuperMealPage();
        break;
      case '/superMealFilterScreen':
        builder = SuperMealFilterScreen();
        break;
      case '/superMealDetailScreen':
        builder = SuperMealDetailScreen();
        break;
      case '/addSuperMealScreen':
        builder = AddSuperMealScreen();
        break;
      case '/waterTrackerScreen':
        builder = WaterTrackerPage();
        break;
      case '/setReminderScreen':
        builder = SetReminderPage();
        break;
      case '/dayEndQuestionScreen':
        builder = DayEndQuestionScreen();
        break;
      case '/recommendedMealScreen':
        builder = RecommendMealScreen();
        break;
      case '/createMealPlamScreen':
        builder = CreateMealPlanScreen();
        break;
      case '/statisticScreen':
        builder = StatisticScreen();
        break;
      case '/pickChallengeScreen':
        builder = PickChallengeScreen();
        break;

//      case 'attachment/loading':
//        LoadingPageArgument args = settings.arguments;
//        builder = LoadingPage(
//          accessToken: args.accessToken,
//          type: args.type,
//        );
//        break;

      default:
        throw Exception('Attachment: Invalid route: ${settings.name}');
        break;
    }

    return MaterialPageRoute(
      builder: (_) => builder,
      fullscreenDialog: fullscreenDialog,
      settings: settings,
    );
  }
}

class _Content extends StatefulWidget {
  @override
  _ContentState createState() => _ContentState();
}

class _ContentState extends State<_Content> {
  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  Widget build(BuildContext context) {
    return SplashScreen();
  }
}
