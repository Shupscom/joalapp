import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'dart:io';
import 'package:dio/adapter.dart';
import 'package:joalapp/api/api_util.dart';
import 'package:joalapp/utils/error_interceptor.dart';
import 'package:joalapp/utils/http_utils.dart';
import 'package:jwt_decode/jwt_decode.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthRepository{
 static HttpClient client = new HttpClient();
  static Future<String> authenticate(String email, String password) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String authApi = ApiUtil.MAIN_API_URL + "/auth/login";
    final dio = HttpUtils.getInstance();
    dio.interceptors.add(ErrorInterceptor());
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    Map<String, String> headers = {
      "Accept": "application/json",
      "Content-Type": "application/json"
    };
    Map<String, String> body = {"email": email, "password": password};
    try {
      Response response = await dio.post(authApi, options: Options(headers: headers, responseType: ResponseType.plain), data: body);
      if (response.statusCode == 200) {
        var valueData = json.decode(response.data);
        var token = valueData['data']['token'];
        sharedPreferences.setString("token", token,);
         return response.data;
      }
    } catch (e) {
      print(e);
      throw Exception(e.response.data);
    }
  }
  static Future<String> register({
    @required String email,
    @required String password,
  }) async {
    String authApi = ApiUtil.MAIN_API_URL + "/auth/signup";
    final dio = HttpUtils.getInstance();
    dio.interceptors.add(ErrorInterceptor());
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    Map<String, String> headers = {
      "Accept": "application/json",
      "Content-Type": "application/json"
    };
    Map<String, dynamic> body = {
      "email": email,
      "password": password,
    };
    try {
      Response response = await dio.post(authApi,
          options: Options(headers: headers, responseType: ResponseType.plain),
          data: body);
      print(response.statusCode);
      if(response.statusCode == 200){
        return response.data;
      }
    }
    on DioError catch (e) {
      throw Exception(e.response.data);
    }
  }

 static Future<String> socialauth({
   @required String email,
   @required String authType,
   @required String authSocialPlatform,
   @required String authSocialId


 }) async {
   String authApi = ApiUtil.MAIN_API_URL + "/auth/signup";
   final dio = HttpUtils.getInstance();
   dio.interceptors.add(ErrorInterceptor());
   (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
       (HttpClient client) {
     client.badCertificateCallback =
         (X509Certificate cert, String host, int port) => true;
     return client;
   };
   Map<String, String> headers = {
     "Accept": "application/json",
     "Content-Type": "application/json"
   };
   Map<String, dynamic> body = {
     "email": email,
     "authType": authType,
     "authSocialPlatform": authSocialPlatform,
     "authSocialId": authSocialId
   };
   try {
     Response response = await dio.post(authApi,
         options: Options(headers: headers, responseType: ResponseType.plain),
         data: body);
     print(response.statusCode);
     if(response.statusCode == 200){
       return response.data;
     }
   }
   on DioError catch (e) {
     throw Exception(e.response.data);
   }
 }

 static Future<String> socialauthlogin({
   @required String email,
   @required String authType,
 }) async {
   String authApi = ApiUtil.MAIN_API_URL + "/auth/login";
   final dio = HttpUtils.getInstance();
   dio.interceptors.add(ErrorInterceptor());
   (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
       (HttpClient client) {
     client.badCertificateCallback =
         (X509Certificate cert, String host, int port) => true;
     return client;
   };
   Map<String, String> headers = {
     "Accept": "application/json",
     "Content-Type": "application/json"
   };
   Map<String, dynamic> body = {
     "email": email,
     "authType": authType,
   };
   try {
     Response response = await dio.post(authApi,
         options: Options(headers: headers, responseType: ResponseType.plain),
         data: body);
     print(response.statusCode);
     if (response.statusCode == 200) {
       return response.data;
     }
   }
   on DioError catch (e) {
     throw Exception(e.response.data);
   }
 }



 static Future<String> recoverPassword({
   @required String email,
 }) async {
   String authApi = ApiUtil.MAIN_API_URL + "/auth/password/reset";
   final dio = HttpUtils.getInstance();
   dio.interceptors.add(ErrorInterceptor());
   (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
       (HttpClient client) {
     client.badCertificateCallback =
         (X509Certificate cert, String host, int port) => true;
     return client;
   };
   Map<String, String> headers = {
     "Accept": "application/json",
     "Content-Type": "application/json"
   };
   Map<String, dynamic> body = {
     "email": email
   };
   try {
     Response response = await dio.post(authApi,
         options: Options(headers: headers, responseType: ResponseType.plain),
         data: body);
     print(response.statusCode);
     if(response.statusCode == 200){
       return response.data;
     }
   }
   on DioError catch (e) {
     throw Exception(e.response.data);
   }
 }

 static Future<String> recoverPasswordCode({
   @required int code,
   @required String email,
 }) async {
   String authApi = ApiUtil.MAIN_API_URL + "/auth/password/verifycode";
   final dio = HttpUtils.getInstance();
   dio.interceptors.add(ErrorInterceptor());
   (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
       (HttpClient client) {
     client.badCertificateCallback =
         (X509Certificate cert, String host, int port) => true;
     return client;
   };
   Map<String, String> headers = {
     "Accept": "application/json",
     "Content-Type": "application/json"
   };
   Map<String, dynamic> body = {
     "code": code,
     "email": email
   };
   try {
     Response response = await dio.post(authApi,
         options: Options(headers: headers, responseType: ResponseType.plain),
         data: body);
     print(response.statusCode);
     if(response.statusCode == 200){
       return response.data;
     }
   }
   on DioError catch (e) {
     throw Exception(e.response.data);
   }
 }
 static Future<String> newPassword({
   @required String token,
   @required String password,
 }) async {
   String authApi = ApiUtil.MAIN_API_URL + "/auth/password/reset";
   final dio = HttpUtils.getInstance();
   dio.interceptors.add(ErrorInterceptor());
   (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
       (HttpClient client) {
     client.badCertificateCallback =
         (X509Certificate cert, String host, int port) => true;
     return client;
   };
   Map<String, String> headers = {
     "Accept": "application/json",
     "Content-Type": "application/json"
   };
   Map<String, dynamic> body = {
     "token": token,
     "password": password
   };
   try {
     Response response = await dio.patch(authApi,
         options: Options(headers: headers, responseType: ResponseType.plain),
         data: body);
     if(response.statusCode == 200){
       return response.data;
     }
   }
   on DioError catch (e) {
     throw Exception(e.response.data);
   }
 }

 static Future<String> genderUpdate({
   String gender,
 }) async {
   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
   var token = sharedPreferences.getString("token");
   String authApi = ApiUtil.MAIN_API_URL + "/user";
   final dio = HttpUtils.getInstance();
   dio.interceptors.add(ErrorInterceptor());
   (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
       (HttpClient client) {
     client.badCertificateCallback =
         (X509Certificate cert, String host, int port) => true;
     return client;
   };
   Map<String, String> headers = {
     "Accept": "application/json",
     "Content-Type": "application/json",
   "Authorization": "Bearer $token"
   };
   Map<String, dynamic> body = {
     "gender": gender,
   };
   try {
     Response response = await dio.patch(authApi,
         options: Options(headers: headers, responseType: ResponseType.plain),
         data: body);
     if(response.statusCode == 200){
       return response.data;
     }
   }
   on DioError catch (e) {
     throw Exception(e.response.data);
   }
 }

 static Future<String> dobUpdate({
   String dob,
 }) async {
   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
   var token = sharedPreferences.getString("token");
   String authApi = ApiUtil.MAIN_API_URL + "/user";
   final dio = HttpUtils.getInstance();
   dio.interceptors.add(ErrorInterceptor());
   (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
       (HttpClient client) {
     client.badCertificateCallback =
         (X509Certificate cert, String host, int port) => true;
     return client;
   };
   Map<String, String> headers = {
     "Accept": "application/json",
     "Content-Type": "application/json",
     "Authorization": "Bearer $token"
   };
   Map<String, dynamic> body = {
     "dob": dob,
   };
   try {
     Response response = await dio.patch(authApi,
         options: Options(headers: headers, responseType: ResponseType.plain),
         data: body);
     print(response.statusCode);
     print(response.data);
     if(response.statusCode == 200){
       return response.data;
     }
   }
   on DioError catch (e) {
     throw Exception(e.response.data);
   }

 }

 static Future<String> heightUpdate({
  int height,
   String heightUnit
 }) async {
   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
   var token = sharedPreferences.getString("token");
   String authApi = ApiUtil.MAIN_API_URL + "/user";
   final dio = HttpUtils.getInstance();
   dio.interceptors.add(ErrorInterceptor());
   (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
       (HttpClient client) {
     client.badCertificateCallback =
         (X509Certificate cert, String host, int port) => true;
     return client;
   };
   Map<String, String> headers = {
     "Accept": "application/json",
     "Content-Type": "application/json",
     "Authorization": "Bearer $token"
   };
   Map<String, dynamic> body = {
     "height": height,
     "heightUnit": heightUnit
   };
   try {
     Response response = await dio.patch(authApi,
         options: Options(headers: headers, responseType: ResponseType.plain),
         data: body);
     print(response.statusCode);
     print(response.data);
     if(response.statusCode == 200){
       return response.data;
     }
   }
   on DioError catch (e) {
     throw Exception(e.response.data);
   }

 }

 static Future<String> weightUpdate({
   int weight,
   String weightUnit
 }) async {
   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
   var token = sharedPreferences.getString("token");
   String authApi = ApiUtil.MAIN_API_URL + "/user";
   final dio = HttpUtils.getInstance();
   dio.interceptors.add(ErrorInterceptor());
   (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
       (HttpClient client) {
     client.badCertificateCallback =
         (X509Certificate cert, String host, int port) => true;
     return client;
   };
   Map<String, String> headers = {
     "Accept": "application/json",
     "Content-Type": "application/json",
   "Authorization": "Bearer $token"
   };
   Map<String, dynamic> body = {
     "weight": weight,
     "weightUnit": weightUnit
   };
   try {
     Response response = await dio.patch(authApi,
         options: Options(headers: headers, responseType: ResponseType.plain),
         data: body);
     print(response.statusCode);
     print(response.data);
     if(response.statusCode == 200){
       return response.data;
     }
   }
   on DioError catch (e) {
     throw Exception(e.response.data);
   }

 }

 static Future<String> activityUpdate({
   String  activityLevel,
 }) async {
   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
   var token = sharedPreferences.getString("token");
   String authApi = ApiUtil.MAIN_API_URL + "/user";
   final dio = HttpUtils.getInstance();
   dio.interceptors.add(ErrorInterceptor());
   (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
       (HttpClient client) {
     client.badCertificateCallback =
         (X509Certificate cert, String host, int port) => true;
     return client;
   };
   Map<String, String> headers = {
     "Accept": "application/json",
     "Content-Type": "application/json",
   "Authorization": "Bearer $token"
   };
   Map<String, dynamic> body = {
     "activityLevel": activityLevel,
   };
   try {
     Response response = await dio.patch(authApi,
         options: Options(headers: headers, responseType: ResponseType.plain),
         data: body);
     print(response.statusCode);
     print(response.data);
     if(response.statusCode == 200){
       return response.data;
     }
   }
   on DioError catch (e) {
     throw Exception(e.response.data);
   }
 }

 static Future<String> wgoalUpdate({
   String  weightGoal,
 }) async {
   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
   var token = sharedPreferences.getString("token");
   String authApi = ApiUtil.MAIN_API_URL + "/user";
   final dio = HttpUtils.getInstance();
   dio.interceptors.add(ErrorInterceptor());
   (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
       (HttpClient client) {
     client.badCertificateCallback =
         (X509Certificate cert, String host, int port) => true;
     return client;
   };
   Map<String, String> headers = {
     "Accept": "application/json",
     "Content-Type": "application/json",
     "Authorization": "Bearer $token"
   };
   Map<String, dynamic> body = {
     "weightGoal": weightGoal,
   };
   try {
     Response response = await dio.patch(authApi,
         options: Options(headers: headers, responseType: ResponseType.plain),
         data: body);
     print(response.statusCode);
     print(response.data);
     if(response.statusCode == 200){
       return response.data;
     }
   }
   on DioError catch (e) {
     throw Exception(e.response.data);
   }

 }

 static Future<String> userUpdate({
   String firstName, String lastName, int height, int weight, String gender,
   String  weightGoal, String activityLevel, int age, String heightUnit, String weightUnit
 }) async {
   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
   var token = sharedPreferences.getString("token");
   String authApi = ApiUtil.MAIN_API_URL + "/user";
   final dio = HttpUtils.getInstance();
   dio.interceptors.add(ErrorInterceptor());
   (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
       (HttpClient client) {
     client.badCertificateCallback =
         (X509Certificate cert, String host, int port) => true;
     return client;
   };
   Map<String, String> headers = {
     "Accept": "application/json",
     "Content-Type": "application/json",
     "Authorization": "Bearer $token"
   };
   Map<String, dynamic> body = {
     "firstName": firstName,
     "lastName": lastName,
     "height": height,
     "weight": weight,
     "age": age,
     "gender": gender,
     "weightGoal": weightGoal,
     "activityLevel": activityLevel,
     "weightUnit": weightUnit,
     "heightUnit": heightUnit
   };
   try {
     Response response = await dio.patch(authApi,
         options: Options(headers: headers, responseType: ResponseType.plain),
         data: body);
     print(response.statusCode);
     print(response.data);
     if(response.statusCode == 200){
       return response.data;
     }
   }
   on DioError catch (e) {
     throw Exception(e.response.data);
   }
 }

 static Future<String> metricUpdate({
   String heightUnit, String weightUnit
 }) async {
    print("*******");
    print(weightUnit);
   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
   var token = sharedPreferences.getString("token");
   String authApi = ApiUtil.MAIN_API_URL + "/user";
   final dio = HttpUtils.getInstance();
   dio.interceptors.add(ErrorInterceptor());
   (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
       (HttpClient client) {
     client.badCertificateCallback =
         (X509Certificate cert, String host, int port) => true;
     return client;
   };
   Map<String, String> headers = {
     "Accept": "application/json",
     "Content-Type": "application/json",
     "Authorization": "Bearer $token"
   };
   Map<String, dynamic> body = {
     "heightUnit": heightUnit,
     "weightUnit": weightUnit
   };
   try {
     Response response = await dio.patch(authApi,
         options: Options(headers: headers, responseType: ResponseType.plain),
         data: body);
     print(response.statusCode);
     print(response.data);
     if(response.statusCode == 200){
       return response.data;
     }
   }
   on DioError catch (e) {
     throw Exception(e.response.data);
   }
 }


 static Future<bool> feedBackPost({
   @required String comment,
 }) async {
   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
   var token = sharedPreferences.getString("token");
   String authApi = ApiUtil.MAIN_API_URL + "/comment";
   final dio = HttpUtils.getInstance();
   dio.interceptors.add(ErrorInterceptor());
   (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
       (HttpClient client) {
     client.badCertificateCallback =
         (X509Certificate cert, String host, int port) => true;
     return client;
   };
   Map<String, String> headers = {
     "Accept": "application/json",
     "Content-Type": "application/json",
     "Authorization": "Bearer $token"
   };
   Map<String, dynamic> body = {
     "body": comment,
   };
   try {
     Response response = await dio.post(authApi,
         options: Options(headers: headers, responseType: ResponseType.plain),
         data: body);
     print(response.statusCode);
     if(response.statusCode == 200){
       return true;
     }
   }
   on DioError catch (e) {
     throw Exception(e.response.data);
   }
 }


 Future<String> getCurrentUser() async {
   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
   var token = sharedPreferences.getString("token");
   var payload = Jwt.parseJwt(token);
   print(payload);
   var user_id = payload['id'];
   print(user_id);
   String authApi = ApiUtil.MAIN_API_URL + "/user/" + user_id;
   Map<String, dynamic> headers = {
     "Accept": "application/json",
     "Content-Type": "application/json",
     "Authorization": "Bearer $token"
   };
   final dio = HttpUtils.getInstance();
   dio.interceptors.add(ErrorInterceptor());
   (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
       (HttpClient client) {
     client.badCertificateCallback =
         (X509Certificate cert, String host, int port) => true;
     return client;
   };
   try {
     Response response =
     await dio.patch(authApi, options: Options(headers: headers));
     return response.data;
   } on DioError catch (e) {
     throw Exception(e.message);
   }
 }

}