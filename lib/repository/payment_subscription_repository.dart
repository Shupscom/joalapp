import 'dart:io';

import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:joalapp/api/api_util.dart';
import 'package:joalapp/utils/error_interceptor.dart';
import 'package:joalapp/utils/http_utils.dart';

class PaymentSubscriptionRepository{
  static HttpClient client = new HttpClient();

  static Future<String> payment({
   @required String currency,
   @required String plan,
   @required String channel,
   @required String cardNumber,
   @required String cvv,
   @required int expiryMonth,
   @required int expiryYear
  }) async {
    String authApi = ApiUtil.MAIN_API_URL + "/payment/process";
    final dio = HttpUtils.getInstance();
    dio.interceptors.add(ErrorInterceptor());
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    Map<String, String> headers = {
      "Accept": "application/json",
      "Content-Type": "application/json"
    };
    Map<String, dynamic> body = {
     "currency": currency,
      "plan": plan,
       "channel": channel,
       "cardNumber": cardNumber,
        "cvv": cvv,
        "expiryMonth": expiryMonth,
         "expiryYear": expiryYear
    };
    try {
      Response response = await dio.post(authApi,
          options: Options(headers: headers, responseType: ResponseType.plain),
          data: body);
      print(response.statusCode);
      if(response.statusCode == 200){
        return response.data;
      }
    }
    on DioError catch (e) {
      throw Exception(e.response.data);
    }
  }

}