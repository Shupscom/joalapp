
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:dio/adapter.dart';
import 'package:joalapp/api/api_util.dart';
import 'package:joalapp/utils/error_interceptor.dart';
import 'package:joalapp/utils/http_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NuggetRepository{
  static HttpClient client = new HttpClient();
  static Future<String> getNuggetByCategory() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");
    String authApi = ApiUtil.MAIN_API_URL + "/nugget?limit=50&category=general";
    final dio = HttpUtils.getInstance();
    dio.interceptors.add(ErrorInterceptor());
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    Map<String, String> headers = {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
    try {
      Response response = await dio.get(authApi, options: Options(headers: headers, responseType: ResponseType.plain));
      if (response.statusCode == 200) {
        return response.data;
      }
    } catch (e) {
      print(e);
      throw Exception(e.response.data);
    }
  }
  static Future<String> getNuggetByFruitCategory() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");
    String authApi = ApiUtil.MAIN_API_URL + "/nugget?limit=50&category=fruits";
    final dio = HttpUtils.getInstance();
    dio.interceptors.add(ErrorInterceptor());
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    Map<String, String> headers = {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
    try {
      Response response = await dio.get(authApi, options: Options(headers: headers, responseType: ResponseType.plain));
      if (response.statusCode == 200) {
        return response.data;
      }
    } catch (e) {
      print(e);
      throw Exception(e.response.data);
    }
  }

  static Future<String> getNuggetByNutritiousCategory() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");
    String authApi = ApiUtil.MAIN_API_URL + "/nugget?limit=50&category=nutrition";
    final dio = HttpUtils.getInstance();
    dio.interceptors.add(ErrorInterceptor());
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    Map<String, String> headers = {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
    try {
      Response response = await dio.get(authApi, options: Options(headers: headers, responseType: ResponseType.plain));
      if (response.statusCode == 200) {
        return response.data;
      }
    } catch (e) {
      print(e);
      throw Exception(e.response.data);
    }
  }

  static Future<String> getNuggetByExerciseCategory() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");
    String authApi = ApiUtil.MAIN_API_URL + "/nugget?limit=50&category=exercise";
    final dio = HttpUtils.getInstance();
    dio.interceptors.add(ErrorInterceptor());
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    Map<String, String> headers = {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
    try {
      Response response = await dio.get(authApi, options: Options(headers: headers, responseType: ResponseType.plain));
      if (response.statusCode == 200) {
        return response.data;
      }
    } catch (e) {
      print(e);
      throw Exception(e.response.data);
    }
  }
}