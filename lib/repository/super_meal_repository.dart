
import 'dart:io';

import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:joalapp/api/api_util.dart';
import 'package:joalapp/utils/error_interceptor.dart';
import 'package:joalapp/utils/http_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SuperMealRepository{
  static HttpClient client = new HttpClient();
  static Future<String> getMealByVegan() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");
    String authApi = ApiUtil.MAIN_API_URL + "/food?group=60bb2580dcdb8200157cf7ea";
    final dio = HttpUtils.getInstance();
    dio.interceptors.add(ErrorInterceptor());
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    Map<String, String> headers = {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
    try {
      Response response = await dio.get(authApi, options: Options(headers: headers, responseType: ResponseType.plain));
      if (response.statusCode == 200) {
        return response.data;
      }
    } catch (e) {
      print(e);
      throw Exception(e.response.data);
    }
  }

  static Future<String> getMealByCategory(String group, String category ) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");
    String authApi = ApiUtil.MAIN_API_URL + "/food?group=" + group  + "&category=" + category;

    final dio = HttpUtils.getInstance();
    dio.interceptors.add(ErrorInterceptor());
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    Map<String, String> headers = {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
    try {
      Response response = await dio.get(authApi, options: Options(headers: headers, responseType: ResponseType.plain));
      if (response.statusCode == 200) {
        return response.data;
      }
    } catch (e) {
      print(e);
      throw Exception(e.response.data);
    }
  }

  static Future<String> getMealByNonVegan() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");
    String authApi = ApiUtil.MAIN_API_URL + "/food?group=60bb2580dcdb8200157cf7eb";
    final dio = HttpUtils.getInstance();
    dio.interceptors.add(ErrorInterceptor());
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    Map<String, String> headers = {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
    try {
      Response response = await dio.get(authApi, options: Options(headers: headers, responseType: ResponseType.plain));
      if (response.statusCode == 200) {
        return response.data;
      }
    } catch (e) {
      print(e);
      throw Exception(e.response.data);
    }
  }

  static Future<String> getMealByHeadFighter() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");
    String authApi = ApiUtil.MAIN_API_URL + "/food?group=60bb2580dcdb8200157cf7ec";
    final dio = HttpUtils.getInstance();
    dio.interceptors.add(ErrorInterceptor());
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    Map<String, String> headers = {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
    try {
      Response response = await dio.get(authApi, options: Options(headers: headers, responseType: ResponseType.plain));
      if (response.statusCode == 200) {
        return response.data;
      }
    } catch (e) {
      print(e);
      throw Exception(e.response.data);
    }
  }
  static Future<String> getMealByImprovedSexLife() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");
    String authApi = ApiUtil.MAIN_API_URL + "/food?group=60bb2580dcdb8200157cf7ed";
    final dio = HttpUtils.getInstance();
    dio.interceptors.add(ErrorInterceptor());
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    Map<String, String> headers = {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
    try {
      Response response = await dio.get(authApi, options: Options(headers: headers, responseType: ResponseType.plain));
      if (response.statusCode == 200) {
        return response.data;
      }
    } catch (e) {
      print(e);
      throw Exception(e.response.data);
    }
  }
  static Future<String> getMealByAlkaline() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");
    String authApi = ApiUtil.MAIN_API_URL + "/food?group=60bb2580dcdb8200157cf7ee";
    final dio = HttpUtils.getInstance();
    dio.interceptors.add(ErrorInterceptor());
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    Map<String, String> headers = {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
    try {
      Response response = await dio.get(authApi, options: Options(headers: headers, responseType: ResponseType.plain));
      if (response.statusCode == 200) {
        return response.data;
      }
    } catch (e) {
      print(e);
      throw Exception(e.response.data);
    }
  }
  static Future<String> getMealByCalciumRich() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");
    String authApi = ApiUtil.MAIN_API_URL + "/food?group=60bb2580dcdb8200157cf7ef";
    final dio = HttpUtils.getInstance();
    dio.interceptors.add(ErrorInterceptor());
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    Map<String, String> headers = {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
    try {
      Response response = await dio.get(authApi, options: Options(headers: headers, responseType: ResponseType.plain));
      if (response.statusCode == 200) {
        return response.data;
      }
    } catch (e) {
      print(e);
      throw Exception(e.response.data);
    }
  }

  static Future<String> getMealByIronRich() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");
    String authApi = ApiUtil.MAIN_API_URL + "/food?group=60bb2580dcdb8200157cf7f0";
    final dio = HttpUtils.getInstance();
    dio.interceptors.add(ErrorInterceptor());
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    Map<String, String> headers = {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
    try {
      Response response = await dio.get(authApi, options: Options(headers: headers, responseType: ResponseType.plain));
      if (response.statusCode == 200) {
        return response.data;
      }
    } catch (e) {
      print(e);
      throw Exception(e.response.data);
    }
  }
  static Future<String> getMealByHighFiber() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");
    String authApi = ApiUtil.MAIN_API_URL + "/food?group=60bb2580dcdb8200157cf7f1";
    final dio = HttpUtils.getInstance();
    dio.interceptors.add(ErrorInterceptor());
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    Map<String, String> headers = {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
    try {
      Response response = await dio.get(authApi, options: Options(headers: headers, responseType: ResponseType.plain));
      if (response.statusCode == 200) {
        return response.data;
      }
    } catch (e) {
      print(e);
      throw Exception(e.response.data);
    }
  }
}