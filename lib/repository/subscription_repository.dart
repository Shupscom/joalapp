import 'dart:io';
import 'package:dio/dio.dart';
import 'package:dio/adapter.dart';
import 'package:joalapp/api/api_util.dart';
import 'package:joalapp/utils/error_interceptor.dart';
import 'package:joalapp/utils/http_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
class SubscriptionRepository{
  static HttpClient client = new HttpClient();
  static Future<String> getSubcriptionActive() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");
    print(token);
    String authApi = ApiUtil.MAIN_API_URL + "/subscription/active";
    final dio = HttpUtils.getInstance();
    dio.interceptors.add(ErrorInterceptor());
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    Map<String, String> headers = {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
    try {
      Response response = await dio.get(authApi, options: Options(headers: headers, responseType: ResponseType.plain));
      if (response.statusCode == 200) {
        return response.data;
      }
    } catch (e) {
      print(e);
      throw Exception(e.response.data);
    }
  }
  static Future<String> getAllSubcription() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");
    String authApi = ApiUtil.MAIN_API_URL + "/subscriptionplan";
    final dio = HttpUtils.getInstance();
    dio.interceptors.add(ErrorInterceptor());
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    Map<String, String> headers = {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
    try {
      Response response = await dio.get(authApi, options: Options(headers: headers, responseType: ResponseType.plain));
      if (response.statusCode == 200) {
        return response.data;
      }
    } catch (e) {
      print(e);
      throw Exception(e.response.data);
    }
  }
}