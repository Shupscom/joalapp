import 'dart:io';

import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:joalapp/api/api_util.dart';
import 'package:joalapp/model/self_challenge_model.dart';
import 'package:joalapp/utils/error_interceptor.dart';
import 'package:joalapp/utils/http_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ReminderRepository{
  static HttpClient client = new HttpClient();

  static Future<String> reminderSchedule({
    @required String type,
    @required int hour,
    @required int minute,
    @required List<int> days,
    @required int frequency
  }) async {
    String authApi = ApiUtil.MAIN_API_URL + "/reminder";
    final dio = HttpUtils.getInstance();
    dio.interceptors.add(ErrorInterceptor());
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    Map<String, String> headers = {
      "Accept": "application/json",
      "Content-Type": "application/json"
    };
    Map<String, dynamic> body = {
     "type": type,
      "hour": hour,
      "minute": minute,
      "selectedDays": days,
      "frequency": frequency
    };
    try {
      Response response = await dio.post(authApi,
          options: Options(headers: headers, responseType: ResponseType.plain),
          data: body);
      print(response.statusCode);
      if(response.statusCode == 200){
        return response.data;
      }
    }
    on DioError catch (e) {
      throw Exception(e.response.data);
    }
  }

  static Future<String> reminderMealSchedule({
    @required String type,
    @required int hour,
    @required int minute,
    @required List<int> days,
    @required String meal
  }) async {
    String authApi = ApiUtil.MAIN_API_URL + "/reminder";
    final dio = HttpUtils.getInstance();
    dio.interceptors.add(ErrorInterceptor());
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    Map<String, String> headers = {
      "Accept": "application/json",
      "Content-Type": "application/json"
    };
    Map<String, dynamic> body = {
      "type": type,
      "hour": hour,
      "minute": minute,
      "selectedDays": days,
      "meal": meal
    };
    try {
      Response response = await dio.post(authApi,
          options: Options(headers: headers, responseType: ResponseType.plain),
          data: body);
      print(response.statusCode);
      if(response.statusCode == 200){
        return response.data;
      }
    }
    on DioError catch (e) {
      throw Exception(e.response.data);
    }
  }

  static Future<String> getChallengeGroup() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");
    String authApi = ApiUtil.MAIN_API_URL + "/challengegroup";
    final dio = HttpUtils.getInstance();
    dio.interceptors.add(ErrorInterceptor());
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    Map<String, String> headers = {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
    try {
      Response response = await dio.get(authApi, options: Options(headers: headers, responseType: ResponseType.plain));
      if (response.statusCode == 200) {
        return response.data;
      }
    } catch (e) {
      print(e);
      throw Exception(e.response.data);
    }
  }

  static Future<String> reminderChallenge({
      List<SelfChallengeModel> challenges
  }) async {
    String authApi = ApiUtil.MAIN_API_URL + "/challenge";
    final dio = HttpUtils.getInstance();
    dio.interceptors.add(ErrorInterceptor());
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    Map<String, String> headers = {
      "Accept": "application/json",
      "Content-Type": "application/json"
    };
    Map<String, dynamic> body = {
      "challenges": challenges
    };
    try {
      Response response = await dio.post(authApi,
          options: Options(headers: headers, responseType: ResponseType.plain),
          data: body);
      print(response.statusCode);
      if(response.statusCode == 200){
        return response.data;
      }
    }
    on DioError catch (e) {
      throw Exception(e.response.data);
    }
  }


  static Future<String> getAllReminder() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");
    String authApi = ApiUtil.MAIN_API_URL + "/reminder";
    final dio = HttpUtils.getInstance();
    dio.interceptors.add(ErrorInterceptor());
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    Map<String, String> headers = {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
    try {
      Response response = await dio.get(authApi, options: Options(headers: headers, responseType: ResponseType.plain));
      if (response.statusCode == 200) {
        return response.data;
      }
    } catch (e) {
      print(e);
      throw Exception(e.response.data);
    }
  }

  static Future<String> getActiveChallenge() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");
    String authApi = ApiUtil.MAIN_API_URL + "/challenge?status=active";
    final dio = HttpUtils.getInstance();
    dio.interceptors.add(ErrorInterceptor());
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    Map<String, String> headers = {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
    try {
      Response response = await dio.get(authApi, options: Options(headers: headers, responseType: ResponseType.plain));
      if (response.statusCode == 200) {
        return response.data;
      }
    } catch (e) {
      print(e);
      throw Exception(e.response.data);
    }
  }
  static Future<String> getPastChallenge() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var token = sharedPreferences.getString("token");
    String authApi = ApiUtil.MAIN_API_URL + "/challenge?status=past";
    final dio = HttpUtils.getInstance();
    dio.interceptors.add(ErrorInterceptor());
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    Map<String, String> headers = {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
    try {
      Response response = await dio.get(authApi, options: Options(headers: headers, responseType: ResponseType.plain));
      if (response.statusCode == 200) {
        return response.data;
      }
    } catch (e) {
      print(e);
      throw Exception(e.response.data);
    }
  }

  static Future<bool> endChallenge({
    @required String challengeID,
  }) async {
    String authApi = ApiUtil.MAIN_API_URL + "/challenge/" + challengeID + "/end";
    final dio = HttpUtils.getInstance();
    dio.interceptors.add(ErrorInterceptor());
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    Map<String, String> headers = {
      "Accept": "application/json",
      "Content-Type": "application/json"
    };
//    Map<String, dynamic> body = {
//      "token": token,
//
//    };
    try {
      Response response = await dio.patch(authApi,
          options: Options(headers: headers, responseType: ResponseType.plain));
      if(response.statusCode == 200){
        return true;
      }
    }
    on DioError catch (e) {
      throw Exception(e.response.data);
    }
  }


}