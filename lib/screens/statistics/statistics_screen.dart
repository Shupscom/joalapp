import 'dart:convert';

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:joalapp/components/constants.dart';
import 'package:joalapp/components/make_bottom.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:table_calendar/table_calendar.dart';
class StatisticScreen extends StatefulWidget {
  @override
  _StatisticScreenState createState() => _StatisticScreenState();
}

class _StatisticScreenState extends State<StatisticScreen> with SingleTickerProviderStateMixin{
  @override
  Widget build(BuildContext context) {
    int _selectedIndex = 0;
    TabController _tabController;
    CalendarFormat _calendarFormat = CalendarFormat.month;
    DateTime _focusedDay = DateTime.now();
    DateTime _selectedDay;
    var userdata;

    @override
    void initState() {
      // TODO: implement initState
      super.initState();
      _tabController = TabController(length: 4, vsync: this);
      _tabController.addListener(() {
        setState(() {
          _selectedIndex = _tabController.index;
        });
      });
    }

    sizess(context);
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        bottomNavigationBar: MakeBottom(active: "statistic", userData: userdata,),
        appBar: AppBar(
//        leading: IconButton(
//          icon: Icon(Icons.arrow_back, color: Colors.black),
//          onPressed: () => Navigator.of(context).pop(),
//        ),
          backgroundColor: ksecondaryColour,
          elevation: 3,
          shadowColor: kprimaryColour,
          title: Align(
            alignment: Alignment(-1.5,0),
            child: Padding(
              padding: EdgeInsets.only(top: 16.0),
              child: Text("Statistics",
                style: TextStyle(
                    color: kprimaryColour,
                    fontSize: ScreenUtil().setSp(24.0)
                ),
              ),
            ),
          ),
          actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(
                      left: 10.0,
                      right: 10.0,
                      top: 16.0
                ),
                child: SvgPicture.asset("assets/images/calendar.svg")
            ),
          ],
          bottom: PreferredSize(
            preferredSize: const Size.fromHeight(100),
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  TableCalendar(
                    rowHeight: 45.0,
                    daysOfWeekHeight: 14.0,
                    firstDay: kFirstDay,
                    lastDay: kLastDay,
                    focusedDay: _focusedDay,
                    calendarFormat: CalendarFormat.week,
                    headerVisible: true,
                    startingDayOfWeek: StartingDayOfWeek.monday,
                    selectedDayPredicate: (day) {
                      return isSameDay(_selectedDay, day);
                    },
                    onDaySelected: (selectedDay, focusedDay) {
                      if (!isSameDay(_selectedDay, selectedDay)) {
                        // Call `setState()` when updating the selected day
                        setState(() {
                          _selectedDay = selectedDay;
                          _focusedDay = focusedDay;
                        });
                      }
                    },
                    headerStyle: HeaderStyle(
                        formatButtonVisible: false,
                        headerPadding: EdgeInsets.zero,
                        headerMargin: EdgeInsets.zero,
                        titleTextStyle: TextStyle(fontSize: 14.0)
//                    leftChevronMargin: EdgeInsets.all(0.0),
//                    rightChevronMargin: EdgeInsets.all(0.0)
                    ),
                    calendarStyle: CalendarStyle(
                      todayDecoration: BoxDecoration(
                        color: kprimaryColour,
                        shape: BoxShape.circle,
                      ),
                      selectedDecoration: BoxDecoration(
                        color: kprimaryColour,
                        shape: BoxShape.circle,
                      ),

                    ),

                    onFormatChanged: (format) {
                      if (_calendarFormat != format) {
                        // Call `setState()` when updating calendar format
                        setState(() {
                          _calendarFormat = format;
                        });
                      }
                    },
                    onPageChanged: (focusedDay) {
                      // No need to call `setState()` here
                      _focusedDay = focusedDay;
                    },
                  ),

//                  TableCalendar(

//                    calendarController: _controller,
//                    calendarStyle: CalendarStyle(
//                        todayColor: kprimaryColour,
//                        selectedColor: kprimaryColour,
//                        todayStyle: TextStyle(
//                            fontWeight: FontWeight.bold,
//                            fontSize: 16.0,
//                            color: Colors.white,
//                        ),
//                    ),


                  // )
                ],
              ),
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
            color: ksecondaryColour,
            child: Column(
//              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: ScreenUtil().setHeight(50.0),
                  child: TabBar(
                    isScrollable: true,
                    controller: _tabController,
                    labelColor: Colors.white,
                    unselectedLabelColor: ksecondaryTextColour,
                    indicatorColor: kprimaryColour,
                    indicator: BoxDecoration(
                        color: kprimaryColour
                    ),
                    tabs: [
                      Tab(child: Text('Breakfast',),
                      ),
                      Tab(child: Text('Snack',),
                      ),
                      Tab(child: Text('Lunch',),
                      ),
                      Tab(child: Text('Dinner',),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.70,
                  child: TabBarView(
                      children: [
                        SingleChildScrollView(child:
                         Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children: [
                             SizedBox(height: ScreenUtil().setHeight(100),),

                            PieChart(
                              PieChartData(

                                // read about it in the PieChartData section
                              ),
                              swapAnimationDuration: Duration(milliseconds: 150), // Optional
//                              swapAnimationCurve: Curves.linear, // Optional
                            ),
                             Text("Today Stat",
                              style: TextStyle(
                                color: ksecondaryTextColour
                              ),
                             ),
                             Container(
                               margin: EdgeInsets.all(ScreenUtil().setWidth(16)),
                               padding: EdgeInsets.all(ScreenUtil().setWidth(16)),
                               decoration: BoxDecoration(
                                   color: ksecondaryColour,
                                   boxShadow: [
                                     BoxShadow(
                                       color: Color.fromRGBO(0, 0, 0, 0.25),
                                       blurRadius: 20,
                                       offset: Offset(0, 2), // Shadow position
                                     ),
                                   ]
                               ),
                               child: GestureDetector(
                                 onTap: (){
                                   Navigator.pushNamed(context, "/pickChallengeScreen");
                                 },
                                 child: Row(
                                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                   children: [
                                     Text("Pick a challenge"),
                                     SvgPicture.asset("assets/images/arrow_click.svg", color: Colors.black,),
                                  ]
                                 ),
                               ),
                             ),
                             SizedBox(height: ScreenUtil().setHeight(20),),
                             Container(child: NestedTabBar())
                           ],
                         )
                        ),
                        SingleChildScrollView(child: Container()),
                        SingleChildScrollView(child: Container()),
                        SingleChildScrollView(child: Container())
                      ]
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class NestedTabBar extends StatefulWidget {
  @override
  _NestedTabBarState createState() => _NestedTabBarState();
}
class _NestedTabBarState extends State<NestedTabBar>
    with TickerProviderStateMixin {
  TabController _nestedTabController;
  @override
  void initState() {
    super.initState();
    _nestedTabController = new TabController(length: 2, vsync: this);
  }
  @override
  void dispose() {
    super.dispose();
    _nestedTabController.dispose();
  }
  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    return Column(
//      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(16),
            right: ScreenUtil().setWidth(16),
          ),
          child: TabBar(
            controller: _nestedTabController,
            indicatorColor: kprimaryColour,
            labelColor: kprimaryColour,
            unselectedLabelColor: Colors.black,
//            isScrollable: true,
            tabs: <Widget>[
              Tab(
                child: Text("Active challenges"),
              ),
              Tab(
                child: Text("Active challenges"),
              ),

            ],
          ),
        ),
        Container(
          height: screenHeight * 0.70,
//          margin: EdgeInsets.only(left: 16.0, right: 16.0),
          child: TabBarView(
            controller: _nestedTabController,
            children: <Widget>[
              SingleChildScrollView(
                child: Text("Te"),
              ),
              Container(child: Text("Tf"))
            ],
          ),
        )
      ],
    );
  }
}
