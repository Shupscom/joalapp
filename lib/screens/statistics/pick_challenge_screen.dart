import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:joalapp/components/constants.dart';
class PickChallengeScreen extends StatefulWidget {
  @override
  _PickChallengeScreenState createState() => _PickChallengeScreenState();
}

class _PickChallengeScreenState extends State<PickChallengeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: ksecondaryColour,
        elevation: 0,
        centerTitle: true,
        title: Padding(
          padding: EdgeInsets.only(
              top: ScreenUtil().setWidth(20.0)
          ),
          child: Text("Pick a challenge",
            style: TextStyle(
              color: kprimaryColour,
//                fontSize: ScreenUtil().setSp(18.0)
            ),
          ),
        ),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Text("Add",
              style: TextStyle(
                  color: Colors.black
              ),
            ),
          ),
        ],
      ),
      body: Container(
        color: ksecondaryColour,
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: ScreenUtil().setHeight(20),),
              Container(
                height: ScreenUtil().setHeight(70),
                margin: EdgeInsets.all(ScreenUtil().setWidth(16)),
                padding: EdgeInsets.all(ScreenUtil().setWidth(16)),
                decoration: BoxDecoration(
                    color: ksecondaryColour,
                    boxShadow: [
                      BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.25),
                        blurRadius: 20,
                        offset: Offset(0, 2), // Shadow position
                      ),
                    ]
                ),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Sweets"),
                      Checkbox(
                          value: true
//                        onChanged: (bool value) {
//                          setState(() {
//                            this.showvalue = value;
//                          });
//                        },
                      ),
                    ]
                ),
              ),
              Container(
                height: ScreenUtil().setHeight(70),
                margin: EdgeInsets.all(ScreenUtil().setWidth(16)),
                padding: EdgeInsets.all(ScreenUtil().setWidth(16)),
                decoration: BoxDecoration(
                    color: ksecondaryColour,
                    boxShadow: [
                      BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.25),
                        blurRadius: 20,
                        offset: Offset(0, 2), // Shadow position
                      ),
                    ]
                ),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Sweets"),
                      Checkbox(
                          value: true
//                        onChanged: (bool value) {
//                          setState(() {
//                            this.showvalue = value;
//                          });
//                        },
                      ),
                    ]
                ),
              ),
              Container(
                height: ScreenUtil().setHeight(70),
                margin: EdgeInsets.all(ScreenUtil().setWidth(16)),
                padding: EdgeInsets.all(ScreenUtil().setWidth(16)),
                decoration: BoxDecoration(
                    color: ksecondaryColour,
                    boxShadow: [
                      BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.25),
                        blurRadius: 20,
                        offset: Offset(0, 2), // Shadow position
                      ),
                    ]
                ),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Sweets"),
                      Checkbox(
                          value: true
//                        onChanged: (bool value) {
//                          setState(() {
//                            this.showvalue = value;
//                          });
//                        },
                      ),
                    ]
                ),
              ),
              Container(
                height: ScreenUtil().setHeight(70),
                margin: EdgeInsets.all(ScreenUtil().setWidth(16)),
                padding: EdgeInsets.all(ScreenUtil().setWidth(16)),
                decoration: BoxDecoration(
                    color: ksecondaryColour,
                    boxShadow: [
                      BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.25),
                        blurRadius: 20,
                        offset: Offset(0, 2), // Shadow position
                      ),
                    ]
                ),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Sweets"),
                      Checkbox(
                          value: true
//                        onChanged: (bool value) {
//                          setState(() {
//                            this.showvalue = value;
//                          });
//                        },
                      ),
                    ]
                ),
              ),
              Container(
                height: ScreenUtil().setHeight(70),
                margin: EdgeInsets.all(ScreenUtil().setWidth(16)),
                padding: EdgeInsets.all(ScreenUtil().setWidth(16)),
                decoration: BoxDecoration(
                    color: ksecondaryColour,
                    boxShadow: [
                      BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.25),
                        blurRadius: 20,
                        offset: Offset(0, 2), // Shadow position
                      ),
                    ]
                ),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Sweets"),
                      Checkbox(
                          value: true
//                        onChanged: (bool value) {
//                          setState(() {
//                            this.showvalue = value;
//                          });
//                        },
                      ),
                    ]
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
