import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:joalapp/components/constants.dart';
import 'package:joalapp/screens/landing_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  BuildContext context;
  var _duration = new Duration(seconds: 2);
  final String _isScreenBaorded = "isScreenBaorded";

  Future<bool> getScreenBaorded() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(_isScreenBaorded) ?? false;
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Timer(_duration, navigationPage );
  }

  void navigationPage() async{
    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (BuildContext context) => LandingScreen()));
//    Navigator.pushNamed(context, '/welcomeScreen');

  }
  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    setState(() => this.context = context);
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    ScreenUtil.init(context,width:  width, height: height, allowFontScaling: true );
    return Scaffold(
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset("assets/images/jaolsplashlogo.png"),
              SizedBox(height: ScreenUtil().setHeight(5.0),),
              SvgPicture.asset('assets/images/Jaoltext.svg', height: ScreenUtil().setHeight(40.0),),
//            Text("Jaol",
//              style: TextStyle(
//                fontWeight: FontWeight.bold,
//                fontSize: ScreenUtil().setSp(50.0),
//                color: kprimaryColour,
//                fontFamily: 'Circular Std',
//              ),
//            )
            ],
          ),
        ),
      ),
    );
  }
}
