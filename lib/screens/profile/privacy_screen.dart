import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:joalapp/components/constants.dart';
class PrivacyScreen extends StatefulWidget {
  @override
  _PrivacyScreenState createState() => _PrivacyScreenState();
}

class _PrivacyScreenState extends State<PrivacyScreen> {
  @override
  Widget build(BuildContext context) {
    sizess(context);
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: ksecondaryColour,
        elevation: 0,
        centerTitle: true,
        title: Text("Privacy Policy",
          style: TextStyle(
              color: kprimaryColour
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          color: ksecondaryColour,
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'This privacy policy applies to the use of this product being developed by '
                    'Jaol technologies limited. We reserve the right to change this policy at any given time, '
                    'of which you will be promptly updated. If you want tomake sure that you are up to date with the '
                    'latest changes, we advise you to frequently visit this part.',
                style: TextStyle(
                    color: Colors.black,
                    height: 1.4
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(20.0),),
              Text('What User Data We Collect',
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(10.0),),
              Text('When you access this mobile application, we may collect the following data:',
                style: TextStyle(
                  color: Colors.black,
                    height: 1.4
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(5.0),),
              Text("Your personal information such as age, name, email address, weight, height and gender"),
              SizedBox(height: ScreenUtil().setHeight(5.0),),
              Text('Other information such as goal plan and activity level'),
              SizedBox(height: ScreenUtil().setHeight(20.0),),
              Text('Why We Collect Your Data',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(10.0),),
              Text('We are collecting your data for several reasons: ',
                style: TextStyle(
                    color: Colors.black,
                    height: 1.4
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(5.0),),
              Text("To verify your identity."),
              SizedBox(height: ScreenUtil().setHeight(5.0),),
              Text('To better understand your needs.'),
              SizedBox(height: ScreenUtil().setHeight(5.0),),
              Text("To fill out a research survey to improve our product offerings.To send you subscription emails"),
              SizedBox(height: ScreenUtil().setHeight(5.0),),
              Text('To better understand your needs.'),
              SizedBox(height: ScreenUtil().setHeight(20.0),),
              Text('for your receipt and expiry date notifications.',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(10.0),),
              Text('Jaol technologies limited is committed to securing your data and keeping it confidential. Jaol technologies limited hasdone everything within its power to prevent data theft, unauthorized access, and disclosure by implementing the latest technologies and software, which help us safeguard all the information we collect.',
                style: TextStyle(
                    color: Colors.black,
                    height: 1.4
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(10.0),),
              Text('We are collecting your data for several reasons: To verify your identity. To better understand your needs.To fill out a research survey to improve our product offerings.To send you subscription emails for your receipt and expiry date notifications.',
                style: TextStyle(
                    color: Colors.black,
                    height: 1.4
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(20.0),),
              Text('Links to Other Platforms',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(10.0),),
              Text('Our product only contain links to online payment platforms and google map. If you click on these links, Jaoltechnologies limited is not held responsible for your data and privacy protection. Visiting online payment platformsand using google map are not governed by this privacy policy agreement. Make sure to read the privacy policy documentation of the website you go to from our application.',
                style: TextStyle(
                    color: Colors.black,
                    height: 1.4
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(10.0),),
              Text('Users are not required to sign this document. However, they are deemed to have given consent by continuing to usethe application, thus creating a binding contract between the users of the application and the operator.',
                style: TextStyle(
                    color: Colors.black,
                    height: 1.4
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}






