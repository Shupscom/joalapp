import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:joalapp/bloc/metric/metric_bloc.dart';
import 'package:joalapp/bloc/metric/metric_event.dart';
import 'package:joalapp/bloc/metric/metric_state.dart';
import 'package:joalapp/components/constants.dart';
import 'package:joalapp/screens/profile/profile_screen.dart';

class MetricImperialScreen extends StatefulWidget {
  @override
  _MetricImperialScreenState createState() => _MetricImperialScreenState();
}

class _MetricImperialScreenState extends State<MetricImperialScreen> {
  final GlobalKey<FormState> _key = GlobalKey<FormState>();
  String heightUnit;
  String weightUnit;
  bool weightEnable = false;
  bool heightEnable = false;
  bool buttonEnable = false;

  @override
  Widget build(BuildContext context) {
    _onMetricButtonPressed() {
      if (_key.currentState.validate()) {
        print(weightUnit);
        BlocProvider.of<MetricBloc>(context).add((MetricButton(
          weightUnit: weightUnit,
          heightUnit: heightUnit
        )));
      } else {
//        setState(() {
//          _autoValidate = true;
//        });
      }
    }
    sizess(context);
    return BlocListener<MetricBloc,MetricState>(
      listener: (context,state){
        if (state is MetricFailure) {
          print(state.error);
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text('${state.error}'),
              backgroundColor: Colors.red,
            ),
          );
        }
        if (state is MetricSuccess) {
//          Scaffold.of(context).showSnackBar(
//            SnackBar(
//              content: Text('Sucessfully Updated'),
//              backgroundColor: Colors.green,
//            ),
//          );
          Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => ProfileScreen(userData: state.userData)));
        }
      },
      child: BlocBuilder<MetricBloc,MetricState>(
        builder: (context,state){
         return Scaffold(
              appBar: AppBar(
                leading: IconButton(
                  icon: Icon(Icons.arrow_back, color: Colors.black),
                  onPressed: () => Navigator.of(context).pop(),
                ),
                backgroundColor: ksecondaryColour,
                elevation: 0,
                centerTitle: true,
                title: Text(
                  "Metric and Imperial Unit",
                  style: TextStyle(color: kprimaryColour),
                ),
                actions: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: GestureDetector(
                      onTap: state is MetricLoading
                          ? () {}
                          : _onMetricButtonPressed,
                      child: Text(
                        "Save",
                        style: TextStyle(
                            color: buttonEnable == true ? kprimaryColour : Color(0xFF222222)),
                      ),
                    ),
                  ),
                ],
              ),
              body: SingleChildScrollView(
                child: Container(
                  color: ksecondaryColour,
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
                  child: Form(
                    key: _key,
                    onChanged: (){
                      buttonEnable = true;
                    },
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: ScreenUtil().setHeight(40.0),
                        ),
                        Text(
                          "Weight",
                          style: TextStyle(color: ksecondaryTextColour),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(10.0),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: ScreenUtil().setWidth(8.0)),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(3.0),
                              border: Border.all(color: Color(0xFFD0F1DD), width: 1.0)),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                                isExpanded: true,
                                value: weightUnit,
                                items: [
                                  DropdownMenuItem(
                                    child: Text("KG"),
                                    value: "KG",
                                  ),
                                  DropdownMenuItem(
                                    child: Text("LB"),
                                    value: "LBS",
                                  ),
                                ],
                                hint: Text("Select"),
                                onChanged: (value) {
                                  setState(() {
                                    weightEnable = true;
                                    buttonEnable = true;
                                    weightUnit = value;
                                  });
                                }),
                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(10.0),
                        ),
                        Text(
                          "Height",
                          style: TextStyle(color: ksecondaryTextColour),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(10.0),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: ScreenUtil().setWidth(8.0)),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(3.0),
//                       color: Colors.cyan,
                              border: Border.all(color: Color(0xFFD0F1DD), width: 1.0)),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                                isExpanded: true,
                                value: heightUnit,
                                items: [
                                  DropdownMenuItem(
                                    child: Text("IN"),
                                    value: "IN",
                                  ),
                                  DropdownMenuItem(
                                    child: Text("CM"),
                                    value: "CM",
                                  ),
                                ],
                                hint: Text("Select"),
                                onChanged: (value) {
                                  setState(() {
                                    heightEnable = true;
                                    buttonEnable = true;
                                    heightUnit = value;
                                  });
                                }),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              )
          );
        }
      ),
      );
  }
}
