import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:joalapp/components/constants.dart';
class HowScreen extends StatefulWidget {
  @override
  _HowScreenState createState() => _HowScreenState();
}

class _HowScreenState extends State<HowScreen> {
  @override
  Widget build(BuildContext context) {
    sizess(context);
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: ksecondaryColour,
        elevation: 0,
        centerTitle: true,
        title: Text("How it works",
          style: TextStyle(
              color: kprimaryColour
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          color: ksecondaryColour,
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
          'Smart diet & calories counter is a mobile app that manages the physical, mental, emotional, '
              'and psychological wellness of its amazing users. It contains six befitting sections. '
              'This is a walkthrough for users with the aim of ensuring users derive maximumvalue for '
              'using the app. Below is the brief for each section of the app',

                style: TextStyle(
                 color: Colors.black,
                    height: 1.4
               ),
              ),
              SizedBox(height: ScreenUtil().setHeight(20.0),),
              RichText(
                  text: TextSpan(
                    text: 'Profile Section: ',
                    style: TextStyle(fontFamily: 'Circular Std',
                        color: Colors.black,
                      fontWeight: FontWeight.bold
                    ),
                    children: <TextSpan>[
                      TextSpan(text: 'This section archives user’s personal data that are inputted during the short & simple onboarding. The user cantoggle with the personal data as the time progresses. Important information such as the diet objective (Gain weight, maintainweight & lose weight), display picture (optional) and subscription package, theme color, feedback to us @hismartdiet@gmail.com, rating, recommendations, and private policy are warehoused in this section. ',
                          style: TextStyle(
                        fontFamily: 'Circular Std',
                        color: Colors.black,
                              height: 1.4,
                              fontWeight: FontWeight.normal
                      )),
                    ],
                  )
              ),
              SizedBox(height: ScreenUtil().setHeight(20.0),),
              RichText(
                  text: TextSpan(
                    text: 'Diet Manager: ',
                    style: TextStyle(fontFamily: 'Circular Std',
                      color: Colors.black,
                      fontWeight: FontWeight.bold
                    ),
                    children: <TextSpan>[
                      TextSpan(text: 'This section manages the diet for the user. It contains an editable recommended meal plan, focused on the diet goalof the user and the user can add the meal to their meal plan. For the create meal plan option, user creates his/her preferred mealplan with the use of search function from our recipe database. The app will request for location access to direct you to nearestsupermarket store to get recipe. Each recipe contains macronutrient data. Meals created by the user remains in the diet plan untilthe user make change(s). Oh yeah, we know you may eat at restaurant or social gathering. Just log in the recipes used in preparingthe meal to know the macronutrients. Please log in uncooked recipes for a better diet result.',
                          style: TextStyle(
                            fontFamily: 'Circular Std',
                            color: Colors.black,
                              height: 1.4,
                              fontWeight: FontWeight.normal
                          )),
                    ],
                  )
              ),
              SizedBox(height: ScreenUtil().setHeight(20.0),),
              RichText(
                  text: TextSpan(
                    text: 'Statistics: ',
                    style: TextStyle(fontFamily: 'Circular Std',
                      color: Colors.black,
                      fontWeight: FontWeight.bold
                    ),
                    children: <TextSpan>[
                      TextSpan(text: ' Macronutrients in the selected recipes from the diet manager reports here. It has charts showing macronutrient per meal(breakfast, lunch, dinner and snacks), daily macronutrients summary for your meals & weekly macronutrients summary. User canalso do self-challenge(s) and see the records. A challenge is successful if the user did not cancel the challenge before it ends.',
                          style: TextStyle(
                            fontFamily: 'Circular Std',
                            color: Colors.black,
                              height: 1.4,
                              fontWeight: FontWeight.normal
                          )),
                    ],
                  )
              ),
              SizedBox(height: ScreenUtil().setHeight(20.0),),
              RichText(
                  text: TextSpan(
                    text: 'Super meals: ',
                    style: TextStyle(fontFamily: 'Circular Std',
                      color: Colors.black,
                      fontWeight: FontWeight.bold
                    ),
                    children: <TextSpan>[
                      TextSpan(text: 'Our lovely recipe collections for vegans, non-vegans, headache relief, sex drive, fiber-rich, iron-dense, alkaline,calcium-rich are available for your knowledge so you can add them to your diet from our recipe database by using search button..',
                          style: TextStyle(
                            fontFamily: 'Circular Std',
                            color: Colors.black,
                              height: 1.4,
                              fontWeight: FontWeight.normal
                          )),
                    ],
                  )
              ),
              SizedBox(height: ScreenUtil().setHeight(20.0),),
              RichText(
                  text: TextSpan(
                    text: 'Tracker: ',
                    style: TextStyle(fontFamily: 'Circular Std',
                      color: Colors.black,
                      fontWeight: FontWeight.bold
                    ),
                    children: <TextSpan>[
                      TextSpan(text: 'The responsibility to your sound health lies with you but hey, we got your back. Set reminders for water drinking frequencyand the app will just prompt you. No such thing as one cap fits all but there is a minimum standard (Male adults: 3.7ltrs, Femaleadults: 2.7ltrs including water in your food) for daily water intake. Replicate same for your meals &avoid unintentional meal skips.',
                          style: TextStyle(
                            fontFamily: 'Circular Std',
                            color: Colors.black,
                              height: 1.4,
                              fontWeight: FontWeight.normal
                          )),
                    ],
                  )
              ),
              SizedBox(height: ScreenUtil().setHeight(20.0),),
              RichText(
                  text: TextSpan(
                    text: 'Nuggets: ',
                    style: TextStyle(fontFamily: 'Circular Std',
                      color: Colors.black,
                      fontWeight: FontWeight.bold
                    ),
                    children: <TextSpan>[
                      TextSpan(text: 'Worried about tips to living a healthy life? This section is for you. Our nuggets can be shared on social media platforms.You can either read & listen or read only. Download the app and get a lifetime access to this knowledge enriching feature for free.',
                          style: TextStyle(
                            fontFamily: 'Circular Std',
                            color: Colors.black,
                              height: 1.4,
                            fontWeight: FontWeight.normal
                          )),
                    ],
                  )
              ),
              SizedBox(height: ScreenUtil().setHeight(20.0),),
              Text('You can upgrade to a new package at any time. Say, you upgraded from lite package to growth or from growth to standard whilstyour current package has not finished, your new subscription will not start counting until the end of the current package.',
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(20.0),),
              Text('We sincerely hope that you would have a very pleasant experience through our easy to navigate features. Just toggle the functionsand achieve your desired health goal.',
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
