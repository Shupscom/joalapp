import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:joalapp/bloc/subscription/subscription_bloc.dart';
import 'package:joalapp/bloc/subscription/subscription_state.dart';
import 'package:joalapp/components/constants.dart';
import 'package:joalapp/model/subscription_model.dart';
import 'package:joalapp/screens/subscription/subscription_plan_page.dart';
import 'package:joalapp/screens/subscription/subscription_plan_screen.dart';
class SubscriptionUpgradeScreen extends StatefulWidget {
  @override
  _SubscriptionUpgradeScreenState createState() => _SubscriptionUpgradeScreenState();
}

class _SubscriptionUpgradeScreenState extends State<SubscriptionUpgradeScreen> {
  bool buttonEnable = false;
  String borderEnable = "";
  String subscriptionId;
  SubscriptionModel subscriptionModel;
  List<Prices> prices;
  Prices usdPrice;
  Prices normalPrice;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () => Navigator.of(context).pop(),
          ),
          backgroundColor: ksecondaryColour,
          elevation: 0,
          centerTitle: true,
          title: Text(
            "Subscription",
            style: TextStyle(color: kprimaryColour),
          )),
        body: BlocBuilder<SubscriptionBloc,SubscriptionState>(
          builder: (context,state){
            if (state is SubscriptionLoading) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(height: ScreenUtil().setHeight(250),),
                  Center(child: CircularProgressIndicator()),
                ],
              );
            }
            return Container(
              color: ksecondaryColour,
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: ScreenUtil().setHeight(20),),
                  ListView.builder(
                    itemCount: state.subscriptionData.length,
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int position){
                      prices = state.subscriptionData[position].prices;
                      usdPrice = prices.firstWhere((i) => i.currency.code == "USD", orElse: () => null);
                      return  Column(
                        children: [
                          GestureDetector(
                            onTap: (){
                              setState(() {
                                buttonEnable = true;
                                borderEnable = state.subscriptionData[position].name;
                                subscriptionId = state.subscriptionData[position].sId;
                                subscriptionModel = state.subscriptionData[position];
                              });
                            },
                            child: SubContainer(title: state.subscriptionData[position].name,
                              trailing: "\$ ${usdPrice.amount}",
                              subtitle1: state.subscriptionData[position].frequencyType, subtitle2: state.subscriptionData[position].description,
                              borderEnable: borderEnable.toString(),),
                          ),
                          SizedBox(height: ScreenUtil().setHeight(20),),
                        ],
                      );
                    },
                  ),
                  SizedBox(height: ScreenUtil().setHeight(30),),
            Container(
                        child: Material(
                          elevation: 2.0,
                          color: buttonEnable ? kprimaryColour : ksecondaryTextColour,
                          borderRadius: BorderRadius.circular(20.0),
                          child: MaterialButton(
                            onPressed:(){
                              buttonEnable ?  Navigator.push(context, MaterialPageRoute(builder: (context) =>
                                  SubscriptionPlanPage(subscriptionModel: subscriptionModel,),
                                  )) : null;
                            },
                            minWidth: ScreenUtil().setWidth(420.0),
                            height: ScreenUtil().setHeight(42.0),
                            child: Text(
                              "Subscribe",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: ScreenUtil().setSp(18.0),
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Circular Std',
                              ),
                            ),
                          ),
                        ),
                      ),
                ],
              ),
//              Column(
////                 crossAxisAlignment: CrossAxisAlignment.start,
//                children: [
//                  SizedBox(height: ScreenUtil().setHeight(20),),
//
//
//
////                      GestureDetector(
////                        onTap: (){
////                          setState(() {
////                            buttonEnable = true;
////                            growthEnable = false;
////                            essentialEnable = false;
////                            standardEnable = false;
////                            liteEnable = true;
////                          });
////                        },
////                        child: SubContainer(title: "Lite Plan", trailing: "\$ 1.27",
////                          subtitle1: "Weekly", subtitle2: "", borderEnable: liteEnable,),
////                      ),
////                      SizedBox(height: ScreenUtil().setHeight(20),),
////                      GestureDetector(
////                        onTap: (){
////                          setState(() {
////                            buttonEnable = true;
////                            growthEnable = true;
////                            liteEnable = false;
////                            essentialEnable = false;
////                            standardEnable = false;
////                          });
////                        },
////                        child: SubContainer(title: "Growth Plan", trailing: "\$ 3.56",
////                          subtitle1: "Monthly", subtitle2: "25% off the Lite Plan", borderEnable: growthEnable,),
////                      ),
////                      SizedBox(height: ScreenUtil().setHeight(20),),
////                      GestureDetector(
////                        onTap: (){
////                          setState(() {
////                            buttonEnable = true;
////                            essentialEnable = true;
////                            growthEnable = false;
////                            liteEnable = false;
////                            standardEnable = false;
////                          });
////                        },
////                        child: SubContainer(title: "Essential Plan", trailing: "\$ 17.10",
////                          subtitle1: "Biannually", subtitle2: "40% off the Growth Plan", borderEnable: essentialEnable,),
////                      ),
////                      SizedBox(height: ScreenUtil().setHeight(20),),
////                      GestureDetector(
////                        onTap: (){
////                          setState(() {
////                            buttonEnable = true;
////                            standardEnable = true;
////                            essentialEnable = false;
////                            growthEnable = false;
////                            liteEnable = false;
////                          });
////                        },
////                        child: SubContainer(title: "Standard Plan", trailing: "\$ 25.65",
////                          subtitle1: "Yearly", subtitle2: "55% off the Growth Plan", borderEnable: standardEnable,),
////                      ),
//                      SizedBox(height: ScreenUtil().setHeight(30),),
//                      Container(
//                        child: Material(
//                          elevation: 2.0,
//                          color: buttonEnable ? kprimaryColour : ksecondaryTextColour,
//                          borderRadius: BorderRadius.circular(20.0),
//                          child: MaterialButton(
//                            onPressed:(){
//
//                            },
//                            minWidth: ScreenUtil().setWidth(420.0),
//                            height: ScreenUtil().setHeight(42.0),
//                            child: Text(
//                              "Subscribe",
//                              style: TextStyle(
//                                color: Colors.white,
//                                fontSize: ScreenUtil().setSp(18.0),
//                                fontWeight: FontWeight.bold,
//                                fontFamily: 'Circular Std',
//                              ),
//                            ),
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),

            );
          },
        ));
  }
}

class SubContainer extends StatelessWidget {
  String title;
  String trailing;
  String subtitle1;
  String subtitle2;
  String borderEnable;

  SubContainer({
  @required this.title,
    @required this.trailing,
    @required this.subtitle1,
    @required this.subtitle2,
   this.borderEnable,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setHeight(130),
      margin: EdgeInsets.only(
          left: ScreenUtil().setWidth(8.0),
          right: ScreenUtil().setWidth(8.0)),
      padding:
      EdgeInsets.all(ScreenUtil().setWidth(16.0),
      ),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10.0),
                    border: borderEnable == title
                        ? Border.all(
                        color: Color(0xFF6ACB55), width: 2.0)
                        : Border.all(
                        color: Color(0xFF95AAB0), width: 1.0),
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.25),
              blurRadius: 8,
              offset: Offset(0, 2), // Shadow position
            ),
          ]),
      child: Column(
        children: [
          SizedBox(height: ScreenUtil().setHeight(10.0),),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
                Text(title,
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(20),
                    fontWeight: FontWeight.bold,
                  ),
                ),
              Text(trailing,
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(20),
                  fontWeight: FontWeight.bold,
                  color: Theme.of(context).primaryColor
                ),
              )
            ],
          ),
          SizedBox(height: ScreenUtil().setHeight(10.0),),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(subtitle1,
                style: TextStyle(
                  color: ksecondaryTextColour
                ),
              ),
              Text(subtitle2,
                style: TextStyle(
                    color: ksecondaryTextColour
                ),
              ),
            ],
          )
        ],
      )
    );
  }
}
