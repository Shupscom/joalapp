import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:joalapp/components/constants.dart';
import 'package:joalapp/repository/subscription_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SubscriptionScreen extends StatefulWidget {
  @override
  _SubscriptionScreenState createState() => _SubscriptionScreenState();
}

class _SubscriptionScreenState extends State<SubscriptionScreen> {
  bool _loading = true;
  var subdata;
  var data;
  var dataCheck;

  void _getUserInfo() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var sdata = sharedPreferences.get("subdata");
    setState(() {
      subdata = json.decode(sdata);
      print(subdata);
      dataCheck = subdata['data'];
    });
  }

  @override
  void initState() {
     _getUserInfo();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    sizess(context);
    return Scaffold(
        appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.black),
              onPressed: () => Navigator.of(context).pop(),
            ),
            backgroundColor: Theme.of(context).backgroundColor,
            elevation: 0,
            centerTitle: true,
            title: Text(
              "Subscription",
              style: TextStyle(color: kprimaryColour),
            )),
        body: Container(
          color: ksecondaryColour,
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              dataCheck != null ?
              Center(
              child: Container(
              width: ScreenUtil().setWidth(380),
          height: ScreenUtil().setHeight(205),
          margin: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
          padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              color: ksecondaryColour,
              boxShadow: [
                BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.25),
                  blurRadius: 20,
                  offset: Offset(0, 2), // Shadow position
                ),
              ]
          ),
          child: Stack(
            alignment: Alignment.topCenter,
            children: [
              Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                    Text(subdata['data']['plan']['name']),
                      Text(subdata['data']['amount'].toString()),
                  ],),
                  SizedBox(height: ScreenUtil().setHeight(20),),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Expiry Date'),
                      Text(subdata['data']['amount'].toString()),
                    ],)
                ],
              ),
              Positioned(
                bottom: 5,
                right: 5,
                child: Container(
                  child: Material(
                    elevation: 2.0,
                    color: Theme.of(context).primaryColor,
                    child: MaterialButton(
                      onPressed: () {
                        Navigator.pushNamed(context, '/subscriptionUpgradeScreen');
                      },
                      minWidth: ScreenUtil().setWidth(100.0),
                      height: ScreenUtil().setHeight(42.0),
                      child: Text(
                        "Change Plan",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: ScreenUtil().setSp(18.0),
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Circular Std',
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
    )
                  : Center(
                child: Container(
                  width: ScreenUtil().setWidth(380),
                  height: ScreenUtil().setHeight(205),
                  margin: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
                  padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      color: ksecondaryColour,
                      boxShadow: [
                        BoxShadow(
                          color: Color.fromRGBO(0, 0, 0, 0.25),
                          blurRadius: 20,
                          offset: Offset(0, 2), // Shadow position
                        ),
                      ]
                  ),
                  child: Stack(
                    alignment: Alignment.topCenter,
                    children: [
                         Text("You have no active subscription",
                          style: TextStyle(
                            color: ksecondaryTextColour,
                          ),
                         ),
                      Positioned(
                        bottom: 5,
                        right: 5,
                        child: Container(
                          child: Material(
                            elevation: 2.0,
                            color: Theme.of(context).primaryColor,
                            child: MaterialButton(
                              onPressed: () {
                                Navigator.pushNamed(context, '/subscriptionUpgradeScreen');
                              },
                              minWidth: ScreenUtil().setWidth(100.0),
                              height: ScreenUtil().setHeight(42.0),
                              child: Text(
                                "Subscribe",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: ScreenUtil().setSp(18.0),
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Circular Std',
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(20),),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("Subscription history",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: ScreenUtil().setSp(18.0)
                ),
                ),
              )
//              Center(child: SvgPicture.asset('assets/images/pin_icon.svg', color: Color(0xFFA91C1C),)),

            ],
          ),
        ));
  }
}

class SubRow extends StatelessWidget {
   String left;
   String right;
   SubRow({@required this.left, @required this.right});
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(left,
         style: TextStyle(
           color: ksecondaryTextColour
         ),
        ),
        Text(right,
          style: TextStyle(
              color: Colors.black
          ),
        )
      ],
    );
  }
}
