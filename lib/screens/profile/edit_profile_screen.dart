import 'package:dropdown_formfield/dropdown_formfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:joalapp/bloc/edit_profile/edit_profile_bloc.dart';
import 'package:joalapp/bloc/edit_profile/edit_profile_event.dart';
import 'package:joalapp/bloc/edit_profile/edit_profile_state.dart';
import 'package:joalapp/components/constants.dart';
import 'package:joalapp/screens/profile/profile_screen.dart';

class EditProfileScreen extends StatefulWidget {
  Map<String, dynamic> userData;

  EditProfileScreen({@required this.userData});

  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  final GlobalKey<FormState> _key = GlobalKey<FormState>();
  TextEditingController _firstNameController = TextEditingController();
  TextEditingController _lastNameController = TextEditingController();
  TextEditingController _genderController = TextEditingController();
  TextEditingController _ageController = TextEditingController();
  TextEditingController _heightController = TextEditingController();
  TextEditingController _weightController = TextEditingController();
  TextEditingController _activityLevelController = TextEditingController();
  TextEditingController _weightGoalController = TextEditingController();
  bool buttonEnable = false;
  bool _autoValidate = false;
  var heightUnit;
  var weightUnit;

  calculateAge(DateTime birthDate) {
    DateTime currentDate = DateTime.now();
    int age = currentDate.year - birthDate.year;
    int month1 = currentDate.month;
    int month2 = birthDate.month;
    if (month2 > month1) {
      age--;
    } else if (month1 == month2) {
      int day1 = currentDate.day;
      int day2 = birthDate.day;
      if (day2 > day1) {
        age--;
      }
    }
    return age;
  }

  void _getUserInfo() async {
    setState(() {
      print(widget.userData);
      _firstNameController.text = widget.userData['firstName'];
      _lastNameController.text = widget.userData['lastName'];
      _genderController.text = widget.userData['gender'];
      _ageController.text =
          calculateAge(DateTime.parse(widget.userData['dob'])).toString();
      _heightController.text = widget.userData['height'].toString();
      _weightController.text = widget.userData['weight'].toString();
      _activityLevelController.text = widget.userData['activityLevel'];
      _weightGoalController.text = widget.userData['weightGoal'];
      heightUnit = widget.userData['heightUnit'];
      weightUnit = widget.userData['weightUnit'];

    });
  }

  @override
  void initState() {
    // TODO: implement initState
    _getUserInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _onUpdateButtonPressed() {
      if (_key.currentState.validate()) {
        BlocProvider.of<EditProfileBloc>(context).add(EditProfileButton(
            firstName: _firstNameController.text.trim(),
            lastName: _lastNameController.text.trim(),
            gender: _genderController.text.trim(),
            height: int.parse(_heightController.text.trim()),
            weight: int.parse(_weightController.text.trim()),
            age: int.parse(_ageController.text),
            activityLevel: _activityLevelController.text.trim(),
            weightGoal: _weightGoalController.text.trim(),
           weightUnit: weightUnit,
           heightUnit: heightUnit
        ));

      } else {
//        setState(() {
//          _autoValidate = true;
//        });
      }
    }

    sizess(context);
    return BlocListener<EditProfileBloc, EditProfileState>(
          listener: (context, state) {
            if (state is EditProfileFailure) {
              print(state.error);
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text('${state.error}'),
                  backgroundColor: Colors.red,
                ),
              );
            }
            if (state is EditProfileSuccess) {
              Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ProfileScreen(userData: state.userData)));
            }
          },
      child: BlocBuilder<EditProfileBloc,EditProfileState>(
        builder: (context, state){
          return Scaffold(
            appBar: AppBar(
              leading: IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.black),
                onPressed: () => Navigator.of(context).pop(),
              ),
              backgroundColor: Theme.of(context).backgroundColor,
              elevation: 0,
              centerTitle: true,
              title: Text(
                "About",
                style: TextStyle(color: kprimaryColour),
              ),
              actions: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: GestureDetector(
                    onTap: state is EditProfileLoading
                        ? () {}
                        : _onUpdateButtonPressed,
                    child: state is EditProfileLoading ? SpinKitFadingCircle(color: Colors.white) : Text(
                      "Save",
                      style: TextStyle(
                          color: buttonEnable == true
                              ? kprimaryColour
                              : Color(0xFFCAD9E0)),
                    ),
                  ),
                ),
              ],
            ),
            body: SingleChildScrollView(
              child: Container(
                color: Theme.of(context).backgroundColor,
                width: MediaQuery.of(context).size.width,
//          height: MediaQuery.of(context).size.height,
                padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
                child: Form(
                  autovalidate: _autoValidate,
                  key: _key,
                  onChanged: () {
                    buttonEnable = true;
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: ScreenUtil().setHeight(40.0),
                      ),
                      Center(
                        child: CircleAvatar(
                          radius: 50,
                          backgroundColor: Color(0xFFD0F1DD),
                          child: SvgPicture.asset(
                            'assets/images/camera.svg',
//                       Image.asset(
//                         'assets/images/avatar.png',
//                         fit: BoxFit.cover,
//                         height: 100,
//                         width: 100,
//
                          ),
                        ),
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(20.0),
                      ),
                      Text(
                        "First Name",
                        style: TextStyle(color: ksecondaryTextColour),
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(10.0),
                      ),
                      TextFormField(
                          controller: _firstNameController,
                          keyboardType: TextInputType.text,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter your firstname';
                            }
                            return null;
                          },
                          onTap: () {
                           setState(() {
                             buttonEnable = true;
                           });
                          },
                          style: TextStyle(color: ksecondaryTextColour),
                          decoration: kTextFieldDecoration),
                      SizedBox(
                        height: ScreenUtil().setHeight(10.0),
                      ),
                      Text(
                        "Last Name",
                        style: TextStyle(color: ksecondaryTextColour),
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(10.0),
                      ),
                      TextFormField(
                          controller: _lastNameController,
                          keyboardType: TextInputType.text,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter your lastname';
                            }
                            return null;
                          },
                          onTap: () {
                           setState(() {
                             buttonEnable = true;
                           });
                          },
                          style: TextStyle(color: ksecondaryTextColour),
                          decoration: kTextFieldDecoration),
                      SizedBox(
                        height: ScreenUtil().setHeight(10.0),
                      ),
                      Text(
                        "Gender",
                        style: TextStyle(color: ksecondaryTextColour),
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(10.0),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: ScreenUtil().setWidth(8.0)),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(3.0),
//                       color: Colors.cyan,
                            border:
                            Border.all(color: Color(0xFFD0F1DD), width: 1.0)),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton(
                              isExpanded: true,
                              value: _genderController.text,
                              items: [
                                DropdownMenuItem(
                                  child: Text("Male"),
                                  value: "M",
                                ),
                                DropdownMenuItem(
                                  child: Text("Female"),
                                  value: "F",
                                ),
                              ],
                              onChanged: (value) {
                                setState(() {
                                  _genderController.text = value;
                                  buttonEnable = true;
                                });
                              }),
                        ),
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(10.0),
                      ),
                      Text(
                        "Age",
                        style: TextStyle(color: ksecondaryTextColour),
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(10.0),
                      ),
                      TextFormField(
                          controller: _ageController,
                          keyboardType: TextInputType.number,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter your age';
                            }
                            return null;
                          },
                          onTap: () {
                            setState(() {
                              buttonEnable = true;
                            });
                          },
                          style: TextStyle(color: ksecondaryTextColour),
                          decoration: kTextFieldDecoration),
                      SizedBox(
                        height: ScreenUtil().setHeight(10.0),
                      ),
                      Text(
                        "Height",
                        style: TextStyle(color: ksecondaryTextColour),
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(10.0),
                      ),
                      TextFormField(
                          controller: _heightController,
                          keyboardType: TextInputType.number,
//                        validator: validateEmail,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter your height';
                            }
                            return null;
                          },
                          onTap: () {
                            setState(() {
                              buttonEnable = true;
                            });
                          },
                          style: TextStyle(color: ksecondaryTextColour),
                          decoration: kTextFieldDecoration),
                      Text(
                        "Current Weight",
                        style: TextStyle(color: ksecondaryTextColour),
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(10.0),
                      ),
                      TextFormField(
                          controller: _weightController,
                          style: TextStyle(color: ksecondaryTextColour),
                          onTap: () {
                            setState(() {
                              buttonEnable = true;
                            });
                          },
                          keyboardType: TextInputType.number,
//                        validator: validateEmail,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter your email or phone number';
                            }
                            return null;
                          },
                          decoration: kTextFieldDecoration),
                      SizedBox(
                        height: ScreenUtil().setHeight(10.0),
                      ),
                      Text(
                        "Activity Level",
                        style: TextStyle(color: ksecondaryTextColour),
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(10.0),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: ScreenUtil().setWidth(8.0)),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(3.0),
                            border:
                            Border.all(color: Color(0xFFD0F1DD), width: 1.0)),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton(
                              isExpanded: true,
                              value: _activityLevelController.text,
                              items: [
                                DropdownMenuItem(
                                  child: Text("Little or no acitivity"),
                                  value: "NONE",
                                ),
                                DropdownMenuItem(
                                  child: Text("Moderately active"),
                                  value: "LESS",
                                ),
                                DropdownMenuItem(
                                  child: Text("Active"),
                                  value: "ACTIVE",
                                ),
                                DropdownMenuItem(
                                  child: Text("Very Active"),
                                  value: "HIGH",
                                ),
                              ],
                              onChanged: (value) {
                                setState(() {
                                  _activityLevelController.text = value;
                                  buttonEnable = true;
                                });
                              }),
                        ),
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(10.0),
                      ),
                      Text(
                        "Weight Goal",
                        style: TextStyle(color: ksecondaryTextColour),
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(10.0),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: ScreenUtil().setWidth(8.0)),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(3.0),
//                       color: Colors.cyan,
                            border:
                            Border.all(color: Color(0xFFD0F1DD), width: 1.0)),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton(
                              isExpanded: true,
                              value: _weightGoalController.text,
                              items: [
                                DropdownMenuItem(
                                  child: Text("Gain Weight"),
                                  value: "GAIN",
                                ),
                                DropdownMenuItem(
                                  child: Text("Maintain Weight"),
                                  value: "MAINTAIN",
                                ),
                                DropdownMenuItem(
                                  child: Text("Lose Weight"),
                                  value: "LOSE",
                                ),
                              ],
                              onChanged: (value) {
                                setState(() {
                                  _weightGoalController.text = value;
                                  buttonEnable = true;
                                });
                              }),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
