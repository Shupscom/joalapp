import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/subscription/subscription_bloc.dart';
import 'package:joalapp/bloc/subscription/subscription_event.dart';
import 'package:joalapp/screens/profile/subscription_upgrade_screen.dart';

class SubscriptionUpgradePage  extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<SubscriptionBloc>(
        create: (context){
          return SubscriptionBloc()..add(LoadSubscriptionData());
        },
        child: SubscriptionUpgradeScreen()
    );
  }
}
