import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/edit_profile/edit_profile_bloc.dart';
import 'package:joalapp/bloc/edit_profile/edit_profile_state.dart';
import 'package:joalapp/screens/profile/edit_profile_screen.dart';
import 'package:joalapp/screens/profile/profile_screen.dart';
class EditProfilePage extends StatelessWidget {
  Map<String,dynamic> userData;
  EditProfilePage({this.userData});
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context){
        return EditProfileBloc();
      },
      child: EditProfileScreen(userData: userData)
    );
  }
}
