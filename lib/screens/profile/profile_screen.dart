import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:joalapp/components/constants.dart';
import 'package:joalapp/components/make_bottom.dart';
import 'package:joalapp/model/dark_theme.dart';
import 'package:joalapp/repository/auth_repository.dart';
import 'package:joalapp/screens/profile/edit_profile_page.dart';
import 'package:joalapp/screens/profile/edit_profile_screen.dart';
import 'package:provider/provider.dart';
class ProfileScreen extends StatefulWidget {
  Map<String,dynamic> userData;

  ProfileScreen({@required this.userData});
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController _commentController = TextEditingController();
  var height;
  var weight;
  var age;
  var name;
  var email;
  var gender;
  var activityLevel;
  var bmi;
  var weightGoal;
  var allData;
  var bmiValue;
  var heightUnit;
  var weightUnit;
  void _getUserInfo() async {
    setState(() {
      name = widget.userData['firstName'] == null ? '' : '${widget.userData['firstName']} ${widget.userData['lastName']}' ;
      height = widget.userData['height'].toString();
      weight = widget.userData['weight'].toString();
      email = widget.userData['username'];
      gender = widget.userData['gender'];
      activityLevel = widget.userData['activityLevel'];
      weightGoal = widget.userData['weightGoal'];
      age = calculateAge(DateTime.parse(widget.userData['dob']));
      bmi = widget.userData['bmi']['value'];
      bmiValue = widget.userData['bmi']['weight'];
      allData = widget.userData;
      heightUnit = widget.userData['heightUnit'];
      weightUnit = widget.userData['weightUnit'];
      print(widget.userData);
    });
  }
  @override
  void initState() {
    _getUserInfo();
    // TODO: implement initState
    super.initState();
  }
  calculateAge(DateTime birthDate) {
    DateTime currentDate = DateTime.now();
    int age = currentDate.year - birthDate.year;
    int month1 = currentDate.month;
    int month2 = birthDate.month;
    if (month2 > month1) {
      age--;
    } else if (month1 == month2) {
      int day1 = currentDate.day;
      int day2 = birthDate.day;
      if (day2 > day1) {
        age--;
      }
    }
    return age;
  }

  void _showDeleteDialog(BuildContext context) async {
    await showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0)),
            child: Container(
              padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
              height: ScreenUtil().setHeight(250),
              width: ScreenUtil().setWidth(500),
              child: Column(
                children: [
                  Row(
                    children: [
                      SvgPicture.asset('assets/images/delete_ icon.svg'),
                      SizedBox(width: ScreenUtil().setWidth(10.0),),
                      Expanded(
                        child: Text("Are you sure you want to delete your account? if you continue with this process you "
                            "will lose your profile and subscription to various plans",
                          overflow: TextOverflow.visible,
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: ScreenUtil().setHeight(50.0),),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: (){
                          Navigator.of(context).pop();
                        },
                        child: Container(
                          height: ScreenUtil().setHeight(45.0),
                          width: ScreenUtil().setWidth(126.0),
                          padding: EdgeInsets.all(ScreenUtil().setWidth(8.0)),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            border: Border.all(color: ksecondaryTextColour, width: 1.0 ),
                            color: ksecondaryColour,
                          ),
                          child: Text("Cancel",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Color(0xFF222222),
//                              fontWeight: FontWeight.bold,
                                fontFamily: "Circular Std"
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(30),),
                      GestureDetector(
                        onTap: (){},
                        child: Container(
                          height: ScreenUtil().setHeight(45.0),
                          width: ScreenUtil().setWidth(126.0),
                          padding: EdgeInsets.all(ScreenUtil().setWidth(8.0)),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Color(0xFFA91C1C
                            ),
                          ),
                          child: Text("Delete",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.white,
                            ),
                          ),

                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          );
        });
  }
  void _showRateDialog(BuildContext context) async {
    await showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
            child: Container(
              padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
              height: ScreenUtil().setHeight(410),
              width: ScreenUtil().setWidth(500),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  SizedBox(height: ScreenUtil().setHeight(10.0),),
                  Center(
                    child: CircleAvatar(
                        radius: 35,
                        backgroundColor: Colors.white,
                        child:  Image.asset(
                          'assets/images/avatar.png',
                          fit: BoxFit.cover,
                          height: 70,
                          width: 70,
                        )
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(20.0),),
                  SvgPicture.asset("assets/images/ratings.svg"),
                  SizedBox(height: ScreenUtil().setHeight(20.0),),
                  TextFormField(
                    minLines: 2,
                    maxLines: 5,
                    keyboardType: TextInputType.multiline,
                    decoration: InputDecoration(
                      hintText: 'Comment......',
                      hintStyle: TextStyle(
                          color: Colors.grey
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20.0)),
                      ),
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(20.0),),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: (){
                          Navigator.of(context).pop();
                        },
                        child: Container(
//                          height: ScreenUtil().setHeight(50.0),
                          width: ScreenUtil().setWidth(126.0),
                          padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            border: Border.all(color: ksecondaryTextColour, width: 1.0 ),
                            color: ksecondaryColour,
                          ),
                          child: Text("Cancel",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Color(0xFF222222),
//                              fontWeight: FontWeight.bold,
                                fontFamily: "Circular Std"
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(30),),
                      GestureDetector(
                        onTap: (){},
                        child: Container(
//                          height: ScreenUtil().setHeight(50.0),
                          width: ScreenUtil().setWidth(126.0),
                          padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: kprimaryColour,
                          ),
                          child: Text("Send",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),

                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          );
        });
  }
  void _showFeedbackDialog(BuildContext context) async {
    await showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
            child: FeedbackDialogue(formKey: _formKey, commentController: _commentController, scaffoldKey: _scaffoldKey),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    final themeChange = Provider.of<DarkThemeProvider>(context);
    sizess(context);
    return Scaffold(
      key: _scaffoldKey,
      bottomNavigationBar: MakeBottom(active: 'Profile', userData: allData,),
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
//          height: MediaQuery.of(context).size.height,
          color: Theme.of(context).backgroundColor,
          padding: EdgeInsets.only(
            top: ScreenUtil().setWidth(16),
            bottom:  ScreenUtil().setWidth(16),
          ),
          child: Column(
            children: [
              SizedBox(height: ScreenUtil().setHeight(50.0),),
//              Center(
//                child: CircleAvatar(
//                  radius: 50,
//                  backgroundColor: Colors.white,
//                  child:  Image.asset(
//                    'assets/images/avatar.png',
//                    fit: BoxFit.cover,
//                    height: 100,
//                    width: 100,
//                  )
//                ),
//              ),
              Center(
                child: CircleAvatar(
                  radius: 50,
                  backgroundColor: Color(0xFFD0F1DD),
                  child: SvgPicture.asset(
                    'assets/images/camera.svg',
//                       Image.asset(
//                         'assets/images/avatar.png',
//                         fit: BoxFit.cover,
//                         height: 100,
//                         width: 100,
//
                  ),
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(10.0),),
              name == null ? SizedBox() : Text(name,
              style: TextStyle(
                fontSize: ScreenUtil().setSp(18.0),
                fontWeight: FontWeight.bold,
                color: kprimaryColour
              ),
              ),
              SizedBox(height: ScreenUtil().setHeight(10.0),),
              RichText(
                  text: TextSpan(
                    text: 'Your Body Mass Index (BMI) is ',
                    style: TextStyle(fontSize: ScreenUtil().setSp(18.0),color: ksecondaryTextColour,
                    fontFamily: "Circular Std"),
                    children: <TextSpan>[
                      TextSpan(text: '$bmi', style: TextStyle(
                      fontFamily: "Circular Std",
                          color: kprimaryColour,
                          fontSize: ScreenUtil().setSp(18.0))),
                    ],
                  )
              ),
              SizedBox(height: ScreenUtil().setHeight(10.0),),
             Text("This is considered $bmiValue",
               style: TextStyle(
                   color: ksecondaryTextColour
               ),
             ) ,
              SizedBox(height: ScreenUtil().setHeight(20.0),),
              Padding(
                padding: EdgeInsets.all(ScreenUtil().setWidth(20.0)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("About",
                      style: TextStyle(
                          color: ksecondaryTextColour
                      ),
                    ),
                GestureDetector(
                  onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => EditProfilePage(userData: widget.userData) ));
                  },
                    child: SvgPicture.asset('assets/images/edit_arrow.svg'),
                )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  left: ScreenUtil().setWidth(20.0),
                  right: ScreenUtil().setWidth(20.0)
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.25),
                        blurRadius: 20,
                        offset: Offset(0, 2), // Shadow position
                      ),
                    ]
                ),
                child: Column(
                  children: [
                    GestureDetector(
                      child: ListTile(
                        leading: SvgPicture.asset('assets/images/gender.svg',color: kprimaryColour,),
                        title: Align(
                            alignment: Alignment(-1.2, 0),
                            child: Text("Gender")
                        ),
                        trailing: gender == "F" ? Text("Female") : Text("Male"),
                      ),
                    ),
                    GestureDetector(
                      child: ListTile(
                        leading: SvgPicture.asset('assets/images/age.svg'),
                        title: Align(
                            alignment: Alignment(-1.2, 0),
                            child: Text("Age")
                        ),
                        trailing: Text(age.toString()),
                      ),
                    ),
                    GestureDetector(
                      child: ListTile(
                        leading: SvgPicture.asset('assets/images/height.svg'),
                        title: Align(
                            alignment: Alignment(-1.2, 0),
                            child: Text("Height")
                        ),
                        trailing: Text("${height} ${heightUnit}"),
                      ),
                    ),
                    GestureDetector(
                      child: ListTile(
                        leading: SvgPicture.asset('assets/images/current_weight.svg'),
                        title: Align(
                            alignment: Alignment(-1.2, 0),
                            child: Text("Current Weight")
                        ),
                        trailing: Text("${weight} ${weightUnit}"),
                      ),
                    ),
                    GestureDetector(
                      child: ListTile(
                        leading: SvgPicture.asset('assets/images/activity.svg',),
                        title: Align(
                            alignment: Alignment(-1.2, 0),
                            child: Text("Activity Level")
                        ),
                        trailing: activityLevel == "ACTIVE"
                        ? Text("Active") : activityLevel == "HIGH" ? Text("Very Active") :
                        activityLevel == "LESS" ? Text('Moderately active') : Text('Little or no acitivity')
                      ),
                    ),
                    GestureDetector(
                      child: ListTile(
                        leading: SvgPicture.asset('assets/images/weight_goal.svg',),
                        title: Align(
                            alignment: Alignment(-1.2, 0),
                            child: Text("Weight Goal")
                        ),
                        trailing:  weightGoal == "GAIN"
                            ? Text("Gain Weight") : weightGoal == "LOSE" ? Text("Lose Weight") :
                         Text('Maintain Weight')
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(30.0),),
              Row(
                children: [
                  Padding(
                    padding:  EdgeInsets.only(left: ScreenUtil().setSp(16)),
                    child: Text("General Settings",
                      style: TextStyle(
                          color: ksecondaryTextColour
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: ScreenUtil().setHeight(10.0),),
              Container(
                margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(20.0),
                    right: ScreenUtil().setWidth(20.0)
                ),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.25),
                        blurRadius: 20,
                        offset: Offset(0, 2), // Shadow position
                      ),
                    ]
                ),
                child: Column(
                  children: [
                    GestureDetector(
                      child: ListTile(
                        leading: SvgPicture.asset('assets/images/dark.svg'),
                        title: Align(
                            alignment: Alignment(-1.2, 0),
                            child: Text("Dark Mode")
                        ),
                        trailing: Switch(
                            onChanged: (bool value){
                             setState(() {
                               themeChange.darkTheme = value;
                               themeChange.darkThemePreference.setDarkTheme(value);
                             });
                            },
                          value: themeChange.darkTheme,
                          activeColor: kprimaryColour,
                          activeTrackColor: Color(0xFFF1F1F1),
                          inactiveThumbColor: Colors.white,
                          inactiveTrackColor: Color.fromRGBO(34, 31, 31, 0.26),
                        )  ,
                      ),
                    ),
                    GestureDetector(
                      onTap: (){
                        Navigator.pushNamed(context, "/metricScreen");
                      },
                      child: ListTile(
                        leading: SvgPicture.asset('assets/images/metric_icon.svg'),
                        title: Align(
                            alignment: Alignment(-1.2, 0),
                            child: Text("Metric and Imperial Unit")
                        ),
                        trailing: SvgPicture.asset('assets/images/arrow_click.svg',),
                      ),
                    ),
                    GestureDetector(
                      onTap: (){
                        Navigator.pushNamed(context, "/howScreen");
                      },
                      child: ListTile(
                        leading: SvgPicture.asset('assets/images/how_icon.svg'),
                        title: Align(
                            alignment: Alignment(-1.2, 0),
                            child: Text("How it works")
                        ),
                        trailing: SvgPicture.asset('assets/images/arrow_click.svg',),
                      ),
                    ),
                    GestureDetector(
                      onTap: (){
                        Navigator.pushNamed(context, "/subscriptionScreen");
                      },
                      child: ListTile(
                        leading: SvgPicture.asset('assets/images/sub_icon.svg'),
                        title: Align(
                            alignment: Alignment(-1.2, 0),
                            child: Text("Subscription")
                        ),
                        trailing: SvgPicture.asset('assets/images/arrow_click.svg',),
                      ),
                    ),
                    GestureDetector(
                      onTap: (){
                        _showDeleteDialog(context);
                      },
                      child: ListTile(
                        leading: SvgPicture.asset('assets/images/delete_icon.svg'),
                        title: Align(
                            alignment: Alignment(-1.2, 0),
                            child: Text("Delete Profile")
                        ),
                        trailing: SvgPicture.asset('assets/images/arrow_click.svg',) ,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(30.0),),
              Row(
                children: [
                  Padding(
                    padding:  EdgeInsets.only(left: ScreenUtil().setSp(16)),
                    child: Text("Support Us",
                      style: TextStyle(
                          color: ksecondaryTextColour
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: ScreenUtil().setHeight(10.0),),
              Container(
                margin: EdgeInsets.only(
                    left: ScreenUtil().setWidth(20.0),
                    right: ScreenUtil().setWidth(20.0)
                ),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.25),
                        blurRadius: 20,
                        offset: Offset(0, 2), // Shadow position
                      ),
                    ]
                ),
                child: Column(
                  children: [
                    GestureDetector(
                      child: ListTile(
                        leading: SvgPicture.asset('assets/images/share_icon.svg'),
                        title: Align(
                            alignment: Alignment(-1.2, 0),
                            child: Text("Share Us")
                        ),
                        trailing: SvgPicture.asset('assets/images/arrow_click.svg',),
                      ),
                    ),
//                    GestureDetector(
//                      onTap: (){
//                        _showRateDialog(context);
//                      },
//                      child: ListTile(
//                        leading: SvgPicture.asset('assets/images/rate_icon.svg'),
//                        title: Align(
//                            alignment: Alignment(-1.2, 0),
//                            child: Text("Rate Us")
//                        ),
//                        trailing: SvgPicture.asset('assets/images/arrow_click.svg',),
//                      ),
//                    ),
                    GestureDetector(
                      onTap: (){
                        _showFeedbackDialog(context);
                      },
                      child: ListTile(
                        leading: SvgPicture.asset('assets/images/feedback_icon.svg'),
                        title: Align(
                            alignment: Alignment(-1.2, 0),
                            child: Text("Feedback")
                        ),
                        trailing: SvgPicture.asset('assets/images/arrow_click.svg',),
                      ),
                    ),
                    GestureDetector(
                      onTap: (){
                        Navigator.pushNamed(context, "/privacyScreen");
                      },
                      child: ListTile(
                        leading: SvgPicture.asset('assets/images/policy_icon.svg'),
                        title: Align(
                            alignment: Alignment(-1.2, 0),
                            child: Text("Privacy Policy")
                        ),
                        trailing: SvgPicture.asset('assets/images/arrow_click.svg',),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class FeedbackDialogue extends StatefulWidget {
  const FeedbackDialogue({
    Key key,
    @required GlobalKey<FormState> formKey,
    @required TextEditingController commentController,
    @required GlobalKey<ScaffoldState> scaffoldKey,
  }) : _formKey = formKey, _commentController = commentController, _scaffoldKey = scaffoldKey, super(key: key);

  final GlobalKey<FormState> _formKey;
  final TextEditingController _commentController;
  final GlobalKey<ScaffoldState> _scaffoldKey;

  @override
  _FeedbackDialogueState createState() => _FeedbackDialogueState();
}

class _FeedbackDialogueState extends State<FeedbackDialogue> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
        height: ScreenUtil().setHeight(420),
        width: ScreenUtil().setWidth(500),
        child: Form(
          key: widget._formKey,
          child: Column(
            children: [
              SizedBox(height: ScreenUtil().setHeight(10.0),),
              Text("Share Your Feedback ",
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: ScreenUtil().setSp(18.0)
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(10.0),),
              RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    text: 'How satisfied are you with ',
                    style: TextStyle(fontFamily: 'Circular Std',
                        color: ksecondaryTextColour
                    ),
                    children: <TextSpan>[
                      TextSpan(text: 'Jaol ', style: TextStyle(
                          fontFamily: 'Circular Std',
                          color: kprimaryColour,
                          )),
                    ],
                  )
              ),
              SizedBox(height: ScreenUtil().setHeight(10.0),),
              SvgPicture.asset("assets/images/emoji.svg"),
              SizedBox(height: ScreenUtil().setHeight(20.0),),
              TextFormField(
                controller: widget._commentController,
                minLines: 2,
                maxLines: 5,
                keyboardType: TextInputType.multiline,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter your comments';
                  }
                  return null;
                },
                decoration: InputDecoration(
                  hintText: 'Comment......',
                  hintStyle: TextStyle(
                      color: Colors.grey
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  ),
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(20.0),),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: (){
                      Navigator.of(context).pop();
                    },
                    child: Container(
//                            height: ScreenUtil().setHeight(50.0),
                      width: ScreenUtil().setWidth(126.0),
                      padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        border: Border.all(color: ksecondaryTextColour, width: 1.0 ),
                        color: ksecondaryColour,
                      ),
                      child: Text("Cancel",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Color(0xFF222222),
//                              fontWeight: FontWeight.bold,
                            fontFamily: "Circular Std"
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: ScreenUtil().setWidth(30),),
                  InkWell(
                    onTap: () async {

                        if (widget._formKey.currentState.validate()){
                          setState(()  async {
                          try {
                            final bool success = await AuthRepository.feedBackPost(comment: widget._commentController.text);
                            if(success == true){
                              print(success);
                              widget._scaffoldKey.currentState.showSnackBar(SnackBar(
                                content: Text('Feedback sent successfully'),
                                backgroundColor: Colors.green,
                              ));
                              Navigator.of(context).pop();
                            }
                          } catch (e) {
                            print(e.message);
                          }
                          });
                        }
                      },
                    child: Container(
                      width: ScreenUtil().setWidth(126.0),
                      padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: kprimaryColour,
                      ),
                      child: Text("Send",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),

                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
