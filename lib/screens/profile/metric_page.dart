import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/metric/metric_bloc.dart';
import 'package:joalapp/screens/profile/metric_screen.dart';

class MetricPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context){
          return MetricBloc();
        },
        child: MetricImperialScreen()
    );
  }
}