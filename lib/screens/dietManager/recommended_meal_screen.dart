import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:joalapp/components/constants.dart';
import 'package:joalapp/components/icon_content.dart';
import 'package:joalapp/components/make_bottom.dart';
import 'package:joalapp/components/reusable_card.dart';
import 'package:joalapp/screens/dietManager/meal_nutrients.dart';
import 'package:joalapp/screens/dietManager/view_all_meal.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:table_calendar/table_calendar.dart';

import 'create_meal_plan_screen.dart';
class RecommendMealScreen extends StatefulWidget {
  @override
  _RecommendMealScreenState createState() => _RecommendMealScreenState();
}

class _RecommendMealScreenState extends State<RecommendMealScreen> with SingleTickerProviderStateMixin{
  int _selectedIndex = 0;
  TabController _tabController;
  CalendarFormat _calendarFormat = CalendarFormat.month;
  DateTime _focusedDay = DateTime.now();
  DateTime _selectedDay;
  DateTime planDate = DateTime.now();
  String planPeriod = "BREAKFAST";
  var userdata;
  var subdata;
  var dataCheck;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getUserInfo();
    _tabController = TabController(length: 5, vsync: this);
    _tabController.addListener(() {
      setState(() {
        _selectedIndex = _tabController.index;
      });
    });
  }
  void _getUserInfo() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var data = sharedPreferences.get("userdata");
    var sdata = sharedPreferences.get("subdata");
    setState(() {
      userdata = json.decode(data);
      subdata = json.decode(sdata);
      dataCheck = subdata['data'];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        DefaultTabController(
          length: 5,
          child: Scaffold(
            bottomNavigationBar: MakeBottom(active: "diet_manager", userData: userdata,),
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(150.0),
              child: AppBar(
                backgroundColor: ksecondaryColour,
                elevation: 3,
                shadowColor: kprimaryColour,
                title: Align(
                  alignment: Alignment(-1.5,0),
                  child: Padding(
                    padding: EdgeInsets.only(top: 16.0),
                    child: Text("Diet Manager",
                      style: TextStyle(
                          color: kprimaryColour,
                          fontSize: ScreenUtil().setSp(24.0)
                      ),
                    ),
                  ),
                ),
                actions: <Widget>[
                  Padding(
                      padding:  EdgeInsets.only(
                          left: 10.0,
                          right: 10.0,
                          top: 16.0
                      ),
                      child: SvgPicture.asset("assets/images/calendar.svg")
                  ),
                ],
                bottom: PreferredSize(
                  preferredSize: const Size.fromHeight(100),
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        TableCalendar(
                          rowHeight: 45.0,
                          daysOfWeekHeight: 14.0,
                          firstDay: kFirstDay,
                          lastDay: kLastDay,
                          focusedDay: _focusedDay,
                          calendarFormat: CalendarFormat.week,
                          headerVisible: true,
                          startingDayOfWeek: StartingDayOfWeek.monday,
                          selectedDayPredicate: (day) {
                            return isSameDay(_selectedDay, day);
                          },
                          onDaySelected: (selectedDay, focusedDay) {
                            if (!isSameDay(_selectedDay, selectedDay)) {
                              // Call `setState()` when updating the selected day
                              setState(() {
                                _selectedDay = selectedDay;
                                _focusedDay = focusedDay;
                                planDate = selectedDay;
                              });
                            }
                          },
                          headerStyle: HeaderStyle(
                              formatButtonVisible: false,
                              headerPadding: EdgeInsets.zero,
                              headerMargin: EdgeInsets.zero,
                              titleTextStyle: TextStyle(fontSize: 14.0)
//                    leftChevronMargin: EdgeInsets.all(0.0),
//                    rightChevronMargin: EdgeInsets.all(0.0)
                          ),
                          calendarStyle: CalendarStyle(
                            todayDecoration: BoxDecoration(
                              color: kprimaryColour,
                              shape: BoxShape.circle,
                            ),
                            selectedDecoration: BoxDecoration(
                              color: kprimaryColour,
                              shape: BoxShape.circle,
                            ),

                          ),

                          onFormatChanged: (format) {
                            if (_calendarFormat != format) {
                              // Call `setState()` when updating calendar format
                              setState(() {
                                _calendarFormat = format;
                              });
                            }
                          },
                          onPageChanged: (focusedDay) {
                            // No need to call `setState()` here
                            _focusedDay = focusedDay;
                          },
                        ),

//                  TableCalendar(

//                    calendarController: _controller,
//                    calendarStyle: CalendarStyle(
//                        todayColor: kprimaryColour,
//                        selectedColor: kprimaryColour,
//                        todayStyle: TextStyle(
//                            fontWeight: FontWeight.bold,
//                            fontSize: 16.0,
//                            color: Colors.white,
//                        ),
//                    ),


                        // )
                      ],
                    ),
                  ),
                ),
              ),
            ),
            body: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
                color: ksecondaryColour,
                child: Column(
                  children: [
                    Container(
                      height: ScreenUtil().setHeight(50.0),
                      child: TabBar(
                        onTap: (tabIndex) {
                          switch (tabIndex) {
                            case 0:
                               setState(() {
                                 planPeriod = "BREAKFAST";
                               });
                              break;
                            case 1:
                              setState(() {
                                planPeriod = "BSNACK";
                                print(planPeriod);
                              });
                              break;
                            case 2:
                              planPeriod = "LUNCH";
                              break;
                            case 3:
                              planPeriod = "LSNACK";
                              break;
                            case 4:
                              planPeriod = "DINNER";
                              break;
                          }
                        },
                        isScrollable: true,
                        controller: _tabController,
                        labelColor: Colors.white,
                        unselectedLabelColor: ksecondaryTextColour,
                        indicatorColor: kprimaryColour,
                        indicator: BoxDecoration(
                            color: kprimaryColour
                        ),
                        tabs: [
                          Tab(child: Text('Breakfast',),
                          ),
                          Tab(child: Text('Snack',),
                          ),
                          Tab(child: Text('Lunch',),
                          ),
                          Tab(child: Text('Snack',),
                          ),
                          Tab(child: Text('Dinner',),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height,
                      child: TabBarView(
                        controller: _tabController,
                          children: [
                            SingleChildScrollView(child: Container(
                              child: Column(
                                children: [
                                  SizedBox(height: ScreenUtil().setHeight(20),),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text("Recommended Diet", style: TextStyle(
                                        color: Colors.black
                                      ),),
                                      GestureDetector(
                                          onTap: (){
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(builder: (context) => ViewAllMealScreen()));
                                          },
                                          child: Text("View all", style: TextStyle(color: ksecondaryTextColour),))
                                    ],
                                  ),
                                  SizedBox(height: ScreenUtil().setHeight(10),),
                                  Container(
                                    height: ScreenUtil().setHeight(270),
                                    child: PageView.builder(
                                      scrollDirection: Axis.horizontal,
                                      itemCount: 3,
                                      reverse: true,
                                      itemBuilder: (BuildContext context, int position){
                                        return Card(
                                            shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(15.0),
                                            ),
                                            elevation: 3.0,
                                            margin: new EdgeInsets.symmetric(
                                                horizontal: 10.0,
                                                vertical: 6.0),
                                            child: Column(children: <Widget>[
                                              Stack(
                                                children: [
                                                  Container(
                                                    height: 120,
                                                    decoration: BoxDecoration(
                                                      image: DecorationImage(
                                                        image:
                                                        AssetImage(
                                                            'assets/images/nugget_image.png'),
                                                        fit: BoxFit.fill,
                                                      ),
                                                    ),
                                                  ),
                                                  Positioned(
                                                      right: 0,
                                                      child: GestureDetector(
                                                         onTap: (){
                                                           Navigator.push(
                                                               context,
                                                               MaterialPageRoute(builder: (context) => MealNutrientScreen()));
                                                         },
                                                          child: SvgPicture.asset("assets/images/diet_icon.svg")))
                                                ],
                                              ),
                                              Column(
                                                children: [
                                                  Padding(
                                                    padding: const EdgeInsets.all(8.0),
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: <Widget>[
                                                        Text("Greek plain yogurt (200g)", style: TextStyle(
                                                            color: Colors.black
                                                        ),),
                                                        Text("61cal", style: TextStyle(
                                                            color: ksecondaryTextColour
                                                        ),),

//                                            Expanded(
//                                              child: Padding(
//                                                padding:
//                                                const EdgeInsets
//                                                    .all(16.0),
//                                                child: Text(state.nuggetData[position].body,
//                                                  overflow: TextOverflow.ellipsis,
//                                                  maxLines: 3,
//                                                  style: TextStyle(
//                                                      fontWeight:
//                                                      FontWeight
//                                                          .w500),
//                                                ),
//                                              ),
//                                            ),
                                                      ],
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding: EdgeInsets.all(8.0),
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        Row(
                                                          children: [
                                                            Text('Protein'),
                                                            SizedBox(width: 10,),
                                                            Text('7g', style: TextStyle(color: ksecondaryTextColour),)
                                                          ],
                                                        ),
                                                        Row(
                                                          children: [
                                                            Text('Protein'),
                                                            SizedBox(width: 10,),
                                                            Text('7g', style: TextStyle(color: ksecondaryTextColour),)
                                                          ],
                                                        ),
                                                        Row(
                                                          children: [
                                                            Text('Protein'),
                                                            SizedBox(width: 10,),
                                                            Text('7g', style: TextStyle(color: ksecondaryTextColour),)
                                                          ],
                                                        )
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ]));
                                      },
                                    ),
                                  ),
                                  SizedBox(height: ScreenUtil().setHeight(30),),
                                  Row(
                                    children: [
                                      Text("Breakfast Diet", style: TextStyle(
                                          color: Colors.black),),
                                    ],
                                  ),
                                  SizedBox(height: ScreenUtil().setHeight(20),),
                                 Text('When you add new diet component they will show up here'),
                                  SizedBox(height: ScreenUtil().setHeight(20),),
                                  Center(
                                    child: GestureDetector(
                                      onTap: (){
                                        Navigator.push(context,
                                            MaterialPageRoute(
                                              builder: (context) =>
                                              CreateMealPlanScreen(planDate: planDate, planPeriod: planPeriod,)
                                            ));
                                      },
                                      child: Text("Add Component",
                                        style: TextStyle(
                                            color: kprimaryColour
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: ScreenUtil().setHeight(20),),
                                  Container(
                                    padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
                                    height: ScreenUtil().setHeight(140),
                                    width: ScreenUtil().setWidth(400),
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(10),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Color.fromRGBO(0, 0, 0, 0.25),
                                            blurRadius: 20,
                                            offset: Offset(0, 2), // Shadow position
                                          ),
                                        ]
                                    ),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Image.asset("assets/images/greek.png"),
//                          SizedBox(width: ScreenUtil().setWidth(2),),
                                            Column(
                                              children: [
                                                Text("Greek plain yoghurt (200g)", style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: ScreenUtil().setSp(16.0)
                                                ),),
                                                SizedBox(height: ScreenUtil().setHeight(10),),
                                                Padding(
                                                  padding: const EdgeInsets.only(left: 24.0),
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    children: [
                                                      MealNutrientWidget(up: "Calories", down: "129cal",),
                                                      SizedBox(width: ScreenUtil().setWidth(15),),
                                                      MealNutrientWidget(up: "Protein", down: "10g",),
                                                      SizedBox(width: ScreenUtil().setWidth(15),),
                                                      MealNutrientWidget(up: "Fat", down: "9g",),
                                                      SizedBox(width: ScreenUtil().setWidth(15),),
                                                      MealNutrientWidget(up: "Carbs", down: "43g",),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
                                    height: ScreenUtil().setHeight(140),
                                    width: ScreenUtil().setWidth(400),
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(10),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Color.fromRGBO(0, 0, 0, 0.25),
                                            blurRadius: 20,
                                            offset: Offset(0, 2), // Shadow position
                                          ),
                                        ]
                                    ),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Image.asset("assets/images/greek.png"),
//                          SizedBox(width: ScreenUtil().setWidth(2),),
                                            Column(
                                              children: [
                                                Text("Greek plain yoghurt (200g)", style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: ScreenUtil().setSp(16.0)
                                                ),),
                                                SizedBox(height: ScreenUtil().setHeight(10),),
                                                Padding(
                                                  padding: const EdgeInsets.only(left: 24.0),
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    children: [
                                                      MealNutrientWidget(up: "Calories", down: "129cal",),
                                                      SizedBox(width: ScreenUtil().setWidth(15),),
                                                      MealNutrientWidget(up: "Protein", down: "10g",),
                                                      SizedBox(width: ScreenUtil().setWidth(15),),
                                                      MealNutrientWidget(up: "Fat", down: "9g",),
                                                      SizedBox(width: ScreenUtil().setWidth(15),),
                                                      MealNutrientWidget(up: "Carbs", down: "43g",),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),)),
                            SingleChildScrollView(child: NestedTabBar()),
                            SingleChildScrollView(child: Container()),
                            SingleChildScrollView(child: Container()),
                            SingleChildScrollView(child: Container())
                          ]
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        dataCheck == null ? Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Color.fromRGBO(34, 34, 34, 0.8),
          padding: EdgeInsets.only(bottom: 16),
          child: Column(
            children: [
              SizedBox(height: ScreenUtil().setHeight(100),),
              SvgPicture.asset("assets/images/lock.svg"),
              SizedBox(height: ScreenUtil().setHeight(20),),
              Text("Unlock Everything",
              style: TextStyle(
                color: Colors.white,
                fontSize: ScreenUtil().setSp(18.0),
              ),
              ),
              SizedBox(height: ScreenUtil().setHeight(20),),
              Padding(
                padding: EdgeInsets.only(
                  left: ScreenUtil().setWidth(16.0),
                  right: ScreenUtil().setWidth(16.0)
                ),
                child: Text("Get access to all our wonderful features of Diet manager, Meal Nutrient statistics, and Daily reminders",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(10),),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.only(
                    left: ScreenUtil().setWidth(8.0),
                    right: ScreenUtil().setWidth(8.0)
                  ),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Reusable(
                          onPress: (){
//                          Navigator.pushNamed (context, '/search');
                          },
                          cardChild: IconContent(
                            up: "Lite Plan",
                            middle: "\$ 1.27 ",
                            middleDown: "Weekly",
                            down: " ",
                          ),
                          colour: Colors.white,
                        ),
                      ),
                      Expanded(
                        child: Reusable(
                          onPress: (){
//                          Navigator.pushNamed (context, '/hymnbook');
                          } ,
                          cardChild: new IconContent(
                            up: "Growth Plan",
                            middle: "\$ 3.56",
                            middleDown: "Monthly",
                            down: "25% off the Lite Plan",
                          ),
                          colour: Colors.white,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.only(
                    left: ScreenUtil().setWidth(8.0),
                   right: ScreenUtil().setWidth(8.0)
                  ),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Reusable(
                          onPress: (){
//                          Navigator.pushNamed (context, '/search');
                          },
                          cardChild: IconContent(
                            up: "Essential Plan",
                            middle: "\$ 17.10",
                            middleDown: "Biannually",
                            down: "40% off the Growth Plan",
                          ),
                          colour: Colors.white,
                        ),
                      ),
                      Expanded(
                        child: Reusable(
                          onPress: (){
//                          Navigator.pushNamed (context, '/hymnbook');
                          } ,
                          cardChild: new IconContent(
                            up: "Standard Plan",
                            middle: "\$ 25.65",
                            middleDown: "Yearly",
                            down: "55% off the Growth Plan",
                          ),
                          colour: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(100),),
              Material(
                elevation: 2.0,
                color: kprimaryColour,
                borderRadius: BorderRadius.circular(50.0),
                child: MaterialButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/emailsignUpScreen');
                  },
                  minWidth: ScreenUtil().setWidth(400.0),
                  height: ScreenUtil().setHeight(42.0),
                  child: Text(
                    "Upgrade",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: ScreenUtil().setSp(18.0),
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Circular Std',
                    ),
                  ),
                ),
              ),
            ],
          ),
        ): SizedBox(),
      ],
    );
  }
}

class NestedTabBar extends StatefulWidget {
  @override
  _NestedTabBarState createState() => _NestedTabBarState();
}
class _NestedTabBarState extends State<NestedTabBar>
    with TickerProviderStateMixin {
  TabController _nestedTabController;
  @override
  void initState() {
    super.initState();
    _nestedTabController = new TabController(length: 2, vsync: this);
  }
  @override
  void dispose() {
    super.dispose();
    _nestedTabController.dispose();
  }
  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    return Column(
//      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(16),
            right: ScreenUtil().setWidth(16),
          ),
          child: TabBar(
            controller: _nestedTabController,
            indicatorColor: kprimaryColour,
            labelColor: Colors.black,
            unselectedLabelColor: kprimaryColour,
//            isScrollable: true,
            tabs: <Widget>[
              Tab(
                child: Container(
                  child: Text("Recommended Meal"),
                ),
              ),
              Tab(
                child: Container(
                  child: Text("My Meal"),
                ),
              ),

            ],
          ),
        ),
        Container(
          height: screenHeight * 0.70,
//          margin: EdgeInsets.only(left: 16.0, right: 16.0),
          child: TabBarView(
            controller: _nestedTabController,
            children: <Widget>[
              SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.only(
                      left: 8.0
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: ScreenUtil().setHeight(20),),
                      Text("Meal Nutrients"),
                      SizedBox(height: ScreenUtil().setHeight(10),),
                      Container(
                        padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
                        height: ScreenUtil().setHeight(120),
                        width: ScreenUtil().setWidth(400),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.25),
                              blurRadius: 20,
                              offset: Offset(0, 2), // Shadow position
                            ),
                          ]
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                MealNutrientWidget(up: "Calories", down: "129cal",),
                                MealNutrientWidget(up: "Protein", down: "10g",),
                                MealNutrientWidget(up: "Fat", down: "9g",),
                                MealNutrientWidget(up: "Carbs", down: "43g",),
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(20),),
                      Text("Components"),
                      SizedBox(height: ScreenUtil().setHeight(10),),
                      Container(
                        padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
                        height: ScreenUtil().setHeight(140),
                        width: ScreenUtil().setWidth(400),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.25),
                                blurRadius: 20,
                                offset: Offset(0, 2), // Shadow position
                              ),
                            ]
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Image.asset("assets/images/greek.png"),
//                          SizedBox(width: ScreenUtil().setWidth(2),),
                            Column(
                              children: [
                                Center(
                                  child: Text("Greek plain yoghurt (200g)", style: TextStyle(
                                    color: Colors.black,
                                    fontSize: ScreenUtil().setSp(16.0)
                                  ),),
                                ),
                                SizedBox(height: ScreenUtil().setHeight(10),),
                                Padding(
                                  padding: const EdgeInsets.only(left: 24.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      MealNutrientWidget(up: "Calories", down: "129cal",),
                                      SizedBox(width: ScreenUtil().setWidth(15),),
                                      MealNutrientWidget(up: "Protein", down: "10g",),
                                      SizedBox(width: ScreenUtil().setWidth(15),),
                                      MealNutrientWidget(up: "Fat", down: "9g",),
                                      SizedBox(width: ScreenUtil().setWidth(15),),
                                      MealNutrientWidget(up: "Carbs", down: "43g",),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(10),),
                      Container(
                        padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
                        height: ScreenUtil().setHeight(140),
                        width: ScreenUtil().setWidth(400),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.25),
                                blurRadius: 20,
                                offset: Offset(0, 2), // Shadow position
                              ),
                            ]
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Image.asset("assets/images/greek.png"),
//                          SizedBox(width: ScreenUtil().setWidth(2),),
                            Column(
                              children: [
                                Center(
                                  child: Text("Greek plain yoghurt (200g)", style: TextStyle(
                                      color: Colors.black,
                                      fontSize: ScreenUtil().setSp(16.0)
                                  ),),
                                ),
                                SizedBox(height: ScreenUtil().setHeight(10),),
                                Padding(
                                  padding: const EdgeInsets.only(left: 24.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      MealNutrientWidget(up: "Calories", down: "129cal",),
                                      SizedBox(width: ScreenUtil().setWidth(15),),
                                      MealNutrientWidget(up: "Protein", down: "10g",),
                                      SizedBox(width: ScreenUtil().setWidth(15),),
                                      MealNutrientWidget(up: "Fat", down: "9g",),
                                      SizedBox(width: ScreenUtil().setWidth(15),),
                                      MealNutrientWidget(up: "Carbs", down: "43g",),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(10),),
                    Container(
                      padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
                      height: ScreenUtil().setHeight(140),
                      width: ScreenUtil().setWidth(400),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.25),
                              blurRadius: 20,
                              offset: Offset(0, 2), // Shadow position
                            ),
                          ]
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Image.asset("assets/images/greek.png"),
//                          SizedBox(width: ScreenUtil().setWidth(2),),
                          Column(
                            children: [
                              Center(
                                child: Text("Greek plain yoghurt (200g)", style: TextStyle(
                                    color: Colors.black,
                                    fontSize: ScreenUtil().setSp(16.0)
                                ),),
                              ),
                              SizedBox(height: ScreenUtil().setHeight(10),),
                              Padding(
                                padding: const EdgeInsets.only(left: 24.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    MealNutrientWidget(up: "Calories", down: "129cal",),
                                    SizedBox(width: ScreenUtil().setWidth(15),),
                                    MealNutrientWidget(up: "Protein", down: "10g",),
                                    SizedBox(width: ScreenUtil().setWidth(15),),
                                    MealNutrientWidget(up: "Fat", down: "9g",),
                                    SizedBox(width: ScreenUtil().setWidth(15),),
                                    MealNutrientWidget(up: "Carbs", down: "43g",),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    ],
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                    left: 8.0
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: ScreenUtil().setHeight(40),),
                    Center(
                      child: Text("When you create new foods they will show up here",
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(30),),
                    GestureDetector(
                      onTap: (){
                        Navigator.pushNamed(context, "/createMealPlamScreen");
                      },
                      child: Center(
                        child: Text("Create Meal",
                          style: TextStyle(
                            color: kprimaryColour
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(20),),
                    Text("Meal Nutrients"),
                    SizedBox(height: ScreenUtil().setHeight(10),),
                    Container(
                      padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
                      height: ScreenUtil().setHeight(120),
                      width: ScreenUtil().setWidth(400),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.25),
                              blurRadius: 20,
                              offset: Offset(0, 2), // Shadow position
                            ),
                          ]
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              MealNutrientWidget(up: "Calories", down: "129cal",),
                              MealNutrientWidget(up: "Protein", down: "10g",),
                              MealNutrientWidget(up: "Fat", down: "9g",),
                              MealNutrientWidget(up: "Carbs", down: "43g",),
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(20),),
                  ],
                ),
              ),

            ],
          ),
        )
      ],
    );
  }
}

class MealNutrientWidget extends StatelessWidget {
    String up;
    String down;

    MealNutrientWidget({@required this.up, @required this.down});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(up),
        SizedBox(height: ScreenUtil().setHeight(10),),
        Text(down),
      ],
    );
  }
}
