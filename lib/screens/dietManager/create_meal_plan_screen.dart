import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:joalapp/components/constants.dart';

class CreateMealPlanScreen extends StatefulWidget {
  String planPeriod;
  DateTime planDate;

  CreateMealPlanScreen({@required this.planPeriod, @required this.planDate});

  @override
  _CreateMealPlanScreenState createState() => _CreateMealPlanScreenState();
}

class _CreateMealPlanScreenState extends State<CreateMealPlanScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(widget.planPeriod);
    print(widget.planDate);
  }
  @override
  Widget build(BuildContext context) {
    sizess(context);
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: ksecondaryColour,
        elevation: 0,
        centerTitle: true,
        title: Padding(
          padding: EdgeInsets.only(top: ScreenUtil().setWidth(20.0)),
          child: Text(
            "Create Meal Plan",
            style: TextStyle(
              color: kprimaryColour,
//                fontSize: ScreenUtil().setSp(18.0)
            ),
          ),
        ),
      ),
      body: Container(
        color: ksecondaryColour,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: ScreenUtil().setHeight(30),
              ),
              Padding(
                padding: EdgeInsets.only(
                    left: ScreenUtil().setWidth(16.0),
                    right: ScreenUtil().setWidth(16.0)),
                child: TextField(
//              onChanged: onSearchTextChanged,
//              controller: _editingController,
                  style: TextStyle(color: Colors.black),
                  decoration: InputDecoration(
                    contentPadding:
                        EdgeInsets.symmetric(vertical: 15.0, horizontal: 16.0),
                    // labelText: "Search",
                    hintText: "Search",
                    suffixIcon: Icon(
                      Icons.search,
                      color: ksecondaryTextColour,
                    ),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Color(0xFFD0F1DD)),
                        borderRadius: BorderRadius.all(Radius.circular(25.0))),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Color(0xFFD0F1DD)),
                        borderRadius: BorderRadius.all(Radius.circular(25.0))),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Color(0xFFD0F1DD)),
                        borderRadius: BorderRadius.all(Radius.circular(25.0))),
                  ),
                ),
              ),
              SizedBox(
                height: ScreenUtil().setHeight(50),
              ),
              SvgPicture.asset("assets/images/file_search.svg"),
              SizedBox(
                height: ScreenUtil().setHeight(20),
              ),
              Text("Start typing the name of the food item in the search bar"),
              SizedBox(
                height: ScreenUtil().setHeight(20),
              ),
              Container(
                margin: EdgeInsets.all(ScreenUtil().setWidth(16)),
                padding: EdgeInsets.all(ScreenUtil().setWidth(16)),
                decoration: BoxDecoration(color: ksecondaryColour, boxShadow: [
                  BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.25),
                    blurRadius: 20,
                    offset: Offset(0, 2), // Shadow position
                  ),
                ]),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Bread (Wheat)"),
                    Row(
                      children: [
                        Text(
                          "128 cal",
                          style: TextStyle(color: kprimaryColour),
                        ),
                        SizedBox(
                          width: ScreenUtil().setWidth(10),
                        ),
                        SvgPicture.asset("assets/images/plus.svg")
                      ],
                    )
                  ],
                ),
              ),
              SizedBox(
                height: ScreenUtil().setHeight(10),
              ),
              Container(
                margin: EdgeInsets.all(ScreenUtil().setWidth(16)),
                padding: EdgeInsets.all(ScreenUtil().setWidth(16)),
                decoration: BoxDecoration(color: ksecondaryColour, boxShadow: [
                  BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.25),
                    blurRadius: 20,
                    offset: Offset(0, 2), // Shadow position
                  ),
                ]),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Bread (Wheat)"),
                    Row(
                      children: [
                        Text(
                          "128 cal",
                          style: TextStyle(color: kprimaryColour),
                        ),
                        SizedBox(
                          width: ScreenUtil().setWidth(10),
                        ),
                        SvgPicture.asset("assets/images/plus.svg")
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
