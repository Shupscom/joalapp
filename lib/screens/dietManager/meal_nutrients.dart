import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:joalapp/components/constants.dart';
class MealNutrientScreen extends StatefulWidget {
  @override
  _MealNutrientScreenState createState() => _MealNutrientScreenState();
}

class _MealNutrientScreenState extends State<MealNutrientScreen> {
  @override
  Widget build(BuildContext context) {
    sizess(context);
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: ksecondaryColour,
        elevation: 0,
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: GestureDetector(
              onTap: (){
                Navigator.pushNamed(context, "/addSuperMealScreen");
              },
              child: Text("Add",
                style: TextStyle(
                    color: Colors.black
                ),
              ),
            ),
          ),
        ],
      ),
      body: Container(
        color: ksecondaryColour,
        padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: ScreenUtil().setHeight(20.0),),
            Container(
              width: ScreenUtil().setWidth(500),
              child: Image.asset("assets/images/meal_detail.png",
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(height: ScreenUtil().setHeight(30.0),),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                    "Brocolli",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: ScreenUtil().setSp(16.0)
                    )
                ),
                Text(
                    "Shop Item",
                    style: TextStyle(
                        color:  Colors.black,
                        fontSize: ScreenUtil().setSp(16.0)
                    )
                ),
              ],
            ),
            SizedBox(height: ScreenUtil().setHeight(20.0),),
            Container(
              padding: EdgeInsets.only(left: ScreenUtil().setWidth(8.0)),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(3.0),
//                       color: Colors.cyan,
                  border: Border.all(color: Color(0xFFD0F1DD), width: 1.0)),
              child: DropdownButtonHideUnderline(
                child: DropdownButton(
                    isExpanded: true,
                    value: "Male",
                    items: [
                      DropdownMenuItem(
                        child: Text("Male"),
                        value: "Male",
                      ),
                      DropdownMenuItem(
                        child: Text("Female"),
                        value: "Female",
                      ),

                    ],
                    onChanged: (value) {
                      setState(() {
//                    _value = value;
                      });
                    }),
              ),
            ),
            SizedBox(height: ScreenUtil().setHeight(30.0),),
            Text("Nutrients",
              style: TextStyle(
                  fontSize: ScreenUtil().setSp(16.0)
              ),
            ),
            SizedBox(height: ScreenUtil().setHeight(20.0),),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                    "Carbs",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: ScreenUtil().setSp(16.0)
                    )
                ),
                Text(
                    "4.5g",
                    style: TextStyle(
                        color:  Colors.black,
                        fontSize: ScreenUtil().setSp(16.0)
                    )
                ),
              ],
            ),
            SizedBox(height: ScreenUtil().setHeight(20.0),),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                    "Proteins",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: ScreenUtil().setSp(16.0)
                    )
                ),
                Text(
                    "1.9g",
                    style: TextStyle(
                        color:  Colors.black,
                        fontSize: ScreenUtil().setSp(16.0)
                    )
                ),
              ],
            ),
            SizedBox(height: ScreenUtil().setHeight(20.0),),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                    "Fats",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: ScreenUtil().setSp(16.0)
                    )
                ),
                Text(
                    "0.3g",
                    style: TextStyle(
                        color:  Colors.black,
                        fontSize: ScreenUtil().setSp(16.0)
                    )
                ),
              ],
            ),
            SizedBox(height: ScreenUtil().setHeight(20.0),),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                    "Calories",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: ScreenUtil().setSp(16.0)
                    )
                ),
                Text(
                    "27.5cal",
                    style: TextStyle(
                        color:  Colors.black,
                        fontSize: ScreenUtil().setSp(16.0)
                    )
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
