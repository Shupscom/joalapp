import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:joalapp/components/constants.dart';

class ViewAllMealScreen extends StatefulWidget {
  @override
  _ViewAllMealScreenState createState() => _ViewAllMealScreenState();
}

class _ViewAllMealScreenState extends State<ViewAllMealScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: ksecondaryColour,
        elevation: 0,
      ),
      body: Container(
        padding: EdgeInsets.only(left: ScreenUtil().setWidth(16), right: ScreenUtil().setWidth(16)),
        decoration: BoxDecoration(
          color: ksecondaryColour
        ),
        child: ListView.builder(
            itemCount: 5,
            itemBuilder: (BuildContext context, int position) {
              return Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  elevation: 3.0,
                  margin:
                      new EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                  child: Column(children: <Widget>[
                    Stack(
                      children: [
                        Container(
                          height: 120,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image:
                                  AssetImage('assets/images/nugget_image.png'),
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                        Positioned(
                            right: 0,
                            child: GestureDetector(
                                child: SvgPicture.asset(
                                    "assets/images/diet_icon.svg")))
                      ],
                    ),
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                "Greek plain yogurt (200g)",
                                style: TextStyle(color: Colors.black),
                              ),
                              Text(
                                "61cal",
                                style: TextStyle(color: ksecondaryTextColour),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  Text('Protein'),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    '7g',
                                    style:
                                        TextStyle(color: ksecondaryTextColour),
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  Text('Protein'),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    '7g',
                                    style:
                                        TextStyle(color: ksecondaryTextColour),
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  Text('Protein'),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    '7g',
                                    style:
                                        TextStyle(color: ksecondaryTextColour),
                                  )
                                ],
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ]));
            }),
      ),
    );
  }
}
