import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/register/register_bloc.dart';
import 'package:joalapp/screens/signup_screen.dart';

class SignUpPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (BuildContext context) {
          return RegisterBloc(context);
        },
        child: SignUpScreen()

    );
  }
}