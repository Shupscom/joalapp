import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:joalapp/bloc/register/register_bloc.dart';
import 'package:joalapp/bloc/register/register_event.dart';
import 'package:joalapp/bloc/register/register_state.dart';
import 'package:joalapp/components/constants.dart';
import 'package:joalapp/components/linked_text.dart';
import 'package:joalapp/screens/onboarding/email_siginup_screen.dart';
import 'package:joalapp/screens/onboarding/email_signin_page.dart';
import 'package:joalapp/screens/onboarding/email_signup_page.dart';
import 'package:joalapp/screens/onboarding/gender_page.dart';
class SignUpScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    _onRegisterFacebookButtonPressed() {
      Future.delayed(Duration(seconds: 8));
      BlocProvider.of<RegisterBloc>(context).add(RegisterFacebookButton());
    }
    _onRegisterGoogleButtonPressed() {
      Future.delayed(Duration(seconds: 8));
      BlocProvider.of<RegisterBloc>(context).add(RegisterGoogleButton());
    }
    sizess(context);
    return BlocListener<RegisterBloc,RegisterState>(
      listener: (context, state){
        if(state is EmailSuccess){
          Navigator.push(context, MaterialPageRoute(builder: (context) =>
              GenderPage(userData: state.userData)));
        }
        if(state is EmailLoading){
          return Center(child: CircularProgressIndicator());
        }
      },
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () => Navigator.of(context).pop(),
          ),
          backgroundColor: ksecondaryColour,
          elevation: 0,
        ),
        body: Container(
          color: ksecondaryColour,
         child: SingleChildScrollView(
           child: Column(
             crossAxisAlignment: CrossAxisAlignment.center,
             mainAxisAlignment: MainAxisAlignment.start,
             children: [
               SizedBox(height: ScreenUtil().setHeight(50.0),),
               Center(
                 child: SvgPicture.asset('assets/images/signuplogo.svg',
                 ),
               ),
               Text("REGISTER WITH",
                textAlign: TextAlign.center,
               style: TextStyle(
                  color: Color(0xFF666A86),
                   fontSize:  ScreenUtil().setSp(18.0),
                   fontFamily: "Circular Std"
               ),
               ),
               SizedBox(height: ScreenUtil().setHeight(50.0),),
               Row(
                 mainAxisAlignment: MainAxisAlignment.center,
                 children: [
                   GestureDetector(
                     onTap: (){
                        _onRegisterFacebookButtonPressed();
                     },
                     child: Container(
                         decoration: BoxDecoration(
                             boxShadow: [
                               BoxShadow(
                                   blurRadius: 8,
                                   offset: Offset(0, 15),
                                   color: Colors.grey.withOpacity(.1),
                                   spreadRadius: -9)
                             ]
                         ),
                         child: SvgPicture.asset('assets/images/facebook.svg')),
                   ),
                   SizedBox(width: ScreenUtil().setWidth(30),),
                   GestureDetector(
                     onTap: (){
                       _onRegisterGoogleButtonPressed();
                     },
                     child: Container(
                         decoration: BoxDecoration(
                             boxShadow: [
                               BoxShadow(
                                   blurRadius: 8,
                                   offset: Offset(0, 15),
                                   color: Colors.grey.withOpacity(.1),
                                   spreadRadius: -9)
                             ]
                         ),
                         child: SvgPicture.asset('assets/images/google.svg')),
                   ),
                 ],
               ),
               SizedBox(height: ScreenUtil().setHeight(50.0),),
               Row(
                 mainAxisAlignment: MainAxisAlignment.center,
                 children: <Widget>[
                 SvgPicture.asset('assets/images/Line.svg'),
                   SizedBox(
                     width: ScreenUtil().setWidth(8.0),
                   ),
                  Text("0R",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                  color: Color(0xFF666A86),
                  fontSize:  ScreenUtil().setSp(20.0),
                  fontFamily: "Circular Std"
                  ),
                  ),
                  SizedBox(
                  width: ScreenUtil().setWidth(8.0),
                  ),
                   SvgPicture.asset('assets/images/Line.svg'),
                 ],
               ),

               SizedBox(height: ScreenUtil().setHeight(50.0),),
               Material(
                 elevation: 2.0,
                 color: kprimaryColour,
                 borderRadius: BorderRadius.circular(20.0),
                 child: MaterialButton(
                   onPressed: () {
                     Navigator.pushNamed(context, '/emailsignUpScreen');
                   },
                   minWidth: ScreenUtil().setWidth(380.0),
                   height: ScreenUtil().setHeight(42.0),
                   child: Text(
                     "EMAIL REGISTRATION",
                     style: TextStyle(
                       color: Colors.white,
                       fontSize: ScreenUtil().setSp(18.0),
                       fontWeight: FontWeight.bold,
                       fontFamily: 'Circular Std',
                     ),
                   ),
                 ),
               ),
               SizedBox(height: ScreenUtil().setHeight(10.0),),
               Row(
                 mainAxisAlignment: MainAxisAlignment.center,
                 children: <Widget>[
                   Text(
                     "Already have an account?",
                     style: TextStyle(
                         color: Color(0xFF535353),
                         fontWeight: FontWeight.w500,
                         fontSize: ScreenUtil().setSp(18.0)),
                   ),
                   SizedBox(
                     width: ScreenUtil().setWidth(8.0),
                   ),
                   LinkedText(
                     ontap: () {
//                   setScreenBaorded(true);
                       Navigator.pushNamed(context, '/emailsignInScreen');
                     },
                     title: "SIGN IN",
                     colour: kprimaryColour,
                   ),
                 ],
               ),
             ],
           ),
         ),
        ),
      ),
    );
  }
}
