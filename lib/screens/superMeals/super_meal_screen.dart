import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:joalapp/bloc/supermeals/alkalines/alkalines_bloc.dart';
import 'package:joalapp/bloc/supermeals/alkalines/alkalines_event.dart';
import 'package:joalapp/bloc/supermeals/alkalines/alkalines_state.dart';
import 'package:joalapp/bloc/supermeals/calciumrich/calciumrich_bloc.dart';
import 'package:joalapp/bloc/supermeals/calciumrich/calciumrich_state.dart';
import 'package:joalapp/bloc/supermeals/headache-fighter/headache-fighter_bloc.dart';
import 'package:joalapp/bloc/supermeals/headache-fighter/headache-fighter_event.dart';
import 'package:joalapp/bloc/supermeals/headache-fighter/headache-fighter_state.dart';
import 'package:joalapp/bloc/supermeals/highfiber/highfiber_bloc.dart';
import 'package:joalapp/bloc/supermeals/highfiber/highfiber_state.dart';
import 'package:joalapp/bloc/supermeals/improvedsex/improvedsex_bloc.dart';
import 'package:joalapp/bloc/supermeals/improvedsex/improvedsex_state.dart';
import 'package:joalapp/bloc/supermeals/ironrich/ironrich_bloc.dart';
import 'package:joalapp/bloc/supermeals/ironrich/ironrich_state.dart';
import 'package:joalapp/bloc/supermeals/non-vegan/non-vegan_bloc.dart';
import 'package:joalapp/bloc/supermeals/non-vegan/non-vegan_event.dart';
import 'package:joalapp/bloc/supermeals/non-vegan/non-vegan_state.dart';
import 'package:joalapp/bloc/supermeals/super_meal_bloc.dart';
import 'package:joalapp/bloc/supermeals/super_meal_event.dart';
import 'package:joalapp/bloc/supermeals/super_meal_state.dart';
import 'package:joalapp/components/constants.dart';
import 'package:joalapp/components/make_bottom.dart';
import 'package:joalapp/screens/superMeals/supermeal_details.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SuperMealScreen extends StatefulWidget {
  @override
  _SuperMealScreenState createState() => _SuperMealScreenState();
}

class _SuperMealScreenState extends State<SuperMealScreen>
    with SingleTickerProviderStateMixin {
  int _selectedIndex = 0;
  TabController _tabController;
  bool showFilter = false;
  bool vegetables = false;
  bool fruits = false;
  bool nuts = false;
  bool spices = false;
  var userdata;

  @override
  void initState() {
    // TODO: implement initState
    _getUserInfo();
    super.initState();
    _tabController = TabController(length: 8, vsync: this);
    _tabController.addListener(() {
      setState(() {
        _selectedIndex = _tabController.index;
      });
    });
  }
  void _getUserInfo() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var data = sharedPreferences.get("userdata");
    setState(() {
      userdata = json.decode(data);
      print(userdata);
    });
  }
  @override
  Widget build(BuildContext context) {
    sizess(context);
    return DefaultTabController(
      length: 8,
      child: Scaffold(
        bottomNavigationBar: MakeBottom(
          active: 'supermeal',
          userData: userdata,
        ),
        appBar: AppBar(
          backgroundColor: ksecondaryColour,
          elevation: 2,
          title: Align(
            alignment: Alignment(-2, 0),
            child: Padding(
              padding: EdgeInsets.only(
                  top: ScreenUtil().setWidth(20.0)
              ),
              child: Text("World Super Meals",
                style: TextStyle(
                    color: kprimaryColour,
                    fontSize: ScreenUtil().setSp(28.0)
                ),
              ),
            ),
          ),
          bottom: TabBar(
            onTap: (tabIndex) {
              switch (tabIndex) {
                case 0:
                  setState(() {
                    showFilter = false;
                    vegetables = false;
                    fruits = false;
                    nuts = false;
                    spices = false;
                  });
                  SuperMealBloc()
                    ..add(LoadVeganButton());
                  break;
                case 1:
                  setState(() {
                    showFilter = false;
                    vegetables = false;
                    fruits = false;
                    nuts = false;
                    spices = false;
                  });
                  NonVeganBloc()
                    ..add(LoadNonVeganButton());
                  break;
                case 2:
                  setState(() {
                    showFilter = false;
                    vegetables = false;
                    fruits = false;
                    nuts = false;
                    spices = false;
                  });
                  HeadacheFighterBloc()
                    ..add(LoadHeadAcheFighterButton());
                  break;
                case 3:
                  setState(() {
                    showFilter = false;
                    vegetables = false;
                    fruits = false;
                    nuts = false;
                    spices = false;
                  });
                  AlkalinesBloc()
                    ..add(LoadlkalinesButton());
                  break;
                case 4:
                  setState(() {
                    showFilter = false;
                    vegetables = false;
                    fruits = false;
                    nuts = false;
                    spices = false;
                  });
                  SuperMealBloc()
                    ..add(LoadVeganButton());
                  break;
                case 5:
                  setState(() {
                    showFilter = false;
                    vegetables = false;
                    fruits = false;
                    nuts = false;
                    spices = false;
                  });
                  NonVeganBloc()
                    ..add(LoadNonVeganButton());
                  break;
                case 6:
                  setState(() {
                    showFilter = false;
                    vegetables = false;
                    fruits = false;
                    nuts = false;
                    spices = false;
                  });
                  HeadacheFighterBloc()
                    ..add(LoadHeadAcheFighterButton());
                  break;
                case 7:
                  setState(() {
                    showFilter = false;
                    vegetables = false;
                    fruits = false;
                    nuts = false;
                    spices = false;
                  });
                  AlkalinesBloc()
                    ..add(LoadlkalinesButton());
                  break;
              }
            },
            labelColor: kprimaryColour,
            unselectedLabelColor: ksecondaryTextColour,
            isScrollable: true,
            controller: _tabController,
            indicatorColor: kprimaryColour,
            tabs: [
              Tab(child: Text("Vegan"),),
              Tab(child: Text("Non-vegan"),),
              Tab(child: Text("Headache fighter"),),
              Tab(child: Text("Alkalines"),),
              Tab(child: Text("Improved sex life"),),
              Tab(child: Text("Calcium rich"),),
              Tab(child: Text("High fiber"),),
              Tab(child: Text("Iron rich"),),
            ],
          ),
        ),
        body: Container(
          padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
          child: TabBarView(
            controller: _tabController,
            children: [
              BlocBuilder<SuperMealBloc, SuperMealState>(
                  builder: (context, state) {
                    if (state is SuperMealLoading) {
                      return Column(
                        children: [
                          SizedBox(height: ScreenUtil().setHeight(250),),
                          CircularProgressIndicator(),
                        ],
                      );
                    }
                    if(state is SuperMealSuccess){
                      return Stack(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 30.0),
                            child: ListView(
                              children: [
                                ListView.builder(
                                    itemCount: state.superMealData.length,
                                    scrollDirection: Axis.vertical,
                                    physics: NeverScrollableScrollPhysics(),
                                    shrinkWrap: true,
                                    itemBuilder: (BuildContext context,
                                        int position) {
                                      return GestureDetector(
                                        onTap: () {
                                          Navigator.push(context,
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    SuperMealDetailScreen(
                                                        superMealModel: state
                                                            .superMealData[position]),
                                              ));
                                        },
                                        child: Card(
                                            elevation: 3.0,
                                            margin: new EdgeInsets.symmetric(
                                                horizontal: 10.0, vertical: 6.0),
                                            child: Column(
                                                children: <Widget>[
                                                  Container(
                                                    height: 120,
                                                    decoration: BoxDecoration(
                                                      borderRadius: BorderRadius
                                                          .only(

                                                      ),
                                                      image: DecorationImage(
                                                        image: NetworkImage(state.superMealData[position].imgSrc),
                                                        fit: BoxFit.cover,
                                                      ),
                                                    ),
                                                  ),
                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment
                                                        .spaceBetween,
                                                    children: <Widget>[
                                                      Padding(
                                                        padding: const EdgeInsets
                                                            .all(16.0),
                                                        child: Text(state
                                                            .superMealData[position]
                                                            .name,
                                                          style: TextStyle(
                                                              fontWeight: FontWeight
                                                                  .w500
                                                          ),),
                                                      ),
                                                      Padding(
                                                        padding: const EdgeInsets
                                                            .all(16.0),
                                                        child: Text('${state.superMealData[position].nutrients.calories.toString()}cal',
                                                          style: TextStyle(
                                                              fontWeight: FontWeight
                                                                  .w500,
                                                              color: kprimaryColour
                                                          ),),
                                                      ),
                                                    ],
                                                  ),
                                                ]
                                            )
                                        ),
                                      );
                                    }

                                ),
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                showFilter = showFilter == false ? true : false;
                              });
                            },
                            child: Container(
                              margin: EdgeInsets.all(
                                  ScreenUtil().setWidth(8.0)),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text("Filter",
                                    style: TextStyle(
                                        color: kprimaryColour
                                    ),
                                  ),
                                  SvgPicture.asset(
                                      'assets/images/filter-left.svg')
                                ],
                              ),
                            ),
                          ),
                          showFilter ? Positioned(
                            top: 25,
                            right: 5,
                            child: Container(
                              width: ScreenUtil().setWidth(200),
                              height: ScreenUtil().setHeight(300),
                              child: Card(
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Checkbox(
                                          activeColor: kprimaryColour,
                                          value: this.vegetables,
                                          onChanged: (bool value) {
                                            setState(() {
                                              this.nuts = false;
                                              this.spices = false;
                                              this.fruits = false;
                                              this.vegetables = value;
                                            });
                                            BlocProvider.of<SuperMealBloc>(
                                                context).add(
                                                LoadByCateegory(
                                                    group: "60bb2580dcdb8200157cf7ea",
                                                    category: "60bb2580dcdb8200157cf7e7")
                                            );
                                          },
                                        ),
                                        Text("Vegetables",
                                          style: TextStyle(
                                              color: kprimaryColour
                                          ),)
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Checkbox(
                                          activeColor: kprimaryColour,
                                          value: this.fruits,
                                          onChanged: (bool value) {
                                            setState(() {
                                              this.vegetables = false;
                                              this.nuts = false;
                                              this.spices = false;
                                              this.fruits = value;
                                            });
                                            BlocProvider.of<SuperMealBloc>(
                                                context).add(
                                                LoadByCateegory(
                                                    group: "60bb2580dcdb8200157cf7ea",
                                                    category: "60bb2580dcdb8200157cf7e6")
                                            );
                                          },
                                        ),
                                        Text("Fruits",
                                          style: TextStyle(
                                              color: kprimaryColour),
                                        )
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Checkbox(
                                          activeColor: kprimaryColour,
                                          value: this.nuts,
                                          onChanged: (bool value) {
                                            setState(() {
                                              this.nuts = false;
                                              this.spices = false;
                                              this.fruits = false;
                                              this.nuts = value;
                                            });
                                            BlocProvider.of<SuperMealBloc>(
                                                context).add(
                                                LoadByCateegory(
                                                    group: "60bb2580dcdb8200157cf7ea",
                                                    category: "60bb2580dcdb8200157cf7e8")
                                            );
                                          },
                                        ),
                                        Text("Nuts and Seeds", style: TextStyle(
                                            color: kprimaryColour
                                        ),)
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Checkbox(
                                          activeColor: kprimaryColour,
                                          value: this.spices,
                                          onChanged: (bool value) {
                                            setState(() {
                                              this.spices = value;
                                              this.nuts = false;
                                              this.fruits = false;
                                              this.vegetables = false;
                                            });
                                            BlocProvider.of<SuperMealBloc>(
                                                context).add(
                                                LoadByCateegory(
                                                    group: "60bb2580dcdb8200157cf7ea",
                                                    category: "60bb2580dcdb8200157cf7e9")
                                            );
                                          },
                                        ),
                                        Text("Spices", style: TextStyle(
                                            color: kprimaryColour
                                        ),)
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ) : SizedBox(),
                        ],
                      );
                    }
                    if(state is SuperMealInitial){
                      return Stack(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 30.0),
                            child: ListView(
                              children: [
                                ListView.builder(
                                    itemCount: state.superMealData.length,
                                    scrollDirection: Axis.vertical,
                                    physics: NeverScrollableScrollPhysics(),
                                    shrinkWrap: true,
                                    itemBuilder: (BuildContext context,
                                        int position) {
                                      return GestureDetector(
                                        onTap: () {
                                          Navigator.push(context,
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    SuperMealDetailScreen(
                                                        superMealModel: state
                                                            .superMealData[position]),
                                              ));
                                        },
                                        child: Card(
                                            elevation: 3.0,
                                            margin: new EdgeInsets.symmetric(
                                                horizontal: 10.0, vertical: 6.0),
                                            child: Column(
                                                children: <Widget>[
                                                  Container(
                                                    height: 120,
                                                    decoration: BoxDecoration(
                                                      borderRadius: BorderRadius
                                                          .only(

                                                      ),
                                                      image: DecorationImage(
                                                        image: NetworkImage(state.superMealData[position].imgSrc),
                                                        fit: BoxFit.cover,
                                                      ),
                                                    ),
                                                  ),
                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment
                                                        .spaceBetween,
                                                    children: <Widget>[
                                                      Padding(
                                                        padding: const EdgeInsets
                                                            .all(16.0),
                                                        child: Text(state
                                                            .superMealData[position]
                                                            .name,
                                                          style: TextStyle(
                                                              fontWeight: FontWeight
                                                                  .w500
                                                          ),),
                                                      ),
                                                      Padding(
                                                        padding: const EdgeInsets
                                                            .all(16.0),
                                                        child: Text('${state.superMealData[position].nutrients.calories}cal',
                                                          style: TextStyle(
                                                              fontWeight: FontWeight
                                                                  .w500,
                                                              color: kprimaryColour
                                                          ),),
                                                      ),
                                                    ],
                                                  ),
                                                ]
                                            )
                                        ),
                                      );
                                    }

                                ),
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                showFilter = showFilter == false ? true : false;
                              });
                            },
                            child: Container(
                              margin: EdgeInsets.all(
                                  ScreenUtil().setWidth(8.0)),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text("Filter",
                                    style: TextStyle(
                                        color: kprimaryColour
                                    ),
                                  ),
                                  SvgPicture.asset(
                                      'assets/images/filter-left.svg')
                                ],
                              ),
                            ),
                          ),
                          showFilter ? Positioned(
                            top: 25,
                            right: 5,
                            child: Container(
                              width: ScreenUtil().setWidth(200),
                              height: ScreenUtil().setHeight(300),
                              child: Card(
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Checkbox(
                                          activeColor: kprimaryColour,
                                          value: this.vegetables,
                                          onChanged: (bool value) {
                                            setState(() {
                                              this.nuts = false;
                                              this.spices = false;
                                              this.fruits = false;
                                              this.vegetables = value;
                                            });
                                            BlocProvider.of<SuperMealBloc>(
                                                context).add(
                                                LoadByCateegory(
                                                    group: "60bb2580dcdb8200157cf7ea",
                                                    category: "60bb2580dcdb8200157cf7e7")
                                            );
                                          },
                                        ),
                                        Text("Vegetables",
                                          style: TextStyle(
                                              color: kprimaryColour
                                          ),)
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Checkbox(
                                          activeColor: kprimaryColour,
                                          value: this.fruits,
                                          onChanged: (bool value) {
                                            setState(() {
                                              this.vegetables = false;
                                              this.nuts = false;
                                              this.spices = false;
                                              this.fruits = value;
                                            });
                                            BlocProvider.of<SuperMealBloc>(
                                                context).add(
                                                LoadByCateegory(
                                                    group: "60bb2580dcdb8200157cf7ea",
                                                    category: "60bb2580dcdb8200157cf7e6")
                                            );
                                          },
                                        ),
                                        Text("Fruits",
                                          style: TextStyle(
                                              color: kprimaryColour),
                                        )
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Checkbox(
                                          activeColor: kprimaryColour,
                                          value: this.nuts,
                                          onChanged: (bool value) {
                                            setState(() {
                                              this.nuts = false;
                                              this.spices = false;
                                              this.fruits = false;
                                              this.nuts = value;
                                            });
                                            BlocProvider.of<SuperMealBloc>(
                                                context).add(
                                                LoadByCateegory(
                                                    group: "60bb2580dcdb8200157cf7ea",
                                                    category: "60bb2580dcdb8200157cf7e8")
                                            );
                                          },
                                        ),
                                        Text("Nuts and Seeds", style: TextStyle(
                                            color: kprimaryColour
                                        ),)
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Checkbox(
                                          activeColor: kprimaryColour,
                                          value: this.spices,
                                          onChanged: (bool value) {
                                            setState(() {
                                              this.spices = value;
                                              this.nuts = false;
                                              this.fruits = false;
                                              this.vegetables = false;
                                            });
                                            BlocProvider.of<SuperMealBloc>(
                                                context).add(
                                                LoadByCateegory(
                                                    group: "60bb2580dcdb8200157cf7ea",
                                                    category: "60bb2580dcdb8200157cf7e9")
                                            );
                                          },
                                        ),
                                        Text("Spices", style: TextStyle(
                                            color: kprimaryColour
                                        ),)
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ) : SizedBox(),
                        ],
                      );
                    }
                    return SizedBox();
                  }
              ),
              BlocBuilder<NonVeganBloc, NonVeganState>(
                  builder: (context, state) {
                    if (state is NonVeganLoading) {
                      return Column(
                        children: [
                          SizedBox(height: ScreenUtil().setHeight(250),),
                          CircularProgressIndicator(),
                        ],
                      );
                    }
                      return ListView(
                        children: [
                          ListView.builder(
                              itemCount: state.superMealData.length,
                              scrollDirection: Axis.vertical,
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemBuilder: (BuildContext context,
                                  int position) {
                                return GestureDetector(
                                  onTap: () {
                                    Navigator.push(context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              SuperMealDetailScreen(
                                                  superMealModel: state
                                                      .superMealData[position]),
                                        ));
                                  },
                                  child: Card(
                                      elevation: 3.0,
                                      margin: new EdgeInsets.symmetric(
                                          horizontal: 10.0, vertical: 6.0),
                                      child: Column(
                                          children: <Widget>[
                                            Container(
                                              height: 120,
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius
                                                    .only(

                                                ),
                                                image: DecorationImage(
                                                  image: NetworkImage(state.superMealData[position].imgSrc),
                                                  fit: BoxFit.cover,
                                                ),
                                              ),
                                            ),
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment
                                                  .spaceBetween,
                                              children: <Widget>[
                                                Padding(
                                                  padding: const EdgeInsets
                                                      .all(16.0),
                                                  child: Text(state
                                                      .superMealData[position]
                                                      .name,
                                                    style: TextStyle(
                                                        fontWeight: FontWeight
                                                            .w500
                                                    ),),
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                      .all(16.0),
                                                  child: Text('${state.superMealData[position].nutrients.calories}cal',
                                                    style: TextStyle(
                                                        fontWeight: FontWeight
                                                            .w500,
                                                        color: kprimaryColour
                                                    ),),
                                                ),
                                              ],
                                            ),
                                          ]
                                      )
                                  ),
                                );
                              }

                          ),
                        ],
                      );

                  }
              ),
              BlocBuilder<HeadacheFighterBloc, HeadacheFighterState>(
                  builder: (context, state) {
                    if (state is HeadacheFighterLoading) {
                      return Column(
                        children: [
                          SizedBox(height: ScreenUtil().setHeight(250),),
                          CircularProgressIndicator(),
                        ],
                      );
                    }
//                    if(state is HeadacheFighterSuccess){
//                      return Stack(
//                        children: [
//                          Container(
//                            margin: EdgeInsets.only(top: 30.0),
//                            child: ListView(
//                              children: [
//                                ListView.builder(
//                                    itemCount: state.superMealData.length,
//                                    scrollDirection: Axis.vertical,
//                                    physics: NeverScrollableScrollPhysics(),
//                                    shrinkWrap: true,
//                                    itemBuilder: (BuildContext context,
//                                        int position) {
//                                      return GestureDetector(
//                                        onTap: () {
//                                          Navigator.push(context,
//                                              MaterialPageRoute(
//                                                builder: (context) =>
//                                                    SuperMealDetailScreen(
//                                                        superMealModel: state
//                                                            .superMealData[position]),
//                                              ));
//                                        },
//                                        child: Card(
//                                            elevation: 3.0,
//                                            margin: new EdgeInsets.symmetric(
//                                                horizontal: 10.0, vertical: 6.0),
//                                            child: Column(
//                                                children: <Widget>[
//                                                  Container(
//                                                    height: 120,
//                                                    decoration: BoxDecoration(
//                                                      borderRadius: BorderRadius
//                                                          .only(
//
//                                                      ),
//                                                      image: DecorationImage(
//                                                        image: NetworkImage(state.superMealData[position].imgSrc),
//                                                        fit: BoxFit.cover,
//                                                      ),
//                                                    ),
//                                                  ),
//                                                  Row(
//                                                    mainAxisAlignment: MainAxisAlignment
//                                                        .spaceBetween,
//                                                    children: <Widget>[
//                                                      Padding(
//                                                        padding: const EdgeInsets
//                                                            .all(16.0),
//                                                        child: Text(state
//                                                            .superMealData[position]
//                                                            .name,
//                                                          style: TextStyle(
//                                                              fontWeight: FontWeight
//                                                                  .w500
//                                                          ),),
//                                                      ),
//                                                      Padding(
//                                                        padding: const EdgeInsets
//                                                            .all(16.0),
//                                                        child: Text(state.superMealData[position].nutrients.calories.toString(),
//                                                          style: TextStyle(
//                                                              fontWeight: FontWeight
//                                                                  .w500,
//                                                              color: kprimaryColour
//                                                          ),),
//                                                      ),
//                                                    ],
//                                                  ),
//                                                ]
//                                            )
//                                        ),
//                                      );
//                                    }
//
//                                ),
//                              ],
//                            ),
//                          ),
//                          GestureDetector(
//                            onTap: () {
//                              setState(() {
//                                showFilter = showFilter == false ? true : false;
//                              });
//                            },
//                            child: Container(
//                              margin: EdgeInsets.all(
//                                  ScreenUtil().setWidth(8.0)),
//                              child: Row(
//                                mainAxisAlignment: MainAxisAlignment.end,
//                                children: [
//                                  Text("Filter",
//                                    style: TextStyle(
//                                        color: kprimaryColour
//                                    ),
//                                  ),
//                                  SvgPicture.asset(
//                                      'assets/images/filter-left.svg')
//                                ],
//                              ),
//                            ),
//                          ),
//                          showFilter ? Positioned(
//                            top: 25,
//                            right: 5,
//                            child: Container(
//                              width: ScreenUtil().setWidth(200),
//                              height: ScreenUtil().setHeight(300),
//                              child: Card(
//                                child: Column(
//                                  children: [
//                                    Row(
//                                      children: [
//                                        Checkbox(
//                                          activeColor: kprimaryColour,
//                                          value: this.vegetables,
//                                          onChanged: (bool value) {
//                                            setState(() {
//                                              this.nuts = false;
//                                              this.spices = false;
//                                              this.fruits = false;
//                                              this.vegetables = value;
//                                            });
//                                            BlocProvider.of<HeadacheFighterBloc>(
//                                                context).add(
//                                                LoadHeadFighterByCategory(
//                                                    group: "60bb2580dcdb8200157cf7ec",
//                                                    category: "60bb2580dcdb8200157cf7e7")
//                                            );
//                                          },
//                                        ),
//                                        Text("Vegetables",
//                                          style: TextStyle(
//                                              color: kprimaryColour
//                                          ),)
//                                      ],
//                                    ),
//                                    Row(
//                                      children: [
//                                        Checkbox(
//                                          activeColor: kprimaryColour,
//                                          value: this.fruits,
//                                          onChanged: (bool value) {
//                                            setState(() {
//                                              this.vegetables = false;
//                                              this.nuts = false;
//                                              this.spices = false;
//                                              this.fruits = value;
//                                            });
//                                            BlocProvider.of<HeadacheFighterBloc>(
//                                                context).add(
//                                                LoadHeadFighterByCategory(
//                                                    group: "60bb2580dcdb8200157cf7ec",
//                                                    category: "60bb2580dcdb8200157cf7e6")
//                                            );
//                                          },
//                                        ),
//                                        Text("Fruits",
//                                          style: TextStyle(
//                                              color: kprimaryColour),
//                                        )
//                                      ],
//                                    ),
//                                    Row(
//                                      children: [
//                                        Checkbox(
//                                          activeColor: kprimaryColour,
//                                          value: this.nuts,
//                                          onChanged: (bool value) {
//                                            setState(() {
//                                              this.nuts = false;
//                                              this.spices = false;
//                                              this.fruits = false;
//                                              this.nuts = value;
//                                            });
//                                            BlocProvider.of<HeadacheFighterBloc>(
//                                                context).add(
//                                                LoadHeadFighterByCategory(
//                                                    group: "60bb2580dcdb8200157cf7ec",
//                                                    category: "60bb2580dcdb8200157cf7e8")
//                                            );
//                                          },
//                                        ),
//                                        Text("Nuts and Seeds", style: TextStyle(
//                                            color: kprimaryColour
//                                        ),)
//                                      ],
//                                    ),
//                                    Row(
//                                      children: [
//                                        Checkbox(
//                                          activeColor: kprimaryColour,
//                                          value: this.spices,
//                                          onChanged: (bool value) {
//                                            setState(() {
//                                              this.spices = value;
//                                              this.nuts = false;
//                                              this.fruits = false;
//                                              this.vegetables = false;
//                                            });
//                                            BlocProvider.of<HeadacheFighterBloc>(
//                                                context).add(
//                                                LoadHeadFighterByCategory(
//                                                    group: "60bb2580dcdb8200157cf7ec",
//                                                    category: "60bb2580dcdb8200157cf7e9")
//                                            );
//                                          },
//                                        ),
//                                        Text("Spices", style: TextStyle(
//                                            color: kprimaryColour
//                                        ),)
//                                      ],
//                                    ),
//                                  ],
//                                ),
//                              ),
//                            ),
//                          ) : SizedBox(),
//                        ],
//                      );
//                    }\
                    return ListView(
                      children: [
                        ListView.builder(
                            itemCount: state.superMealData.length,
                            scrollDirection: Axis.vertical,
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemBuilder: (BuildContext context,
                                int position) {
                              return GestureDetector(
                                onTap: () {
                                  Navigator.push(context,
                                      MaterialPageRoute(
                                        builder: (context) =>
                                            SuperMealDetailScreen(
                                                superMealModel: state
                                                    .superMealData[position]),
                                      ));
                                },
                                child: Card(
                                    elevation: 3.0,
                                    margin: new EdgeInsets.symmetric(
                                        horizontal: 10.0, vertical: 6.0),
                                    child: Column(
                                        children: <Widget>[
                                          Container(
                                            height: 120,
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius
                                                  .only(

                                              ),
                                              image: DecorationImage(
                                                image: NetworkImage(state.superMealData[position].imgSrc),
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment
                                                .spaceBetween,
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets
                                                    .all(16.0),
                                                child: Text(state
                                                    .superMealData[position]
                                                    .name,
                                                  style: TextStyle(
                                                      fontWeight: FontWeight
                                                          .w500
                                                  ),),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets
                                                    .all(16.0),
                                                child: Text('${state.superMealData[position].nutrients.calories}cal',
                                                  style: TextStyle(
                                                      fontWeight: FontWeight
                                                          .w500,
                                                      color: kprimaryColour
                                                  ),),
                                              ),
                                            ],
                                          ),
                                        ]
                                    )
                                ),
                              );
                            }

                        ),
                      ],
                    );
                  }
              ),
              BlocBuilder<AlkalinesBloc, AlkalinesState>(
                  builder: (context, state) {
                    if (state is AlkalinesLoading) {
                      return Column(
                        children: [
                          SizedBox(height: ScreenUtil().setHeight(250),),
                          CircularProgressIndicator(),
                        ],
                      );
                    }
                    return ListView(
                      children: [
                        ListView.builder(
                            itemCount: state.superMealData.length,
                            scrollDirection: Axis.vertical,
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemBuilder: (BuildContext context,
                                int position) {
                              return GestureDetector(
                                onTap: () {
                                  Navigator.push(context,
                                      MaterialPageRoute(
                                        builder: (context) =>
                                            SuperMealDetailScreen(
                                                superMealModel: state
                                                    .superMealData[position]),
                                      ));
                                },
                                child: Card(
                                    elevation: 3.0,
                                    margin: new EdgeInsets.symmetric(
                                        horizontal: 10.0, vertical: 6.0),
                                    child: Column(
                                        children: <Widget>[
                                          Container(
                                            height: 120,
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius
                                                  .only(

                                              ),
                                              image: DecorationImage(
                                                image: NetworkImage(state.superMealData[position].imgSrc),
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment
                                                .spaceBetween,
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets
                                                    .all(16.0),
                                                child: Text(state
                                                    .superMealData[position]
                                                    .name,
                                                  style: TextStyle(
                                                      fontWeight: FontWeight
                                                          .w500
                                                  ),),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets
                                                    .all(16.0),
                                                child: Text('${state.superMealData[position].nutrients.calories}cal',
                                                  style: TextStyle(
                                                      fontWeight: FontWeight
                                                          .w500,
                                                      color: kprimaryColour
                                                  ),),
                                              ),
                                            ],
                                          ),
                                        ]
                                    )
                                ),
                              );
                            }

                        ),
                      ],
                    );
                  }
              ),
              BlocBuilder<ImprovedSexBloc, ImprovedSexState>(
                  builder: (context, state) {
                    if (state is ImprovedSexSLoading) {
                      return Column(
                        children: [
                          SizedBox(height: ScreenUtil().setHeight(250),),
                          CircularProgressIndicator(),
                        ],
                      );
                    }
                    return ListView(
                      children: [
                        ListView.builder(
                            itemCount: state.superMealData.length,
                            scrollDirection: Axis.vertical,
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemBuilder: (BuildContext context,
                                int position) {
                              return GestureDetector(
                                onTap: () {
                                  Navigator.push(context,
                                      MaterialPageRoute(
                                        builder: (context) =>
                                            SuperMealDetailScreen(
                                                superMealModel: state
                                                    .superMealData[position]),
                                      ));
                                },
                                child: Card(
                                    elevation: 3.0,
                                    margin: new EdgeInsets.symmetric(
                                        horizontal: 10.0, vertical: 6.0),
                                    child: Column(
                                        children: <Widget>[
                                          Container(
                                            height: 120,
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius
                                                  .only(

                                              ),
                                              image: DecorationImage(
                                                image: NetworkImage(state.superMealData[position].imgSrc),
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment
                                                .spaceBetween,
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets
                                                    .all(16.0),
                                                child: Text(state
                                                    .superMealData[position]
                                                    .name,
                                                  style: TextStyle(
                                                      fontWeight: FontWeight
                                                          .w500
                                                  ),),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets
                                                    .all(16.0),
                                                child: Text('${state.superMealData[position].nutrients.calories}cal',
                                                  style: TextStyle(
                                                      fontWeight: FontWeight
                                                          .w500,
                                                      color: kprimaryColour
                                                  ),),
                                              ),
                                            ],
                                          ),
                                        ]
                                    )
                                ),
                              );
                            }

                        ),
                      ],
                    );
                  }
              ),
              BlocBuilder<CalciumRichBloc, CalciumRichState>(
                  builder: (context, state) {
                    if (state is CalciumRichLoading) {
                      return Column(
                        children: [
                          SizedBox(height: ScreenUtil().setHeight(250),),
                          CircularProgressIndicator(),
                        ],
                      );
                    }
                    return ListView(
                      children: [
                        ListView.builder(
                            itemCount: state.superMealData.length,
                            scrollDirection: Axis.vertical,
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemBuilder: (BuildContext context,
                                int position) {
                              return GestureDetector(
                                onTap: () {
                                  Navigator.push(context,
                                      MaterialPageRoute(
                                        builder: (context) =>
                                            SuperMealDetailScreen(
                                                superMealModel: state
                                                    .superMealData[position]),
                                      ));
                                },
                                child: Card(
                                    elevation: 3.0,
                                    margin: new EdgeInsets.symmetric(
                                        horizontal: 10.0, vertical: 6.0),
                                    child: Column(
                                        children: <Widget>[
                                          Container(
                                            height: 120,
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius
                                                  .only(

                                              ),
                                              image: DecorationImage(
                                                image: NetworkImage(state.superMealData[position].imgSrc),
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment
                                                .spaceBetween,
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets
                                                    .all(16.0),
                                                child: Text(state
                                                    .superMealData[position]
                                                    .name,
                                                  style: TextStyle(
                                                      fontWeight: FontWeight
                                                          .w500
                                                  ),),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets
                                                    .all(16.0),
                                                child: Text('${state.superMealData[position].nutrients.calories}cal',
                                                  style: TextStyle(
                                                      fontWeight: FontWeight
                                                          .w500,
                                                      color: kprimaryColour
                                                  ),),
                                              ),
                                            ],
                                          ),
                                        ]
                                    )
                                ),
                              );
                            }

                        ),
                      ],
                    );
                  }
              ),
              BlocBuilder<HighFiberBloc, HighFiberState>(
                  builder: (context, state) {
                    if (state is HighFiberLoading) {
                      return Column(
                        children: [
                          SizedBox(height: ScreenUtil().setHeight(250),),
                          CircularProgressIndicator(),
                        ],
                      );
                    }
                    return ListView(
                      children: [
                        ListView.builder(
                            itemCount: state.superMealData.length,
                            scrollDirection: Axis.vertical,
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemBuilder: (BuildContext context,
                                int position) {
                              return GestureDetector(
                                onTap: () {
                                  Navigator.push(context,
                                      MaterialPageRoute(
                                        builder: (context) =>
                                            SuperMealDetailScreen(
                                                superMealModel: state
                                                    .superMealData[position]),
                                      ));
                                },
                                child: Card(
                                    elevation: 3.0,
                                    margin: new EdgeInsets.symmetric(
                                        horizontal: 10.0, vertical: 6.0),
                                    child: Column(
                                        children: <Widget>[
                                          Container(
                                            height: 120,
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius
                                                  .only(

                                              ),
                                              image: DecorationImage(
                                                image: NetworkImage(state.superMealData[position].imgSrc),
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment
                                                .spaceBetween,
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets
                                                    .all(16.0),
                                                child: Text(state
                                                    .superMealData[position]
                                                    .name,
                                                  style: TextStyle(
                                                      fontWeight: FontWeight
                                                          .w500
                                                  ),),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets
                                                    .all(16.0),
                                                child: Text('${state.superMealData[position].nutrients.calories}cal',
                                                  style: TextStyle(
                                                      fontWeight: FontWeight
                                                          .w500,
                                                      color: kprimaryColour
                                                  ),),
                                              ),
                                            ],
                                          ),
                                        ]
                                    )
                                ),
                              );
                            }

                        ),
                      ],
                    );
                  }
              ),
              BlocBuilder<IronRichBloc, IronRichState>(
                  builder: (context, state) {
                    if (state is IronRichLoading) {
                      return Column(
                        children: [
                          SizedBox(height: ScreenUtil().setHeight(250),),
                          CircularProgressIndicator(),
                        ],
                      );
                    }
                    return ListView(
                      children: [
                        ListView.builder(
                            itemCount: 5,
                            scrollDirection: Axis.vertical,
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemBuilder: (BuildContext context,
                                int position) {
                              return GestureDetector(
                                onTap: () {
                                  Navigator.push(context,
                                      MaterialPageRoute(
                                        builder: (context) =>
                                            SuperMealDetailScreen(
                                                superMealModel: state
                                                    .superMealData[position]),
                                      ));
                                },
                                child: Card(
                                    elevation: 3.0,
                                    margin: new EdgeInsets.symmetric(
                                        horizontal: 10.0, vertical: 6.0),
                                    child: Column(
                                        children: <Widget>[
                                          Container(
                                            height: 120,
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius
                                                  .only(

                                              ),
                                              image: DecorationImage(
                                                image: NetworkImage(state.superMealData[position].imgSrc),
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment
                                                .spaceBetween,
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets
                                                    .all(16.0),
                                                child: Text(state
                                                    .superMealData[position]
                                                    .name,
                                                  style: TextStyle(
                                                      fontWeight: FontWeight
                                                          .w500
                                                  ),),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets
                                                    .all(16.0),
                                                child: Text('${state.superMealData[position].nutrients.calories}cal',
                                                  style: TextStyle(
                                                      fontWeight: FontWeight
                                                          .w500,
                                                      color: kprimaryColour
                                                  ),),
                                              ),
                                            ],
                                          ),
                                        ]
                                    )
                                ),
                              );
                            }

                        ),
                      ],
                    );
                  }
              ),
            ],
          ),
        ),
      ),
    );
  }
}
