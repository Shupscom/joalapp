import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:joalapp/components/constants.dart';
class SuperMealFilterScreen extends StatefulWidget {
  @override
  _SuperMealFilterScreenState createState() => _SuperMealFilterScreenState();
}

class _SuperMealFilterScreenState extends State<SuperMealFilterScreen>  with SingleTickerProviderStateMixin {
  int _selectedIndex = 0;
  TabController _tabController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = TabController(length: 4, vsync: this);
    _tabController.addListener(() {
      setState(() {
        _selectedIndex = _tabController.index;
      });
    });
  }
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: ksecondaryColour,
          elevation: 2,
          title: Align(
            alignment: Alignment(-5, 0),
            child: Padding(
              padding: EdgeInsets.only(
                  top: ScreenUtil().setWidth(20.0)
              ),
              child: Text("World Super Meals",
                style: TextStyle(
                    color: kprimaryColour,
                    fontSize: ScreenUtil().setSp(28.0)
                ),
              ),
            ),
          ),
          actions: [
            Padding(
              padding:  EdgeInsets.only(
                top: 20.0,
                right: 20.0
              ),
              child: Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text("Filter",
                      style: TextStyle(
                          color: kprimaryColour
                      ),
                    ),
                    SvgPicture.asset('assets/images/filter-left.svg')
                  ],
                ),
              ),
            ),
          ],
          bottom: TabBar(
            labelColor: kprimaryColour,
            unselectedLabelColor: ksecondaryTextColour,
            isScrollable: true,
            indicatorColor: kprimaryColour,
            tabs: [
              Tab(child: Text("Vegen"),),
              Tab(child: Text("Non-vegen"),),
              Tab(child: Text("Headache fighter"),),
              Tab(child: Text("Alkalines"),),
            ],
          ),
        ),
        body: Container(
          padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
          child: Stack(
            children: [
              Container(
                child: TabBarView(
                  controller: _tabController,
                  children: [
                    SingleChildScrollView(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pushNamed(context, '/singleNuggetScreen');
//                 Navigator.push(context, MaterialPageRoute(builder: (context) => GarmentDetail(garments[position])));
                        },
                        child: ListView.builder(
                            itemCount: 5,
                            scrollDirection: Axis.vertical,
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemBuilder: (BuildContext context, int position) {
                              return Card(
                                  elevation: 3.0,
                                  margin: new EdgeInsets.symmetric(
                                      horizontal: 10.0, vertical: 6.0),
                                  child: Column(
                                      children: <Widget>[
                                        Container(
                                          height: 120,
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.only(

                                            ),
                                            image: DecorationImage(
                                              image: AssetImage('assets/images/nugget_image.png'),
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                        ),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.all(16.0),
                                              child: Text('Brocolli (78g)',
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w500
                                                ),),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.all(16.0),
                                              child: Text('27.5cal',
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w500,
                                                    color: kprimaryColour
                                                ),),
                                            ),
                                          ],
                                        ),
                                      ]
                                  )
                              );
                            }

                        ),
                      ),
                    ),
                    Container(
                      child: Text("Tade"),
                    ),
                    Container(
                      child: Text("Tade"),
                    ),
                    Container(
                      child: Text("Tade"),
                    )

                  ],
                ),
              ),
//              GestureDetector(
//                onTap: (){
//                  Navigator.pushNamed(context, "/superMealFilterScreen");
//                },
//                child: Container(
//                  margin: EdgeInsets.all(ScreenUtil().setWidth(8.0)),
//                  child: Row(
//                    mainAxisAlignment: MainAxisAlignment.end,
//                    children: [
//                      Text("Filter",
//                        style: TextStyle(
//                            color: kprimaryColour
//                        ),
//                      ),
//                      SvgPicture.asset('assets/images/filter-left.svg')
//                    ],
//                  ),
//                ),
//              ),
            ],
          ),
        ),
      ),
    );
  }
}
