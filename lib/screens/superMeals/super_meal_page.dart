import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/nuggets/nugget_event.dart';
import 'package:joalapp/bloc/supermeals/alkalines/alkalines_bloc.dart';
import 'package:joalapp/bloc/supermeals/alkalines/alkalines_event.dart';
import 'package:joalapp/bloc/supermeals/calciumrich/calciumrich_bloc.dart';
import 'package:joalapp/bloc/supermeals/calciumrich/calciumrich_event.dart';
import 'package:joalapp/bloc/supermeals/headache-fighter/headache-fighter_bloc.dart';
import 'package:joalapp/bloc/supermeals/headache-fighter/headache-fighter_event.dart';
import 'package:joalapp/bloc/supermeals/highfiber/highfiber_bloc.dart';
import 'package:joalapp/bloc/supermeals/highfiber/highfiber_event.dart';
import 'package:joalapp/bloc/supermeals/improvedsex/improvedsex_bloc.dart';
import 'package:joalapp/bloc/supermeals/improvedsex/improvedsex_event.dart';
import 'package:joalapp/bloc/supermeals/ironrich/ironnrich_event.dart';
import 'package:joalapp/bloc/supermeals/ironrich/ironrich_bloc.dart';
import 'package:joalapp/bloc/supermeals/non-vegan/non-vegan_bloc.dart';
import 'package:joalapp/bloc/supermeals/non-vegan/non-vegan_event.dart';
import 'package:joalapp/bloc/supermeals/super_meal_bloc.dart';
import 'package:joalapp/bloc/supermeals/super_meal_event.dart';
import 'package:joalapp/screens/superMeals/super_meal_screen.dart';

class SuperMealPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<SuperMealBloc>(
          create: (BuildContext context){
            return SuperMealBloc()..add(LoadVeganButton());
          },
        ),
        BlocProvider<NonVeganBloc>(
          create: (BuildContext context){
            return NonVeganBloc()..add(LoadNonVeganButton());
          },
        ),
        BlocProvider<HeadacheFighterBloc>(
          create: (BuildContext context){
            return HeadacheFighterBloc()..add(LoadHeadAcheFighterButton());
          },
        ),
        BlocProvider<AlkalinesBloc>(
          create: (BuildContext context){
            return AlkalinesBloc()..add(LoadlkalinesButton());
          },
        ),
        BlocProvider<ImprovedSexBloc>(
          create: (BuildContext context){
            return ImprovedSexBloc()..add(LoadImprovedSexButton());
          },
        ),
        BlocProvider<CalciumRichBloc>(
          create: (BuildContext context){
            return CalciumRichBloc()..add(LoadCalciumRichButton());
          },
        ),
        BlocProvider<HighFiberBloc>(
          create: (BuildContext context){
            return HighFiberBloc()..add(LoadHighFiberButton());
          },
        ),
        BlocProvider<IronRichBloc>(
          create: (BuildContext context){
            return IronRichBloc()..add(IronRichButton());
          },
        ),
      ],
      child: SuperMealScreen(),
    );
  }
}