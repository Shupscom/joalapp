import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/gender/gender_bloc.dart';
import 'package:joalapp/bloc/register/register_bloc.dart';
import 'package:joalapp/bloc/register/register_state.dart';
import 'package:joalapp/screens/onboarding/activity_screen.dart';
import 'package:joalapp/screens/onboarding/date_screen.dart';
import 'package:joalapp/screens/onboarding/gender_page.dart';
import 'package:joalapp/screens/onboarding/gender_screen.dart';
import 'package:joalapp/screens/onboarding/email_siginup_screen.dart';
import 'package:joalapp/screens/onboarding/height_screen.dart';
import 'package:joalapp/screens/onboarding/weight_goal_screen.dart';
import 'package:joalapp/screens/onboarding/weight_screen.dart';
import 'package:joalapp/screens/profile/profile_screen.dart';
import 'package:flutter/services.dart';

typedef VoidCallback = void Function();

class RegisterSignUpPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
      return BlocProvider(
          create: (BuildContext context) {
            return RegisterBloc(context);
          },
        child: EmailSignUpScreen()
//        BlocBuilder<RegisterBloc,RegisterState>(
//          builder: (context,state){
//            if(state is RegisterSuccess){
//              print(state.userData);
//              return GenderPage(userData: state.userData);
//            }
//            if(state is EmailSuccess){
//              return GenderPage(
//              userData: state.userData,
//              );
////
//            }
//
//           return EmailSignUpScreen();
//          },
//        ),
      );
  }
}
