import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/recover/passwordcode/password_code_bloc.dart';
import 'package:joalapp/screens/onboarding/password_code_screen.dart';

class PasswordCodePage extends StatelessWidget {
   String userdata;
   PasswordCodePage({@required this.userdata});
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context){
          return PasswordCodeBloc();
        },
        child: PasswordCodeRecovery(userData: userdata,)
    );
  }
}
