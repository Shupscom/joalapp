import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:joalapp/bloc/recover/passwordcode/password_code_bloc.dart';
import 'package:joalapp/bloc/recover/passwordcode/password_code_event.dart';
import 'package:joalapp/bloc/recover/passwordcode/password_code_state.dart';
import 'package:joalapp/components/constants.dart';
import 'package:joalapp/components/linked_text.dart';
import 'package:joalapp/screens/onboarding/new_password_page.dart';
import 'package:pin_entry_text_field/pin_entry_text_field.dart';
import 'package:shared_preferences/shared_preferences.dart';
class PasswordCodeRecovery extends StatefulWidget {
  String userData;
  PasswordCodeRecovery({@required this.userData});
  @override
  _PasswordCodeRecoveryState createState() => _PasswordCodeRecoveryState();
}
class _PasswordCodeRecoveryState extends State<PasswordCodeRecovery> {

  final GlobalKey<FormState> _key = GlobalKey<FormState>();
  final _tokenController = TextEditingController();
  bool tokenEnable = false;
  bool buttonEnable = false;
  bool _autoValidate = false;
  var email;
  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter Valid Email';
    else
      return null;
  }
  void _getUserInfo() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    email = sharedPreferences.getString('user_email');
    print(email);
  }
  @override
  void initState() {
    // TODO: implement initState
    _getUserInfo();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    _onRecoverCodeButtonPressed(){
      if(_key.currentState.validate()){
        BlocProvider.of<PasswordCodeBloc>(context).add(PasswordCodeButton(
            code: int.parse(_tokenController.text),
             email: email
        ));
      }else{
        setState(() {
          _autoValidate = true;
        });
      }
    }
      sizess(context);
      return Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.black),
              onPressed: () => Navigator.of(context).pop(),
            ),
            backgroundColor: ksecondaryColour,
            elevation: 0,
          ),
          body: BlocListener<PasswordCodeBloc,PasswordCodeState>(
            listener: (context, state) {
              if (state is PasswordCodeFailure) {
                Scaffold.of(context).showSnackBar(
                  SnackBar(
                    content: Text('${state.error}'),
                    backgroundColor: Colors.red,
                  ),
                );
              }
              if(state is PasswordCodeSuccess){
                Navigator.push(context, MaterialPageRoute(builder: (context) => NewPasswordPage(userData: state.userData,)));
              }
            },
            child: BlocBuilder<PasswordCodeBloc,PasswordCodeState>(
              builder: (context,state){
                return Container(
                  color: Colors.white,
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  padding: EdgeInsets.all(ScreenUtil().setWidth(16)),
                  child: Form(
                    key: _key,
                    autovalidate: _autoValidate,
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Enter 4 Digits Code",
                            style: TextStyle(
                                fontSize: ScreenUtil().setSp(28),
                                color: kprimaryColour,
                                fontWeight: FontWeight.bold,
                                fontFamily: "Circular Std"
                            ),
                          ),
                          SizedBox(
                            height: ScreenUtil().setHeight(20),
                          ),
                          Text("Enter the 4 digits code we just sent you in your email address",
                            style: TextStyle(
                                color: ksecondaryTextColour,
                                fontSize: ScreenUtil().setWidth(18.0)
                            ),
                          ),
                          SizedBox(
                            height: ScreenUtil().setHeight(70),
                          ),
                          PinEntryTextField(
                            showFieldAsBox: false,
                            fieldWidth: ScreenUtil().setWidth(50.0),
                            onSubmit: (String pin){
                              _tokenController.text = pin;

                            }, // end onSubmit
                          ),
                          SizedBox(
                            height: ScreenUtil().setHeight(70.0),
                          ),
                          Container(
                            child: Material(
                              elevation: 2.0,
                              color: kprimaryColour,
                              borderRadius: BorderRadius.circular(20.0),
                              child: MaterialButton(
                                onPressed:  state is PasswordCodeLoading
                                    ? () {}
                                    : _onRecoverCodeButtonPressed,
                                minWidth: ScreenUtil().setWidth(420.0),
                                height: ScreenUtil().setHeight(42.0),
                                child: state is PasswordCodeLoading ? SpinKitFadingCircle(color: Colors.white) : Text(
                                  "Continue",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: ScreenUtil().setSp(18.0),
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Circular Std',
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: ScreenUtil().setHeight(10.0),),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "If you didn’t receive a code",
                                style: TextStyle(
                                    color: ksecondaryTextColour,
                                    fontWeight: FontWeight.w500,
                                    fontSize: ScreenUtil().setSp(18.0)),
                              ),
                              SizedBox(
                                width: ScreenUtil().setWidth(8.0),
                              ),
                              LinkedText(
                                ontap: () {
//                                  Navigator.pushNamed(context, '/passwordRecovery');
                                },
                                title: "Resend",
                                colour: kprimaryColour,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          )
      );
    }
  }
