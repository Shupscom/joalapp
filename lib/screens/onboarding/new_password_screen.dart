import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/svg.dart';
import 'package:joalapp/bloc/recover/newpassword/newpassword_bloc.dart';
import 'package:joalapp/bloc/recover/newpassword/newpassword_event.dart';
import 'package:joalapp/bloc/recover/newpassword/newpassword_state.dart';
import 'package:joalapp/components/constants.dart';
import 'package:joalapp/screens/onboarding/email_signin_page.dart';
class NewPasswordScreen extends StatefulWidget {
  String userdata;
  NewPasswordScreen({@required this.userdata});
  @override
  _NewPasswordScreenState createState() => _NewPasswordScreenState();
}

class _NewPasswordScreenState extends State<NewPasswordScreen> {
  final GlobalKey<FormState> _loginkey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final _passwordController = TextEditingController();
  final _confirmPasswordController = TextEditingController();
  bool _autoValidate = false;
  bool _showPassword = false;
  bool emailEnable = false;
  bool passwordEnable = false;
  bool buttonEnable = false;
  bool passwordFilled = false;
  bool lengthCheck = null;
  bool letterCheck = null;
  bool numberCheck = null;
  bool symbolCheck = null;
  @override
  Widget build(BuildContext context) {
    _onNewPasswordButtonPressed(){
      if(_loginkey.currentState.validate()){
        if(_passwordController.text == _confirmPasswordController.text){
          BlocProvider.of<NewPasswordBloc>(context).add(NewPasswordButton(
              token: widget.userdata,
              password: _passwordController.text
          ));
        }else{
          scaffoldKey.currentState.showSnackBar(
            SnackBar(
              content: Text('Password mistmatch'),
              backgroundColor: Colors.red,
            ),
          );
        }
      }else{
        setState(() {
          _autoValidate = true;
        });
      }
    }
    sizess(context);
    return BlocListener<NewPasswordBloc, NewPasswordState>(
      listener: (BuildContext context, state) {
        if (state is NewPasswordSuccess) {
          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => EmailSignInPage()));
        }
        if (state is NewPasswordFailure) {
          print(state.error);
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text('${state.error}'),
              backgroundColor: Colors.red,
            ),
          );
        }
      },
      child: BlocBuilder<NewPasswordBloc, NewPasswordState>(
          builder: (context, state) {
            return Scaffold(
              key: scaffoldKey,
              appBar: AppBar(
                leading: IconButton(
                  icon: Icon(Icons.arrow_back, color: Colors.black),
                  onPressed: () => Navigator.pop(context,false),
                ),
                backgroundColor: ksecondaryColour,
                elevation: 0,
              ),
              body: Container(
                color: Colors.white,
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                padding: EdgeInsets.all(ScreenUtil().setWidth(16)),
                child: Form(
                  key: _loginkey,
                  autovalidate: _autoValidate,
                  onChanged: (){
                    setState(() {
                      buttonEnable = passwordFilled == true ? true : false;
                    });
                  },
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Reset Password",
                          style: TextStyle(
                              fontSize: ScreenUtil().setSp(28),
                              color: kprimaryColour,
                              fontWeight: FontWeight.bold,
                              fontFamily: "Circular Std"
                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(20),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(20),
                        ),
                        TextFormField(
                          controller: _passwordController,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Password is required.';
                            }
                            return null;
                          },
                          onTap: () {
                            setState(() {
                              passwordFilled = true;
                            });
                          },
                          onChanged: (value) {
                            if (passwordValidation(value)) {
                              setState(() {
                                passwordEnable = true;
                              });
                            }
                          },
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                          ),
                          decoration: InputDecoration(
                              labelText: 'New Password',
                              labelStyle: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontFamily: 'Circular Std',
                                  color: ksecondaryTextColour
                              ),
                              suffixIcon: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    _showPassword = !_showPassword;
                                  });
                                },
                                child: Icon(
                                  _showPassword
                                      ? Icons.visibility
                                      : Icons.visibility_off,
                                  color: Color(0xFFADA5A5),
                                ),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                  BorderSide(color: ksecondaryTextColour
                                  ))),
                          obscureText: !_showPassword,
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(20),
                        ),
                        passwordFilled == true ? Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                lengthCheck == null ?
                                SvgPicture.asset('assets/images/ellipse_check.svg') :
                                lengthCheck == true ?
                                SvgPicture.asset('assets/images/scheck.svg') :
                                lengthCheck == false ?
                                SvgPicture.asset('assets/images/error_check.svg') : Container(),
                                SizedBox(
                                  width: ScreenUtil().setHeight(20),
                                ),
                                Text("Use 8 or more Characters",
                                  style: TextStyle(
                                      fontSize: 14.0
                                  ),)
                              ],
                            ),
                            SizedBox(
                              height: ScreenUtil().setHeight(10),
                            ),
                            Row(
                              children: <Widget>[
                                letterCheck == null ?
                                SvgPicture.asset('assets/images/ellipse_check.svg') :
                                letterCheck == true ?
                                SvgPicture.asset('assets/images/scheck.svg') :
                                letterCheck == false ?
                                SvgPicture.asset('assets/images/error_check.svg') : Container(),
                                SizedBox(
                                  width: ScreenUtil().setHeight(20),
                                ),
                                Text("Use Uppercase and lowercase letters (e.g Aa)",
                                  style: TextStyle(
                                      fontSize: 14.0
                                  ),)
                              ],
                            ),
                            SizedBox(
                              height: ScreenUtil().setHeight(10),
                            ),
                            Row(
                              children: <Widget>[
                                numberCheck == null ?
                                SvgPicture.asset('assets/images/ellipse_check.svg') :
                                numberCheck == true ?
                                SvgPicture.asset('assets/images/scheck.svg') :
                                numberCheck == false ?
                                SvgPicture.asset('assets/images/error_check.svg') : Container(),
                                SizedBox(
                                  width: ScreenUtil().setHeight(20),
                                ),
                                Text("Use a number (e.g) 1234",
                                  style: TextStyle(
                                      fontSize: 14.0
                                  ),)
                              ],
                            ),
                            SizedBox(
                              height: ScreenUtil().setHeight(10),
                            ),
                            Row(
                              children: <Widget>[
                                symbolCheck == null ?
                                SvgPicture.asset('assets/images/ellipse_check.svg') :
                                symbolCheck == true ?
                                SvgPicture.asset('assets/images/scheck.svg') :
                                symbolCheck == false ?
                                SvgPicture.asset('assets/images/error_check.svg') : Container(),
                                SizedBox(
                                  width: ScreenUtil().setHeight(20),
                                ),
                                Text("Use a symbol (e.g) @#\$",
                                  style: TextStyle(
                                      fontSize: 14.0
                                  ),)
                              ],
                            ),

                          ],
                        ): Container(),
                        SizedBox(
                          height: ScreenUtil().setHeight(10.0),
                        ),
                        TextFormField(
                          controller: _confirmPasswordController,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Password is required.';
                            }
                            return null;
                          },
                          onTap: () {
                            setState(() {
                              passwordFilled = true;
                            });
                          },
                          onChanged: (value) {
                            if (passwordValidation(value)) {
                              setState(() {
                                passwordEnable = true;
                              });
                            }
                          },
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                          ),
                          decoration: InputDecoration(
                              labelText: 'Retype Password',
                              labelStyle: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontFamily: 'Circular Std',
                                  color: ksecondaryTextColour
                              ),
                              suffixIcon: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    _showPassword = !_showPassword;
                                  });
                                },
                                child: Icon(
                                  _showPassword
                                      ? Icons.visibility
                                      : Icons.visibility_off,
                                  color: Color(0xFFADA5A5),
                                ),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                  BorderSide(color: ksecondaryTextColour
                                  ))),
                          obscureText: !_showPassword,
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(70.0),
                        ),
                        Container(
                          child: Material(
                            elevation: 2.0,
                            color: buttonEnable == true ? kprimaryColour : ksecondaryTextColour,
                            borderRadius: BorderRadius.circular(20.0),
                            child: MaterialButton(
                              onPressed: buttonEnable ? state is NewPasswordLoading
                                  ? () {}
                                  : _onNewPasswordButtonPressed : null,
                              minWidth: ScreenUtil().setWidth(420.0),
                              height: ScreenUtil().setHeight(42.0),
                              child: state is NewPasswordLoading
                                  ? SpinKitFadingCircle(color: Colors.white) : Text(
                                "Reset",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: ScreenUtil().setSp(18.0),
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Circular Std',
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          }
      ),
    );
  }
  bool validateStructure(String value){
    String  pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(value);
  }

  bool letterCaseStructure(String value){
    String pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])';
    RegExp regExp = new RegExp(pattern);
    print(regExp.hasMatch(value));
    return regExp.hasMatch(value);
  }
  bool numberCaseStructure(String value){
    String pattern = r'^(?=.*?[0-9])';
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(value);
  }
  bool symbolCaseStructure(String value){
    String pattern = r'^(?=.*?[!@#\$&*~])';
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(value);
  }
  bool passwordValidation(String value){
    bool isCompleted = false;
    bool lengthCompleted;
    bool letterCompleted;
    bool numberCompleted;
    bool symbolCompleted;
    if(value.length < 8){
      setState(() {
        lengthCheck = false;
        lengthCompleted = false;
      });
    }else{
      setState(() {
        lengthCheck = true;
        lengthCompleted = true;
      });
    }
    if(!letterCaseStructure(value)){
      setState(() {
        letterCheck = false;
        letterCompleted = false;
      });
    }else{
      setState(() {
        letterCheck  = true;
        letterCompleted = true;
      });
    }
    if(!numberCaseStructure(value)){
      setState(() {
        numberCheck = false;
        numberCompleted = false;
      });
    }else{
      setState(() {
        numberCheck = true;
        numberCompleted = true;
      });
    }
    if(!symbolCaseStructure(value)){
      setState(() {
        symbolCheck = false;
        symbolCompleted = false;
      });
    }else{
      setState(() {
        symbolCheck = true;
        symbolCompleted = true;
      });
    }
    isCompleted =  lengthCompleted  && letterCompleted  && numberCompleted && symbolCompleted ? true : false;
    return isCompleted;
  }
}
