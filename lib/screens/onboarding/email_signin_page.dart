import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/login/login_bloc.dart';
import 'package:joalapp/screens/onboarding/email_signin_screen.dart';
class EmailSignInPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider<LoginBloc>(
        create: (BuildContext context) => LoginBloc(),
        child: EmailSignInScreen(),
      ),
    );
  }
}
