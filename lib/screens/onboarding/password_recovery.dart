import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:joalapp/bloc/recover/recover_bloc.dart';
import 'package:joalapp/bloc/recover/recover_event.dart';
import 'package:joalapp/bloc/recover/recover_state.dart';
import 'package:joalapp/components/constants.dart';
import 'package:joalapp/components/linked_text.dart';
import 'package:joalapp/screens/onboarding/password_code_page.dart';
import 'package:joalapp/screens/onboarding/password_code_screen.dart';
class PasswordRecoveryScreen extends StatefulWidget {
  @override
  _PasswordRecoveryScreenState createState() => _PasswordRecoveryScreenState();
}

class _PasswordRecoveryScreenState extends State<PasswordRecoveryScreen> {
  final GlobalKey<FormState> _key = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  bool emailEnable = false;
  bool buttonEnable = false;
  bool _autoValidate = false;
  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter Valid Email';
    else
      return null;
  }
  @override
  Widget build(BuildContext context) {
    _onRecoverButtonPressed(){
      if(_key.currentState.validate()){
        BlocProvider.of<RecoverBloc>(context).add(RecoverButton(
            email: _emailController.text
        ));
      }else{
        setState(() {
          _autoValidate = true;
        });
      }
    }

    sizess(context);
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () => Navigator.of(context).pop(),
          ),
          backgroundColor: ksecondaryColour,
          elevation: 0,
        ),
        body: BlocListener<RecoverBloc,RecoverState>(
          listener: (context, state) {
            if (state is RecoverFailure) {
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text('${state.error}'),
                  backgroundColor: Colors.red,
                ),
              );
            }
            if(state is RecoverSuccess){
//              Scaffold.of(context).showSnackBar(
////                SnackBar(
////                  content: Text('${state.userData}'),
////                  backgroundColor: Colors.green,
////                ),
////              );
              Navigator.push(context, MaterialPageRoute(builder: (context) => PasswordCodePage(userdata: state.userData,)));
            }
          },
          child: BlocBuilder<RecoverBloc,RecoverState>(
            builder: (context,state){
              return Container(
                color: Colors.white,
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                padding: EdgeInsets.all(ScreenUtil().setWidth(16)),
                child: Form(
                  key: _key,
                  autovalidate: _autoValidate,
                  onChanged: (){
                    setState(() {
                      buttonEnable = emailEnable == true ? true : false;
                    });
                  },
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Recover password",
                          style: TextStyle(
                              fontSize: ScreenUtil().setSp(28),
                              color: kprimaryColour,
                              fontWeight: FontWeight.bold,
                              fontFamily: "Circular Std"
                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(20),
                        ),
                        Text("Enter your email to recover your password",
                          style: TextStyle(
                              color: ksecondaryTextColour,
                              fontSize: ScreenUtil().setWidth(18.0)
                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(70),
                        ),
                        TextFormField(
                          controller: _emailController,
                          keyboardType: TextInputType.emailAddress,
                          validator: validateEmail,
                          onTap: () {
                            setState(() {
                              emailEnable = true;
                              print(emailEnable);
                            });
//                          emailEnable = true;
                          },

                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w600,
                          ),
                          decoration: InputDecoration(
                              labelText: 'Email',
                              labelStyle: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontFamily: 'Circular Std',
                                  color: ksecondaryTextColour
                              ),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                  BorderSide(color: ksecondaryTextColour
                                  ))),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(70.0),
                        ),
                        Container(
                          child: Material(
                            elevation: 2.0,
                            color: buttonEnable == true ? kprimaryColour : ksecondaryTextColour,
                            borderRadius: BorderRadius.circular(20.0),
                            child: MaterialButton(
                              onPressed: buttonEnable == true ? state is RecoverLoading
                                  ? () {}
                                  : _onRecoverButtonPressed : null,
                              minWidth: ScreenUtil().setWidth(420.0),
                              height: ScreenUtil().setHeight(42.0),
                              child: state is RecoverLoading ? SpinKitFadingCircle(color: Colors.white) : Text(
                                "SEND",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: ScreenUtil().setSp(18.0),
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Circular Std',
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: ScreenUtil().setHeight(10.0),),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "We will send a recovery code to your email",
                              style: TextStyle(
                                  color: ksecondaryTextColour,
                                  fontWeight: FontWeight.w500,
                                  fontSize: ScreenUtil().setSp(18.0)),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        )
    );
  }
}
