import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/height/height_bloc.dart';
import 'package:joalapp/bloc/height/height_state.dart';
import 'package:joalapp/screens/onboarding/height_screen.dart';
import 'package:joalapp/screens/onboarding/weight_page.dart';
class HeightPage extends StatelessWidget {
  Map<String,dynamic> userData;
  HeightPage({this.userData});
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context){
        return HeightBloc();
      },
      child: HeightScreen(userData: userData)
//      BlocBuilder<HeightBloc,HeightState>(
//        builder: (context,state){
//          if(state is HeightSuccess){
//            print(state.userData);
//            return WeightPage(userData: state.userData,);
//          }
//          return HeightScreen(userData: userData );
//        },
//      ),
    );
  }
}
