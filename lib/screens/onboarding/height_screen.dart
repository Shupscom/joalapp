import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:joalapp/bloc/height/height_bloc.dart';
import 'package:joalapp/bloc/height/height_event.dart';
import 'package:joalapp/bloc/height/height_state.dart';
import 'package:joalapp/components/constants.dart';
import 'package:joalapp/screens/onboarding/weight_page.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';
class HeightScreen extends StatefulWidget {
  Map<String,dynamic> userData;
  HeightScreen({
    @required this.userData,
  });
  @override
  _HeightScreenState createState() => _HeightScreenState();
}
class _HeightScreenState extends State<HeightScreen> {
  final GlobalKey<FormState> _heightKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  TextEditingController _heightController = TextEditingController();
  bool buttonEnable = false;
  String unit = "CM";
  @override
  Widget build(BuildContext context) {
    _onHeightButtonPressed(){
      Future.delayed(Duration(seconds: 8));
      if(_heightKey.currentState.validate()){
        BlocProvider.of<HeightBloc>(context).add(HeightButton(
            height: int.parse(_heightController.text),
            heightUnit: unit
        ));
      }else{
        setState(() {
          _autoValidate = true;
        });
      }
    }
    sizess(context);
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () => Navigator.of(context).pop(),
          ),
          backgroundColor: ksecondaryColour,
          elevation: 0,
          title: StepProgressIndicator(
            totalSteps: 6,
            currentStep: 3,
            direction: Axis.horizontal,
            progressDirection: TextDirection.ltr,
            selectedColor: Color(0xFF7AD374),
          ),
        ),
        body: BlocListener<HeightBloc,HeightState>(
          listener: (context, state) {
            if (state is HeightFailure) {
              print(state.error);
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text('${state.error}'),
                  backgroundColor: Colors.red,
                ),
              );
            }
            if(state is HeightSuccess){
              Navigator.push(context, MaterialPageRoute(builder: (context) => WeightPage(userData: state.userData)));
            }
          },
          child: BlocBuilder<HeightBloc,HeightState>(
            builder: (context, state){
              return Container(
                color: Colors.white,
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Form(
                      key: _heightKey,
                      autovalidate: _autoValidate,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            "Height",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: kprimaryColour,
                                fontWeight: FontWeight.bold,
                                fontSize: ScreenUtil().setSp(28.0)),
                          ),
                          SizedBox(
                            height: ScreenUtil().setHeight(200.0),
                          ),
                        ],
                      ),
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: ScreenUtil().setWidth(100),
//                      height: ScreenUtil().setHeight(200),
                            child: TextFormField(
                              controller: _heightController,
                              inputFormatters: <TextInputFormatter>[
                                FilteringTextInputFormatter.digitsOnly
                              ], // Only numbers can be entered
                              keyboardType: TextInputType.number,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                  fontSize: ScreenUtil().setSp(50.0)

                              ),
                              onTap: (){
                                setState(() {
                                  buttonEnable = true;
                                });
                              },
                              decoration: InputDecoration(
                                  hintText: '174',
                                  hintStyle: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'Circular Std',
                                      color: Color.fromRGBO(34, 34, 34, 0.6),
                                      fontSize: ScreenUtil().setSp(50.0)
                                  ),
                                  focusedBorder: UnderlineInputBorder(
                                      borderSide:
                                      BorderSide(color: ksecondaryTextColour
                                      ))),
                            ),
                          ),
                          SizedBox(
                            width: ScreenUtil().setWidth(20.0),
                          ),
                          GestureDetector(
                            onTap: (){
                              setState(() {
                                unit = "CM";
                              });
                            },
                            child: Container(
                              width: ScreenUtil().setWidth(50),
                              height: ScreenUtil().setHeight(50),
                              decoration: BoxDecoration(
                                  color: unit == "CM" ? kprimaryColour : ksecondaryColour,
                                  border: unit != "CM" ? Border.all(color: ksecondaryTextColour, width: 1.0) : null,
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(20),
                                      bottomLeft: Radius.circular(20)
                                  )
                              ),
                              child: Center(
                                child: Text("cm",
                                  style: TextStyle(
                                      color: unit =="CM" ? ksecondaryColour : Colors.black,
                                      fontWeight: FontWeight.bold
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: ScreenUtil().setWidth(20.0),
                          ),
                          GestureDetector(
                            onTap: (){
                              setState(() {
                                unit = "IN";
                              });
                            },
                            child: Container(
                              width: ScreenUtil().setWidth(50),
                              height: ScreenUtil().setHeight(50),
                              decoration: BoxDecoration(
                               color: unit == "IN" ? kprimaryColour : ksecondaryColour,
                                  border: unit != "IN" ? Border.all(color: ksecondaryTextColour, width: 1.0) : null,
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(20),
                                      bottomRight: Radius.circular(20)
                                  )
                              ),
                              child: Center(
                                child: Text("in",
                                  style: TextStyle(
                                      color: unit =="IN" ? ksecondaryColour : Colors.black,
                                      fontWeight: FontWeight.bold
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Positioned(
                      left: 16,
                      right: 16,
                      bottom: 50,
                      child: Container(
                        child: Material(
                          elevation: 2.0,
                          color: buttonEnable == true ? kprimaryColour : ksecondaryTextColour,
                          borderRadius: BorderRadius.circular(20.0),
                          child: MaterialButton(
                            onPressed: buttonEnable == true ? state is HeightLoading
                                ? () {}
                                : _onHeightButtonPressed : null,
                            minWidth: ScreenUtil().setWidth(420.0),
                            height: ScreenUtil().setHeight(42.0),
                            child: state is HeightLoading ? SpinKitFadingCircle(color: Colors.white) : Text(
                              "Next",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: ScreenUtil().setSp(18.0),
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Circular Std',
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
        ),
    );
  }
}
