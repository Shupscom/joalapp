import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/weight/weight_bloc.dart';
import 'package:joalapp/bloc/weight/weight_state.dart';
import 'package:joalapp/screens/onboarding/activity_page.dart';
import 'package:joalapp/screens/onboarding/weight_screen.dart';
class WeightPage extends StatelessWidget {
  Map<String,dynamic> userData;
  WeightPage({this.userData});
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context){
        return WeightBloc();
      },
      child: WeightScreen(userData: userData,)
    );
  }
}
