import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:joalapp/components/constants.dart';
class LocationScreen extends StatefulWidget {
  @override
  _LocationScreenState createState() => _LocationScreenState();
}

class _LocationScreenState extends State<LocationScreen> {
  @override
  Widget build(BuildContext context) {
    sizess(context);
    return Scaffold(
      body: Container(
        color: ksecondaryColour,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          children: [
            SizedBox(height: ScreenUtil().setHeight(200),),
            SvgPicture.asset('assets/images/pana.svg'),
            SizedBox(height: ScreenUtil().setHeight(30),),
            Text("Need your location",
            style: TextStyle(
              fontSize: ScreenUtil().setSp(28.0),
              fontWeight: FontWeight.bold,
              color: kprimaryColour
            ),
            ),
            SizedBox(height: ScreenUtil().setHeight(20),),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Allow App to access your device location to enable it deliver a more efficient service",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: ksecondaryTextColour,
                fontSize: ScreenUtil().setSp(16.0)
              ),
              ),
            ),
            SizedBox(height: ScreenUtil().setHeight(50),),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: ScreenUtil().setHeight(70.0),
                  width: ScreenUtil().setWidth(146.0),
                  padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20.0),
                    color: ksecondaryColour,
                  ),
                  child: Text("Decline",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: kprimaryColour,
                        fontWeight: FontWeight.bold,
                        fontSize: ScreenUtil().setSp(18.0),
                         fontFamily: "Circular Std"
                    ),
                  ),

                ),
                SizedBox(width: ScreenUtil().setWidth(30),),
                Container(
                  height: ScreenUtil().setHeight(70.0),
                  width: ScreenUtil().setWidth(146.0),
                  padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20.0),
                    color: kprimaryColour,
                  ),
                  child: Text("Allow",
                   textAlign: TextAlign.center,
                   style: TextStyle(
                     color: Colors.white,
                     fontWeight: FontWeight.bold,
                     fontSize: ScreenUtil().setSp(18.0)
                   ),
                  ),

                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
