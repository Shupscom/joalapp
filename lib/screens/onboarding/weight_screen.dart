import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:joalapp/bloc/weight/weight_bloc.dart';
import 'package:joalapp/bloc/weight/weight_event.dart';
import 'package:joalapp/bloc/weight/weight_state.dart';
import 'package:joalapp/components/constants.dart';
import 'package:joalapp/screens/onboarding/activity_page.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';
class WeightScreen extends StatefulWidget {
   Map<String, dynamic> userData;

  WeightScreen({this.userData,});
  @override
  _WeightScreenState createState() => _WeightScreenState();
}

class _WeightScreenState extends State<WeightScreen> {
  final GlobalKey<FormState> _weightKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  TextEditingController _weightController = TextEditingController();
  bool buttonEnable = false;
  String unit = "KG";
  @override
  Widget build(BuildContext context) {
    print(unit);
    _onWeightButtonPressed(){
      Future.delayed(Duration(seconds: 8));
      if(_weightKey.currentState.validate()){
        BlocProvider.of<WeightBloc>(context).add(WeightButton(
            weight: int.parse(_weightController.text),
            weightUnit: unit
        ));
      }else{
        setState(() {
          _autoValidate = true;
        });
      }
    }
    sizess(context);
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: ksecondaryColour,
        elevation: 0,
        title: StepProgressIndicator(
          totalSteps: 6,
          currentStep: 4,
          direction: Axis.horizontal,
          progressDirection: TextDirection.ltr,
          selectedColor: Color(0xFF7AD374),
        ),
      ),
      body: BlocListener<WeightBloc,WeightState>(
        listener: (context, state) {
          if (state is WeightFailure) {
            print(state.error);
            Scaffold.of(context).showSnackBar(
              SnackBar(
                content: Text('${state.error}'),
                backgroundColor: Colors.red,
              ),
            );
          }
          if(state is WeightSuccess){
            Navigator.push(context, MaterialPageRoute(builder: (context) => ActivityPage(userData: state.userData)));
          }
        },
        child: BlocBuilder<WeightBloc,WeightState>(
          builder: (context,state){
           return Container(
              color: Colors.white,
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Form(
                   key: _weightKey,
                    autovalidate: _autoValidate,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "Current Weight?",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: kprimaryColour,
                              fontWeight: FontWeight.bold,
                              fontSize: ScreenUtil().setSp(28.0)),
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: ScreenUtil().setWidth(100),
                          child: TextFormField(
                            controller: _weightController,
                            keyboardType: TextInputType.number,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                                fontSize: ScreenUtil().setSp(50.0)
                            ),
                            onTap: (){
                              setState(() {
                                buttonEnable = true;
                              });
                            },
                            decoration: InputDecoration(
                                hintText: '75',
                                hintStyle: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Circular Std',
                                    color: Color.fromRGBO(34, 34, 34, 0.6),
                                    fontSize: ScreenUtil().setSp(50.0)
                                ),
                                focusedBorder: UnderlineInputBorder(
                                    borderSide:
                                    BorderSide(color: ksecondaryTextColour
                                    ))),
                          ),
                        ),
                        SizedBox(
                          width: ScreenUtil().setWidth(20.0),
                        ),
                        GestureDetector(
                          onTap: (){
                            setState(() {
                              unit = "KG";
                            });
                          },
                          child: Container(
                            width: ScreenUtil().setWidth(50),
                            height: ScreenUtil().setHeight(50),
                            decoration: BoxDecoration(
                                color: unit == "KG" ? kprimaryColour : ksecondaryColour,
                                border: unit != "KG" ? Border.all(color: ksecondaryTextColour, width: 1.0) : null,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(20),
                                    bottomLeft: Radius.circular(20)
                                )
                            ),
                            child: Center(
                              child: Text("Kg",
                                style: TextStyle(
                                    color: unit =="KG" ? ksecondaryColour : Colors.black,
                                    fontWeight: FontWeight.bold
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: ScreenUtil().setWidth(20.0),
                        ),
                        GestureDetector(
                          onTap: (){
                            setState(() {
                              unit = "LBS";
                            });
                          },
                          child: Container(
                            width: ScreenUtil().setWidth(50),
                            height: ScreenUtil().setHeight(50),
                            decoration: BoxDecoration(
                              color: unit == "LBS" ? kprimaryColour : ksecondaryColour,
                                border: unit != "LBS" ? Border.all(color: ksecondaryTextColour, width: 1.0) : null,
                                borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(20),
                                    bottomRight: Radius.circular(20)
                                )
                            ),
                            child: Center(
                              child: Text("Lb",
                                style: TextStyle(
                                    color: unit =="LBS" ? ksecondaryColour : Colors.black,
                                    fontWeight: FontWeight.bold
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    left: 16,
                    right: 16,
                    bottom: 50,
                    child: Container(
                      child: Material(
                        elevation: 2.0,
                        color: buttonEnable == true ? kprimaryColour : ksecondaryTextColour,
                        borderRadius: BorderRadius.circular(20.0),
                        child: MaterialButton(
                          onPressed: buttonEnable == true ? state is WeightLoading
                              ? () {}
                               : _onWeightButtonPressed : null,
                          minWidth: ScreenUtil().setWidth(420.0),
                          height: ScreenUtil().setHeight(42.0),
                          child: state is WeightLoading ? SpinKitFadingCircle(color: Colors.white) : Text(
                            "Next",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: ScreenUtil().setSp(18.0),
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Circular Std',
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          }
        ),
      ),
    );
  }
}
