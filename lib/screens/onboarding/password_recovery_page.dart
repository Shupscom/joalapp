import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/recover/recover_bloc.dart';
import 'package:joalapp/screens/onboarding/password_recovery.dart';
class PasswordRecoveryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context){
          return RecoverBloc();
        },
        child: PasswordRecoveryScreen()
    );
  }
}
