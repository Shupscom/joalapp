import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/gender/gender_bloc.dart';
import 'package:joalapp/bloc/gender/gender_state.dart';
import 'package:joalapp/screens/onboarding/gender_screen.dart';

import 'date_page.dart';

class GenderPage extends StatelessWidget {
  Map<String,dynamic> userData;
  GenderPage({this.userData});
  @override
  Widget build(BuildContext context) {
     return BlocProvider(
      create: (BuildContext context){
        return GenderBloc(context);
      },
      child: GenderScreen(userData: userData)

//      BlocBuilder<GenderBloc,GenderState>(
//        builder: (context,state){
//          if(state is GenderSuccess){
//            print(state.userData);
//            return DatePage(userData: state.userData);
//          }
//          return GenderScreen(userData: userData );
//        },
//      ),
    );
  }
}
