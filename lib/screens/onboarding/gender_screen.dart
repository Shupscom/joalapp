import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:joalapp/attachment.dart';
import 'package:joalapp/bloc/gender/gender_bloc.dart';
import 'package:joalapp/bloc/gender/gender_event.dart';
import 'package:joalapp/bloc/gender/gender_state.dart';
import 'package:joalapp/bloc/register/register_bloc.dart';
import 'package:joalapp/bloc/register/register_event.dart';
import 'package:joalapp/bloc/register/register_state.dart';
import 'package:joalapp/components/constants.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

import 'date_page.dart';

class GenderScreen extends StatefulWidget {
  Map<String,dynamic> userData;

  GenderScreen({
   @required this.userData,
   });

  @override
  _GenderScreenState createState() => _GenderScreenState();
}

class _GenderScreenState extends State<GenderScreen> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
//  static final GlobalKey<NavigatorState> attachmentNavKey = GlobalKey<NavigatorState>();
  bool onTappedMale = false;
  bool onTappedFemale = false;
  String gender = null;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _onGenderButtonPressed() {
        Future.delayed(Duration(seconds: 8));
        BlocProvider.of<GenderBloc>(context).add(GenderButton(
            gender: gender
        ));
    }
    sizess(context);
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () => Navigator.of(context).pop()
        ),
        backgroundColor: ksecondaryColour,
        elevation: 0,
        title: StepProgressIndicator(
          totalSteps: 6,
          currentStep: 1,
          direction: Axis.horizontal,
          progressDirection: TextDirection.ltr,
          selectedColor: Color(0xFF7AD374),
        ),
      ),
      body: BlocListener<GenderBloc,GenderState>(
          listener: (context, state) {
            if (state is GenderFailure) {
              print(state.error);
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text('${state.error}'),
                  backgroundColor: Colors.red,
                ),
              );
            }
            if(state is GenderSuccess){
              Navigator.push(context, MaterialPageRoute(builder: (context) => DatePage(userData: state.userData)));
          }

          },
        child: BlocBuilder<GenderBloc,GenderState>(
        builder: (context, state) {
          return Container(
            color: Colors.white,
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Stack(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      "Gender",
                      style: TextStyle(
                          color: kprimaryColour,
                          fontWeight: FontWeight.bold,
                          fontSize: ScreenUtil().setSp(28.0)),
                    ),
                    SizedBox(
                      height: ScreenUtil().setHeight(200.0),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Column(
                          children: [
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  onTappedMale = true;
                                  onTappedFemale = false;
                                  gender = "M";
                                });
                              },
                              child: Container(
                                width: ScreenUtil().setWidth(150.0),
                                height: ScreenUtil().setHeight(175.0),
                                child: Stack(
                                  children: [
                                    Center(
                                      child: SvgPicture.asset('assets/images/male.svg'),
                                    ),
                                    onTappedMale ? Positioned(
                                        right: 16, top: 16,
                                        child: SvgPicture.asset('assets/images/mark.svg')
                                    ): SizedBox(),
                                  ],
                                ),
                                decoration: BoxDecoration(
                                    border: onTappedMale == true
                                        ? Border.all(
                                        color: Color(0xFF6ACB55), width: 2.0)
                                        : Border.all(
                                        color: Colors.white, width: 0.0),
                                    borderRadius: BorderRadius.circular(5.0),
                                    color: Colors.white,
                                    boxShadow: [
                                      BoxShadow(
                                        color: Color.fromRGBO(0, 0, 0, 0.25),
                                        blurRadius: 8,
                                        offset: Offset(0, 2), // Shadow position
                                      ),
                                    ]
                                ),
                              ),
                            ),
                            SizedBox(
                              height: ScreenUtil().setHeight(20.0),
                            ),
                            Text(
                              "Male",
                              style: TextStyle(
                                  color: ksecondaryTextColour,
                                  fontSize: ScreenUtil().setSp(18),
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                        SizedBox(
                          width: ScreenUtil().setWidth(20.0),
                        ),
                        Column(
                          children: [
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  onTappedFemale = true;
                                  onTappedMale = false;
                                  gender = "F";
                                });
                              },
                              child: Container(
                                width: ScreenUtil().setWidth(150.0),
                                height: ScreenUtil().setHeight(175.0),
                                child: Stack(
                                  children: [
                                    Center(
                                      child:
                                      SvgPicture.asset('assets/images/female.svg'),
                                    ),
                                    onTappedFemale ? Positioned(
                                        right: 16, top: 16,
                                        child: SvgPicture.asset('assets/images/mark.svg')
                                    ): SizedBox(),
                                  ],

                                ),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5.0),
                                    border: onTappedFemale == true
                                        ? Border.all(
                                        color: Color(0xFF6ACB55), width: 2.0)
                                        : Border.all(
                                        color: Colors.white, width: 0.0),
                                    color: Colors.white,
                                    boxShadow: [
                                      BoxShadow(
                                        color: Color.fromRGBO(0, 0, 0, 0.25),
                                        blurRadius: 8,
                                        offset: Offset(0, 2), // Shadow position
                                      ),
                                    ]),
                              ),
                            ),
                            SizedBox(
                              height: ScreenUtil().setHeight(20.0),
                            ),
                            Text(
                              "Female",
                              style: TextStyle(
                                  color: ksecondaryTextColour,
                                  fontSize: ScreenUtil().setSp(18),
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                Positioned(
                  left: 16,
                  right: 16,
                  bottom: 50,
                  child: Container(
                    child: Material(
                      elevation: 2.0,
                      color: onTappedFemale || onTappedMale == true ? kprimaryColour : ksecondaryTextColour,
                      borderRadius: BorderRadius.circular(20.0),
                      child: MaterialButton(
                        onPressed: onTappedFemale || onTappedMale == true ? state is GenderLoading
                            ? () {}
                            : _onGenderButtonPressed : null,
                        minWidth: ScreenUtil().setWidth(420.0),
                        height: ScreenUtil().setHeight(42.0),
                        child: state is GenderLoading ? SpinKitFadingCircle(color: Colors.white) : Text(
                          "Next",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: ScreenUtil().setSp(18.0),
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Circular Std',
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        }
        )
      ),
    );
  }
}
