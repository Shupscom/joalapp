import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/activity/activity_bloc.dart';
import 'package:joalapp/bloc/activity/activity_state.dart';
import 'package:joalapp/screens/onboarding/activity_screen.dart';
import 'package:joalapp/screens/onboarding/weight_goal_page.dart';
class ActivityPage extends StatelessWidget {
  Map<String,dynamic> userData;
  ActivityPage({this.userData});
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context){
        return ActivityBloc();
      },
      child: ActivityLevelScreen(userData: userData)
    );
  }
}
