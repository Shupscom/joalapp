import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/wgoal/wgoal_state.dart';
import 'package:joalapp/screens/onboarding/weight_goal_screen.dart';
import 'package:joalapp/screens/profile/profile_screen.dart';
import 'package:flutter/material.dart';
import 'package:joalapp/bloc/wgoal/wgoal_bloc.dart';

class WeightGoalPage extends StatelessWidget {
  Map<String,dynamic> userData;
  WeightGoalPage({this.userData});
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context){
        return WgoalBloc();
      },
      child: BlocBuilder<WgoalBloc,WgoalState>(
        builder: (context,state){
          if(state is WgoalSuccess){
            return ProfileScreen(userData: userData,);
          }
          return WeightGoalScreen(userData: userData );
        },
      ),
    );
  }
}
