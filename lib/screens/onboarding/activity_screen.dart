import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:joalapp/bloc/activity/activity_bloc.dart';
import 'package:joalapp/bloc/activity/activity_event.dart';
import 'package:joalapp/bloc/activity/activity_state.dart';
import 'package:joalapp/components/constants.dart';
import 'package:joalapp/screens/onboarding/weight_goal_page.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

class ActivityLevelScreen extends StatefulWidget {
  Map<String, dynamic> userData;

  ActivityLevelScreen({
    this.userData,
  });

  @override
  _ActivityLevelScreenState createState() => _ActivityLevelScreenState();
}

class _ActivityLevelScreenState extends State<ActivityLevelScreen> {
  bool noActive = false;
  bool moderatelyActive = false;
  bool active = false;
  bool veryActive = false;
  bool buttonEnable = false;
  String activity = null;

  @override
  Widget build(BuildContext context) {
    _onActivityButtonPressed() {
      BlocProvider.of<ActivityBloc>(context)
          .add(ActivityButton(activity: activity));
    }

    sizess(context);
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: ksecondaryColour,
        elevation: 0,
        title: StepProgressIndicator(
          totalSteps: 6,
          currentStep: 5,
          direction: Axis.horizontal,
          progressDirection: TextDirection.ltr,
          selectedColor: Color(0xFF7AD374),
        ),
      ),
      body: BlocListener<ActivityBloc, ActivityState>(
        listener: (context, state) {
          if (state is ActivityFailure) {
            print(state.error);
            Scaffold.of(context).showSnackBar(
              SnackBar(
                content: Text('${state.error}'),
                backgroundColor: Colors.red,
              ),
            );
          }
          if (state is ActivitySuccess) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        WeightGoalPage(userData: state.userData)));
          }
        },
        child:
            BlocBuilder<ActivityBloc, ActivityState>(builder: (context, state) {
          return Container(
            color: Colors.white,
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Stack(
              alignment: Alignment.center,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      "Activity Level",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: kprimaryColour,
//                      fontWeight: FontWeight.bold,
                          fontWeight: FontWeight.w900,
                          fontSize: ScreenUtil().setSp(28.0)),
                    ),
                    SizedBox(
                      height: ScreenUtil().setHeight(50),
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          noActive = true;
                          moderatelyActive = false;
                          active = false;
                          veryActive = false;
                          buttonEnable = true;
                          activity = "NONE";
                        });
                      },
                      child: Container(
                        margin: EdgeInsets.only(
                            left: ScreenUtil().setWidth(16.0),
                            right: ScreenUtil().setWidth(16.0)),
                        padding:
                            EdgeInsets.only(left: ScreenUtil().setWidth(8.0)),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10.0),
                            border: noActive
                                ? Border.all(
                                    color: Color(0xFF6ACB55), width: 2.0)
                                : Border.all(
                                    color: Color(0xFF95AAB0), width: 1.0),
                            boxShadow: [
                              BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.25),
                                blurRadius: 8,
                                offset: Offset(0, 2), // Shadow position
                              ),
                            ]),
                        child: ListTile(
                          leading: Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: SvgPicture.asset(
                                'assets/images/no_activity.svg'),
                          ),
                          title: Text(
                            "Little or no acitivity",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
//                         fontSize: ScreenUtil().setSp(18.0),
                            ),
                          ),
                          subtitle: Text(
                            "I rearly move about much",
                            style: TextStyle(
//                         fontSize: ScreenUtil().setSp(14.0),
                                color: ksecondaryTextColour),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: ScreenUtil().setHeight(10.0),
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          noActive = false;
                          moderatelyActive = true;
                          active = false;
                          veryActive = false;
                          buttonEnable = true;
                          activity = "LESS";
                        });
                      },
                      child: Container(
                        margin: EdgeInsets.only(
                            left: ScreenUtil().setWidth(16.0),
                            right: ScreenUtil().setWidth(16.0)),
                        padding:
                            EdgeInsets.only(left: ScreenUtil().setWidth(8.0)),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10.0),
                            border: moderatelyActive
                                ? Border.all(
                                    color: Color(0xFF6ACB55), width: 2.0)
                                : Border.all(
                                    color: Color(0xFF95AAB0), width: 1.0),
                            boxShadow: [
                              BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.25),
                                blurRadius: 8,
                                offset: Offset(0, 2), // Shadow position
                              ),
                            ]),
                        child: ListTile(
                          leading: Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: SvgPicture.asset(
                                'assets/images/moderately_active.svg'),
                          ),
                          title: Text(
                            "Moderately active",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
//                         fontSize: ScreenUtil().setSp(18.0),
                            ),
                          ),
                          subtitle: Text(
                            "I occassionally workout",
                            style: TextStyle(
//                         fontSize: ScreenUtil().setSp(14.0),
                                color: ksecondaryTextColour),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: ScreenUtil().setHeight(10.0),
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          noActive = false;
                          moderatelyActive = false;
                          active = true;
                          veryActive = false;
                          buttonEnable = true;
                          activity = "ACTIVE";
                        });
                      },
                      child: Container(
                        margin: EdgeInsets.only(
                            left: ScreenUtil().setWidth(16.0),
                            right: ScreenUtil().setWidth(16.0)),
                        padding:
                            EdgeInsets.only(left: ScreenUtil().setWidth(8.0)),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10.0),
                            border: active
                                ? Border.all(
                                    color: Color(0xFF6ACB55), width: 2.0)
                                : Border.all(
                                    color: Color(0xFF95AAB0), width: 1.0),
                            boxShadow: [
                              BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.25),
                                blurRadius: 8,
                                offset: Offset(0, 2), // Shadow position
                              ),
                            ]),
                        child: ListTile(
                          leading: Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: SvgPicture.asset('assets/images/active.svg'),
                          ),
                          title: Text(
                            "Active",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
//                         fontSize: ScreenUtil().setSp(18.0),
                            ),
                          ),
                          subtitle: Text(
                            "I always workout or perform an activities",
                            style: TextStyle(
//                         fontSize: ScreenUtil().setSp(14.0),
                                color: ksecondaryTextColour),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: ScreenUtil().setHeight(10.0),
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          noActive = false;
                          moderatelyActive = false;
                          active = false;
                          veryActive = true;
                          buttonEnable = true;
                          activity = "HIGH";
                        });
                      },
                      child: Container(
                        margin: EdgeInsets.only(
                            left: ScreenUtil().setWidth(16.0),
                            right: ScreenUtil().setWidth(16.0)),
                        padding:
                            EdgeInsets.only(left: ScreenUtil().setWidth(8.0)),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10.0),
                            border: veryActive
                                ? Border.all(
                                    color: Color(0xFF6ACB55), width: 2.0)
                                : Border.all(
                                    color: Color(0xFF95AAB0), width: 1.0),
                            boxShadow: [
                              BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.25),
                                blurRadius: 8,
                                offset: Offset(0, 2), // Shadow position
                              ),
                            ]),
                        child: ListTile(
                          leading: Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: SvgPicture.asset(
                                'assets/images/no_activity.svg'),
                          ),
                          title: Text(
                            "Very Active",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
//                         fontSize: ScreenUtil().setSp(18.0),
                            ),
                          ),
                          subtitle: Text(
                            "I am extremely active",
                            style: TextStyle(
//                         fontSize: ScreenUtil().setSp(14.0),
                                color: ksecondaryTextColour),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Positioned(
                  left: 16,
                  right: 16,
                  bottom: 50,
                  child: Container(
                    child: Material(
                      elevation: 2.0,
                      color: buttonEnable == true
                          ? kprimaryColour
                          : ksecondaryTextColour,
                      borderRadius: BorderRadius.circular(20.0),
                      child: MaterialButton(
                        onPressed: buttonEnable == true
                            ? state is ActivityLoading
                                ? () {}
                                : _onActivityButtonPressed
                            : null,
                        minWidth: ScreenUtil().setWidth(420.0),
                        height: ScreenUtil().setHeight(42.0),
                        child: state is ActivityLoading
                            ? SpinKitFadingCircle(color: Colors.white)
                            : Text(
                                "Next",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: ScreenUtil().setSp(18.0),
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Circular Std',
                                ),
                              ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        }),
      ),
    );
  }
}
