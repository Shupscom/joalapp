import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:joalapp/bloc/register/register_event.dart';
import 'package:joalapp/bloc/weight/weight_state.dart';
import 'package:joalapp/bloc/wgoal/wgoal_bloc.dart';
import 'package:joalapp/bloc/wgoal/wgoal_event.dart';
import 'package:joalapp/bloc/wgoal/wgoal_state.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:joalapp/components/constants.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

class WeightGoalScreen extends StatefulWidget {
  Map<String,dynamic> userData;
  WeightGoalScreen({
    this.userData,
  });
  @override
  _WeightGoalScreenState createState() => _WeightGoalScreenState();
}

class _WeightGoalScreenState extends State<WeightGoalScreen> {
  bool loseWeight = false;
  bool maintainWeight = false;
  bool gainWeight = false;
  String weightGain = null;
  bool buttonEnable = false;
  calculateAge(DateTime birthDate) {
    DateTime currentDate = DateTime.now();
    int age = currentDate.year - birthDate.year;
    int month1 = currentDate.month;
    int month2 = birthDate.month;
    if (month2 > month1) {
      age--;
    } else if (month1 == month2) {
      int day1 = currentDate.day;
      int day2 = birthDate.day;
      if (day2 > day1) {
        age--;
      }
    }
    return age;
  }
  @override
  Widget build(BuildContext context) {
    _onWeightGainButtonPressed(){
      BlocProvider.of<WgoalBloc>(context).add(WgoalButton(
          weightGoal: weightGain
      ));
    }
    sizess(context);
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: ksecondaryColour,
        elevation: 0,
        title: StepProgressIndicator(
          totalSteps: 6,
          currentStep: 6,
          direction: Axis.horizontal,
          progressDirection: TextDirection.ltr,
          selectedColor: Color(0xFF7AD374),
        ),
      ),
      body: BlocListener<WgoalBloc,WgoalState>(
        listener: (context, state) {
          if (state is WgoalFailure) {
            print(state.error);
            Scaffold.of(context).showSnackBar(
              SnackBar(
                content: Text('${state.error}'),
                backgroundColor: Colors.red,
              ),
            );
          }
        },
        child: BlocBuilder<WgoalBloc,WgoalState>(
          builder: (context,state){
            return Container(
              color: Colors.white,
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "Weight Goal",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: kprimaryColour,
                            fontWeight: FontWeight.bold,
                            fontSize: ScreenUtil().setSp(28.0)),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(50),),
                      GestureDetector(
                        onTap: (){
                          setState(() {
                            loseWeight = true;
                            maintainWeight = false;
                            gainWeight = false;
                            buttonEnable = true;
                            weightGain = "GAIN";
                          });
                        },
                        child: Container(
                          margin: EdgeInsets.only(
                              left: ScreenUtil().setWidth(16.0),
                              right: ScreenUtil().setWidth(16.0)
                          ),
                          padding: EdgeInsets.all(ScreenUtil().setWidth(20.0)),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10.0),
                              border: loseWeight ? Border.all(color: Color(0xFF6ACB55), width: 2.0) : Border.all(color: Color(0xFF95AAB0), width: 1.0),
                              boxShadow: [
                                BoxShadow(
                                  color: Color.fromRGBO(0, 0, 0, 0.25),
                                  blurRadius: 8,
                                  offset: Offset(0, 2), // Shadow position
                                ),
                              ]
                          ),
                          child: Center(
                            child:  Text("Gain Weight",
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(18.0),
                                fontWeight: FontWeight.bold,
                              ),

                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(20.0),),
                      GestureDetector(
                        onTap: (){
                          setState(() {
                            loseWeight = false;
                            maintainWeight = true;
                            gainWeight = false;
                            buttonEnable = true;
                            weightGain = "MAINTAIN";

                          });
                        },
                        child: Container(
                          margin: EdgeInsets.only(
                              left: ScreenUtil().setWidth(16.0),
                              right: ScreenUtil().setWidth(16.0)
                          ),
                          padding: EdgeInsets.all(ScreenUtil().setWidth(20.0)),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10.0),
                              border: maintainWeight ? Border.all(color: Color(0xFF6ACB55), width: 2.0) : Border.all(color: Color(0xFF95AAB0), width: 1.0),
                              boxShadow: [
                                BoxShadow(
                                  color: Color.fromRGBO(0, 0, 0, 0.25),
                                  blurRadius: 8,
                                  offset: Offset(0, 2), // Shadow position
                                ),
                              ]
                          ),
                          child: Center(
                            child:  Text("Maintain Weight",
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(18.0),
                                fontWeight: FontWeight.bold,
                              ),

                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(20.0),),
                      GestureDetector(
                        onTap: (){
                          setState(() {
                            loseWeight = false;
                            maintainWeight = false;
                            gainWeight = true;
                            buttonEnable = true;
                            weightGain = "LOSE";
                          });
                        },
                        child: Container(
                          margin: EdgeInsets.only(
                              left: ScreenUtil().setWidth(16.0),
                              right: ScreenUtil().setWidth(16.0)
                          ),
                          padding: EdgeInsets.all(ScreenUtil().setWidth(20.0)),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10.0),
                              border: gainWeight ? Border.all(color: Color(0xFF6ACB55), width: 2.0) : Border.all(color: Color(0xFF95AAB0), width: 1.0),
                              boxShadow: [
                                BoxShadow(
                                  color: Color.fromRGBO(0, 0, 0, 0.25),
                                  blurRadius: 8,
                                  offset: Offset(0, 2), // Shadow position
                                ),
                              ]
                          ),
                          child: Center(
                            child:  Text("Lose Weight",
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(18.0),
                                fontWeight: FontWeight.bold,
                              ),

                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Positioned(
                    left: 16,
                    right: 16,
                    bottom: 50,
                    child: Container(
                      child: Material(
                        elevation: 2.0,
                        color: buttonEnable == true ? kprimaryColour : ksecondaryTextColour,
                        borderRadius: BorderRadius.circular(20.0),
                        child: MaterialButton(
                          onPressed: buttonEnable == true ? state is WgoalLoading
                              ? () {}
                              : _onWeightGainButtonPressed : null,
                          minWidth: ScreenUtil().setWidth(420.0),
                          height: ScreenUtil().setHeight(42.0),
                          child: state is WgoalLoading ? SpinKitFadingCircle(color: Colors.white): Text(
                            "Complete",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: ScreenUtil().setSp(18.0),
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Circular Std',
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
