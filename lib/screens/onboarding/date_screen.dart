import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:joalapp/bloc/date/date_bloc.dart';
import 'package:joalapp/bloc/date/date_event.dart';
import 'package:joalapp/bloc/date/date_state.dart';
import 'package:joalapp/bloc/register/register_bloc.dart';
import 'package:joalapp/bloc/register/register_event.dart';
import 'package:joalapp/bloc/register/register_state.dart';
import 'package:joalapp/components/constants.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';
import 'package:intl/intl.dart';

import '../../attachment.dart';
import 'height_page.dart';
class DateBirthScreen extends StatefulWidget {
   Map<String,dynamic> userData;
  DateBirthScreen({
   this.userData
  });
  @override
  _DateBirthScreenState createState() => _DateBirthScreenState();
}



class _DateBirthScreenState extends State<DateBirthScreen> {
  final format = DateFormat("yyyy-MM-dd");
  final GlobalKey<FormState> _key = GlobalKey<FormState>();
  TextEditingController _dobController = TextEditingController();
  DateTime selectedDate;
  DateTime picked;
  String _dateValue;
  bool buttonEnable = false;
  static String dateConverted(DateTime date, {String format = ""}) {
    if (date == null) {
      return '';
    }
    if (format.isEmpty)
      return DateFormat('EEEE, dd MMMM yyyy, hh:mm a').format(date);
    return DateFormat(format).format(date);
  }
  void _setDate() async {
    DateTime selectedDate = await showDatePicker(
        initialDatePickerMode: DatePickerMode.year,
        initialDate: DateTime.now(),
        firstDate: DateTime(1900),
        lastDate: DateTime.now(),
        context: context);
    _dateValue = selectedDate.toIso8601String();
    _dobController.text = dateConverted(selectedDate, format: 'dd-MMMM-yyyy');
    setState(() {
      buttonEnable = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    _onDateButtonPressed() {
      Future.delayed(Duration(seconds: 8));
      BlocProvider.of<DateBloc>(context).add(DateButton(
          dob: _dobController.text
      ));
    }
    sizess(context);
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () =>  App.attachmentNavKey.currentState.pop()
          ),
          backgroundColor: ksecondaryColour,
          elevation: 0,
          title: StepProgressIndicator(
            totalSteps: 6,
            currentStep: 2,
            direction: Axis.horizontal,
//            progressDirection: TextDirection.,
            selectedColor: Color(0xFF7AD374),
          ),
        ),
        body: BlocListener<DateBloc,DateState>(
          listener: (context, state) {
            if (state is DateFailure) {
              print(state.error);
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text('${state.error}'),
                  backgroundColor: Colors.red,
                ),
              );
            }
            if(state is DateSuccess){
              Navigator.push(context, MaterialPageRoute(builder: (context) => HeightPage(userData: state.userData)));
            }
          },
          child: BlocBuilder<DateBloc,DateState>(
            builder: (context, state){
              return Container(
                color: Colors.white,
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "Date of Birth",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: kprimaryColour,
                              fontWeight: FontWeight.bold,
                              fontSize: ScreenUtil().setSp(28.0)),
                        ),
                        SizedBox(height: ScreenUtil().setHeight(200),),
                        Padding(
                          padding: EdgeInsets.all(ScreenUtil().setWidth(20)),
                          child: TextFormField(
                          controller: _dobController,
                          style: TextStyle(
                            color: Color(0xFF160304),
                            fontWeight: FontWeight.w600,
                          ),
                          textInputAction: TextInputAction.next,
                          onTap: _setDate,
                          readOnly: true,
                          validator: (String text) {
                            if (text.isEmpty) {
                              return "Select date of birth";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                              hintText: "Enter customer's date of birth",
                              labelText: 'Date of Birth',
                              labelStyle: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontFamily: 'Gilroy',
                                  color: Color(0xFF535353)
                              ),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                  BorderSide(color: Color(0xFF666666)))
                          ),
                      ),
                        ),

                      ],
                    ),
                    Positioned(
                      left: 16,
                      right: 16,
                      bottom: 50,
                      child: Container(
                        child: Material(
                          elevation: 2.0,
                          color: buttonEnable == true ? kprimaryColour : ksecondaryTextColour,
                          borderRadius: BorderRadius.circular(20.0),
                          child: MaterialButton(
                            onPressed: buttonEnable == true ? state is DateLoading
                                ? () {}
                                : _onDateButtonPressed : null,
                            minWidth: ScreenUtil().setWidth(420.0),
                            height: ScreenUtil().setHeight(42.0),
                            child: state is DateLoading ? SpinKitFadingCircle(color: Colors.white)
                           : Text(
                              "Next",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: ScreenUtil().setSp(18.0),
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Circular Std',
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              );
            }

          ),
        ),
    );
  }
}


