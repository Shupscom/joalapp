import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/date/date_bloc.dart';
import 'package:joalapp/bloc/date/date_state.dart';
import 'package:joalapp/screens/onboarding/date_screen.dart';
import 'package:joalapp/screens/onboarding/height_page.dart';

class DatePage extends StatelessWidget {
  Map<String,dynamic> userData;
  DatePage({this.userData});
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context){
        return DateBloc();
      },
      child: DateBirthScreen(userData: userData)
//      BlocBuilder<DateBloc,DateState>(
//        builder: (context,state){
//          if(state is DateSuccess){
//            print(state.userData);
//           return HeightPage(userData: userData,);
//          }
//          return
//        },
//      ),
    );
  }
}
