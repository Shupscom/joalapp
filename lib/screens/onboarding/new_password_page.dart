
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/recover/newpassword/newpassword_bloc.dart';
import 'package:joalapp/screens/onboarding/new_password_screen.dart';

class NewPasswordPage extends StatelessWidget {
  String userData;
  NewPasswordPage({@required this.userData});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context){
          return NewPasswordBloc();
        },
        child: NewPasswordScreen(userdata: userData,)
    );
  }
}
