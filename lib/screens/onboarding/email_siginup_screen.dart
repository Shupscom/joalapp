import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/svg.dart';
import 'package:joalapp/bloc/register/register_bloc.dart';
import 'package:joalapp/bloc/register/register_event.dart';
import 'package:joalapp/bloc/register/register_state.dart';
import 'package:joalapp/components/constants.dart';

import 'gender_page.dart';
class EmailSignUpScreen extends StatefulWidget {
  @override
  _EmailSignUpScreenState createState() => _EmailSignUpScreenState();
}

class _EmailSignUpScreenState extends State<EmailSignUpScreen> {
  VoidCallback voidCallback;
  final GlobalKey<FormState> _key = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  bool _autoValidate = false;
  bool _showPassword = false;
  var email, userdata;
  bool emailEnable = false;
  bool passwordEnable = false;
  bool buttonEnable = false;
  bool passwordFilled = false;
  bool lengthCheck = null;
  bool letterCheck = null;
  bool numberCheck = null;
  bool symbolCheck = null;
  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter Valid Email';
    else
      return null;
  }
  @override
  Widget build(BuildContext context) {
    _onRegisterButtonPressed() {
      if (_key.currentState.validate()) {
        Future.delayed(Duration(seconds: 8));
        BlocProvider.of<RegisterBloc>(context).add(EmailButton(
           context: context,
            email: _emailController.text.trim(),
            password: _passwordController.text.trim()
        ));
      } else {
        setState(() {
          _autoValidate = true;
        });
      }
    }
    _onRegisterFacebookButtonPressed() {
        Future.delayed(Duration(seconds: 8));
        BlocProvider.of<RegisterBloc>(context).add(RegisterFacebookButton());
    }
    _onRegisterGoogleButtonPressed() {
      Future.delayed(Duration(seconds: 8));
      BlocProvider.of<RegisterBloc>(context).add(RegisterGoogleButton());
    }


    sizess(context);
    return BlocListener<RegisterBloc,RegisterState>(
          listener: (context, state) {
            if (state is EmailFailure) {
              print(state.error);
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text('${state.error}'),
                  backgroundColor: Colors.red,
                ),
              );
            }
            if(state is EmailSuccess){
              Navigator.push(context, MaterialPageRoute(builder: (context) =>
                  GenderPage(userData: state.userData)));
            }
          },
          child: BlocBuilder<RegisterBloc, RegisterState>(builder: (context, state){
           return Scaffold(
             appBar: AppBar(
               leading: IconButton(
                 icon: Icon(Icons.arrow_back, color: Colors.black),
                 onPressed: () => Navigator.of(context).pop(),
               ),
               backgroundColor: ksecondaryColour,
               elevation: 0,
             ),
             body: Container(
                color: Colors.white,
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                padding: EdgeInsets.all(ScreenUtil().setWidth(16)),
                child: Form(
                  key: _key,
                  autovalidate: _autoValidate,
                  onChanged: (){
                    setState(() {
                      buttonEnable =  emailEnable == true && passwordFilled == true ? true : false;
                    });
                  },
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Sign up",
                          style: TextStyle(
                              fontSize: ScreenUtil().setSp(28),
                              color: kprimaryColour,
                              fontWeight: FontWeight.bold,
                              fontFamily: "Circular Std"
                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(20),
                        ),
                        TextFormField(
                          controller: _emailController,
                          keyboardType: TextInputType.emailAddress,
                          validator: validateEmail,
                          onTap: () {
                            setState(() {
                              emailEnable = true;
                            });
                          },
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w600,
                          ),
                          decoration: InputDecoration(
                              labelText: 'Email',
                              labelStyle: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontFamily: 'Circular Std',
                                  color: ksecondaryTextColour
                              ),
                              // hintText: 'EMAIL',
                              // hintStyle: ,
                              focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                  BorderSide(color: ksecondaryTextColour
                                  ))),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(20),
                        ),
                        TextFormField(
                          controller: _passwordController,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Password is required.';
                            }
                            if(!validateStructure(value)){
                              return 'passwords must have at least one special character';
                            }
                            if(value.length < 8){
                              return 'password must be at least 8 digits long';
                            }
                            print(value);
                            return null;
                          },
                          onTap: () {
                            setState(() {
                              passwordFilled = true;
                            });
                          },
                          onChanged: (value) {
                            print(passwordValidation(value));
                            if (passwordValidation(value)) {
                              setState(() {
                                passwordEnable = true;
                              });
                            }
                          },
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                          ),
                          decoration: InputDecoration(
                              labelText: 'Password',
                              labelStyle: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontFamily: 'Circular Std',
                                  color: ksecondaryTextColour
                              ),
                              suffixIcon: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    _showPassword = !_showPassword;
                                  });
                                },
                                child: Icon(
                                  _showPassword
                                      ? Icons.visibility
                                      : Icons.visibility_off,
                                  color: Color(0xFFADA5A5),
                                ),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                  BorderSide(color: ksecondaryTextColour
                                  ))),
                          obscureText: !_showPassword,
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(20),
                        ),
                        passwordFilled == true ? Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                lengthCheck == null ?
                                SvgPicture.asset('assets/images/ellipse_check.svg') :
                                lengthCheck == true ?
                                SvgPicture.asset('assets/images/scheck.svg') :
                                lengthCheck == false ?
                                SvgPicture.asset('assets/images/error_check.svg') : Container(),
                                SizedBox(
                                  width: ScreenUtil().setHeight(20),
                                ),
                                Text("Use 8 or more Characters",
                                  style: TextStyle(
                                      fontSize: 14.0
                                  ),)
                              ],
                            ),
                            SizedBox(
                              height: ScreenUtil().setHeight(10),
                            ),
                            Row(
                              children: <Widget>[
                                letterCheck == null ?
                                SvgPicture.asset('assets/images/ellipse_check.svg') :
                                letterCheck == true ?
                                SvgPicture.asset('assets/images/scheck.svg') :
                                letterCheck == false ?
                                SvgPicture.asset('assets/images/error_check.svg') : Container(),
                                SizedBox(
                                  width: ScreenUtil().setHeight(20),
                                ),
                                Text("Use Uppercase and lowercase letters (e.g Aa)",
                                  style: TextStyle(
                                      fontSize: 14.0
                                  ),)
                              ],
                            ),
                            SizedBox(
                              height: ScreenUtil().setHeight(10),
                            ),
                            Row(
                              children: <Widget>[
                                numberCheck == null ?
                                SvgPicture.asset('assets/images/ellipse_check.svg') :
                                numberCheck == true ?
                                SvgPicture.asset('assets/images/scheck.svg') :
                                numberCheck == false ?
                                SvgPicture.asset('assets/images/error_check.svg') : Container(),
                                SizedBox(
                                  width: ScreenUtil().setHeight(20),
                                ),
                                Text("Use a number (e.g) 1234",
                                  style: TextStyle(
                                      fontSize: 14.0
                                  ),)
                              ],
                            ),
                            SizedBox(
                              height: ScreenUtil().setHeight(10),
                            ),
                            Row(
                              children: <Widget>[
                                symbolCheck == null ?
                                SvgPicture.asset('assets/images/ellipse_check.svg') :
                                symbolCheck == true ?
                                SvgPicture.asset('assets/images/scheck.svg') :
                                symbolCheck == false ?
                                SvgPicture.asset('assets/images/error_check.svg') : Container(),
                                SizedBox(
                                  width: ScreenUtil().setHeight(20),
                                ),
                                Text("Use a symbol (e.g) @#\$",
                                  style: TextStyle(
                                      fontSize: 14.0
                                  ),)
                              ],
                            ),

                          ],
                        ): Container(),
                        SizedBox(
                          height: ScreenUtil().setHeight(70.0),
                        ),
                        Container(
                          child: Material(
                            elevation: 2.0,
                            color: buttonEnable == true ? kprimaryColour : ksecondaryTextColour,
                            borderRadius: BorderRadius.circular(20.0),
                            child: MaterialButton(
                              onPressed:
//                                  (){
//                                Navigator.push(context,
//                                    MaterialPageRoute(builder: (context) => GenderPage(userData: userdata)));
//                              },
                              buttonEnable ? state is EmailLoading
                                  ? () {}
                                  : _onRegisterButtonPressed : null,
//
                              minWidth: ScreenUtil().setWidth(420.0),
                              height: ScreenUtil().setHeight(42.0),
                              child:
                              state is EmailLoading
                                  ? SpinKitFadingCircle(color: Colors.white)
                                  : Text(
                                "SIGN UP",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: ScreenUtil().setSp(18.0),
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Circular Std',
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(70.0),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SvgPicture.asset('assets/images/Line.svg'),
                            SizedBox(
                              width: ScreenUtil().setWidth(8.0),
                            ),
                            Text("OR SIGN UP WITH",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Color(0xFF666A86),
                                  fontSize:  ScreenUtil().setSp(20.0),
                                  fontFamily: "Circular Std"
                              ),
                            ),
                            SizedBox(
                              width: ScreenUtil().setWidth(8.0),
                            ),
                            SvgPicture.asset('assets/images/Line.svg'),
                          ],
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(20.0),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            GestureDetector(
                              onTap: (){
                                _onRegisterFacebookButtonPressed();
                              },
                              child: Container(
                                  decoration: BoxDecoration(
                                      boxShadow: [
                                        BoxShadow(
                                            blurRadius: 8,
                                            offset: Offset(0, 15),
                                            color: Colors.grey.withOpacity(.1),
                                            spreadRadius: -9)
                                      ]
                                  ),
                                  child: SvgPicture.asset('assets/images/facebook.svg')),
                            ),
                            SizedBox(width: ScreenUtil().setWidth(30),),
                            GestureDetector(
                              onTap: (){
                                _onRegisterGoogleButtonPressed();
                              },
                              child: Container(
                                  decoration: BoxDecoration(
                                      boxShadow: [
                                        BoxShadow(
                                            blurRadius: 8,
                                            offset: Offset(0, 15),
                                            color: Colors.grey.withOpacity(.1),
                                            spreadRadius: -9)
                                      ]
                                  ),
                                  child: SvgPicture.asset('assets/images/google.svg')),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
           );
          }

          ),
        );
  }
  bool validateStructure(String value){
    String  pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(value);
  }

  bool letterCaseStructure(String value){
    String pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])';
    RegExp regExp = new RegExp(pattern);
    print(regExp.hasMatch(value));
    return regExp.hasMatch(value);
  }
  bool numberCaseStructure(String value){
    String pattern = r'^(?=.*?[0-9])';
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(value);
  }
  bool symbolCaseStructure(String value){
    String pattern = r'^(?=.*?[!@#\$&*~])';
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(value);
  }
  bool passwordValidation(String value){
    bool isCompleted = false;
    bool lengthCompleted;
    bool letterCompleted;
    bool numberCompleted;
    bool symbolCompleted;
    if(value.length < 8){
      setState(() {
        lengthCheck = false;
        lengthCompleted = false;
      });
    }else{
      setState(() {
        lengthCheck = true;
        lengthCompleted = true;
      });
    }
    if(!letterCaseStructure(value)){
      setState(() {
        letterCheck = false;
        letterCompleted = false;
      });
    }else{
      setState(() {
        letterCheck  = true;
        letterCompleted = true;
      });
    }
    if(!numberCaseStructure(value)){
      setState(() {
        numberCheck = false;
        numberCompleted = false;
      });
    }else{
      setState(() {
        numberCheck = true;
        numberCompleted = true;
      });
    }
    if(!symbolCaseStructure(value)){
      print(validateStructure(value));
      setState(() {
        symbolCheck = false;
        symbolCompleted = false;
      });
    }else{
      setState(() {
        symbolCheck = true;
        symbolCompleted = true;
      });
    }
    isCompleted =  lengthCompleted  && letterCompleted  && numberCompleted && symbolCompleted ? true : false;
    return isCompleted;
  }
}

