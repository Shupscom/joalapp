import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:http/http.dart';
import 'package:image_picker/image_picker.dart';
import 'package:joalapp/components/constants.dart';
import 'package:joalapp/model/nugget_model.dart';
import 'package:joalapp/utils/flutter_tts.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share_plus/share_plus.dart';

class SingleNuggetScreen extends StatefulWidget {
  final NuggetModel nuggetModel;
  SingleNuggetScreen({@required this.nuggetModel});
  @override
  _SingleNuggetScreenState createState() => _SingleNuggetScreenState();
}
enum TtsState { playing, stopped, paused, continued }

class _SingleNuggetScreenState extends State<SingleNuggetScreen> {
  String text;
  String subject;
  List<String> imagePaths = [];

  FlutterTts flutterTts = FlutterTts();
  bool showBtn = false;
  String language;
  String engine;
  double volume = 0.5;
  double pitch = 1.0;
  double rate = 0.5;
  bool isCurrentLanguageInstalled = false;

  TtsState ttsState = TtsState.stopped;

  get isPlaying => ttsState == TtsState.playing;
  get isStopped => ttsState == TtsState.stopped;
  get isPaused => ttsState == TtsState.paused;
  get isContinued => ttsState == TtsState.continued;

  bool get isIOS => !kIsWeb && Platform.isIOS;
  bool get isAndroid => !kIsWeb && Platform.isAndroid;
  bool get isWeb => kIsWeb;

  _speak(String text) async{
    print(await flutterTts.getLanguages);
    await flutterTts.setLanguage('en-US');
    await flutterTts.setPitch(1);
    await flutterTts.speak(text);
  }

  initTts() {
    flutterTts = FlutterTts();

    if (isAndroid) {
      _getDefaultEngine();
    }

    flutterTts.setStartHandler(() {
      setState(() {
        print("Playing");
        ttsState = TtsState.playing;
      });
    });

    flutterTts.setCompletionHandler(() {
      setState(() {
        print("Complete");
        ttsState = TtsState.stopped;
      });
    });

    flutterTts.setCancelHandler(() {
      setState(() {
        print("Cancel");
        ttsState = TtsState.stopped;
      });
    });

    if (isWeb || isIOS) {
      flutterTts.setPauseHandler(() {
        setState(() {
          print("Paused");
          ttsState = TtsState.paused;
        });
      });

      flutterTts.setContinueHandler(() {
        setState(() {
          print("Continued");
          ttsState = TtsState.continued;
        });
      });
    }

    flutterTts.setErrorHandler((msg) {
      setState(() {
        print("error: $msg");
        ttsState = TtsState.stopped;
      });
    });
  }
  Future<dynamic> _getLanguages() => flutterTts.getLanguages;

  Future<dynamic> _getEngines() => flutterTts.getEngines;

  Future _getDefaultEngine() async {
    var engine = await flutterTts.getDefaultEngine;
    if (engine != null) {
      print(engine);
    }
  }
  Future _stop() async {
    var result = await flutterTts.stop();
    if (result == 1) setState(() => ttsState = TtsState.stopped);
  }

  Future _pause() async {
    var result = await flutterTts.pause();
    if (result == 1) setState(() => ttsState = TtsState.paused);
  }

  @override
  void dispose() {
    super.dispose();
    flutterTts.stop();
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initTts();
    setState(() {
      text = widget.nuggetModel.body;
      subject = widget.nuggetModel.body;
      final pathFile = widget.nuggetModel.imgSrc;
//      if(pathFile != null){
//       setState(() {
//         imagePaths.add(pathFile);
//       });
//      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: ksecondaryColour,
        elevation: 0,
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: GestureDetector(
              onTap: text.isEmpty && imagePaths.isEmpty
                  ? null
                  : () => _onShare(context),
                child: SvgPicture.asset('assets/images/share.svg'))
          ),
        ],
//        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Container(
          color: ksecondaryColour,
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(ScreenUtil().setWidth(16)),
                  height: ScreenUtil().setHeight(300.0),
                  width: ScreenUtil().setWidth(500),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(100),
                      topRight: Radius.circular(100)
                    ),
                  ),
//                  child: Image.asset(
//                    "assets/images/nugget_image.png",
//                    fit: BoxFit.fill,
//                  ),
                   child: Image.network(widget.nuggetModel.imgSrc,
                     fit: BoxFit.fill,
                   ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                    left: ScreenUtil().setWidth(16.0),
                    right: ScreenUtil().setWidth(16.0),
                    top: ScreenUtil().setWidth(16.0),
                    bottom: 0
                  ),
                  child: Text(widget.nuggetModel.body,
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(16.0),
                        color: Colors.black),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomRight,
                    child: GestureDetector(
                      onTap: (){
                          setState(() {
                              showBtn = showBtn == false ? true : false;
//                            _speak(widget.nuggetModel.body);
                          });
                        },
                      child: Container(
                          decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: Color.fromRGBO(0, 0, 0, 0.25),
                                  blurRadius: 100,
                                  offset: Offset(0, 0), // Shadow position
                                ),
                              ]
                          ),
                          child: SvgPicture.asset('assets/images/listen_icon.svg')
                      ),
                    )
                ),
               showBtn ? _btnSection() : SizedBox()
              ]
          ),
        ),
      ),
    );
  }

  Widget _btnSection() {
    if (isAndroid) {
      return Container(
//          padding: EdgeInsets.only(top: 50.0),
          child:
          Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
            _buildButtonColumn(Colors.green, Colors.greenAccent,
                Icons.play_arrow, 'PLAY', _speak),
            _buildButtonColumn(
                Colors.red, Colors.redAccent, Icons.stop, 'STOP', _stop),
          ]));
    } else {
      return Container(
//          padding: EdgeInsets.only(top: 50.0),
          child:
          Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
            _buildButtonColumn(Colors.green, Colors.greenAccent,
                Icons.play_arrow, 'PLAY', _speak),
            _buildButtonColumn(
                Colors.red, Colors.redAccent, Icons.stop, 'STOP', _stop),
            _buildButtonColumn(
                Colors.blue, Colors.blueAccent, Icons.pause, 'PAUSE', _pause),
          ]));
    }
  }
  Column _buildButtonColumn(Color color, Color splashColor, IconData icon,
      String label, Function func) {
    return Column(
//        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          IconButton(
              icon: Icon(icon),
              color: color,
              splashColor: splashColor,
              onPressed: () => _speak(widget.nuggetModel.body)),
          Container(
              margin: const EdgeInsets.only(top: 8.0),
              child: Text(label,
                  style: TextStyle(
                      fontSize: 12.0,
                      fontWeight: FontWeight.w400,
                      color: color)))
        ]);
  }
  void _onShare(BuildContext context) async {

    final box = context.findRenderObject() as RenderBox;

    if (imagePaths.isNotEmpty) {
//      final RenderBox box = context.findRenderObject();
//      var url = widget.nuggetModel.imgSrc;
//      var response = await get(url);
//      final documentDirectory = (await getExternalStorageDirectory()).path;
//      File imgFile = new File('$documentDirectory/flutter.png');
//      imgFile.writeAsBytesSync(response.bodyBytes);
      await Share.shareFiles(imagePaths,
          text: text,
          subject: subject,
          sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
    } else {
      await Share.share(text,
          subject: subject,
          sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
    }
  }
}
