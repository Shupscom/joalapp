import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:joalapp/bloc/nuggets/exercise/exercise_bloc.dart';
import 'package:joalapp/bloc/nuggets/exercise/exercise_event.dart';
import 'package:joalapp/bloc/nuggets/exercise/exercise_state.dart';
import 'package:joalapp/bloc/nuggets/fruits/fruits_bloc.dart';
import 'package:joalapp/bloc/nuggets/fruits/fruits_event.dart';
import 'package:joalapp/bloc/nuggets/fruits/fruits_state.dart';
import 'package:joalapp/bloc/nuggets/nugget_bloc.dart';
import 'package:joalapp/bloc/nuggets/nugget_event.dart';
import 'package:joalapp/bloc/nuggets/nugget_state.dart';
import 'package:joalapp/bloc/nuggets/nutritious/nutritious_bloc.dart';
import 'package:joalapp/bloc/nuggets/nutritious/nutritious_event.dart';
import 'package:joalapp/bloc/nuggets/nutritious/nutritious_state.dart';
import 'package:joalapp/screens/nuggets/single_nuggets_screen.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:joalapp/components/constants.dart';
import 'package:joalapp/components/make_bottom.dart';
import 'package:shared_preferences/shared_preferences.dart';

@JsonSerializable()
class AllNuggetsScreen extends StatefulWidget {
  @override
  _AllNuggetsScreenState createState() => _AllNuggetsScreenState();
}

class _AllNuggetsScreenState extends State<AllNuggetsScreen>
    with SingleTickerProviderStateMixin {
  int _selectedIndex = 0;
  TabController _tabController;
  var userdata;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getUserInfo();
    _tabController = TabController(length: 4, vsync: this);
    _tabController.addListener(() {
      setState(() {
        _selectedIndex = _tabController.index;
      });
    });
  }
  void _getUserInfo() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var data = sharedPreferences.get("userdata");
    setState(() {
      userdata = json.decode(data);
      print(userdata);
    });
  }

  @override
  Widget build(BuildContext context) {
    sizess(context);
    return DefaultTabController(
      length: 4,
//      initialIndex: 0,
      child: Scaffold(
          bottomNavigationBar: MakeBottom(
            active: 'diary',
            userData: userdata,
          ),
          body: Container(
            color: ksecondaryColour,
            width: MediaQuery.of(context).size.width,
//            height: MediaQuery.of(context).size.height,
            padding: EdgeInsets.only(
              left: ScreenUtil().setWidth(16.0),
              right: ScreenUtil().setWidth(16.0)

            ),
            child: SingleChildScrollView(
              child: Column(
//                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    children: [
                      SizedBox(
                        height: ScreenUtil().setHeight(50),
                      ),
                    ],
                  ),
                  Text(
                    "Health Nuggets",
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(30.0),
                        color: kprimaryColour,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: ScreenUtil().setHeight(10),
                  ),
                  Container(
                    child: TabBar(
                      onTap: (tabIndex) {
                        switch (tabIndex) {
                          case 0:
                            NuggetBloc()..add(LoadButton());
                            break;
                          case 1:
                             FruitsBloc()..add(LoadButtonFruits());
                            break;
                          case 2:
                          NutritiousBloc()..add(LoadButtonNutritious());
                            break;
                          case 3:
                          ExerciseBloc()..add(LoadButtonExercise());
                            break;
                        }
                      },
                      isScrollable: true,
                      controller: _tabController,
                      labelColor: kprimaryColour,
                      unselectedLabelColor: Colors.black,
                      indicator: BoxDecoration(),
                      tabs: [
                        Tab(
                          child: Text(
                            'General',
                          ),
                        ),
                        Tab(
                          child: Text(
                            'Fruits',
                          ),
                        ),
                        Tab(
                          child: Text(
                            'Nutritious food',
                          ),
                        ),
                        Tab(
                          child: Text(
                            'Physical Exercise',
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                  height: MediaQuery.of(context).size.height * 0.70,
                    child: TabBarView(
                      controller: _tabController,
                      children: [
                        BlocBuilder<NuggetBloc, NuggetState>(
                          builder: (context, state) {
                            if (state is NuggetLoading) {
                              return Column(
                                children: [
                                  SizedBox(height: ScreenUtil().setHeight(250),),
                                  CircularProgressIndicator(),
                                ],
                              );
                            }
                            print("length is" + state.nuggetData.length.toString());
                            return ListView(
                              children: [
                                ListView.builder(
                                    itemCount: state.nuggetData.length,
                                    scrollDirection: Axis.vertical,
                                    physics:
                                    NeverScrollableScrollPhysics(),
                                    shrinkWrap: true,
                                    itemBuilder: (BuildContext context,
                                        int position) {
                                      return GestureDetector(
                                        onTap: (){
                                          Navigator.push(context, MaterialPageRoute(builder: (context) =>
                                              SingleNuggetScreen(nuggetModel: state.nuggetData[position],
                                              )));
                                        },
                                        child: Card(
                                            elevation: 3.0,
                                            margin: new EdgeInsets.symmetric(
                                                horizontal: 10.0,
                                                vertical: 6.0),
                                            child: Column(children: <Widget>[
                                              Container(
                                                height: 120,
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                  BorderRadius.only(),
                                                  image: DecorationImage(
                                                    image:
                                                    NetworkImage(state.nuggetData[position].imgSrc),
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                              ),
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: <Widget>[
                                                  Expanded(
                                                    child: Padding(
                                                      padding:
                                                      const EdgeInsets
                                                          .all(16.0),
                                                      child: Text(state.nuggetData[position].body,
                                                        overflow: TextOverflow.ellipsis,
                                                        maxLines: 3,
                                                        style: TextStyle(
                                                            fontWeight: FontWeight
                                                                .w500),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ])),
                                      );
                                    })
                              ],
                            );
                          },
                        ),
                        BlocBuilder<FruitsBloc, FruitsState>(
                          builder: (context, state) {
                            if (state is FruitsLoading) {
                              return Center(
                                child: CircularProgressIndicator(),
                              );
                            }
                            print("length is" + state.nuggetData.length.toString());
                            return ListView(
                              children: [
                                ListView.builder(
                                    itemCount: state.nuggetData.length,
                                    scrollDirection: Axis.vertical,
                                    physics:
                                    NeverScrollableScrollPhysics(),
                                    shrinkWrap: true,
                                    itemBuilder: (BuildContext context,
                                        int position) {
                                      return GestureDetector(
                                        onTap: (){
                                          Navigator.push(context, MaterialPageRoute(builder: (context) =>
                                              SingleNuggetScreen(nuggetModel: state.nuggetData[position],
                                              )));
                                        },
                                        child: Card(
                                            elevation: 3.0,
                                            margin: new EdgeInsets.symmetric(
                                                horizontal: 10.0,
                                                vertical: 6.0),
                                            child: Column(children: <Widget>[
                                              Container(
                                                height: 120,
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                  BorderRadius.only(),
                                                  image: DecorationImage(
                                                    image:
                                                    NetworkImage(state.nuggetData[position].imgSrc),
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                              ),
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: <Widget>[
                                                  Expanded(
                                                    child: Padding(
                                                      padding:
                                                      const EdgeInsets
                                                          .all(16.0),
                                                      child: Text(state.nuggetData[position].body,
                                                        overflow: TextOverflow.ellipsis,
                                                        maxLines: 3,
                                                        style: TextStyle(
                                                            fontWeight: FontWeight
                                                                .w500),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ])),
                                      );
                                    })
                              ],
                            );
                          },
                        ),
                        BlocBuilder<NutritiousBloc, NutritiousState>(
                          builder: (context, state) {
                            if (state is NutritiousLoading) {
                              return Center(
                                child: CircularProgressIndicator(),
                              );
                            }
                            return ListView(
                              children: [
                                ListView.builder(
                                    itemCount: state.nuggetData.length,
                                    scrollDirection: Axis.vertical,
                                    physics:
                                    NeverScrollableScrollPhysics(),
                                    shrinkWrap: true,
                                    itemBuilder: (BuildContext context,
                                        int position) {
                                      return GestureDetector(
                                        onTap: (){
                                          Navigator.push(context, MaterialPageRoute(builder: (context) =>
                                              SingleNuggetScreen(nuggetModel: state.nuggetData[position],
                                              )));
                                        },
                                        child: Card(
                                            elevation: 3.0,
                                            margin: new EdgeInsets.symmetric(
                                                horizontal: 10.0,
                                                vertical: 6.0),
                                            child: Column(children: <Widget>[
                                              Container(
                                                height: 120,
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                  BorderRadius.only(),
                                                  image: DecorationImage(
                                                    image:
                                                    NetworkImage(state.nuggetData[position].imgSrc),
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                              ),
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: <Widget>[
                                                  Expanded(
                                                    child: Padding(
                                                      padding:
                                                      const EdgeInsets
                                                          .all(16.0),
                                                      child: Text(state.nuggetData[position].body,
                                                        overflow: TextOverflow.ellipsis,
                                                        maxLines: 3,
                                                        style: TextStyle(
                                                            fontWeight:
                                                            FontWeight
                                                                .w500),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ])),
                                      );
                                    })
                              ],
                            );
                          },
                        ),
                        BlocBuilder<ExerciseBloc, ExerciseState>(
                          builder: (context, state) {
                            if (state is ExerciseLoading) {
                              return Center(
                                child: CircularProgressIndicator(),
                              );
                            }
                            return ListView(
                              children: [
                                ListView.builder(
                                    itemCount: state.nuggetData.length,
                                    scrollDirection: Axis.vertical,
                                    physics:
                                    NeverScrollableScrollPhysics(),
                                    shrinkWrap: true,
                                    itemBuilder: (BuildContext context,
                                        int position) {
                                      return GestureDetector(
                                        onTap: (){
                                          Navigator.push(context, MaterialPageRoute(builder: (context) =>
                                              SingleNuggetScreen(nuggetModel: state.nuggetData[position],
                                              )));
                                        },
                                        child: Card(
                                            elevation: 3.0,
                                            margin: new EdgeInsets.symmetric(
                                                horizontal: 10.0,
                                                vertical: 6.0),
                                            child: Column(children: <Widget>[
                                              Container(
                                                height: 120,
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                  BorderRadius.only(),
                                                  image: DecorationImage(
                                                    image:
                                                    NetworkImage(state.nuggetData[position].imgSrc),
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                              ),
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: <Widget>[
                                                  Expanded(
                                                    child: Padding(
                                                      padding:
                                                      const EdgeInsets
                                                          .all(16.0),
                                                      child: Text(state.nuggetData[position].body,
                                                        overflow: TextOverflow.ellipsis,
                                                        maxLines: 3,
                                                        style: TextStyle(
                                                            fontWeight:
                                                            FontWeight
                                                                .w500),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ])),
                                      );
                                    })
                              ],
                            );
                          },
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          )),
    );
  }
}
