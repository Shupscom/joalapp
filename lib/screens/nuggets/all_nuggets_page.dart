import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/nuggets/exercise/exercise_bloc.dart';
import 'package:joalapp/bloc/nuggets/exercise/exercise_event.dart';
import 'package:joalapp/bloc/nuggets/fruits/fruits_bloc.dart';
import 'package:joalapp/bloc/nuggets/nugget_bloc.dart';
import 'package:joalapp/bloc/nuggets/nutritious/nutritious_bloc.dart';
import 'package:joalapp/bloc/nuggets/nutritious/nutritious_event.dart';
import 'package:joalapp/screens/nuggets/all_nuggets_screen.dart';
import 'package:joalapp/bloc/nuggets/nugget_event.dart';
import 'package:joalapp/bloc/nuggets/fruits/fruits_event.dart';

class AllNuggetsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
      return MultiBlocProvider(
        providers: [
          BlocProvider<NuggetBloc>(
              create: (BuildContext context){
                return NuggetBloc()..add(LoadButton());
              },
          ),
          BlocProvider<FruitsBloc>(
            create: (BuildContext context){
              return FruitsBloc()..add(LoadButtonFruits());
            },
          ),
          BlocProvider<NutritiousBloc>(
            create: (BuildContext context){
              return NutritiousBloc()..add(LoadButtonNutritious());
            },
          ),
          BlocProvider<ExerciseBloc>(
            create: (BuildContext context){
              return ExerciseBloc()..add(LoadButtonExercise());
            },
          ),
        ],
       child: AllNuggetsScreen(),
      );
    }
  }
