import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:joalapp/components/constants.dart';
import 'package:joalapp/model/payment_model.dart';
import 'package:joalapp/screens/profile/profile_screen.dart';

class PaymentSucessfulScreen extends StatefulWidget {
  Map<String, dynamic> paymentModel;

  PaymentSucessfulScreen({@required this.paymentModel});

  @override
  _PaymentSucessfulScreenState createState() => _PaymentSucessfulScreenState();
}

class _PaymentSucessfulScreenState extends State<PaymentSucessfulScreen> {
  @override
  Widget build(BuildContext context) {
    sizess(context);
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: ksecondaryColour,
        elevation: 0,
      ),
      body: Container(
          color: ksecondaryColour,
          width: MediaQuery.of(context).size.width,
//        height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.only(
              left: ScreenUtil().setWidth(20.0),
              right: ScreenUtil().setWidth(20.0),
              bottom: ScreenUtil().setWidth(8.0)),
//            top: ScreenUtil().setWidth(16.0)),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
//            SizedBox(height: ScreenUtil().setHeight(10),),
                Text(
                  "Payment Successful",
                  style: TextStyle(
                      fontSize: ScreenUtil().setSp(18.0),
                      fontWeight: FontWeight.bold,
                      color: kprimaryColour),
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(30),
                ),
                Center(
                  child: Container(
                    width: ScreenUtil().setWidth(350),
                    height: ScreenUtil().setHeight(500),
                    decoration: BoxDecoration(color: Colors.white, boxShadow: [
                      BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.25),
                        blurRadius: 20,
                        offset: Offset(0, 2), // Shadow position
                      ),
                    ]),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: ScreenUtil().setHeight(20),
                        ),
                        Text(
                          "Thank You!",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: ScreenUtil().setSp(16.0),
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(10),
                        ),
                        Text(
                          "Your Tansaction was successful",
                          style: TextStyle(color: ksecondaryTextColour),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(10),
                        ),
                        SvgPicture.asset("assets/images/receipt_divider.svg"),
                        Padding(
                          padding: EdgeInsets.only(
                              left: ScreenUtil().setWidth(16.0),
                              right: ScreenUtil().setWidth(16.0),
                              top: ScreenUtil().setWidth(16.0),
                              bottom: ScreenUtil().setWidth(0.0)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Date",
                                  style: TextStyle(
                                    color: ksecondaryTextColour,
                                  )),
                              Text("Time",
                                  style: TextStyle(
                                    color: ksecondaryTextColour,
                                  )),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                            left: ScreenUtil().setWidth(16.0),
                            right: ScreenUtil().setWidth(16.0),
                            top: ScreenUtil().setWidth(16.0),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("02/05/2021",
                                  style: TextStyle(
                                    color: Colors.black,
                                  )),
                              Text("9:28 AM",
                                  style: TextStyle(
                                    color: Colors.black,
                                  )),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(10),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: ScreenUtil().setWidth(16.0),
                              right: ScreenUtil().setWidth(16.0),
                              top: ScreenUtil().setWidth(16.0),
                              bottom: ScreenUtil().setWidth(0.0)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Plan",
                                  style: TextStyle(
                                    color: ksecondaryTextColour,
                                  )),
                              Text("Amount",
                                  style: TextStyle(
                                    color: ksecondaryTextColour,
                                  )),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                            left: ScreenUtil().setWidth(16.0),
                            right: ScreenUtil().setWidth(16.0),
                            top: ScreenUtil().setWidth(16.0),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Standard Plan",
                                  style: TextStyle(
                                    color: Colors.black,
                                  )),
                              Text("\$ 39.27",
                                  style: TextStyle(
                                    color: Colors.black,
                                  )),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(10),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: ScreenUtil().setWidth(16.0),
                              right: ScreenUtil().setWidth(16.0),
                              top: ScreenUtil().setWidth(16.0),
                              bottom: ScreenUtil().setWidth(0.0)),
                          child: Row(
                            children: [
                              Text("By",
                                  style: TextStyle(
                                    color: ksecondaryTextColour,
                                  )),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                            left: ScreenUtil().setWidth(16.0),
                            right: ScreenUtil().setWidth(16.0),
                            top: ScreenUtil().setWidth(16.0),
                          ),
                          child: Row(
                            children: [
                              Text(widget.paymentModel['user']['firstName'],
                                  style: TextStyle(
                                    color: Colors.black,
                                  )),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(20),
                        ),
                        SvgPicture.asset("assets/images/card_details.svg"),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(80),
                ),
                Container(
                  child: Material(
                    elevation: 2.0,
                    color: kprimaryColour,
                    borderRadius: BorderRadius.circular(20.0),
                    child: MaterialButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ProfileScreen(
                                    userData: widget.paymentModel['user'])));
                      },
                      minWidth: ScreenUtil().setWidth(420.0),
                      height: ScreenUtil().setHeight(42.0),
                      child: Text(
                        "Go To Profile",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: ScreenUtil().setSp(18.0),
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Circular Std',
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
