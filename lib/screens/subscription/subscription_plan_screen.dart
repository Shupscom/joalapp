import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:joalapp/bloc/payment/payment_bloc.dart';
import 'package:joalapp/bloc/payment/payment_event.dart';
import 'package:joalapp/bloc/payment/payment_state.dart';
import 'package:joalapp/components/constants.dart';
import 'package:joalapp/model/subscription_model.dart';
import 'package:intl/intl.dart';
import 'package:joalapp/screens/subscription/payment_successful_screen.dart';
import 'package:joalapp/utils/card_utils.dart';
import 'package:joalapp/utils/input_formatters.dart';

class SubscriptionPlanScreeen extends StatefulWidget {
  SubscriptionModel  subscriptionModel;
  SubscriptionPlanScreeen({@required this.subscriptionModel});
  @override
  _SubscriptionPlanScreeenState createState() =>
      _SubscriptionPlanScreeenState();
}

class _SubscriptionPlanScreeenState extends State<SubscriptionPlanScreeen> {
  final GlobalKey<FormState> _key = GlobalKey<FormState>();
  TextEditingController _cardNumberController = TextEditingController();
  TextEditingController _cvvController = TextEditingController();
  TextEditingController _dateController = TextEditingController();
  bool cvvEnable = false;
  bool cardNumberEnable = false;
  bool dateEnable = false;
  bool buttonEnable = false;
  @override
  Widget build(BuildContext context) {
    _onPaymentButtonPressed(){
      List<int> expiryDate = CardUtils.getExpiryDate(_dateController.text);
      print(expiryDate[0]);
      print(expiryDate[1]);
      if (_key.currentState.validate()) {

        BlocProvider.of<PaymentBloc>(context).add(LoadPaymentButton(
        currency: "60cbab1ac27c4f0015e054f4",
        plan: widget.subscriptionModel.sId,
        channel: "Card",
        cardNumber: CardUtils.getCleanedNumber(_cardNumberController.text),
        cvv: _cvvController.text,
        expiryMonth: expiryDate[0] ,
        expiryYear: expiryDate[1],
        ));
      } else {
//        setState(() {
//          _autoValidate = true;
//        });
      }
    }

    sizess(context);
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: ksecondaryColour,
        elevation: 0,
      ),
      body: BlocListener<PaymentBloc, PaymentState>(
        listener: (context, state) {
          if (state is PaymentFailure) {
            print(state.error);
            Scaffold.of(context).showSnackBar(
              SnackBar(
                content: Text('${state.error}'),
                backgroundColor: Colors.red,
              ),
            );
          }
          if (state is PaymentSuccess) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => PaymentSucessfulScreen(paymentModel: state.paymentModel,)));
          }
        },
        child: BlocBuilder<PaymentBloc,PaymentState>(
           builder: (context,state){
             return SingleChildScrollView(
               child: Container(
                 color: ksecondaryColour,
                 width: MediaQuery.of(context).size.width,
                 padding: EdgeInsets.only(
                     left: ScreenUtil().setWidth(20.0),
                     right: ScreenUtil().setWidth(20.0),
                     bottom: ScreenUtil().setWidth(16.0)
                 ),
                 child:  Form(
                   key: _key,
                   onChanged: (){
                     setState(() {
                       buttonEnable = cvvEnable == true && cardNumberEnable == true && dateEnable == true ? true : false;
                     });
                   },
                   child: Column(
                     crossAxisAlignment: CrossAxisAlignment.start,
                     children: [
//            SizedBox(height: ScreenUtil().setHeight(10),),
                       Text(
                         widget.subscriptionModel.name,
                         style: TextStyle(
                             fontSize: ScreenUtil().setSp(24.0),
                             fontWeight: FontWeight.bold,
                             color: kprimaryColour),
                       ),
                       SizedBox(
                         height: ScreenUtil().setHeight(20),
                       ),
                       Center(child: SvgPicture.asset('assets/images/credit_card.svg')),
                       SizedBox(
                         height: ScreenUtil().setHeight(20),
                       ),
                       Row(
                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                         children: [
                           Text(widget.subscriptionModel.name,
                             style: TextStyle(
                                 color: ksecondaryTextColour,
                                 fontSize: ScreenUtil().setSp(18.0)),
                           ),
                           Text(
                             "\$ ${widget.subscriptionModel.prices[0].amount}",
                             style: TextStyle(
                                 color: kprimaryColour,
                                 fontSize: ScreenUtil().setSp(24.0),
                                 fontWeight: FontWeight.bold),
                           )
                         ],
                       ),
                       Text(
                         "1year (45% off the Growth Package)",
                         style: TextStyle(
                             color: ksecondaryTextColour,
                             fontSize: ScreenUtil().setSp(16.0)),
                       ),
                       SizedBox(
                         height: ScreenUtil().setHeight(40),
                       ),
                       Text(
                         "Payment Details",
                         style: TextStyle(
                             color: Colors.black, fontSize: ScreenUtil().setSp(18.0)),
                       ),
                       SizedBox(
                         height: ScreenUtil().setHeight(20),
                       ),
                       Row(
                         children: [
                           Container(
                             padding: EdgeInsets.all(8),
                             decoration: BoxDecoration(
                                 borderRadius: BorderRadius.circular(10),
                                 color: Colors.white,
                                 border: Border.all(color: kprimaryColour, width: 1.0),
                                 boxShadow: [
                                   BoxShadow(
                                     color: Color.fromRGBO(0, 0, 0, 0.25),
                                     blurRadius: 20,
                                     offset: Offset(0, 2), // Shadow position
                                   ),
                                 ]),
                             width: ScreenUtil().setWidth(100.0),
                             height: ScreenUtil().setHeight(60.0),
                             child: Center(child: Text("Bank Card")),
                           ),
                           SizedBox(
                             width: ScreenUtil().setWidth(10),
                           ),
                           Container(
                             padding: EdgeInsets.all(ScreenUtil().setWidth(8.0)),
                             decoration: BoxDecoration(
                                 borderRadius: BorderRadius.circular(10),
                                 color: Colors.white,
                                 boxShadow: [
                                   BoxShadow(
                                     color: Color.fromRGBO(0, 0, 0, 0.25),
                                     blurRadius: 20,
                                     offset: Offset(0, 2), // Shadow position
                                   ),
                                 ]),
                             width: ScreenUtil().setWidth(120.0),
                             height: ScreenUtil().setHeight(60.0),
                             child: SvgPicture.asset('assets/images/flutterwave.svg'),
                           )
                         ],
                       ),
                       SizedBox(
                         height: ScreenUtil().setHeight(20),
                       ),
                       TextFormField(
                           controller: _cardNumberController,
                           keyboardType: TextInputType.number,
                           inputFormatters:
                           [ WhitelistingTextInputFormatter.digitsOnly,
                             LengthLimitingTextInputFormatter(19),
                             CardNumberInputFormatter()
                           ],
                           onTap: (){
                             setState(() {
                               cardNumberEnable = true;
                             });
                           },
                           validator: CardUtils.validateCardNum,
                           style: TextStyle(color: Colors.black),
                           decoration: kTextFieldDecoration.copyWith(
                               hintText: "Card Number",
                               hintStyle: TextStyle(
                                 color: ksecondaryTextColour,
                               )
                           )
                       ),
                       SizedBox(
                         height: ScreenUtil().setHeight(20),
                       ),
                       Row(
                         children: [
                           Expanded(
                             child: TextFormField(
                                 controller: _dateController,
                                 keyboardType: TextInputType.number,
                                 inputFormatters:
                                 [ WhitelistingTextInputFormatter.digitsOnly,
                                   LengthLimitingTextInputFormatter(4),
                                   CardMonthInputFormatter()
                                 ],
                                onTap: (){
                                   setState(() {
                                     dateEnable = true;
                                   });
                                },
                                validator: CardUtils.validateDate,
                                 style: TextStyle(color: Colors.black),
                                 decoration: kTextFieldDecoration.copyWith(
                                     hintText: 'MM/YY',
                                     hintStyle: TextStyle(
                                       color: ksecondaryTextColour,
                                     )
                                 )
                             ),
                           ),
                           SizedBox(
                             width: ScreenUtil().setHeight(10),
                           ),
                           Expanded(
                             child: TextFormField(
                             controller: _cvvController,
                                 keyboardType: TextInputType.number,
                                 onTap: (){
                                 setState(() {
                                   cvvEnable = true;
                                 });
                                 },
                                 inputFormatters:
                                 [ WhitelistingTextInputFormatter.digitsOnly,
                                   LengthLimitingTextInputFormatter(3) ],
                                  validator: CardUtils.validateCVV,
                                  style: TextStyle(color: Colors.black),
                                    decoration: kTextFieldDecoration.copyWith(
                                     hintText: "Cvv",
                                     hintStyle: TextStyle(
                                       color: ksecondaryTextColour,
                                     )
                                 )
                             ),
                           ),
                         ],
                       ),
                       SizedBox(
                         height: ScreenUtil().setHeight(20),
                       ),
                       Container(
                         child: Material(
                           elevation: 2.0,
                           color: buttonEnable ? kprimaryColour : ksecondaryTextColour,
                           borderRadius: BorderRadius.circular(20.0),
                           child: MaterialButton(
                              onPressed:  buttonEnable ? state is PaymentLoading
                                 ? () {}
                                 : _onPaymentButtonPressed : null,
                             minWidth: ScreenUtil().setWidth(420.0),
                             height: ScreenUtil().setHeight(42.0),
                             child: state is PaymentLoading ? SpinKitFadingCircle(color: Colors.white) : Text(
                               "Confirm Payment",
                               style: TextStyle(
                                 color: Colors.white,
                                 fontSize: ScreenUtil().setSp(18.0),
                                 fontWeight: FontWeight.bold,
                                 fontFamily: 'Circular Std',
                               ),
                             ),
                           ),
                         ),
                       ),
                     ],
                   ),
                 ),
               ),
             );
           },
        ),
      ),
    );
  }
}
