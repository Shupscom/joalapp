import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/payment/payment_bloc.dart';
import 'package:joalapp/model/subscription_model.dart';
import 'package:joalapp/screens/subscription/subscription_plan_screen.dart';

class SubscriptionPlanPage extends StatelessWidget {
  SubscriptionModel  subscriptionModel;
  SubscriptionPlanPage({@required this.subscriptionModel});
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context){
          return PaymentBloc();
        },
        child: SubscriptionPlanScreeen(subscriptionModel: subscriptionModel)
    );
  }
  }

