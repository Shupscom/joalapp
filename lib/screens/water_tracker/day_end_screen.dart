import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:joalapp/components/constants.dart';
class DayEndQuestionScreen extends StatefulWidget {
  @override
  _DayEndQuestionScreenState createState() => _DayEndQuestionScreenState();
}

class _DayEndQuestionScreenState extends State<DayEndQuestionScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: ksecondaryColour,
        width: MediaQuery.of(context).size.width,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: ScreenUtil().setHeight(100),),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text("Did you complete the minimum standard water in-take today?",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: ScreenUtil().setSp(18.0)
                    ),
                  ),
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(40),
                ),
                Image.asset("assets/images/long_cup.jpg")
              ],
            ),
            Positioned(
              left: 16,
              right: 16,
              bottom: 70,
              child: Column(
                children: [
                  Container(
                    child: Material(
                      elevation: 2.0,
                      color: kprimaryColour,
                      borderRadius: BorderRadius.circular(20.0),
                      child: MaterialButton(
                        onPressed: () {
                          Navigator.pushNamed(context, '/heightScreen');
                        },
                        minWidth: ScreenUtil().setWidth(420.0),
                        height: ScreenUtil().setHeight(42.0),
                        child: Text(
                          "Yes",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: ScreenUtil().setSp(18.0),
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Circular Std',
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(50),),
                  Container(
                    child: Material(
                      elevation: 2.0,
                      color: Colors.white,
//                      borderRadius: BorderRadius.circular(20.0),
                      shape: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20.0,),
                          borderSide: BorderSide(
                              color: kprimaryColour
                          )
                      ),
                      child: MaterialButton(
                        onPressed: () {
                          Navigator.pushNamed(context, '/heightScreen');
                        },
                        minWidth: ScreenUtil().setWidth(420.0),
                        height: ScreenUtil().setHeight(42.0),
                        child: Text(
                          "No",
                          style: TextStyle(
                            color: kprimaryColour,
                            fontSize: ScreenUtil().setSp(18.0),
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Circular Std',
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),

          ],
        ),
      ),
    );
  }
}
