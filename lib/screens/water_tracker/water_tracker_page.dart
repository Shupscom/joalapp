import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/reminder/reminder_bloc.dart';
import 'package:joalapp/bloc/reminder/reminder_event.dart';
import 'package:joalapp/screens/water_tracker/water_tracker_screen.dart';
class WaterTrackerPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ReminderBloc>(
          create: (BuildContext context){
            return ReminderBloc()..add(LoadReminderButton());
          },
        ),
//        BlocProvider<FruitsBloc>(
//          create: (BuildContext context){
//            return FruitsBloc()..add(LoadButtonFruits());
//          },
//        ),
//        BlocProvider<NutritiousBloc>(
//          create: (BuildContext context){
//            return NutritiousBloc()..add(LoadButtonNutritious());
//          },
//        ),
//        BlocProvider<ExerciseBloc>(
//          create: (BuildContext context){
//            return ExerciseBloc()..add(LoadButtonExercise());
//          },
//        ),
      ],
      child: WaterTrackerScreen(),
    );
  }
}
