import 'dart:convert';

import 'package:custom_timer/custom_timer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:joalapp/bloc/reminder/reminder_bloc.dart';
import 'package:joalapp/bloc/reminder/reminder_challenge/reminder_challenge_bloc.dart';
import 'package:joalapp/bloc/reminder/reminder_challenge/reminder_challenge_event.dart';
import 'package:joalapp/bloc/reminder/reminder_challenge/reminder_challenge_state.dart';
import 'package:joalapp/bloc/reminder/reminder_event.dart';
import 'package:joalapp/bloc/reminder/reminder_meal/reminder_meal_bloc.dart';
import 'package:joalapp/bloc/reminder/reminder_meal/reminder_meal_event.dart';
import 'package:joalapp/bloc/reminder/reminder_meal/reminder_meal_state.dart';
import 'package:joalapp/bloc/reminder/reminder_water/reminder_water_bloc.dart';
import 'package:joalapp/bloc/reminder/reminder_water/reminder_water_event.dart';
import 'package:joalapp/bloc/reminder/reminder_water/reminder_water_state.dart';
import 'package:joalapp/components/constants.dart';
import 'package:joalapp/model/self_challenge_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
class SetReminderScreen extends StatefulWidget {
  @override
  _SetReminderScreenState createState() => _SetReminderScreenState();
}

class _SetReminderScreenState extends State<SetReminderScreen> with SingleTickerProviderStateMixin{
  int _selectedIndex = 0;
  TabController _tabController;
  var userdata;
  final CustomTimerController _controller = new CustomTimerController();
  DateTime latestTime = DateTime.now();
  String _time;
  int _hour;
  int _minutes;
  String _mealtime;
  int _mealhour;
  int _mealminutes;
  bool showEveryweek = false;
  bool showMealEveryweek = false;
  String _value;
  String _mealValue;
  String type = "WATER";
  List<int> days = [];
  List<int> daysMeal = [];
  bool showMonday = false;
  bool showTuesday = false;
  bool showWednesday = false;
  bool showThursday = false;
  bool showFriday = false;
  bool showSaturday = false;
  bool showSunday = false;
  bool showMealMonday = false;
  bool showMealTuesday = false;
  bool showMealWednesday = false;
  bool showMealThursday = false;
  bool showMealFriday = false;
  bool showMealSaturday = false;
  bool showMealSunday = false;
  bool showChallenge = false;
  var challengedata;
  var dataCheck;
  List<SelfChallengeModel> dataChallenge = [];
  List<bool> _isChecked;
  String _dateValue;
  List<String> challengeGroup = [];
  List<TextEditingController> _startController = [];
  List<TextEditingController>  _endController = [];


  final format = DateFormat("yyyy-MM-dd");
  static String dateConverted(DateTime date, {String format = ""}) {
    if (date == null) {
      return '';
    }
    if (format.isEmpty)
      return DateFormat('EEEE, dd MMMM yyyy, hh:mm a').format(date);
    return DateFormat(format).format(date);
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getUserInfo();
    _time = '${latestTime.hour}:${latestTime.minute}';
    _mealtime = '${latestTime.hour}:${latestTime.minute}';
    _hour = latestTime.hour;
    _minutes = latestTime.minute;
    _mealhour = latestTime.hour;
    _mealminutes = latestTime.minute;
    _tabController = TabController(length: 3, vsync: this);
    _tabController.addListener(() {
      setState(() {
        _selectedIndex = _tabController.index;
        _isChecked = List<bool>.filled(dataCheck.length, false);
      });
    });
  }
  void _getUserInfo() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var data = sharedPreferences.get("userdata");
    var cdata = sharedPreferences.get("challengedata");
    setState(() {
      userdata = json.decode(data);
      print(userdata);
      challengedata = json.decode(cdata);
      print(challengedata);
      dataCheck = challengedata['data'];
    });
  }
  @override
  Widget build(BuildContext context) {
    _onReminderWaterButtonPressed() {
        BlocProvider.of<ReminderWaterBloc>(context).add(ReminderWaterButton(
          type:  type,
          hour: _hour,
          minute: _minutes,
            days: days,
            frequency: int.parse(_value)
        ));
    }
    _onReminderMealButtonPressed() {
      print(daysMeal);
      BlocProvider.of<ReminderMealBloc>(context).add(ReminderMealButton(
          type: type,
          hour: _mealhour,
          minute: _mealminutes,
          days: daysMeal,
          meal: _mealValue
      ));
    }
    _onReminderChallengeButtonPressed() {

      for (int i = 0; i < challengeGroup.length; i++) {
        print("88888");
        print(_startController[i].text);
        print(_endController[i].text);
        dataChallenge.add(SelfChallengeModel(
            challengeGroup: challengeGroup[i], startDate: _startController[i].text, endDate: _endController[i].text
        ));
        print(dataChallenge);
        BlocProvider.of<ReminderChallengeBloc>(context).add(ReminderChallengeButton(
          challengeData: dataChallenge
        ));
      }

    }

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: ()  {
            Navigator.of(context).pop();
            Navigator.pushReplacementNamed(context, "/waterTrackerScreen");
          }
        ),
        backgroundColor: ksecondaryColour,
        elevation: 0,
//        centerTitle: true,
        title: Text("Set Reminder",
          style: TextStyle(
              color: kprimaryColour
          ),
        ),
        actions: <Widget>[
          Padding(
              padding: const EdgeInsets.all(20.0),
              child: GestureDetector(
                onTap: (){
                  Navigator.pushNamed(context, "/dayEndQuestionScreen");
                },
                child: GestureDetector(
                  onTap: (){
                    if(type == "WATER") {
                      _onReminderWaterButtonPressed();
                    }
                    if(type == "MEAL"){
                      _onReminderMealButtonPressed();
                    }
                    if(type == "Challenge"){
                      _onReminderChallengeButtonPressed();
                    }
                  },
                  child: Text("Save",
                    style: TextStyle(
                        color: ksecondaryTextColour
                    ),
                  ),
                ),
              ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          color: ksecondaryColour,
          padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
          child: DefaultTabController(
            length: 3,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Container(
                    height: ScreenUtil().setHeight(80),
                    padding: EdgeInsets.only(
                      top: ScreenUtil().setWidth(16),
                      bottom: ScreenUtil().setWidth(16)
                    ),
                    child: TabBar(
                      controller: _tabController,
                      labelColor: Colors.white,
                      unselectedLabelColor: ksecondaryTextColour,
                      indicatorColor: kprimaryColour,
                      indicator: BoxDecoration(
                        color: kprimaryColour
                      ),
                      onTap: (tabIndex) {
                      switch (tabIndex) {
                        case 0:
                        setState(() {
                          type = "WATER";
                          print(type);
                        });
                        break;
                        case 1:
                        setState(() {
                          type = "MEAL";
                          print(type);
                        });
                        break;
                        case 2:
                        setState(() {
                          type = "Challenge";
                          print(type);
                        });
                        break;
                        }},
                      tabs: [
                        Tab(
                          child: Text("Water"),
                        ),
                        Tab(
                          child: Text("Meal"),
                        ),
                        Tab(
                          child: Text("Self Challenge"),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  height:  MediaQuery.of(context).size.height,
                  child: TabBarView(
                    controller: _tabController,
                      children:[
                    BlocListener<ReminderWaterBloc,ReminderWaterState>(
                      listener: (context, state){
                        if (state is ReminderWaterFailure) {
                          print(state.error);
                          Scaffold.of(context).showSnackBar(
                            SnackBar(
                              content: Text('${state.error}'),
                              backgroundColor: Colors.red,
                            ),
                          );
                        }
                        if (state is ReminderWaterSuccess) {
                          Scaffold.of(context).showSnackBar(
                            SnackBar(
                              content: Text('Water Reminder is successful'),
                              backgroundColor: Colors.green,
                            ),
                          );
                        }
                      },
                      child: BlocBuilder<ReminderWaterBloc,ReminderWaterState>(
                        builder: (context, state){
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Set Time:", style: TextStyle(
                                  color: Color(0xFF222222)
                              ),),
                              SizedBox(height: ScreenUtil().setHeight(20),),
                              Center(
                                child: Container(
                                  width: ScreenUtil().setWidth(370),
                                  height: ScreenUtil().setHeight(200),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10.0),
                                      color: ksecondaryColour,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Color.fromRGBO(0, 0, 0, 0.25),
                                          blurRadius: 20,
                                          offset: Offset(0, 2), // Shadow position
                                        ),
                                      ]

                                  ),
                                  child: Stack(
                                    children: [
                                       GestureDetector(
                                         onTap: (){
                                           DatePicker.showTimePicker(context,
                                               theme: DatePickerTheme(
                                                 containerHeight: 210.0,
                                               ),
                                               showTitleActions: true,
                                               onConfirm: (time) {
                                                 _time = '${time.hour}:${time.minute}';
                                                 _hour = time.hour;
                                                 _minutes = time.minute;
                                                 setState(() {});
                                               },
                                               currentTime: DateTime.now(),
                                               locale: LocaleType.en);
                                        },
                                         child: Center(
                                           child: Text(_time, style: TextStyle(
                                             color: Colors.black,
                                             fontSize: ScreenUtil().setSp(50.0),
                                             fontWeight: FontWeight.bold
                                           ),),
                                         ),
                                       ),
                                      Positioned(
                                        top: 20,
                                        right: 20,
                                        child: Column(
                                          children: [
                                            Text('AM'),
                                            SizedBox(height: ScreenUtil().setHeight(20),),
                                            Text('PM')
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(height: ScreenUtil().setHeight(20),),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text("Repeat", style: TextStyle(color: Color(0xFF222222)),),
                                  Row(
                                    children: [
                                      Text("Everyweek", style: TextStyle(color: Color(0xFF222222)),),
                                      Checkbox(
                                          value: showEveryweek,
                                    onChanged: (bool value) {
                                    setState(() {
                                   this.showEveryweek = value;
                                   showMonday = showMonday == false ? true : false;
                                   showTuesday = showTuesday == false ? true : false;
                                   showWednesday = showWednesday == false ? true : false;
                                   showThursday = showThursday == false ? true : false;
                                   showFriday = showFriday == false ? true : false;
                                   showSaturday = showSaturday == false ? true : false;
                                   showSunday = showSunday == false ? true : false;
                                   days.addAll([0,1,2,3,4,5,6]);
                                    });
                                   },
                                      ),
                                    ],
                                  )
                                ],
                              ),
                              SizedBox(height: ScreenUtil().setHeight(10),),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  GestureDetector(
                                    onTap:(){
                                      setState(() {
                                       showMonday =  showMonday == false ? true : false;
                                       showMonday == false ? days.remove(0) : days.add(0);
                                      });
                                   },
                                    child: Container(
                                      height: ScreenUtil().setHeight(60),
                                      width: ScreenUtil().setWidth(40),
                                      decoration: BoxDecoration(
                                          color: showMonday ? kprimaryColour : ksecondaryTextColour,
                                          shape: BoxShape.circle
                                      ),
                                      child: Center(
                                        child: Text('Mon',
                                          style: TextStyle(
                                              color: ksecondaryColour,
                                              fontSize: 10
                                          ),),
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: ScreenUtil().setHeight(10),),
                                  GestureDetector(
                                    onTap:(){
                                      setState(() {
                                        showTuesday = showTuesday == false ? true : false;
                                        showTuesday == false ? days.remove(1) : days.add(1);
                                      });
                                    },
                                    child: Container(
                                      height: ScreenUtil().setHeight(60),
                                      width: ScreenUtil().setWidth(40),
                                      decoration: BoxDecoration(
                                          color: showTuesday ? kprimaryColour : ksecondaryTextColour,
                                          shape: BoxShape.circle
                                      ),
                                      child: Center(
                                        child: Text('Tue',
                                          style: TextStyle(
                                              color: ksecondaryColour,
                                              fontSize: 10
                                          ),),
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: ScreenUtil().setHeight(10),),
                                  GestureDetector(
                                    onTap:(){
                                    setState(() {
                                      showWednesday = showWednesday == false ? true : false;
                                      showWednesday == false ? days.remove(2) : days.add(2);
                                    });
                                    },
                                    child: Container(
                                      height: ScreenUtil().setHeight(60),
                                      width: ScreenUtil().setWidth(40),
                                      decoration: BoxDecoration(
                                          color: showWednesday ? kprimaryColour : ksecondaryTextColour,
                                          shape: BoxShape.circle
                                      ),
                                      child: Center(
                                        child: Text('Wed',
                                          style: TextStyle(
                                              color: ksecondaryColour,
                                              fontSize: 10
                                          ),),
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: ScreenUtil().setHeight(10),),
                                  GestureDetector(
                                    onTap: (){
                                      setState(() {
                                        showThursday = showThursday == false ? true : false;
                                        showThursday == false ? days.remove(3) : days.add(3);
                                      });
                                   },
                                    child: Container(
                                      height: ScreenUtil().setHeight(60),
                                      width: ScreenUtil().setWidth(40),
                                      decoration: BoxDecoration(
                                          color: showThursday ? kprimaryColour : ksecondaryTextColour,
                                          shape: BoxShape.circle
                                      ),
                                      child: Center(
                                        child: Text('Thur',
                                          style: TextStyle(
                                              color: ksecondaryColour,
                                              fontSize: 10
                                          ),),
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: ScreenUtil().setHeight(10),),
                                  GestureDetector(
                                    onTap: (){
                                      setState(() {
                                        showFriday = showFriday == false ? true : false;
                                        showFriday == false ? days.remove(4) : days.add(4);
                                      });
                                   },
                                    child: Container(
                                      height: ScreenUtil().setHeight(60),
                                      width: ScreenUtil().setWidth(40),
                                      decoration: BoxDecoration(
                                          color: showFriday ? kprimaryColour : ksecondaryTextColour,
                                          shape: BoxShape.circle
                                      ),
                                      child: Center(
                                        child: Text('Fri',
                                          style: TextStyle(
                                              color: ksecondaryColour,
                                              fontSize: 10
                                          ),),
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: ScreenUtil().setHeight(10),),
                                  GestureDetector(
                                    onTap: (){
                                        setState(() {
                                          showSaturday = showSaturday == false ? true : false;
                                          showFriday == false ? days.remove(5) : days.add(5);
                                        });
                                    },
                                    child: Container(
                                      height: ScreenUtil().setHeight(60),
                                      width: ScreenUtil().setWidth(40),
                                      decoration: BoxDecoration(
                                          color: showSaturday ? kprimaryColour : ksecondaryTextColour,
                                          shape: BoxShape.circle
                                      ),
                                      child: Center(
                                        child: Text('Sat',
                                          style: TextStyle(
                                              color: ksecondaryColour,
                                              fontSize: 10
                                          ),),
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: ScreenUtil().setHeight(10),),
                                  GestureDetector(
                                    onTap: (){
                                      setState(() {
                                        showSunday = showSunday == false ? true : false;
                                        showSunday == false ? days.remove(6) : days.add(6);
                                        print(days);
                                      });
                                   },
                                    child: Container(
                                      height: ScreenUtil().setHeight(60),
                                      width: ScreenUtil().setWidth(40),
                                      decoration: BoxDecoration(
                                          color: showSunday ? kprimaryColour : ksecondaryTextColour,
                                          shape: BoxShape.circle
                                      ),
                                      child: Center(
                                        child: Text('Sun',
                                          style: TextStyle(
                                              color: ksecondaryColour,
                                              fontSize: 10
                                          ),),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(height: ScreenUtil().setHeight(20),),
                              Text("Interval", style: TextStyle(color: ksecondaryTextColour, fontSize: ScreenUtil().setSp(16.0)),),
                              SizedBox(height: ScreenUtil().setHeight(10),),
                              Container(
                                padding: EdgeInsets.only(left: ScreenUtil().setWidth(8.0)),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(3.0),
                                    border: Border.all(color: Color(0xFFD0F1DD), width: 1.0)),
                                child: DropdownButtonHideUnderline(
                                  child: DropdownButton(
                                      isExpanded: true,
                                       value: _value,
                                      hint: Text(
                                        "Select an interval",
                                        style: TextStyle(color: Colors.grey),
                                      ),
                                      items: [
                                        DropdownMenuItem(
                                          child: Text("30mins"),
                                          value: "30",
                                        ),
                                        DropdownMenuItem(
                                          child: Text("1hour"),
                                          value: "1",
                                        ),
                                      ],
                                      onChanged: (value) {
                                        setState(() {
                                         _value = value;
                                        });
                                      }),
                                ),
                              ),
                            ],
                          );
                        },
                      ),
                    ),
                   BlocListener<ReminderMealBloc, ReminderMealState>(
                       listener: (context, state){
                         if (state is ReminderMealFailure) {
                           print(state.error);
                           Scaffold.of(context).showSnackBar(
                             SnackBar(
                               content: Text('${state.error}'),
                               backgroundColor: Colors.red,
                             ),
                           );
                         }
                         if (state is ReminderMealSuccess) {
                           Scaffold.of(context).showSnackBar(
                             SnackBar(
                               content: Text('Meal Reminder is successful'),
                               backgroundColor: Colors.green,
                             ),
                           );
                         }
                       },
                     child: BlocBuilder<ReminderMealBloc, ReminderMealState>(
                        builder: (context, state){
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Set Time:", style: TextStyle(
                                  color: Color(0xFF222222)
                              ),),
                              SizedBox(height: ScreenUtil().setHeight(20),),
                              Center(
                                child: Container(
                                  width: ScreenUtil().setWidth(370),
                                  height: ScreenUtil().setHeight(200),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10.0),
                                      color: ksecondaryColour,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Color.fromRGBO(0, 0, 0, 0.25),
                                          blurRadius: 20,
                                          offset: Offset(0, 2), // Shadow position
                                        ),
                                      ]

                                  ),
                                  child: Stack(
                                    children: [
                                      GestureDetector(
                                        onTap: (){
                                          DatePicker.showTimePicker(context,
                                              theme: DatePickerTheme(
                                                containerHeight: 210.0,
                                              ),
                                              showTitleActions: true,
                                              onConfirm: (time) {
                                                _mealtime = '${time.hour}:${time.minute}';
                                                _mealhour = time.hour;
                                                _mealminutes = time.minute;
                                                setState(() {});
                                              },
                                              currentTime: DateTime.now(),
                                              locale: LocaleType.en);
                                        },
                                        child: Center(
                                          child: Text(_mealtime, style: TextStyle(
                                              color: Colors.black,
                                              fontSize: ScreenUtil().setSp(50.0),
                                              fontWeight: FontWeight.bold
                                          ),),
                                        ),
                                      ),
                                      Positioned(
                                        top: 20,
                                        right: 20,
                                        child: Column(
                                          children: [
                                            Text('AM'),
                                            SizedBox(height: ScreenUtil().setHeight(20),),
                                            Text('PM')
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(height: ScreenUtil().setHeight(20),),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text("Repeat", style: TextStyle(color: Color(0xFF222222)),),
                                  Row(
                                    children: [
                                      Text("Everyweek", style: TextStyle(color: Color(0xFF222222)),),
                                      Checkbox(
                                        value: showMealEveryweek,
                                        onChanged: (bool value) {
                                          setState(() {
                                            this.showMealEveryweek = value;
                                            showMealMonday = showMealMonday == false ? true : false;
                                            showMealTuesday = showMealTuesday == false ? true : false;
                                            showMealWednesday = showMealWednesday == false ? true : false;
                                            showMealThursday = showMealThursday == false ? true : false;
                                            showMealFriday = showMealFriday == false ? true : false;
                                            showMealSaturday = showMealSaturday == false ? true : false;
                                            showMealSunday = showMealSunday == false ? true : false;
                                            daysMeal.addAll([0,1,2,3,4,5,6]);
                                          });
                                        },
                                      ),
                                    ],
                                  )
                                ],
                              ),
                              SizedBox(height: ScreenUtil().setHeight(10),),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  GestureDetector(
                                    onTap:(){
                                      setState(() {
                                        showMealMonday =  showMealMonday == false ? true : false;
                                        showMealMonday == false ? daysMeal.remove(0) : daysMeal.add(0);
                                      });
                                    },
                                    child: Container(
                                      height: ScreenUtil().setHeight(60),
                                      width: ScreenUtil().setWidth(40),
                                      decoration: BoxDecoration(
                                          color: showMealMonday ? kprimaryColour : ksecondaryTextColour,
                                          shape: BoxShape.circle
                                      ),
                                      child: Center(
                                        child: Text('Mon',
                                          style: TextStyle(
                                              color: ksecondaryColour,
                                              fontSize: 10
                                          ),),
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: ScreenUtil().setHeight(10),),
                                  GestureDetector(
                                    onTap:(){
                                      setState(() {
                                        showMealTuesday = showMealTuesday == false ? true : false;
                                        showMealTuesday == false ? daysMeal.remove(1) : daysMeal.add(1);
                                      });
                                    },
                                    child: Container(
                                      height: ScreenUtil().setHeight(60),
                                      width: ScreenUtil().setWidth(40),
                                      decoration: BoxDecoration(
                                          color: showMealTuesday ? kprimaryColour : ksecondaryTextColour,
                                          shape: BoxShape.circle
                                      ),
                                      child: Center(
                                        child: Text('Tue',
                                          style: TextStyle(
                                              color: ksecondaryColour,
                                              fontSize: 10
                                          ),),
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: ScreenUtil().setHeight(10),),
                                  GestureDetector(
                                    onTap:(){
                                      setState(() {
                                        showMealWednesday = showMealWednesday == false ? true : false;
                                        showMealWednesday == false ? daysMeal.remove(2) : daysMeal.add(2);
                                      });
                                    },
                                    child: Container(
                                      height: ScreenUtil().setHeight(60),
                                      width: ScreenUtil().setWidth(40),
                                      decoration: BoxDecoration(
                                          color: showMealWednesday ? kprimaryColour : ksecondaryTextColour,
                                          shape: BoxShape.circle
                                      ),
                                      child: Center(
                                        child: Text('Wed',
                                          style: TextStyle(
                                              color: ksecondaryColour,
                                              fontSize: 10
                                          ),),
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: ScreenUtil().setHeight(10),),
                                  GestureDetector(
                                    onTap: (){
                                      setState(() {
                                        showMealThursday = showMealThursday == false ? true : false;
                                        showMealThursday == false ? daysMeal.remove(3) : daysMeal.add(3);
                                      });
                                    },
                                    child: Container(
                                      height: ScreenUtil().setHeight(60),
                                      width: ScreenUtil().setWidth(40),
                                      decoration: BoxDecoration(
                                          color: showMealThursday ? kprimaryColour : ksecondaryTextColour,
                                          shape: BoxShape.circle
                                      ),
                                      child: Center(
                                        child: Text('Thur',
                                          style: TextStyle(
                                              color: ksecondaryColour,
                                              fontSize: 10
                                          ),),
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: ScreenUtil().setHeight(10),),
                                  GestureDetector(
                                    onTap: (){
                                      setState(() {
                                        showMealFriday = showMealFriday == false ? true : false;
                                        showMealFriday == false ? daysMeal.remove(4) : daysMeal.add(4);
                                      });
                                    },
                                    child: Container(
                                      height: ScreenUtil().setHeight(60),
                                      width: ScreenUtil().setWidth(40),
                                      decoration: BoxDecoration(
                                          color: showMealFriday ? kprimaryColour : ksecondaryTextColour,
                                          shape: BoxShape.circle
                                      ),
                                      child: Center(
                                        child: Text('Fri',
                                          style: TextStyle(
                                              color: ksecondaryColour,
                                              fontSize: 10
                                          ),),
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: ScreenUtil().setHeight(10),),
                                  GestureDetector(
                                    onTap: (){
                                      setState(() {
                                        showMealSaturday = showMealSaturday == false ? true : false;
                                        showMealSaturday == false ? daysMeal.remove(5) : daysMeal.add(5);
                                      });
                                    },
                                    child: Container(
                                      height: ScreenUtil().setHeight(60),
                                      width: ScreenUtil().setWidth(40),
                                      decoration: BoxDecoration(
                                          color: showMealSaturday ? kprimaryColour : ksecondaryTextColour,
                                          shape: BoxShape.circle
                                      ),
                                      child: Center(
                                        child: Text('Sat',
                                          style: TextStyle(
                                              color: ksecondaryColour,
                                              fontSize: 10
                                          ),),
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: ScreenUtil().setHeight(10),),
                                  GestureDetector(
                                    onTap: (){
                                      setState(() {
                                        showMealSunday = showMealSunday == false ? true : false;
                                        showMealSunday == false ? daysMeal.remove(6) : daysMeal.add(6);
                                        print(days);
                                      });
                                    },
                                    child: Container(
                                      height: ScreenUtil().setHeight(60),
                                      width: ScreenUtil().setWidth(40),
                                      decoration: BoxDecoration(
                                          color: showMealSunday ? kprimaryColour : ksecondaryTextColour,
                                          shape: BoxShape.circle
                                      ),
                                      child: Center(
                                        child: Text('Sun',
                                          style: TextStyle(
                                              color: ksecondaryColour,
                                              fontSize: 10
                                          ),),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                               SizedBox(height: ScreenUtil().setHeight(20),),
                              Text("Meal", style: TextStyle(color: ksecondaryTextColour, fontSize: ScreenUtil().setSp(16.0)),),
                              SizedBox(height: ScreenUtil().setHeight(10),),
                              Container(
                                padding: EdgeInsets.only(left: ScreenUtil().setWidth(8.0)),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(3.0),
                                    border: Border.all(color: Color(0xFFD0F1DD), width: 1.0)),
                                child: DropdownButtonHideUnderline(
                                  child: DropdownButton(
                                      isExpanded: true,
                                      value: _mealValue,
                                      hint: Text(
                                        "Select meal",
                                        style: TextStyle(color: Colors.grey),
                                      ),
                                      items: [
                                        DropdownMenuItem(
                                          child: Text("BREAKFAST"),
                                          value: "BREAKFAST",
                                        ),
                                        DropdownMenuItem(
                                          child: Text("LUNCH"),
                                          value: "LUNCH",
                                        ),
                                        DropdownMenuItem(
                                          child: Text("DINNER"),
                                          value: "DINNER",
                                        ),
                                      ],
                                      onChanged: (value) {
                                        setState(() {
                                        _mealValue = value;
                                        });
                                      }),
                                ),
                              ),
                            ],
                          );
                        },
                     ),
                   ),
                    BlocListener<ReminderChallengeBloc,ReminderChallengeState>(
                        listener: (context, state){
                          if(state is ReminderChallengeSuccess){
                            Scaffold.of(context).showSnackBar(
                              SnackBar(
                                content: Text('Challenge is successfully created'),
                                backgroundColor: Colors.green,
                              ),
                            );
                          }
                        },
                      child: BlocBuilder<ReminderChallengeBloc,ReminderChallengeState>(
                        builder: (context, state){
                          return Column(
                            children: [
                              GestureDetector(
                                onTap: (){
                                   setState(() {
                                     showChallenge = showChallenge == false ? true : false;
                                     print(showChallenge);
                                   });
                                },
                                child: Container(
                                  margin: EdgeInsets.all(ScreenUtil().setWidth(16)),
                                  padding: EdgeInsets.all(ScreenUtil().setWidth(16)),
                                  decoration: BoxDecoration(
                                      color: ksecondaryColour,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Color.fromRGBO(0, 0, 0, 0.25),
                                          blurRadius: 20,
                                          offset: Offset(0, 2), // Shadow position
                                        ),
                                      ]
                                  ),
                                  child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text("Pick a challenge"),
                                       showChallenge == false ? SvgPicture.asset("assets/images/arrow_click.svg", color: Colors.black,)
                                       : SvgPicture.asset("assets/images/arrow_down.svg", color: Colors.black,)
                                      ]
                                  ),
                                ),
                              ),
                            showChallenge == true ? ListView(
                              shrinkWrap: true,
                               children: [
                                ListView.builder(
                                    itemCount: dataCheck.length,
                                    shrinkWrap: true,
                                    itemBuilder: (BuildContext context, int position){
                                      _startController.add(new TextEditingController());
                                      _endController.add(new TextEditingController());
                                      void _setStartDate() async {
                                        DateTime selectedDate = await showDatePicker(
                                            initialDatePickerMode: DatePickerMode.year,
                                            initialDate: DateTime.now(),
                                            firstDate: DateTime(1900),
                                            lastDate: DateTime(2050),
                                            context: context);
                                        print(selectedDate);
                                        _dateValue = selectedDate.toIso8601String();
                                        print(_dateValue);
                                        _startController[position].text = dateConverted(selectedDate, format: 'dd-MMMM-yyyy');
                                      }
                                      void _setEndDate() async {
                                        DateTime selectedDate = await showDatePicker(
                                            initialDatePickerMode: DatePickerMode.year,
                                            initialDate: DateTime.now(),
                                            firstDate: DateTime(1900),
                                            lastDate: DateTime(2050),
                                            context: context);
//    _dateValue = selectedDate.toIso8601String();
                                        _endController[position].text = dateConverted(selectedDate, format: 'dd-MMMM-yyyy');
                                      }
                                      return Column(
                                        children: [
                                          Container(
                                            margin: EdgeInsets.all(ScreenUtil().setWidth(16)),
                                            padding: EdgeInsets.only(
                                              left: ScreenUtil().setWidth(16.0),
                                              right: ScreenUtil().setWidth(16.0),
                                            ),
                                            decoration: BoxDecoration(
                                                color: ksecondaryColour,
                                                boxShadow: [
                                                  BoxShadow(
                                                    color: Color.fromRGBO(0, 0, 0, 0.25),
                                                    blurRadius: 20,
                                                    offset: Offset(0, 2), // Shadow position
                                                  ),
                                                ]
                                            ),
                                            child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Text(dataCheck[position]['name']),
                                                  Checkbox(
                                                    activeColor: kprimaryColour,
                                                    value: _isChecked[position],
                                                    onChanged: (bool value) {
                                                      setState(() {
                                                        _isChecked[position] = value;
                                                        print(_isChecked[position]);
                                                        if(_isChecked[position] == true) {
                                                          if (!challengeGroup
                                                              .contains(
                                                              dataCheck[position]['_id'])) {
                                                            challengeGroup
                                                                .add(
                                                                dataCheck[position]['_id']);
                                                          }
                                                        }else{
                                                          challengeGroup
                                                              .remove(
                                                              dataCheck[position]['_id']);
                                                        }
                                                      });
                                                      print(challengeGroup);
                                                    },
                                                  ),
                                                ]
                                            ),
                                          ),
                                          _isChecked[position] ? Column(
                                            children: [
                                              Padding(
                                                padding: EdgeInsets.only(
                                                    left: ScreenUtil().setWidth(16.0),
                                                    right: ScreenUtil().setWidth(16.0)
                                                ),
                                                child: TextFormField(
                                                  controller: _startController[position],
                                                  style: TextStyle(
                                                    color: Color(0xFF160304),
                                                    fontWeight: FontWeight.w600,
                                                  ),
                                                  textInputAction: TextInputAction.next,
                                                  onTap: _setStartDate,
                                                  readOnly: true,
                                                  validator: (String text) {
                                                    if (text.isEmpty) {
                                                      return "Select ";
                                                    }
                                                    return null;
                                                  },
                                                  decoration: InputDecoration(
                                                      labelText: 'Start Date',
                                                      labelStyle: TextStyle(
                                                          fontWeight: FontWeight.w500,
                                                          color: Color(0xFF535353)
                                                      ),
                                                      focusedBorder: UnderlineInputBorder(
                                                          borderSide:
                                                          BorderSide(color: Color(0xFF666666)))
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.only(
                                                    left: ScreenUtil().setWidth(16.0),
                                                    right: ScreenUtil().setWidth(16.0)
                                                ),
                                                child: TextFormField(
                                                  controller: _endController[position],
                                                  style: TextStyle(
                                                    color: Color(0xFF160304),
                                                    fontWeight: FontWeight.w600,
                                                  ),
                                                  textInputAction: TextInputAction.next,
                                                  onTap: _setEndDate,
                                                  readOnly: true,
                                                  validator: (String text) {
                                                    if (text.isEmpty) {
                                                      return "End date";
                                                    }
                                                    return null;
                                                  },
                                                  decoration: InputDecoration(
                                                      labelText: 'End Date',
                                                      labelStyle: TextStyle(
                                                          fontWeight: FontWeight.w500,
                                                          color: Color(0xFF535353)
                                                      ),
                                                      focusedBorder: UnderlineInputBorder(
                                                          borderSide:
                                                          BorderSide(color: Color(0xFF666666)))
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ) : SizedBox(),
                                        ],
                                      );
                                    }),

//                                 Container(
//                                   margin: EdgeInsets.all(ScreenUtil().setWidth(16)),
//                                   padding: EdgeInsets.only(
//                                     left: ScreenUtil().setWidth(16.0),
//                                     right: ScreenUtil().setWidth(16.0),
//                                   ),
//                                   decoration: BoxDecoration(
//                                       color: ksecondaryColour,
//                                       boxShadow: [
//                                         BoxShadow(
//                                           color: Color.fromRGBO(0, 0, 0, 0.25),
//                                           blurRadius: 20,
//                                           offset: Offset(0, 2), // Shadow position
//                                         ),
//                                       ]
//                                   ),
//                                   child: Row(
//                                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                                       children: [
//                                         Text("Sugar"),
//                                         Checkbox(
//                                           activeColor: kprimaryColour,
//                                           value: showSugar,
//                                           onChanged: (bool value) {
//                                             setState(() {
//                                               this.showSugar = value;
//                                             });
//                                           },
//                                         ),
//                                       ]
//                                   ),
//                                 ),
//                                 showSugar ? Column(
//                                   children: [
//                                     Padding(
//                                       padding: EdgeInsets.only(
//                                           left: ScreenUtil().setWidth(16.0),
//                                           right: ScreenUtil().setWidth(16.0)
//                                       ),
//                                       child: TextFormField(
////                                       controller: _dobController,
//
//                                         style: TextStyle(
//                                           color: Color(0xFF160304),
//                                           fontWeight: FontWeight.w600,
//                                         ),
//                                         textInputAction: TextInputAction.next,
//                                         onTap: _setDate,
//                                         readOnly: true,
//                                         validator: (String text) {
//                                           if (text.isEmpty) {
//                                             return "Select ";
//                                           }
//                                           return null;
//                                         },
//                                         decoration: InputDecoration(
//                                             labelText: 'Start Date',
//                                             labelStyle: TextStyle(
//                                                 fontWeight: FontWeight.w500,
//                                                 color: Color(0xFF535353)
//                                             ),
//                                             focusedBorder: UnderlineInputBorder(
//                                                 borderSide:
//                                                 BorderSide(color: Color(0xFF666666)))
//                                         ),
//                                       ),
//                                     ),
//                                     Padding(
//                                       padding: EdgeInsets.only(
//                                           left: ScreenUtil().setWidth(16.0),
//                                           right: ScreenUtil().setWidth(16.0)
//                                       ),
//                                       child: TextFormField(
////                                       controller: _dobController,
//
//                                         style: TextStyle(
//                                           color: Color(0xFF160304),
//                                           fontWeight: FontWeight.w600,
//                                         ),
//                                         textInputAction: TextInputAction.next,
//                                         onTap: _setDate,
//                                         readOnly: true,
//                                         validator: (String text) {
//                                           if (text.isEmpty) {
//                                             return "End date";
//                                           }
//                                           return null;
//                                         },
//                                         decoration: InputDecoration(
//                                             labelText: 'End Date',
//                                             labelStyle: TextStyle(
//                                                 fontWeight: FontWeight.w500,
//                                                 color: Color(0xFF535353)
//                                             ),
//                                             focusedBorder: UnderlineInputBorder(
//                                                 borderSide:
//                                                 BorderSide(color: Color(0xFF666666)))
//                                         ),
//                                       ),
//                                     ),
//                                   ],
//                                 ) : SizedBox(),
//                                 Container(
//                                   margin: EdgeInsets.all(ScreenUtil().setWidth(16)),
//                                   padding: EdgeInsets.only(
//                                     left: ScreenUtil().setWidth(16.0),
//                                     right: ScreenUtil().setWidth(16.0),
//                                   ),
//                                   decoration: BoxDecoration(
//                                       color: ksecondaryColour,
//                                       boxShadow: [
//                                         BoxShadow(
//                                           color: Color.fromRGBO(0, 0, 0, 0.25),
//                                           blurRadius: 20,
//                                           offset: Offset(0, 2), // Shadow position
//                                         ),
//                                       ]
//                                   ),
//                                   child: Row(
//                                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                                       children: [
//                                         Text("Chocolates"),
//                                         Checkbox(
//                                           activeColor: kprimaryColour,
//                                           value: showChocolates,
//                                           onChanged: (bool value) {
//                                             setState(() {
//                                               this.showChocolates= value;
//                                             });
//                                           },
//                                         ),
//                                       ]
//                                   ),
//                                 ),
//                                 showChocolates ? Column(
//                                   children: [
//                                     Padding(
//                                       padding: EdgeInsets.only(
//                                           left: ScreenUtil().setWidth(16.0),
//                                           right: ScreenUtil().setWidth(16.0)
//                                       ),
//                                       child: TextFormField(
////                                       controller: _dobController,
//
//                                         style: TextStyle(
//                                           color: Color(0xFF160304),
//                                           fontWeight: FontWeight.w600,
//                                         ),
//                                         textInputAction: TextInputAction.next,
//                                         onTap: _setDate,
//                                         readOnly: true,
//                                         validator: (String text) {
//                                           if (text.isEmpty) {
//                                             return "Select ";
//                                           }
//                                           return null;
//                                         },
//                                         decoration: InputDecoration(
//                                             labelText: 'Start Date',
//                                             labelStyle: TextStyle(
//                                                 fontWeight: FontWeight.w500,
//                                                 color: Color(0xFF535353)
//                                             ),
//                                             focusedBorder: UnderlineInputBorder(
//                                                 borderSide:
//                                                 BorderSide(color: Color(0xFF666666)))
//                                         ),
//                                       ),
//                                     ),
//                                     Padding(
//                                       padding: EdgeInsets.only(
//                                           left: ScreenUtil().setWidth(16.0),
//                                           right: ScreenUtil().setWidth(16.0)
//                                       ),
//                                       child: TextFormField(
////                                       controller: _dobController,
//
//                                         style: TextStyle(
//                                           color: Color(0xFF160304),
//                                           fontWeight: FontWeight.w600,
//                                         ),
//                                         textInputAction: TextInputAction.next,
//                                         onTap: _setDate,
//                                         readOnly: true,
//                                         validator: (String text) {
//                                           if (text.isEmpty) {
//                                             return "End date";
//                                           }
//                                           return null;
//                                         },
//                                         decoration: InputDecoration(
//                                             labelText: 'End Date',
//                                             labelStyle: TextStyle(
//                                                 fontWeight: FontWeight.w500,
//                                                 color: Color(0xFF535353)
//                                             ),
//                                             focusedBorder: UnderlineInputBorder(
//                                                 borderSide:
//                                                 BorderSide(color: Color(0xFF666666)))
//                                         ),
//                                       ),
//                                     ),
//                                   ],
//                                 ) : SizedBox(),
//                                 Container(
//                                   margin: EdgeInsets.all(ScreenUtil().setWidth(16)),
//                                   padding: EdgeInsets.only(
//                                     left: ScreenUtil().setWidth(16.0),
//                                     right: ScreenUtil().setWidth(16.0),
//                                   ),
//                                   decoration: BoxDecoration(
//                                       color: ksecondaryColour,
//                                       boxShadow: [
//                                         BoxShadow(
//                                           color: Color.fromRGBO(0, 0, 0, 0.25),
//                                           blurRadius: 20,
//                                           offset: Offset(0, 2), // Shadow position
//                                         ),
//                                       ]
//                                   ),
//                                   child: Row(
//                                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                                       children: [
//                                         Text("Soda drinks"),
//                                         Checkbox(
//                                           activeColor: kprimaryColour,
//                                           value: showSodaDrinks,
//                                           onChanged: (bool value) {
//                                             setState(() {
//                                               this.showSodaDrinks= value;
//                                             });
//                                           },
//                                         ),
//                                       ]
//                                   ),
//                                 ),
//                                 showSodaDrinks ? Column(
//                                   children: [
//                                     Padding(
//                                       padding: EdgeInsets.only(
//                                           left: ScreenUtil().setWidth(16.0),
//                                           right: ScreenUtil().setWidth(16.0)
//                                       ),
//                                       child: TextFormField(
////                                       controller: _dobController,
//
//                                         style: TextStyle(
//                                           color: Color(0xFF160304),
//                                           fontWeight: FontWeight.w600,
//                                         ),
//                                         textInputAction: TextInputAction.next,
//                                         onTap: _setDate,
//                                         readOnly: true,
//                                         validator: (String text) {
//                                           if (text.isEmpty) {
//                                             return "Select ";
//                                           }
//                                           return null;
//                                         },
//                                         decoration: InputDecoration(
//                                             labelText: 'Start Date',
//                                             labelStyle: TextStyle(
//                                                 fontWeight: FontWeight.w500,
//                                                 color: Color(0xFF535353)
//                                             ),
//                                             focusedBorder: UnderlineInputBorder(
//                                                 borderSide:
//                                                 BorderSide(color: Color(0xFF666666)))
//                                         ),
//                                       ),
//                                     ),
//                                     Padding(
//                                       padding: EdgeInsets.only(
//                                           left: ScreenUtil().setWidth(16.0),
//                                           right: ScreenUtil().setWidth(16.0)
//                                       ),
//                                       child: TextFormField(
////                                       controller: _dobController,
//
//                                         style: TextStyle(
//                                           color: Color(0xFF160304),
//                                           fontWeight: FontWeight.w600,
//                                         ),
//                                         textInputAction: TextInputAction.next,
//                                         onTap: _setDate,
//                                         readOnly: true,
//                                         validator: (String text) {
//                                           if (text.isEmpty) {
//                                             return "End date";
//                                           }
//                                           return null;
//                                         },
//                                         decoration: InputDecoration(
//                                             labelText: 'End Date',
//                                             labelStyle: TextStyle(
//                                                 fontWeight: FontWeight.w500,
//                                                 color: Color(0xFF535353)
//                                             ),
//                                             focusedBorder: UnderlineInputBorder(
//                                                 borderSide:
//                                                 BorderSide(color: Color(0xFF666666)))
//                                         ),
//                                       ),
//                                     ),
//                                   ],
//                                 ) : SizedBox(),
//                                 Container(
//                                   margin: EdgeInsets.all(ScreenUtil().setWidth(16)),
//                                   padding: EdgeInsets.only(
//                                     left: ScreenUtil().setWidth(16.0),
//                                     right: ScreenUtil().setWidth(16.0),
//                                   ),
//                                   decoration: BoxDecoration(
//                                       color: ksecondaryColour,
//                                       boxShadow: [
//                                         BoxShadow(
//                                           color: Color.fromRGBO(0, 0, 0, 0.25),
//                                           blurRadius: 20,
//                                           offset: Offset(0, 2), // Shadow position
//                                         ),
//                                       ]
//                                   ),
//                                   child: Row(
//                                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                                       children: [
//                                         Text("Alcohols"),
//                                         Checkbox(
//                                           activeColor: kprimaryColour,
//                                           value: showAlcohol,
//                                           onChanged: (bool value) {
//                                             setState(() {
//                                               this.showAlcohol= value;
//                                             });
//                                           },
//                                         ),
//                                       ]
//                                   ),
//                                 ),
//                                 showAlcohol ? Column(
//                                   children: [
//                                     Padding(
//                                       padding: EdgeInsets.only(
//                                           left: ScreenUtil().setWidth(16.0),
//                                           right: ScreenUtil().setWidth(16.0)
//                                       ),
//                                       child: TextFormField(
////                                       controller: _dobController,
//
//                                         style: TextStyle(
//                                           color: Color(0xFF160304),
//                                           fontWeight: FontWeight.w600,
//                                         ),
//                                         textInputAction: TextInputAction.next,
//                                         onTap: _setDate,
//                                         readOnly: true,
//                                         validator: (String text) {
//                                           if (text.isEmpty) {
//                                             return "Select ";
//                                           }
//                                           return null;
//                                         },
//                                         decoration: InputDecoration(
//                                             labelText: 'Start Date',
//                                             labelStyle: TextStyle(
//                                                 fontWeight: FontWeight.w500,
//                                                 color: Color(0xFF535353)
//                                             ),
//                                             focusedBorder: UnderlineInputBorder(
//                                                 borderSide:
//                                                 BorderSide(color: Color(0xFF666666)))
//                                         ),
//                                       ),
//                                     ),
//                                     Padding(
//                                       padding: EdgeInsets.only(
//                                           left: ScreenUtil().setWidth(16.0),
//                                           right: ScreenUtil().setWidth(16.0)
//                                       ),
//                                       child: TextFormField(
////                                       controller: _dobController,
//
//                                         style: TextStyle(
//                                           color: Color(0xFF160304),
//                                           fontWeight: FontWeight.w600,
//                                         ),
//                                         textInputAction: TextInputAction.next,
//                                         onTap: _setDate,
//                                         readOnly: true,
//                                         validator: (String text) {
//                                           if (text.isEmpty) {
//                                             return "End date";
//                                           }
//                                           return null;
//                                         },
//                                         decoration: InputDecoration(
//                                             labelText: 'End Date',
//                                             labelStyle: TextStyle(
//                                                 fontWeight: FontWeight.w500,
//                                                 color: Color(0xFF535353)
//                                             ),
//                                             focusedBorder: UnderlineInputBorder(
//                                                 borderSide:
//                                                 BorderSide(color: Color(0xFF666666)))
//                                         ),
//                                       ),
//                                     ),
//                                   ],
//                                 ) : SizedBox(),
//                                 Container(
//                                   margin: EdgeInsets.all(ScreenUtil().setWidth(16)),
//                                   padding: EdgeInsets.only(
//                                     left: ScreenUtil().setWidth(16.0),
//                                     right: ScreenUtil().setWidth(16.0),
//                                   ),
//                                   decoration: BoxDecoration(
//                                       color: ksecondaryColour,
//                                       boxShadow: [
//                                         BoxShadow(
//                                           color: Color.fromRGBO(0, 0, 0, 0.25),
//                                           blurRadius: 20,
//                                           offset: Offset(0, 2), // Shadow position
//                                         ),
//                                       ]
//                                   ),
//                                   child: Row(
//                                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                                       children: [
//                                         Text("Smoking"),
//                                         Checkbox(
//                                           activeColor: kprimaryColour,
//                                           value: showSmoking,
//                                           onChanged: (bool value) {
//                                             setState(() {
//                                               this.showSmoking= value;
//                                             });
//                                           },
//                                         ),
//                                       ]
//                                   ),
//                                 ),
//                                 showSmoking ? Column(
//                                   children: [
//                                     Padding(
//                                       padding: EdgeInsets.only(
//                                           left: ScreenUtil().setWidth(16.0),
//                                           right: ScreenUtil().setWidth(16.0)
//                                       ),
//                                       child: TextFormField(
////                                       controller: _dobController,
//
//                                         style: TextStyle(
//                                           color: Color(0xFF160304),
//                                           fontWeight: FontWeight.w600,
//                                         ),
//                                         textInputAction: TextInputAction.next,
//                                         onTap: _setDate,
//                                         readOnly: true,
//                                         validator: (String text) {
//                                           if (text.isEmpty) {
//                                             return "Select ";
//                                           }
//                                           return null;
//                                         },
//                                         decoration: InputDecoration(
//                                             labelText: 'Start Date',
//                                             labelStyle: TextStyle(
//                                                 fontWeight: FontWeight.w500,
//                                                 color: Color(0xFF535353)
//                                             ),
//                                             focusedBorder: UnderlineInputBorder(
//                                                 borderSide:
//                                                 BorderSide(color: Color(0xFF666666)))
//                                         ),
//                                       ),
//                                     ),
//                                     Padding(
//                                       padding: EdgeInsets.only(
//                                           left: ScreenUtil().setWidth(16.0),
//                                           right: ScreenUtil().setWidth(16.0)
//                                       ),
//                                       child: TextFormField(
////                                       controller: _dobController,
//
//                                         style: TextStyle(
//                                           color: Color(0xFF160304),
//                                           fontWeight: FontWeight.w600,
//                                         ),
//                                         textInputAction: TextInputAction.next,
//                                         onTap: _setDate,
//                                         readOnly: true,
//                                         validator: (String text) {
//                                           if (text.isEmpty) {
//                                             return "End date";
//                                           }
//                                           return null;
//                                         },
//                                         decoration: InputDecoration(
//                                             labelText: 'End Date',
//                                             labelStyle: TextStyle(
//                                                 fontWeight: FontWeight.w500,
//                                                 color: Color(0xFF535353)
//                                             ),
//                                             focusedBorder: UnderlineInputBorder(
//                                                 borderSide:
//                                                 BorderSide(color: Color(0xFF666666)))
//                                         ),
//                                       ),
//                                     ),
//                                   ],
//                                 ) : SizedBox()
                               ],
                             ) : SizedBox(),
                            ],
                          );
                        },
                      ),
                    )
                  ]),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
