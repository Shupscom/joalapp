import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/reminder/reminder_bloc.dart';
import 'package:joalapp/bloc/reminder/reminder_challenge/reminder_challenge_bloc.dart';
import 'package:joalapp/bloc/reminder/reminder_meal/reminder_meal_bloc.dart';
import 'package:joalapp/bloc/reminder/reminder_water/reminder_water_bloc.dart';
import 'package:joalapp/screens/water_tracker/set_reminder_screen.dart';
class SetReminderPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ReminderBloc>(
          create: (BuildContext context){
            return ReminderBloc();
          },
        ),
        BlocProvider<ReminderWaterBloc>(
          create: (BuildContext context){
            return ReminderWaterBloc();
          },
        ),
        BlocProvider<ReminderMealBloc>(
          create: (BuildContext context){
            return ReminderMealBloc();
          },
        ),
        BlocProvider<ReminderChallengeBloc>(
          create: (BuildContext context){
            return ReminderChallengeBloc();
          },
        )
      ],
      child: SetReminderScreen(),
    );
  }
}
