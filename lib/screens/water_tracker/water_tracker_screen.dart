import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:joalapp/bloc/reminder/reminder_bloc.dart';
import 'package:joalapp/bloc/reminder/reminder_state.dart';
import 'package:joalapp/components/constants.dart';
import 'package:joalapp/components/make_bottom.dart';
import 'package:joalapp/model/challenge_model.dart';
import 'package:joalapp/model/reminder_model.dart';
import 'package:intl/intl.dart';
import 'package:joalapp/repository/reminder_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';
class WaterTrackerScreen extends StatefulWidget {
  @override
  _WaterTrackerScreenState createState() => _WaterTrackerScreenState();
}

class _WaterTrackerScreenState extends State<WaterTrackerScreen> {

   ReminderModel _waterReminderModel;
   List<ReminderModel> reminderValues = [];
   var mainTime;
   var nextTime;
   var previousTime;
   var value;
   var  userdata;
   final now = new DateTime.now();

   void _getUserInfo() async {
     SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
     var data = sharedPreferences.get("userdata");

     var reminders = await ReminderRepository.getAllReminder();
     setState(() {
       value = json.decode(reminders);
       print(value);
       for(var item in value['data']){
         ReminderModel reminderModel = ReminderModel.fromJson(item);
         reminderValues.add(reminderModel);
       }
       userdata = json.decode(data);
       print(userdata);
     });
     _waterReminderModel = reminderValues.firstWhere((element) => element.type == "WATER", orElse: () => null);
     var st = DateTime(now.year, now.month, now.day, _waterReminderModel.hour, _waterReminderModel.minute);
     final et = DateTime(now.year, now.month, now.day, _waterReminderModel.hour, _waterReminderModel.minute).add(Duration(days: 2));
     final format = DateFormat('HH:mm');
     final dif = et.difference(st).inMinutes;
     var nextDrinkTime = DateTime(now.year, now.month, now.day, _waterReminderModel.hour, _waterReminderModel.minute).add(Duration(minutes: _waterReminderModel.frequency));
     var previousDrinkTime = DateTime(now.year, now.month, now.day, _waterReminderModel.hour, _waterReminderModel.minute).subtract(Duration(minutes: _waterReminderModel.frequency));
     print(previousDrinkTime);
     mainTime = format.format(st);
     nextTime = format.format(nextDrinkTime);
     previousTime = format.format(previousDrinkTime);
     Timer.periodic(Duration(minutes: _waterReminderModel.frequency), (timer) {
       if (!mounted) {
         return;
       }
       setState(() {
         mainTime = st.add(Duration(minutes: _waterReminderModel.frequency));
         nextTime = nextDrinkTime.add(Duration(minutes: _waterReminderModel.frequency));
         st = mainTime;
         nextDrinkTime = nextTime;
         previousTime  = st.subtract(Duration(minutes: _waterReminderModel.frequency));
         mainTime = format.format(mainTime);
         nextTime = format.format(nextTime);
         previousTime = format.format(previousTime);
       });
     });
   }

   @override
  void initState() {
     _getUserInfo();
    super.initState();

  }
//
   @override
   void dispose() {
     super.dispose();
   }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: MakeBottom(active: 'water', userData: userdata,),
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: ksecondaryColour,
        elevation: 0,
//        centerTitle: true,
        title: Text("Tracker",
          style: TextStyle(
              color: kprimaryColour
          ),
        ),
        actions: <Widget>[
          Padding(
              padding: const EdgeInsets.all(20.0),
              child: GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, "/setReminderScreen");
                  },
                  child: SvgPicture.asset("assets/images/clock.svg")
              )
          ),
        ],
      ),
      body: BlocBuilder<ReminderBloc, ReminderState>(
        builder: (context, state){
          if (state is ReminderLoading) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(child: CircularProgressIndicator()),
              ],
            );
          }

//          for (int i = _waterReminderModel.frequency; i <= dif; i = i + _waterReminderModel.frequency) {
//            print("*********");
//
//            mainTime = st.add(Duration(minutes: i));
//            final format = DateFormat('HH:mm');
//            mainTime = format.format(mainTime);
//               mainTime = st;
//            do {
//
//                Timer.periodic(Duration(seconds: 5), (timer) {
//                  mainTime = st.add(Duration(minutes: _waterReminderModel.frequency));
//                  st = mainTime;
//
//                  final format = DateFormat('HH:mm');
//                  mainTime = format.format(mainTime);
//                  print("0000000000000");
//                  print(mainTime);
//              });
//
//          } while(st.minute <= dif);

//          Timer mytimer = Timer.periodic(Duration(seconds: 5), (timer) {
//            mainTime = st.add(Duration(minutes: _waterReminderModel.frequency));
//            final format = DateFormat('HH:mm');
//            mainTime = format.format(mainTime);
//            print(mainTime);
//           });

          String formatTimeOfDay(int thour, int tminute) {
            final now = new DateTime.now();
            final dt = DateTime(now.year, now.month, now.day, thour, tminute);
            final format = DateFormat('HH:mm'); //"6:00 AM"
            return format.format(dt);
          }
          String formatInterval(int thour , int tminute) {
            final now = new DateTime.now();
            final dt = DateTime(now.year, now.month, now.day, thour, tminute);
            final format = DateFormat('HH:mm'); //"6:00 AM"
            return format.format(dt);
          }
//          Timer mytimer = Timer.periodic(Duration(seconds: 1), (timer) {
//            final now = new DateTime.now();
//           var dtime = formatInterval(_waterReminderModel.hour, _waterReminderModel.minute);
//            mainTime = DateTime(now.year, now.month, now.day, int.parse(dtime.split(":")[0]),int.parse(dtime.split(":")[0]));
//             setState(() {
//               final format = DateFormat('HH:mm'); //"6:00 AM"
//               print(format.format(mainTime));
//             });
//          });


          return SingleChildScrollView(
            child: Container(
//              height: MediaQuery.of(context).size.height,
              padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
              color: ksecondaryColour,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: ScreenUtil().setHeight(20.0),),
                 _waterReminderModel != null ?  Row(
                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                   children: [
                     Column(
                       children: [
                         Text(
                           "Prev", style: TextStyle(color: ksecondaryTextColour),),
                         Text(
                           previousTime, style: TextStyle(color: ksecondaryTextColour),)
                       ],
                     ),
                     Stack(
                       children: [
                         Padding(
                           padding: EdgeInsets.only(top: ScreenUtil().setWidth(60.0), left: ScreenUtil().setWidth(60.0)),
                           child: Column(
                             children: [
                               Text(mainTime, style: TextStyle(color: Colors.black,
                                   fontSize: ScreenUtil().setSp(20.0)),),
                               SizedBox(height: ScreenUtil().setHeight(20.0),),
                               Text("Next Drink",
                                 style: TextStyle(color: ksecondaryTextColour),),

                             ],
                           ),
                         ),
                         Container(
                             child: SvgPicture.asset("assets/images/circle.svg",)
                         ),

                       ],
                     ),
                     Column(
                       children: [
                         Text(
                           "Next", style: TextStyle(color: ksecondaryTextColour),),
                         Text(
                           nextTime, style: TextStyle(color: ksecondaryTextColour),)
                       ],
                     )
                   ],
                 ) :
                 Row(
                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                   children: [
                     Column(
                       children: [
                         Text(
                           "Prev", style: TextStyle(color: ksecondaryTextColour),),
                         Text(
                           "00:00", style: TextStyle(color: ksecondaryTextColour),)
                       ],
                     ),
                     Stack(
                       children: [
                         Padding(
                           padding: EdgeInsets.only(top: ScreenUtil().setWidth(60.0), left: ScreenUtil().setWidth(60.0)),
                           child: Column(
                             children: [
                               Text("00:00", style: TextStyle(color: Colors.black,
                                   fontSize: ScreenUtil().setSp(20.0)),),
                               SizedBox(height: ScreenUtil().setHeight(20.0),),
                               Text("Next Drink",
                                 style: TextStyle(color: ksecondaryTextColour),),

                             ],
                           ),
                         ),
                         Container(
                             child: SvgPicture.asset("assets/images/circle.svg",)
                         ),

                       ],
                     ),
                     Column(
                       children: [
                         Text(
                           "Next", style: TextStyle(color: ksecondaryTextColour),),
                         Text(
                           "00:00", style: TextStyle(color: ksecondaryTextColour),)
                       ],
                     )
                   ],
                 ),
                  SizedBox(height: ScreenUtil().setHeight(20.0),),
                  Padding(
                    padding: EdgeInsets.only(left: ScreenUtil().setWidth(24.0)),
                    child: Text("Reminders", style: TextStyle(color: Colors.black),),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(10.0),),
                  Center(
                    child: Container(
                      width: ScreenUtil().setWidth(370),
                      height: ScreenUtil().setHeight(200),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          color: ksecondaryColour,
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.25),
                              blurRadius: 20,
                              offset: Offset(0, 2), // Shadow position
                            ),
                          ]
                      ),
                      child: Column(
                        children: [
                          SizedBox(height: ScreenUtil().setHeight(10.0),),
                          SvgPicture.asset("assets/images/water_cup.svg"),
                          SizedBox(height: ScreenUtil().setHeight(10.0),),
                          SvgPicture.asset("assets/images/water_cup_1.svg"),
                          SizedBox(height: ScreenUtil().setHeight(10.0),),
                          SvgPicture.asset("assets/images/water_cup_1.svg"),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(20.0),),
                  Container(
                    child: ListView.builder(
                      itemCount: state.reminderData.length,
//                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemBuilder: (BuildContext context, int position){
                        return Column(
                          children: [
                            Container(
                              width: ScreenUtil().setWidth(370),
                              height: ScreenUtil().setHeight(150),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                  color: ksecondaryColour,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Color.fromRGBO(0, 0, 0, 0.25),
                                      blurRadius: 8,
                                      offset: Offset(0, 2), // Shadow position
                                    ),
                                  ]
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.all(16.0),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      children: [
                                        RichText(
                                            text: TextSpan(
                                              text: '${state.reminderData[position].hour}:${state.reminderData[position].minute} ',
                                              style: TextStyle(
                                                  fontSize: ScreenUtil().setSp(32.0),
                                                  fontWeight: FontWeight.w800,
                                                  fontFamily: 'Circular Std',
                                                  color: Color(0xFF222222)
                                              ),
                                              children: <TextSpan>[
                                                state.reminderData[position].hour > 12 ? TextSpan(text:
                                                'PM', style: TextStyle(
                                                    fontWeight: FontWeight.normal,
                                                    fontFamily: 'Circular Std',
                                                    color: ksecondaryTextColour,
                                                    fontSize: ScreenUtil().setSp(16.0)
                                                )
                                                ) : TextSpan(text:
                                                'AM', style: TextStyle(
                                                    fontWeight: FontWeight.normal,
                                                    fontFamily: 'Circular Std',
                                                    color: ksecondaryTextColour,
                                                    fontSize: ScreenUtil().setSp(16.0)
                                                )
                                                ),
                                              ],
                                            )
                                        ),
                                        Text(state.reminderData[position].type),
                                        RichText(
                                            text:
                                            TextSpan(
                                              text: '',
                                              style: TextStyle(
                                                  fontFamily: 'Circular Std',
                                                  color: ksecondaryTextColour
                                              ),
                                              children: <TextSpan>[
                                               state.reminderData[position].selectedDays.contains(0) ? TextSpan(
                                                  text: 'Mon ',
                                                  style: TextStyle(
                                                      fontFamily: 'Circular Std',
                                                      color: ksecondaryTextColour
                                                  ),
                                                ): TextSpan(),
                                                state.reminderData[position].selectedDays.contains(1)  ? TextSpan(
                                                  text: 'Tue ',
                                                  style: TextStyle(
                                                      fontFamily: 'Circular Std',
                                                      color: ksecondaryTextColour
                                                  ),
                                                ) : TextSpan(),
                                                state.reminderData[position].selectedDays.contains(2) ? TextSpan(
                                                  text: 'Wed ',
                                                  style: TextStyle(
                                                      fontFamily: 'Circular Std',
                                                      color: ksecondaryTextColour
                                                  ),
                                                ) : TextSpan(),
                                                state.reminderData[position].selectedDays.contains(3 ) ? TextSpan(
                                                  text: 'Thur ',
                                                  style: TextStyle(
                                                      fontFamily: 'Circular Std',
                                                      color: ksecondaryTextColour
                                                  ),
                                                ): TextSpan(),
                                                state.reminderData[position].selectedDays.contains(4 ) ? TextSpan(
                                                  text: 'Fri ',
                                                  style: TextStyle(
                                                      fontFamily: 'Circular Std',
                                                      color: ksecondaryTextColour
                                                  ),
                                                ): TextSpan(),
                                                state.reminderData[position].selectedDays.contains(5) ? TextSpan(
                                                  text: 'Sat ',
                                                  style: TextStyle(
                                                      fontFamily: 'Circular Std',
                                                      color: ksecondaryTextColour
                                                  ),
                                                ): TextSpan(),
                                                state.reminderData[position].selectedDays.contains(6) ? TextSpan(
                                                  text: 'Sun ',
                                                  style: TextStyle(
                                                      fontFamily: 'Circular Std',
                                                      color: ksecondaryTextColour
                                                  ),
                                                ): TextSpan()
                                              ],
                                            )
                                        ),
                                      ],
                                    ),
                                  ),
                                  Switch(
                                      value: true,
                                      activeColor: Colors.blue,
                                      activeTrackColor: kprimaryColour,
                                      inactiveThumbColor: kprimaryColour,
                                      inactiveTrackColor: Colors.green[100]
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(height: ScreenUtil().setHeight(10),)
                          ],
                        );
                      },
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(20.0),),
                  Container(child: NestedTabBar(challengeModel: state.challengeData,
                    pastChallengeModel: state.pastChallengeData,
                  )
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
class NestedTabBar extends StatefulWidget {
  List<ChallengeModel> challengeModel;
  List<ChallengeModel> pastChallengeModel;
  NestedTabBar( {this.challengeModel, this.pastChallengeModel});

  @override
  _NestedTabBarState createState() => _NestedTabBarState();
}
class _NestedTabBarState extends State<NestedTabBar>
    with TickerProviderStateMixin {
  TabController _nestedTabController;
   String daysValue;
   String hoursValue;
   String minuteValue;
   String secondValue;
  @override
  void initState() {
    super.initState();
    _nestedTabController = new TabController(length: 2, vsync: this);
  }
  @override
  void dispose() {
    super.dispose();
    _nestedTabController.dispose();
  }
  void _showDeleteDialog(BuildContext context, String challengeID) async {
    await showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
            child: Container(
              padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
              height: ScreenUtil().setHeight(250),
              width: ScreenUtil().setWidth(500),
              child: Column(
                children: [
                  Row(
                    children: [
                      SvgPicture.asset('assets/images/delete_ icon.svg'),
                      SizedBox(width: ScreenUtil().setWidth(10.0),),
                      Expanded(
                        child: Text("Are you sure you want to end this challenge",
                          overflow: TextOverflow.visible,
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: ScreenUtil().setHeight(50.0),),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: (){
                          Navigator.of(context).pop();
                        },
                        child: Container(
                          height: ScreenUtil().setHeight(45.0),
                          width: ScreenUtil().setWidth(126.0),
                          padding: EdgeInsets.all(ScreenUtil().setWidth(8.0)),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            border: Border.all(color: ksecondaryTextColour, width: 1.0 ),
                            color: ksecondaryColour,
                          ),
                          child: Text("Cancel",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Color(0xFF222222),
//                              fontWeight: FontWeight.bold,
                                fontFamily: "Circular Std"
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(30),),
                      GestureDetector(
                        onTap: () async {
                          try {
                            final bool success = await ReminderRepository.endChallenge(challengeID: challengeID);
                            if(success == true){
                              Navigator.of(context).pop();
                             _showCongratulationsDialog(context);
                            }
                          } catch (e) {
                            Navigator.of(context).pop();
                           _showlostDialog(context);
                          }
                        },
                        child: Container(
                          height: ScreenUtil().setHeight(45.0),
                          width: ScreenUtil().setWidth(126.0),
                          padding: EdgeInsets.all(ScreenUtil().setWidth(8.0)),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Color(0xFFA91C1C
                            ),
                          ),
                          child: Text("Yes",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),

                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          );
        });
  }
  void _showlostDialog(BuildContext context) async {
    await showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
            child: Container(
              padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
              height: ScreenUtil().setHeight(250),
              width: ScreenUtil().setWidth(500),
              child: Column(
                children: [
                  SizedBox(height: ScreenUtil().setHeight(50.0),),
                  Text('Challenge Ended', style: TextStyle(fontWeight: FontWeight.bold,),),
                  SizedBox(height: ScreenUtil().setHeight(10.0),),
                  SvgPicture.asset('assets/images/delete_emoji.svg'),
                  SizedBox(height: ScreenUtil().setHeight(10.0),),
                  Text('It is so sad to lose you',)
                ],
              ),
            ),
          );
        });
  }
  void _showCongratulationsDialog(BuildContext context) async {
    await showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
            child: Container(
              padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
              height: ScreenUtil().setHeight(250),
              width: ScreenUtil().setWidth(500),
              child: Column(
                children: [
                  SizedBox(height: ScreenUtil().setHeight(50.0),),
                  Text('Congratulations!!!', style: TextStyle(fontWeight: FontWeight.bold,),),
                  SizedBox(height: ScreenUtil().setHeight(10.0),),
                  SvgPicture.asset("assets/images/success_emoji.svg"),
                  SizedBox(height: ScreenUtil().setHeight(10.0),),
                  Text('Challenge completed')
                ],
              ),
            ),
          );
        });
  }
  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(16),
            right: ScreenUtil().setWidth(16),
          ),
          child: TabBar(
            controller: _nestedTabController,
            indicatorColor: kprimaryColour,
            labelColor: kprimaryColour,
            unselectedLabelColor: Colors.black,
//            isScrollable: true,
            tabs: <Widget>[
              Tab(
                child: Text("Active challenges"),
              ),
              Tab(
                child: Text("Past challenges"),
              ),

            ],
          ),
        ),
        Container(
          height: screenHeight * 0.30,
//          margin: EdgeInsets.only(left: 16.0, right: 16.0),
          child: TabBarView(
            controller: _nestedTabController,
            children: <Widget>[
            widget.challengeModel.length != null ? ListView.builder(
                itemCount: widget.challengeModel.length,
                itemBuilder: (BuildContext context, int position){
                  daysValue = DateTime.parse(widget.challengeModel[position].endDate).difference(
                      DateTime.parse(widget.challengeModel[position].startDate)
                  ).inDays.toString();
                  hoursValue = DateTime.parse(widget.challengeModel[position].endDate).difference(
                      DateTime.parse(widget.challengeModel[position].startDate)
                  ).inHours.toString();
                 minuteValue = DateTime.parse(widget.challengeModel[position].endDate).difference(
                     DateTime.parse(widget.challengeModel[position].startDate)
                 ).inMinutes.toString();
                 secondValue = DateTime.parse(widget.challengeModel[position].endDate).difference(
                     DateTime.parse(widget.challengeModel[position].startDate)
                 ).inSeconds.toString();
                 print( DateTime.parse(widget.challengeModel[position].endDate).difference(
                     DateTime.parse(widget.challengeModel[position].startDate)
                 ));
                  return Column(
                    children: [
                      SizedBox(height: ScreenUtil().setHeight(20),),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(widget.challengeModel[position].challengeGroup.name),
                          Text('Days Left:', style: TextStyle(color: ksecondaryTextColour),),
                          GestureDetector(
                               onTap:(){
                                _showDeleteDialog(context, widget.challengeModel[position].sId);
                             },
                              child: Text('End', style: TextStyle(color: Colors.red),))
                        ],
                      ),
                      Column(
//                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Center(
                            child: Text('${daysValue}:   ${hoursValue}:   ${minuteValue}:   ${secondValue}',
                            style: TextStyle(color: kprimaryColour),),
                          ),
                          Center(child: Text('Days    Hr     Mins   Secs'))
                        ],
                      ),
                    ],
                  );
                },
              ): SizedBox(),
              widget.pastChallengeModel.length != null ? Column(
                children: [
                  SizedBox(height: ScreenUtil().setHeight(20),),
                  ListView.builder(
                    itemCount: widget.pastChallengeModel.length,
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int position){
                      return Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(widget.pastChallengeModel[position].challengeGroup.name),
                              Text(widget.pastChallengeModel[position].status)
                            ],
                          ),
                          SizedBox(height: ScreenUtil().setHeight(10),),
                        ],
                      );
                    }
                  ),
                ],
              ): SizedBox()
            ],
          ),
        )
      ],
    );
  }
}

