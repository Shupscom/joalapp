import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:joalapp/components/constants.dart';
import 'package:joalapp/repository/subscription_repository.dart';
import 'package:provider/provider.dart';

class LandingScreen extends StatefulWidget {
  @override
  _LandingScreenState createState() => _LandingScreenState();
}


class _LandingScreenState extends State<LandingScreen> {
  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
//    var test = Provider.of<String>(context);
//    print(test);
    sizess(context);
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(ScreenUtil().setWidth(16.0)),
        color: Theme.of(context).backgroundColor,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height: ScreenUtil().setHeight(200.0),),
            RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  text: 'Welcome to ',
                  style: TextStyle(fontSize: ScreenUtil().setSp(18.0), fontWeight: FontWeight.w800, fontFamily: 'Circular Std',
                   color: Color(0xFF222222)
                  ),
                  children: <TextSpan>[
                    TextSpan(text: 'Jaol ', style: TextStyle(fontWeight: FontWeight.w800,
                        fontFamily: 'Circular Std',
                        color: kprimaryColour,
                        fontSize: ScreenUtil().setSp(18.0))),
                    TextSpan(text: 'Smart Diet Planner & Calories Counter',
                        style: TextStyle(
//                           fontWeight: FontWeight.bold,
                            fontFamily: 'Circular Std',
                            color: Color(0xFF222222),
                            fontSize: ScreenUtil().setSp(18.0))),
                  ],
                )
            ),

            SizedBox(height: ScreenUtil().setHeight(40.0),),
            Image.asset("assets/images/jaolsplashlogo.png"),
            SizedBox(height: ScreenUtil().setHeight(10.0),),
            SvgPicture.asset('assets/images/Jaoltext.svg', height: ScreenUtil().setHeight(50.0),),
            SizedBox(height: ScreenUtil().setHeight( 50.0),),
            Text("Achieve Your Desired Weight Objective with the Right Diet Plan",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: ScreenUtil().setSp(16.0),
                color: Color(0xFF666A86),
                fontFamily: 'Circular Std',
              ),
            ),
            SizedBox(height: ScreenUtil().setHeight( 70.0),),
            Material(
              elevation: 2.0,
              color: kprimaryColour,
              borderRadius: BorderRadius.circular(20.0),
              child: MaterialButton(
                onPressed: () {
                    Navigator.pushNamed(context, '/signupScreen');
                },
                minWidth: ScreenUtil().setWidth(380.0),
                height: ScreenUtil().setHeight(42.0),
                child: Text(
                  "Get Started",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: ScreenUtil().setSp(18.0),
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Circular Std',
                  ),
                ),
              ),
            ),
          ],
        )
      ),
    );
  }
}



