import 'package:equatable/equatable.dart';

abstract class NavigationState extends Equatable{
  @override
  List<Object> get props => [];
}
class NavigationInitial extends NavigationState {}