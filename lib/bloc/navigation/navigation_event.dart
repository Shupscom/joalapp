import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class NavigationEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class NavigationActionPop extends NavigationEvent{}