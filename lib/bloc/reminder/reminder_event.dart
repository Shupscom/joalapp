import 'package:equatable/equatable.dart';


abstract class ReminderEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class LoadReminderButton extends ReminderEvent{}