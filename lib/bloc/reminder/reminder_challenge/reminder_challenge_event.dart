import 'package:equatable/equatable.dart';
import 'package:joalapp/model/self_challenge_model.dart';

abstract class ReminderChallengeEvent extends Equatable{
  @override

  List<Object> get props => [];
}

//class ReminderChallengeButton extends ReminderChallengeEvent{
//  String challengeGroup;
//  String startDate;
//  String endDate;
//
//  ReminderChallengeButton({this.challengeGroup, this.startDate, this.endDate,});
//
//  @override
//  List<Object> get props => [challengeGroup, startDate, endDate];
//}

class ReminderChallengeButton extends ReminderChallengeEvent{
  List<SelfChallengeModel> challengeData;

  ReminderChallengeButton({this.challengeData});

  @override
  List<Object> get props => [challengeData];
}