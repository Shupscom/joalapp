import 'dart:convert';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/reminder/reminder_challenge/reminder_challenge_event.dart';
import 'package:joalapp/bloc/reminder/reminder_challenge/reminder_challenge_state.dart';
import 'package:joalapp/repository/reminder_repository.dart';

class ReminderChallengeBloc extends Bloc<ReminderChallengeEvent,ReminderChallengeState> {

  @override
  // TODO: implement initialState
  ReminderChallengeState get initialState => ReminderChallengeInitial();
  @override
  Stream<ReminderChallengeState> mapEventToState(ReminderChallengeEvent event) async* {
    if (event is ReminderChallengeButton) {
      try {
        var reminder = await ReminderRepository.reminderChallenge(
         challenges: event.challengeData
        );
        var reminderData = json.decode(reminder);
        if (reminderData['success'] == true) {
          yield ReminderChallengeSuccess();
        } else {
          yield ReminderChallengeFailure(
              error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield ReminderChallengeFailure(error: err.message);
      }
    }
  }
}