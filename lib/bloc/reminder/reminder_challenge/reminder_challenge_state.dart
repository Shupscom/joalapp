import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class ReminderChallengeState extends Equatable{
  @override
  List<Object> get props => [];
}

class ReminderChallengeInitial extends ReminderChallengeState {}

class ReminderChallengeLoading extends ReminderChallengeState {}

class ReminderChallengeSuccess extends ReminderChallengeState {

}

class ReminderChallengeFailure extends ReminderChallengeState {
  final String error;

  ReminderChallengeFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ReminderChallengeFailure { error: $error }';

}
