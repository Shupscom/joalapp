import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class ReminderMealState extends Equatable{
  @override
  List<Object> get props => [];
}

class ReminderMealInitial extends ReminderMealState {}

class ReminderMealLoading extends ReminderMealState {}

class ReminderMealSuccess extends ReminderMealState {

}

class ReminderMealFailure extends ReminderMealState {
  final String error;

  ReminderMealFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ReminderMealFailure { error: $error }';

}
