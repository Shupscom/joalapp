import 'package:equatable/equatable.dart';

abstract class ReminderMealEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class ReminderMealButton extends ReminderMealEvent{
  String type;
  int hour;
  int minute;
  List<int> days;
  String meal;

  ReminderMealButton({this.type, this.hour, this.minute, this.days, this.meal});

  @override
  List<Object> get props => [type,hour,minute, days, meal];

}