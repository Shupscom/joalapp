import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/reminder/reminder_meal/reminder_meal_event.dart';
import 'package:joalapp/bloc/reminder/reminder_meal/reminder_meal_state.dart';
import 'package:joalapp/repository/reminder_repository.dart';

class ReminderMealBloc extends Bloc<ReminderMealEvent, ReminderMealState> {
  @override
  // TODO: implement initialState
  ReminderMealState get initialState => ReminderMealInitial();

  @override
  Stream<ReminderMealState> mapEventToState(ReminderMealEvent event) async* {
    if (event is ReminderMealButton) {
      try {
        var reminder = await ReminderRepository.reminderMealSchedule(
            type: event.type,
            hour: event.hour,
            minute: event.minute,
            days: event.days,
            meal: event.meal);
        var reminderData = json.decode(reminder);

//        }
        if (reminderData['success'] == true) {
          yield ReminderMealSuccess();
        } else {
          yield ReminderMealFailure(
              error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield ReminderMealFailure(error: err.message);
      }
    }
  }
}
