import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class ReminderWaterState extends Equatable{
  @override
  List<Object> get props => [];
}

class ReminderWaterInitial extends ReminderWaterState {}

class ReminderWaterLoading extends ReminderWaterState {}

class ReminderWaterSuccess extends ReminderWaterState {

}

class ReminderWaterFailure extends ReminderWaterState {
  final String error;

  ReminderWaterFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ReminderWaterFailure { error: $error }';

}
