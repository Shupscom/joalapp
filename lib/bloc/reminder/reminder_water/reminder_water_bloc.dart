import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/reminder/reminder_water/reminder_water_event.dart';
import 'package:joalapp/bloc/reminder/reminder_water/reminder_water_state.dart';
import 'package:joalapp/repository/reminder_repository.dart';

class ReminderWaterBloc extends Bloc<ReminderWaterEvent, ReminderWaterState> {
  @override
  // TODO: implement initialState
  ReminderWaterState get initialState => ReminderWaterInitial();

  @override
  Stream<ReminderWaterState> mapEventToState(ReminderWaterEvent event) async* {
    if (event is ReminderWaterButton) {
      try {
        var reminder = await ReminderRepository.reminderSchedule(
            type: event.type,
            hour: event.hour,
            minute: event.minute,
            days: event.days,
            frequency: event.frequency);
        var reminderData = json.decode(reminder);
        if (reminderData['success'] == true) {
          yield ReminderWaterSuccess();
        } else {
          yield ReminderWaterFailure(
              error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield ReminderWaterFailure(error: err.message);
      }
    }
  }
}
