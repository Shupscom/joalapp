import 'package:equatable/equatable.dart';

abstract class ReminderWaterEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class ReminderWaterButton extends ReminderWaterEvent{
  String type;
  int hour;
  int minute;
  List<int> days;
  int frequency;

  ReminderWaterButton({this.type, this.hour, this.minute, this.days, this.frequency});

  @override
  List<Object> get props => [type,hour,minute, days, frequency];
}