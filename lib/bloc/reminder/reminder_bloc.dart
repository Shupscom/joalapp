
import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/reminder/reminder_event.dart';
import 'package:joalapp/bloc/reminder/reminder_state.dart';
import 'package:joalapp/model/challenge_model.dart';
import 'package:joalapp/model/reminder_model.dart';
import 'package:joalapp/repository/reminder_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ReminderBloc extends Bloc<ReminderEvent,ReminderState> {

  @override
  // TODO: implement initialState
  ReminderState get initialState => ReminderLoading();

  @override
  Stream<ReminderState> mapEventToState(ReminderEvent event) async* {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    if(event is LoadReminderButton){
      yield ReminderLoading();
      try {
        var reminders = await ReminderRepository.getAllReminder();
        var challenges = await ReminderRepository.getActiveChallenge();
        var pastChallenges = await ReminderRepository.getPastChallenge();
        var reminderData = json.decode(reminders);
        print("**********");
        print(reminderData);
        var challengeData = json.decode(challenges);
        var pastChallengeData = json.decode(pastChallenges);
        sharedPreferences.setString("reminderValues", json.encode(reminderData['data']));
        List<ReminderModel> reminderValues = [];
        for(var item in reminderData['data']){
          ReminderModel reminderModel = ReminderModel.fromJson(item);
          reminderValues.add(reminderModel);
        }
        List<ChallengeModel> challengeValues = [];
        for(var item in challengeData['data']){
          ChallengeModel challengeModel = ChallengeModel.fromJson(item);
          challengeValues.add(challengeModel);
        }
        List<ChallengeModel> pastChallengeValues = [];
        for(var item in pastChallengeData['data']){
          ChallengeModel pastChallengeModel = ChallengeModel.fromJson(item);
          pastChallengeValues.add(pastChallengeModel);
        }
        if (reminderData['success'] == true && challengeData['success'] == true && pastChallengeData['success'] == true) {
          yield ReminderInitial(reminderData: reminderValues, challengeData: challengeValues, pastChallengeData: pastChallengeValues);
        } else {
          yield ReminderFailure(error: 'Something very weird just happened');
        }
      } catch (err) {

        yield ReminderFailure(error: err.message);
      }

    }
  }
}