import 'package:equatable/equatable.dart';
import 'package:joalapp/model/challenge_model.dart';
import 'package:joalapp/model/reminder_model.dart';
import 'package:meta/meta.dart';

abstract class ReminderState extends Equatable{
  List<ReminderModel> reminderData;
  List<ChallengeModel> challengeData;
  List<ChallengeModel> pastChallengeData;

  ReminderState({@required this.reminderData, this.challengeData, this.pastChallengeData});
  @override
  List<Object> get props => [reminderData, challengeData, pastChallengeData];
}

class ReminderInitial extends ReminderState {
  List<ReminderModel> reminderData;
  List<ChallengeModel> challengeData;
  List<ChallengeModel> pastChallengeData;

  ReminderInitial({@required this.reminderData, this.challengeData, this.pastChallengeData});

  @override
  List<Object> get props => [reminderData, challengeData, pastChallengeData];

  @override
  String toString() => 'ReminderInitial { reminderData: $reminderData, challengeData: $challengeData, pastChallengeData: $pastChallengeData  }';

}

class ReminderLoading extends ReminderState {}

class ReminderSuccess extends ReminderState {
  List<ReminderModel> reminderData;

  ReminderSuccess({@required this.reminderData});

  @override
  List<Object> get props => [reminderData];

  @override
  String toString() => 'ReminderSuccess { nuggetData: $reminderData }';
}

class ReminderFailure extends ReminderState {
  final String error;

  ReminderFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ReminderFailure { error: $error }';

}