import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/weight/weight_event.dart';
import 'package:joalapp/bloc/weight/weight_state.dart';
import 'package:joalapp/repository/auth_repository.dart';

class WeightBloc extends Bloc<WeightEvent,WeightState> {

  @override
  // TODO: implement initialState
  WeightState get initialState => WeightInitial();

  @override
  Stream<WeightState> mapEventToState(WeightEvent event) async* {
    if(event is WeightButton){
      yield WeightLoading();
      try {
        var userModel= await AuthRepository.weightUpdate(
            weight: event.weight,
            weightUnit: event.weightUnit
        );
        var value = json.decode(userModel);
        if (value['success'] == true) {
          yield WeightSuccess(userData: value['data']);
        } else {
          yield WeightFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield WeightFailure(error: err.message);
      }

    }
  }
}