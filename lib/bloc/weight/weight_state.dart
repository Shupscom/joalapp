import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class WeightState extends Equatable{
  @override
  List<Object> get props => [];
}

class WeightInitial extends WeightState {}

class WeightLoading extends WeightState {}

class WeightSuccess extends WeightState {
  Map<String,dynamic> userData;

  WeightSuccess({@required this.userData});

  @override
  List<Object> get props => [userData];

  @override
  String toString() => 'WeightSuccess { error: $userData }';
}

class WeightFailure extends WeightState {
  final String error;

  WeightFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'WeightFailure { error: $error }';
}