import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class WeightEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class WeightButton extends WeightEvent{
  final int weight;
  final String weightUnit;
  WeightButton({
    @required this.weight,
    @required this.weightUnit
  });

  @override
  List<Object> get props => [weight];
}