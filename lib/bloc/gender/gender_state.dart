import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class GenderState extends Equatable{
  @override
  List<Object> get props => [];
}

class GenderInitial extends GenderState {}

class GenderLoading extends GenderState {}

class GenderSuccess extends GenderState {
  Map<String,dynamic> userData;

  GenderSuccess({@required this.userData});

  @override
  List<Object> get props => [userData];

  @override
  String toString() => 'GenderSuccess { userData: $userData }';

}

class GenderFailure extends GenderState {
  final String error;

  GenderFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'GenderFailure { error: $error }';
}