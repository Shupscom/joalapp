import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/gender/gender_event.dart';
import 'package:joalapp/bloc/gender/gender_state.dart';
import 'package:joalapp/repository/auth_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GenderBloc extends Bloc<GenderEvent,GenderState> {
  final BuildContext context;
  GenderBloc(this.context) : assert(context != null);

  @override
  // TODO: implement initialState
  GenderState get initialState => GenderInitial();

  @override
  Stream<GenderState> mapEventToState(GenderEvent event) async* {
    if(event is GenderButton){
      yield GenderLoading();
      try {
        var userModel= await AuthRepository.genderUpdate(
            gender: event.gender
        );
        var value = json.decode(userModel);
        if (value['success'] == true) {
          yield GenderSuccess(userData: value['data']);
        } else {
          yield GenderFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield GenderFailure(error: err.message);
      }

    }

  }
}