import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class GenderEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class GenderButton extends GenderEvent{
  final String gender;
  GenderButton({
    @required this.gender,
  });

  @override
  List<Object> get props => [gender];
}