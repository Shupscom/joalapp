import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class RegisterState extends Equatable{
  @override
  List<Object> get props => [];
}

class RegisterInitial extends RegisterState {}

class RegisterLoading extends RegisterState {}

class RegisterSuccess extends RegisterState {
   Map<String,dynamic> userData;

    RegisterSuccess({@required this.userData});

   @override
   List<Object> get props => [userData];

   @override
   String toString() => 'RegisterSuccess { userData: $userData }';
}

class RegisterFailure extends RegisterState {
  final String error;

  RegisterFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'RegisterFailure { error: $error }';
}

//class GenderLoading extends RegisterState {}
//
//class GenderSuccess extends RegisterState {
//  String email;
//  String password;
//  String gender;
//
//  GenderSuccess({
//    @required this.email,
//    @required this.password,
//    @required this.gender
//  });
//
//  @override
//  List<Object> get props => [email,password,gender];
//
//  @override
//  String toString() => 'GenderSuccess { email: $email, password: $password, gender: $gender }';
//}
//
//class GenderFailure extends RegisterState {
//  final String error;
//
//  GenderFailure({@required this.error});
//
//  @override
//  List<Object> get props => [error];
//
//  @override
//  String toString() => 'GenderFailure { error: $error }';
//}
class EmailBack extends RegisterState {}

class EmailLoading extends RegisterState {}

class EmailSuccess extends RegisterState {
  Map<String,dynamic> userData;

  EmailSuccess({
    @required this.userData,

  });
  @override
  List<Object> get props => [userData];

  @override
  String toString() => 'EmailSuccess { userData: $userData}';
}
class EmailFailure extends RegisterState {
  final String error;

  EmailFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'EmailFailure { error: $error }';
}

class DateBirthLoading extends RegisterState {}

class DateBirthSuccess extends RegisterState {
  String email;
  String password;
  String gender;
  String dob;

  DateBirthSuccess({
    @required this.email,
    @required this.password,
    @required this.gender,
    @required this.dob
  });

  @override
  List<Object> get props => [email,password,gender,dob];

  @override
  String toString() => 'DateBirthSuccess { email: $email, password: $password, gender: $gender, dob: $dob }';
}

class HeightLoading extends RegisterState {}

class HeightSuccess extends RegisterState {
  String email;
  String password;
  String gender;
  String dob;
  String height;

  HeightSuccess({
    @required this.email,
    @required this.password,
    @required this.gender,
    @required this.dob,
    @required this.height
  });

  @override
  List<Object> get props => [email,password,gender,dob,height];

  @override
  String toString() => 'DateBirthSuccess { email: $email, password: $password, gender: $gender, dob: $dob, height: $height }';
}

class WeightLoading extends RegisterState {}

class WeightSuccess extends RegisterState {
  String email;
  String password;
  String gender;
  String dob;
  String height;
  String weight;

  WeightSuccess({
    @required this.email,
    @required this.password,
    @required this.gender,
    @required this.dob,
    @required this.height,
    @required this.weight
  });

  @override
  List<Object> get props => [email,password,gender,dob,height,weight];

  @override
  String toString() => 'DateBirthSuccess { email: $email, password: $password, gender: $gender, '
      'dob: $dob, height: $height, weight: $weight }';
}

class ActivityLoading extends RegisterState {}

class ActivitySuccess extends RegisterState {
  String email;
  String password;
  String gender;
  String dob;
  String height;
  String weight;
  String activity;

  ActivitySuccess({
    @required this.email,
    @required this.password,
    @required this.gender,
    @required this.dob,
    @required this.height,
    @required this.weight,
    @required this.activity

  });

  @override
  List<Object> get props => [email,password,gender,dob,height,weight,activity];

  @override
  String toString() => 'DateBirthSuccess { email: $email, password: $password, gender: $gender, '
      'dob: $dob, height: $height, weight: $weight, activity: $activity}';
}

class WeightGainLoading extends RegisterState {}

class WeightGainSuccess extends RegisterState {}
