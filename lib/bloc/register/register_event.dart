import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class RegisterEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class RegisterButton extends RegisterEvent{
  final BuildContext context;
  final String email;
  final String password;
//  final String conPassword;
//  final String height;
//  final String weight;
//  final String gender;
//  final String weightGoal;
//  final String activityLevel;
//  final  int age;

  RegisterButton({
    @required this.context,
    @required this.email,
    @required this.password,
//    @required this.conPassword,
//    @required this.height,
//    @required this.weight,
//    @required this.gender,
//    @required this.weightGoal,
//    @required this.activityLevel,
//    @required this.age
  });


  @override
  List<Object> get props => [email, password,
//    conPassword,height,weight,gender,
//    weightGoal, activityLevel, age
  ];
}
class EmailButton extends RegisterEvent{
  final BuildContext context;
  final String email;
  final String password;
  EmailButton({
    @required this.context,
    @required this.email,
    @required this.password
  });

  @override
  List<Object> get props => [email,password];
}
class RegisterFacebookButton extends RegisterEvent{}

class RegisterGoogleButton extends RegisterEvent{}