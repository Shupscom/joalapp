import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:joalapp/bloc/register/register_event.dart';
import 'package:joalapp/bloc/register/register_state.dart';
import 'package:joalapp/repository/auth_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final BuildContext context;
  GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: <String>[
      'email',
      'https://www.googleapis.com/auth/contacts.readonly',
    ],
  );

  RegisterBloc(this.context) : assert(context != null);

  @override
  // TODO: implement initialState
  RegisterState get initialState => RegisterInitial();

  @override
  Stream<RegisterState> mapEventToState(RegisterEvent event) async* {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    final FacebookLogin facebookSignIn = new FacebookLogin();
    facebookSignIn.loginBehavior = FacebookLoginBehavior.webOnly;
    GoogleSignInAccount _currentUser;
    if (event is RegisterButton) {
      yield RegisterLoading();
      try {
        var userModel = await AuthRepository.register(
          email: event.email,
          password: event.password,
        );
        var value = json.decode(userModel);
        sharedPreferences.setString("token", value['token']);
        if (value['success'] == true) {
          yield RegisterSuccess(userData: value['user']);
        } else {
          yield RegisterFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield RegisterFailure(error: err.message);
      }
    }
    if (event is EmailButton) {
      yield EmailLoading();
      try {
        var userModel = await AuthRepository.register(
          email: event.email,
          password: event.password,
        );
        var value = json.decode(userModel);
        sharedPreferences.setString("token", value['data']['token']);
        var userData = value['data']['user'];
        print(userData['isEmailVerified']);
        if (value['success'] == true) {
          yield EmailSuccess(userData: userData);
        } else {
          yield EmailFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield EmailFailure(error: err.message);
      }
    }
    if (event is RegisterFacebookButton) {
      final FacebookLoginResult result = await facebookSignIn.logIn(['email']);
      switch (result.status) {
        case FacebookLoginStatus.loggedIn:
          final FacebookAccessToken accessToken = result.accessToken;
          var graphResponse = await http.get(
              'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token='
              '${accessToken.token}');
          var profileFaceebook = json.decode(graphResponse.body);
          yield EmailLoading();
          try {
            var userModel = await AuthRepository.socialauth(
                email: profileFaceebook['email'],
                authType: "social",
                authSocialPlatform: "Facebook",
                authSocialId: profileFaceebook['id']);
            var value = json.decode(userModel);
            sharedPreferences.setString("token", value['data']['token']);
            var userData = value['data']['user'];
            if (value['success'] == true) {
              yield EmailSuccess(userData: userData);
            } else {
              yield EmailFailure(error: 'Something very weird just happened');
            }
          } catch (err) {
            print(err);
            yield EmailFailure(error: err.message);
          }
          break;
        case FacebookLoginStatus.cancelledByUser:
          break;
        case FacebookLoginStatus.error:
          break;
      }
    }
    if (event is RegisterGoogleButton) {
      try {
        var here = await _googleSignIn.signIn();
        print(here);
        yield EmailLoading();
        var userModel = await AuthRepository.socialauth(
            email: here.email,
            authType: "social",
            authSocialPlatform: "Google",
            authSocialId: here.id);
        var value = json.decode(userModel);
        sharedPreferences.setString("token", value['data']['token']);
        var userData = value['data']['user'];
        if (value['success'] == true) {
          yield EmailSuccess(userData: userData);
        } else {
          yield EmailFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield EmailFailure(error: err.message);
      }
    }
  }
}
