import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class HeightEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class HeightButton extends HeightEvent{
  final int height;
  final String heightUnit;


  HeightButton({
    @required this.height,
    @required this.heightUnit
  });

  @override
  List<Object> get props => [height,heightUnit];
}