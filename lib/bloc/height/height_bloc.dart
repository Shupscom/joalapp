import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:joalapp/bloc/height/height_event.dart';
import 'package:joalapp/bloc/height/height_state.dart';
import 'package:joalapp/repository/auth_repository.dart';


class HeightBloc extends Bloc<HeightEvent,HeightState> {

  @override
  // TODO: implement initialState
  HeightState get initialState => HeightInitial();

  @override
  Stream<HeightState> mapEventToState(HeightEvent event) async* {
    if(event is HeightButton){
      yield HeightLoading();
      try {
        var userModel= await AuthRepository.heightUpdate(
            height: event.height,
            heightUnit: event.heightUnit
        );
        var value = json.decode(userModel);
        if (value['success'] == true) {
          yield HeightSuccess(userData: value['data']);
        } else {
          yield HeightFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield HeightFailure(error: err.message);
      }

    }
  }
}