import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class HeightState extends Equatable{
  @override
  List<Object> get props => [];
}

class HeightInitial extends HeightState {}

class HeightLoading extends HeightState {}

class HeightSuccess extends HeightState {
  Map<String,dynamic> userData;

  HeightSuccess({@required this.userData});

  @override
  List<Object> get props => [userData];

  @override
  String toString() => 'HeightSuccess { error: $userData }';
}

class HeightFailure extends HeightState {
  final String error;

  HeightFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'HeightFailure { error: $error }';
}