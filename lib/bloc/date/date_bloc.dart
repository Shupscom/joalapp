import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/date/date_event.dart';
import 'package:joalapp/bloc/date/date_state.dart';
import 'package:joalapp/repository/auth_repository.dart';

class DateBloc extends Bloc<DateEvent,DateState> {

  @override
  // TODO: implement initialState
  DateState get initialState => DateInitial();

  @override
  Stream<DateState> mapEventToState(DateEvent event) async* {
    if(event is DateButton){
      yield DateLoading();
      try {
        var userModel= await AuthRepository.dobUpdate(
            dob: event.dob
        );
        var value = json.decode(userModel);
        if (value['success'] == true) {
          yield DateSuccess(userData: value['data']);
        } else {
          yield DateFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield DateFailure(error: err.message);
      }

    }
  }
}