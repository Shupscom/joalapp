import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class DateState extends Equatable{
  @override
  List<Object> get props => [];
}

class DateInitial extends DateState {}

class DateLoading extends DateState {}

class DateSuccess extends DateState {
  Map<String,dynamic> userData;

  DateSuccess({@required this.userData});

  @override
  List<Object> get props => [userData];

  @override
  String toString() => 'DateSuccess { userData: $userData }';
}

class DateFailure extends DateState {
  final String error;

  DateFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'DateFailure { error: $error }';
}