import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class DateEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class DateButton extends DateEvent{
  final String dob;
  DateButton({
    @required this.dob,
  });
  @override
  List<Object> get props => [dob];
}