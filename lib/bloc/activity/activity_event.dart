import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class ActivityEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class ActivityButton extends ActivityEvent{
  final String activity;
  ActivityButton({
    @required this.activity,
  });

  @override
  List<Object> get props => [activity];
}