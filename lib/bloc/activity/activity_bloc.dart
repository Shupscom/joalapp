import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/activity/activity_event.dart';
import 'package:joalapp/bloc/activity/activity_state.dart';
import 'package:joalapp/repository/auth_repository.dart';

class ActivityBloc extends Bloc<ActivityEvent,ActivityState> {

  @override
  // TODO: implement initialState
  ActivityState get initialState => ActivityInitial();

  @override
  Stream<ActivityState> mapEventToState(ActivityEvent event) async* {
    if(event is ActivityButton){
      yield ActivityLoading();
      try {
        var userModel= await AuthRepository.activityUpdate(
            activityLevel: event.activity
        );
        var value = json.decode(userModel);
        if (value['success'] == true) {
          yield ActivitySuccess(userData: value['data']);
        } else {
          yield ActivityFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield ActivityFailure( error: err.message);
      }

    }
  }
}