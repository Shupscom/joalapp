import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class ActivityState extends Equatable{
  @override
  List<Object> get props => [];
}

class ActivityInitial extends ActivityState {}

class ActivityLoading extends ActivityState {}

class ActivitySuccess extends ActivityState {
  Map<String,dynamic> userData;

  ActivitySuccess({@required this.userData});

  @override
  List<Object> get props => [userData];

  @override
  String toString() => 'GenderSuccess { userData: $userData }';
}

class ActivityFailure extends ActivityState {
  final String error;

  ActivityFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ActivityFailure { error: $error }';
}