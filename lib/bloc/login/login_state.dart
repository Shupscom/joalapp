import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class LoginState extends Equatable {
  const LoginState();

  @override
  List<Object> get props => [];
}

class LoginInitial extends LoginState {}

class LoginLoading extends LoginState {}

class LoginSuccess extends LoginState{
  final String token;
  final Map<String,dynamic> userData;
  LoginSuccess(
  {@required this.token, @required this.userData}
  );

  @override
  List<Object> get props => [token, userData];

  @override
  String toString() => 'LoginSuccess { token: $token, userData: $userData }';

}

class LoginFailure extends LoginState {
  final error;

  const LoginFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LoginFailure { error: $error }';
}