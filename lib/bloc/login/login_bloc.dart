import 'dart:convert';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:joalapp/bloc/authentication/authenticate_bloc.dart';
import 'package:joalapp/bloc/login/login_event.dart';
import 'package:joalapp/bloc/login/login_state.dart';
import 'package:joalapp/repository/auth_repository.dart';
import 'package:http/http.dart' as http;
import 'package:joalapp/repository/reminder_repository.dart';
import 'package:joalapp/repository/subscription_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  AuthenticationBloc authenticationBloc = AuthenticationBloc();

  @override
  // TODO: implement initialState
  LoginState get initialState => LoginInitial();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    final FacebookLogin facebookSignIn = new FacebookLogin();
    facebookSignIn.loginBehavior = FacebookLoginBehavior.webOnly;
    GoogleSignIn _googleSignIn = GoogleSignIn(
      scopes: <String>[
        'email',
        'https://www.googleapis.com/auth/contacts.readonly',
      ],
    );

    // TODO: implement mapEventToState
    if (event is LoginButtonPressed) {
      yield LoginLoading();
      try {
        var loginData =
            await AuthRepository.authenticate(event.email, event.password);
        var valueData = json.decode(loginData);
        var token = valueData['data']['token'];
        var userData = valueData['data']['user'];
        var subValue = await SubscriptionRepository.getSubcriptionActive();
        var challengeValue = await ReminderRepository.getChallengeGroup();
        sharedPreferences.setString("userdata", json.encode(userData));
        sharedPreferences.setString("subdata", subValue);
        sharedPreferences.setString("challengedata", challengeValue);
        if (token != null) {
          yield LoginSuccess(token: token, userData: userData);
        } else {
          yield LoginFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        yield LoginFailure(error: err.message ?? 'An unknown error occured');
      }
    }
    if (event is LoginFacebookButton) {
      final FacebookLoginResult result = await facebookSignIn.logIn(['email']);

      switch (result.status) {
        case FacebookLoginStatus.loggedIn:
          final FacebookAccessToken accessToken = result.accessToken;
          var graphResponse = await http.get(
              'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token='
              '${accessToken.token}');
          var profile = json.decode(graphResponse.body);
          yield LoginLoading();
          try {
            var loginData = await AuthRepository.socialauthlogin(
              email: profile['email'],
              authType: "social",
            );
            var valueData = json.decode(loginData);
            var token = valueData['data']['token'];
            var userData = valueData['data']['user'];
            sharedPreferences.setString("token", valueData['data']['token']);
            var subValue = await SubscriptionRepository.getSubcriptionActive();
            var challengeValue = await ReminderRepository.getChallengeGroup();
            sharedPreferences.setString("userdata", json.encode(userData));
            sharedPreferences.setString("subdata", subValue);
            sharedPreferences.setString("challengedata", challengeValue);

            if (token != null) {
              yield LoginSuccess(token: token, userData: userData);
            } else {
              yield LoginFailure(error: 'Something very weird just happened');
            }
          } catch (err) {
            print(err);
            yield LoginFailure(error: err.message);
          }
          break;
        case FacebookLoginStatus.cancelledByUser:
          break;
        case FacebookLoginStatus.error:
          break;
      }
    }
    if (event is LoginGoogleButton) {
      try {
        var here = await _googleSignIn.signIn();
        yield LoginLoading();
        var loginData = await AuthRepository.socialauthlogin(
          email: here.email,
          authType: "social",
        );
        var valueData = json.decode(loginData);
        var token = valueData['data']['token'];
        var userData = valueData['data']['user'];
        sharedPreferences.setString("token", valueData['data']['token']);
        var subValue = await SubscriptionRepository.getSubcriptionActive();
        var challengeValue = await ReminderRepository.getChallengeGroup();
        sharedPreferences.setString("userdata", json.encode(userData));
        sharedPreferences.setString("subdata", subValue);
        sharedPreferences.setString("challengedata", challengeValue);

        if (token != null) {
          yield LoginSuccess(token: token, userData: userData);
        } else {
          yield LoginFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield LoginFailure(error: err.message);
      }
    }
  }
}
