import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class RecoverEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class RecoverButton extends RecoverEvent{
  final String email;

  RecoverButton({
    @required this.email,
  });


  @override
  List<Object> get props => [email
  ];
}