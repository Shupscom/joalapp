import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/recover/recover_event.dart';
import 'package:joalapp/bloc/recover/recover_state.dart';
import 'package:joalapp/repository/auth_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RecoverBloc extends Bloc<RecoverEvent,RecoverState>{
  @override
  // TODO: implement initialState
  RecoverState get initialState => RecoverInitial();
  @override
  Stream<RecoverState> mapEventToState(RecoverEvent event) async*{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
   if(event is RecoverButton){
     yield RecoverLoading();
     try {
       var userModel= await AuthRepository.recoverPassword(email: event.email);
       sharedPreferences.setString("user_email", event.email);
       var value = json.decode(userModel);
       if (value['success'] == true) {
         yield RecoverSuccess(userData: value['data']);
       } else {
         yield RecoverFailure(error: 'Something very weird just happened');
       }
     } catch (err) {
       print(err);
       yield RecoverFailure(error: err.message);
     }
   }
  }

}