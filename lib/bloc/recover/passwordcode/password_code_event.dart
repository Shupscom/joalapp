import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class PasswordCodeEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class PasswordCodeButton extends PasswordCodeEvent{
  final String email;
  final int code;

  PasswordCodeButton({
    @required this.code,
    @required this.email
  });


  @override
  List<Object> get props => [code,email
  ];
}