import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/recover/passwordcode/password_code_event.dart';
import 'package:joalapp/bloc/recover/passwordcode/password_code_state.dart';
import 'package:joalapp/repository/auth_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PasswordCodeBloc extends Bloc<PasswordCodeEvent,PasswordCodeState>{
  @override
  // TODO: implement initialState
  PasswordCodeState get initialState => PasswordCodeInitial();
  @override
  Stream<PasswordCodeState> mapEventToState(PasswordCodeEvent event) async*{

    if(event is PasswordCodeButton){
      yield PasswordCodeLoading();
      try {
        var userModel= await AuthRepository.recoverPasswordCode(
          code: event.code, email: event.email
        );
        var value = json.decode(userModel);
        if (value['success'] == true) {
          yield PasswordCodeSuccess(userData: value['data']);
        } else {
          yield PasswordCodeFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield PasswordCodeFailure(error: err.message);
      }
    }
  }

}