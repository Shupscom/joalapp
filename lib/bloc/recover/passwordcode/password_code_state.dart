import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class PasswordCodeState extends Equatable{
  @override
  List<Object> get props => [];
}

class PasswordCodeInitial extends PasswordCodeState {}

class PasswordCodeLoading extends PasswordCodeState {}

class PasswordCodeSuccess extends PasswordCodeState {
  String userData;

  PasswordCodeSuccess({@required this.userData});

  @override
  List<Object> get props => [userData];

  @override
  String toString() => 'RecoverSuccess { userData: $userData }';
}

class PasswordCodeFailure extends PasswordCodeState {
  final String error;

  PasswordCodeFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'RegisterFailure { error: $error }';
}