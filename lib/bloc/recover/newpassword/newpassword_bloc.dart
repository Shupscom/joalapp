
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:joalapp/bloc/recover/newpassword/newpassword_event.dart';
import 'package:joalapp/bloc/recover/newpassword/newpassword_state.dart';
import 'package:joalapp/repository/auth_repository.dart';

class NewPasswordBloc extends Bloc<NewPasswordEvent,NewPasswordState>{
  @override
  // TODO: implement initialState
  NewPasswordState get initialState => NewPasswordInitial();
  @override
  Stream<NewPasswordState> mapEventToState(NewPasswordEvent event) async*{
    if(event is NewPasswordButton){
      yield NewPasswordLoading();
      try {
        var userModel= await AuthRepository.newPassword(
            token: event.token, password: event.password
        );
        var value = json.decode(userModel);
        print(value);
        if (value['success'] == true) {
          yield NewPasswordSuccess();
        } else {
          yield NewPasswordFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield NewPasswordFailure(error: err.message);
      }
    }
  }

}