import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class NewPasswordState extends Equatable{
  @override
  List<Object> get props => [];
}

class NewPasswordInitial extends NewPasswordState {}

class NewPasswordLoading extends NewPasswordState {}

class NewPasswordSuccess extends NewPasswordState {
}

class NewPasswordFailure extends NewPasswordState {
  final String error;

  NewPasswordFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'PasswordCodeFailure { error: $error }';
}