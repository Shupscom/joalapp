import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class NewPasswordEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class NewPasswordButton extends NewPasswordEvent{
  final String token;
  final String password;

  NewPasswordButton({
    @required this.token,
    @required this.password
  });


  @override
  List<Object> get props => [token,password
  ];
}