import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class RecoverState extends Equatable{
  @override
  List<Object> get props => [];
}

class RecoverInitial extends RecoverState {}

class RecoverLoading extends RecoverState {}

class RecoverSuccess extends RecoverState {
  String userData;

  RecoverSuccess({@required this.userData});

  @override
  List<Object> get props => [userData];

  @override
  String toString() => 'RecoverSuccess { userData: $userData }';
}

class RecoverFailure extends RecoverState {
  final String error;

  RecoverFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'RegisterFailure { error: $error }';
}
