import 'package:equatable/equatable.dart';
import 'package:joalapp/model/nugget_model.dart';
import 'package:meta/meta.dart';

abstract class NuggetState extends Equatable{
  List<NuggetModel> nuggetData;

  NuggetState({@required this.nuggetData});
  @override
  List<Object> get props => [nuggetData];
}

class NuggetInitial extends NuggetState {
  List<NuggetModel> nuggetData;

  NuggetInitial({@required this.nuggetData});

  @override
  List<Object> get props => [nuggetData];

  @override
  String toString() => 'NuggetInitial { nuggetData: $nuggetData }';

}

class NuggetLoading extends NuggetState {}

class NuggetSuccess extends NuggetState {
  List<NuggetModel> nuggetData;

  NuggetSuccess({@required this.nuggetData});

  @override
  List<Object> get props => [nuggetData];

  @override
  String toString() => 'NuggetSuccess { nuggetData: $nuggetData }';
}

class NuggetFailure extends NuggetState {
  final String error;

  NuggetFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'WeightFailure { error: $error }';

}
