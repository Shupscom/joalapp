import 'package:equatable/equatable.dart';
import 'package:joalapp/model/nugget_model.dart';
import 'package:meta/meta.dart';

abstract class ExerciseState extends Equatable{
  List<NuggetModel> nuggetData;

  ExerciseState({@required this.nuggetData});
  @override
  List<Object> get props => [nuggetData];
}

class ExerciseInitial extends ExerciseState {
  List<NuggetModel> nuggetData;

  ExerciseInitial({@required this.nuggetData});

  @override
  List<Object> get props => [nuggetData];

  @override
  String toString() => 'ExerciseInitial { nuggetData: $nuggetData }';

}

class ExerciseLoading extends ExerciseState {}

class ExerciseSuccess extends ExerciseState {
  List<NuggetModel> nuggetData;

  ExerciseSuccess({@required this.nuggetData});

  @override
  List<Object> get props => [nuggetData];

  @override
  String toString() => 'ExerciseSuccess { nuggetData: $nuggetData }';
}

class ExerciseFailure extends ExerciseState {
  final String error;

  ExerciseFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ExerciseFailure { error: $error }';

}
