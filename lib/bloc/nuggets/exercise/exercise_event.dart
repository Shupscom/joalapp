import 'package:equatable/equatable.dart';

abstract class ExerciseEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class LoadButtonExercise extends ExerciseEvent{}