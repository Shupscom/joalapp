import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:joalapp/bloc/nuggets/exercise/exercise_event.dart';
import 'package:joalapp/bloc/nuggets/exercise/exercise_state.dart';
import 'package:joalapp/model/nugget_model.dart';
import 'package:joalapp/repository/nugget_repository.dart';

class ExerciseBloc extends Bloc<ExerciseEvent,ExerciseState> {

  @override
  // TODO: implement initialState
  ExerciseState get initialState => ExerciseLoading();

  @override
  Stream<ExerciseState> mapEventToState(ExerciseEvent event) async* {
    if(event is LoadButtonExercise){
      try {
        var nuggets = await NuggetRepository.getNuggetByExerciseCategory();
        var nuggetData = json.decode(nuggets);
        List<NuggetModel> nuggetValues = [];
        for(var item in nuggetData['data']){
          NuggetModel nuggetModel = NuggetModel.fromJson(item);
          nuggetValues.add(nuggetModel);
        }
        if (nuggetData['success'] == true){
          yield ExerciseInitial(nuggetData: nuggetValues);
        } else {
          yield ExerciseFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield ExerciseFailure(error: err.message);
      }

    }
  }
}