import 'package:equatable/equatable.dart';import 'package:joalapp/model/nugget_model.dart';
import 'package:meta/meta.dart';

abstract class NutritiousState extends Equatable{
  List<NuggetModel> nuggetData;

  NutritiousState({@required this.nuggetData});
  @override
  List<Object> get props => [nuggetData];
}

class NutritiousInitial extends NutritiousState {
  List<NuggetModel> nuggetData;

  NutritiousInitial({@required this.nuggetData});

  @override
  List<Object> get props => [nuggetData];

  @override
  String toString() => 'NutritiousInitial { nuggetData: $nuggetData }';

}

class NutritiousLoading extends NutritiousState {}

class NutritiousSuccess extends NutritiousState {
  List<NuggetModel> nuggetData;

  NutritiousSuccess({@required this.nuggetData});

  @override
  List<Object> get props => [nuggetData];

  @override
  String toString() => 'NuggetSuccess { nuggetData: $nuggetData }';
}

class NutritiousFailure extends NutritiousState {
  final String error;

  NutritiousFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'NutritiousFailure { error: $error }';

}
