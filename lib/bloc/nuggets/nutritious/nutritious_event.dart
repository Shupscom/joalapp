import 'package:equatable/equatable.dart';

abstract class NutritiousEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class LoadButtonNutritious extends NutritiousEvent{}