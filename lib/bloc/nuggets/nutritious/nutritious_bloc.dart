import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:joalapp/bloc/nuggets/nutritious/nutritious_event.dart';
import 'package:joalapp/bloc/nuggets/nutritious/nutritious_state.dart';
import 'package:joalapp/model/nugget_model.dart';
import 'package:joalapp/repository/nugget_repository.dart';

class NutritiousBloc extends Bloc<NutritiousEvent,NutritiousState> {

  @override
  // TODO: implement initialState
  NutritiousState get initialState => NutritiousLoading();

  @override
  Stream<NutritiousState> mapEventToState(NutritiousEvent event) async* {
    if(event is LoadButtonNutritious){
      try {
        var nuggets = await NuggetRepository.getNuggetByNutritiousCategory();
        var nuggetData = json.decode(nuggets);
        print(nuggetData);
        List<NuggetModel> nuggetValues = [];
        for(var item in nuggetData['data']){
          NuggetModel nuggetModel = NuggetModel.fromJson(item);
          nuggetValues.add(nuggetModel);
        }
        if (nuggetData['success'] == true){
          yield NutritiousInitial(nuggetData: nuggetValues);
        } else {
          yield NutritiousFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield NutritiousFailure(error: err.message);
      }

    }
  }
}