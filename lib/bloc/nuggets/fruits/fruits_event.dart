import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class FruitsEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class LoadButtonFruits extends FruitsEvent{}