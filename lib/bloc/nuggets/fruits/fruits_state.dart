import 'package:equatable/equatable.dart';
import 'package:joalapp/model/nugget_model.dart';
import 'package:meta/meta.dart';

abstract class FruitsState extends Equatable{
  List<NuggetModel> nuggetData;

  FruitsState({@required this.nuggetData});
  @override
  List<Object> get props => [nuggetData];
}

class FruitsInitial extends FruitsState {
  List<NuggetModel> nuggetData;

  FruitsInitial({@required this.nuggetData});

  @override
  List<Object> get props => [nuggetData];

  @override
  String toString() => 'FruitsInitial { nuggetData: $nuggetData }';

}

class FruitsLoading extends FruitsState {}

class FruitsSuccess extends FruitsState {
  List<NuggetModel> nuggetData;

  FruitsSuccess({@required this.nuggetData});

  @override
  List<Object> get props => [nuggetData];

  @override
  String toString() => 'FruitsSuccess { nuggetData: $nuggetData }';
}

class FruitsFailure extends FruitsState {
  final String error;

  FruitsFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'FruitsFailure { error: $error }';

}
