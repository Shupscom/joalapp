import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/nuggets/fruits/fruits_event.dart';
import 'package:joalapp/bloc/nuggets/fruits/fruits_state.dart';
import 'package:joalapp/model/nugget_model.dart';
import 'package:joalapp/repository/nugget_repository.dart';

class FruitsBloc extends Bloc<FruitsEvent,FruitsState> {

  @override
  // TODO: implement initialState
  FruitsState get initialState => FruitsLoading();

  @override
  Stream<FruitsState> mapEventToState(FruitsEvent event) async* {
    if(event is LoadButtonFruits){
      try {
        var nuggets = await NuggetRepository.getNuggetByFruitCategory();
        var nuggetData = json.decode(nuggets);
        List<NuggetModel> nuggetValues = [];
        for(var item in nuggetData['data']){
          NuggetModel nuggetModel = NuggetModel.fromJson(item);
          nuggetValues.add(nuggetModel);
        }
        if (nuggetData['success'] == true){
          yield FruitsInitial(nuggetData: nuggetValues);
        } else {
          yield FruitsFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield FruitsFailure(error: err.message);
      }

    }
  }
}