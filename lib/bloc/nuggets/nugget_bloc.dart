import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/nuggets/nugget_event.dart';
import 'package:joalapp/bloc/nuggets/nugget_state.dart';
import 'package:joalapp/model/nugget_model.dart';
import 'package:joalapp/repository/nugget_repository.dart';

class NuggetBloc extends Bloc<NuggetEvent,NuggetState> {

  @override
  // TODO: implement initialState
  NuggetState get initialState => NuggetLoading();

  @override
  Stream<NuggetState> mapEventToState(NuggetEvent event) async* {
    if(event is LoadButton){
      try {
        var nuggets = await NuggetRepository.getNuggetByCategory();
        var nuggetData = json.decode(nuggets);
        print(nuggetData);
        List<NuggetModel> nuggetValues = [];
        for(var item in nuggetData['data']){
          NuggetModel nuggetModel = NuggetModel.fromJson(item);
          nuggetValues.add(nuggetModel);
        }
        if (nuggetData['success'] == true) {
          yield NuggetInitial(nuggetData: nuggetValues);
        } else {
          yield NuggetFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield NuggetFailure(error: err.message);
      }

    }
  }
}