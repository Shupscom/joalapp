import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class NuggetEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class LoadButton extends NuggetEvent{}