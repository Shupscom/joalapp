import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class EditProfileEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class EditProfileButton extends EditProfileEvent{
  final String firstName;
  final String lastName;
  final String gender;
  final int age;
  final int height;
  final int weight;
  final String activityLevel;
  final String weightGoal;
  final String weightUnit;
  final String heightUnit;

  EditProfileButton({
    @required this.firstName,
    @required this.lastName,
    @required this.gender,
    @required this.age,
    @required this.height,
    @required this.weight,
    @required this.activityLevel,
    @required this.weightGoal,
    @required this.weightUnit,
    @required this.heightUnit
  });
  @override
  List<Object> get props => [firstName,lastName,gender,age, height, weight,activityLevel, weightGoal,
  weightUnit, heightUnit];
}