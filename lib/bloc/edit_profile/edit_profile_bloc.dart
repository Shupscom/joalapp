import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/edit_profile/edit_profile_event.dart';
import 'package:joalapp/bloc/edit_profile/edit_profile_state.dart';
import 'package:joalapp/repository/auth_repository.dart';

class EditProfileBloc extends Bloc<EditProfileEvent,EditProfileState> {

  @override
  // TODO: implement initialState
  EditProfileState get initialState => EditProfileInitial();

  @override
  Stream<EditProfileState> mapEventToState(EditProfileEvent event) async* {
    if(event is EditProfileButton){
      yield EditProfileLoading();
      try {
        var userModel= await AuthRepository.userUpdate(
           firstName: event.firstName,
            lastName: event.lastName,
            height: event.height,
            weight: event.weight,
            age: event.age,
            gender: event.gender,
           activityLevel: event.activityLevel,
           weightGoal: event.weightGoal,
            weightUnit: event.weightUnit,
            heightUnit: event.heightUnit
        );
        var value = json.decode(userModel);
        if (value['success'] == true) {
          yield EditProfileSuccess(userData: value['data']);
        } else {
          yield EditProfileFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield EditProfileFailure(error: err.message);
      }

    }
  }
}