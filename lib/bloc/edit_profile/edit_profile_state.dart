import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class EditProfileState extends Equatable{
  @override
  List<Object> get props => [];
}

class EditProfileInitial extends EditProfileState {}

class EditProfileLoading extends EditProfileState {}

class EditProfileSuccess extends EditProfileState {
  Map<String,dynamic> userData;

  EditProfileSuccess({@required this.userData});

  @override
  List<Object> get props => [userData];

  @override
  String toString() => 'EditProfileSuccess { userData: $userData }';
}

class EditProfileFailure extends EditProfileState {
  final String error;

  EditProfileFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'EditProfileFailure { error: $error }';
}