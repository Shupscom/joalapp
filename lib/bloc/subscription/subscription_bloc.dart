import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/subscription/subscription_event.dart';
import 'package:joalapp/bloc/subscription/subscription_state.dart';
import 'package:joalapp/model/subscription_model.dart';
import 'package:joalapp/repository/subscription_repository.dart';

class SubscriptionBloc extends Bloc<SubscriptionEvent,SubscriptionState> {

  @override
  // TODO: implement initialState
  SubscriptionState get initialState => SubscriptionLoading();
  @override
  Stream<SubscriptionState> mapEventToState(SubscriptionEvent event) async* {
    if (event is LoadSubscriptionData) {
      try {
        var subscription = await SubscriptionRepository.getAllSubcription();
        var subscriptionData = json.decode(subscription);
        print(subscriptionData['data']);
        List<SubscriptionModel> subscriptionValues = [];
        for (var item in subscriptionData['data']) {
          SubscriptionModel subscriptionModel = SubscriptionModel.fromJson(item);
          subscriptionValues.add(subscriptionModel);
        }
        if (subscriptionData['success'] == true) {
          yield SubscriptionInitial(subscriptionData: subscriptionValues);
        } else {
          yield SubscriptionFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield SubscriptionFailure(error: err.message);
      }
    }
  }
}