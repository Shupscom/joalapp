import 'package:equatable/equatable.dart';
import 'package:joalapp/bloc/subscription/subscription_event.dart';
import 'package:joalapp/model/subscription_model.dart';
import 'package:meta/meta.dart';

abstract class SubscriptionState extends Equatable{
  List<SubscriptionModel> subscriptionData;

  SubscriptionState({@required this.subscriptionData});
  @override
  List<Object> get props => [subscriptionData];

}
class SubscriptionInitial extends SubscriptionState {
  List<SubscriptionModel> subscriptionData;

  SubscriptionInitial({@required this.subscriptionData});

  @override
  List<Object> get props => [subscriptionData];

  @override
  String toString() => 'SuperMealInitial { subscriptionData: $subscriptionData }';

}

class SubscriptionLoading extends SubscriptionState{}

class SubscriptionSuccess extends SubscriptionState {
}

class SubscriptionFailure extends SubscriptionState {
  final String error;

  SubscriptionFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SubscriptionFailure { error: $error }';

}