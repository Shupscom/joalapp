import 'package:equatable/equatable.dart';

abstract class SubscriptionEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class LoadSubscriptionData extends SubscriptionEvent{}