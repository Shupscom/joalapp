import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:joalapp/bloc/payment/payment_event.dart';
import 'package:joalapp/bloc/payment/payment_state.dart';
import 'package:joalapp/model/payment_model.dart';
import 'package:joalapp/repository/payment_subscription_repository.dart';

class PaymentBloc extends Bloc<PaymentEvent,PaymentState>{
  @override
  // TODO: implement initialState
  PaymentState get initialState => PaymentInitial();
  @override
  Stream<PaymentState> mapEventToState(PaymentEvent event) async*{
    if(event is LoadPaymentButton){
      yield PaymentLoading();
      try {
        var paymentData= await PaymentSubscriptionRepository.payment(currency: event.currency, plan: event.plan,
            channel: event.channel, cardNumber: event.cardNumber, cvv: event.cvv, expiryMonth: event.expiryMonth,
            expiryYear: event.expiryYear);
        var value = json.decode(paymentData);

        if (value['success'] == true) {
          yield PaymentSuccess(paymentModel: value['data']);
        } else {
          yield PaymentFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield PaymentFailure(error: err.message);
      }
    }
  }

}