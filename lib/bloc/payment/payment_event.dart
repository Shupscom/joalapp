import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

abstract class PaymentEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class LoadPaymentButton extends PaymentEvent{
 final String currency;
 final String plan;
 final String channel;
 final String cardNumber;
 final String cvv;
 final int expiryMonth;
 final int expiryYear;

 LoadPaymentButton({
   @required this.currency,
   @required this.plan,
   @required this.channel,
   @required this.cardNumber,
   @required this.cvv,
   @required this.expiryMonth,
   @required this.expiryYear
 });

 @override
 List<Object> get props => [currency,plan,channel,cardNumber,cvv,expiryMonth,expiryYear];
}