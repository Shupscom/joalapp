import 'package:equatable/equatable.dart';
import 'package:joalapp/model/payment_model.dart';
import 'package:meta/meta.dart';

abstract class PaymentState extends Equatable{
  @override
  List<Object> get props => [];
}

class PaymentInitial extends PaymentState {}

class PaymentLoading extends PaymentState {}

class PaymentSuccess extends PaymentState {
  Map<String,dynamic> paymentModel;

  PaymentSuccess({@required this.paymentModel});

  @override
  List<Object> get props => [paymentModel];

  @override
  String toString() => 'RegisterSuccess { paymentModel: $paymentModel }';
}

class PaymentFailure extends PaymentState {
  final String error;

  PaymentFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'RegisterFailure { error: $error }';
}
