import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class WgoalEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class WgoalButton extends WgoalEvent{
  final String weightGoal;
  WgoalButton({
    @required this.weightGoal,
  });

  @override
  List<Object> get props => [weightGoal];
}