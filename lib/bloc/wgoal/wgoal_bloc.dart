import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/wgoal/wgoal_event.dart';
import 'package:joalapp/bloc/wgoal/wgoal_state.dart';
import 'package:joalapp/repository/auth_repository.dart';
import 'package:joalapp/repository/reminder_repository.dart';
import 'package:joalapp/repository/subscription_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

class WgoalBloc extends Bloc<WgoalEvent,WgoalState> {

  @override
  // TODO: implement initialState
  WgoalState get initialState => WgoalInitial();

  @override
  Stream<WgoalState> mapEventToState(WgoalEvent event) async* {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    if(event is WgoalButton){
      yield WgoalLoading();
      try {
        var userModel= await AuthRepository.wgoalUpdate(
            weightGoal: event.weightGoal
        );
        var value = json.decode(userModel);
        sharedPreferences.setString("userdata", json.encode(value['data']));
        var subValue = await SubscriptionRepository.getSubcriptionActive();
        var challengeValue = await ReminderRepository.getChallengeGroup();
        sharedPreferences.setString("challengedata", challengeValue);
        sharedPreferences.setString("subdata", subValue);
        if (value['success'] == true) {
          yield WgoalSuccess(userData: value['data']);
        } else {
          yield WgoalFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield WgoalFailure(error: err.message);
      }

    }
  }
}