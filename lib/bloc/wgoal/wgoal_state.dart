import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class WgoalState extends Equatable{
  @override
  List<Object> get props => [];
}

class WgoalInitial extends WgoalState {}

class WgoalLoading extends WgoalState {}

class WgoalSuccess extends WgoalState {
  Map<String,dynamic> userData;

  WgoalSuccess({@required this.userData});

  @override
  List<Object> get props => [userData];

  @override
  String toString() => 'WgoalSuccess { error: $userData }';
}

class WgoalFailure extends WgoalState {
  final String error;

  WgoalFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'WgoalFailure { error: $error }';
}