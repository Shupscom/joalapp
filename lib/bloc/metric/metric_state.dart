import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class MetricState extends Equatable{
  @override
  List<Object> get props => [];
}

class MetricInitial extends MetricState {}

class MetricLoading extends MetricState {}

class MetricSuccess extends MetricState {
  Map<String,dynamic> userData;

  MetricSuccess({@required this.userData});

  @override
  List<Object> get props => [userData];

  @override
  String toString() => 'EditProfileSuccess { userData: $userData }';
}

class MetricFailure extends MetricState {
  final String error;

  MetricFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'EditProfileFailure { error: $error }';
}