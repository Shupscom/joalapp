import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class MetricEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class MetricButton extends MetricEvent{
  final String heightUnit;
  final String weightUnit;

  MetricButton({
    @required this.heightUnit,
    @required this.weightUnit
  });


  @override
  List<Object> get props => [heightUnit,weightUnit
  ];
}