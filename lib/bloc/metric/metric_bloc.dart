import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/metric/metric_event.dart';
import 'package:joalapp/bloc/metric/metric_state.dart';
import 'package:joalapp/repository/auth_repository.dart';

class MetricBloc extends Bloc<MetricEvent,MetricState> {

  @override
  // TODO: implement initialState
  MetricState get initialState => MetricInitial();

  @override
  Stream<MetricState> mapEventToState(MetricEvent event) async* {
    if(event is MetricButton){
      yield MetricLoading();
      try {
        var userModel= await AuthRepository.metricUpdate(heightUnit: event.heightUnit, weightUnit: event.weightUnit);
        var value = json.decode(userModel);
        if (value['success'] == true) {
          yield MetricSuccess(userData: value['data']);
        } else {
          yield MetricFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield MetricFailure(error: err.message);
      }

    }
  }
}