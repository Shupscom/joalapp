import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class AuthenticationState extends Equatable {
  @override
  List<Object> get props => [];
}

class AuthenticationUninitialized extends AuthenticationState {}

class AuthenticationAuthenticated extends AuthenticationState {
  Map<String,dynamic> userData;

  AuthenticationAuthenticated({@required this.userData});

  @override
  List<Object> get props => [userData];

  @override
  String toString() => 'AuthenticationAuthenticated { userData: $userData }';
}

class AuthenticationStateShowOnBoarding extends AuthenticationState {}

class AuthenticationUnauthenticated extends AuthenticationState {}

class AuthenticationLoading extends AuthenticationState {}

class AuthenticationStateTimeOut extends AuthenticationState{}

class AuthenticationFailure extends AuthenticationState {
  final String message;

  AuthenticationFailure({@required this.message});

  @override
  List<Object> get props => [message];
}