import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:joalapp/bloc/authentication/authenticate_event.dart';
import 'package:joalapp/bloc/authentication/authenticate_state.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthenticationBloc extends Bloc<AuthenticationEvent, AuthenticationState>{
  Timer _inActivityTimer;
  Timer _timeoutTimer;
  bool isSignedIn = false;
  @override
  // TODO: implement initialState
  AuthenticationState get initialState => AuthenticationUninitialized();
  @override
  Stream<AuthenticationState> mapEventToState(AuthenticationEvent event) async* {
//    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    // TODO: implement mapEventToState
    if (event is AuthenticationEventAppInactive) {
      if (_inActivityTimer == null && isSignedIn) {
        print(_inActivityTimer);
        _inActivityTimer = Timer(
          Duration(seconds: 299),
              () {
            _inActivityTimer.cancel();
            _inActivityTimer = null;
            this.add(LoggedOut());
          },
        );
        _timeoutTimer = Timer(Duration(seconds: 301), () {
          _timeoutTimer.cancel();
          _timeoutTimer = null;
          this.add(AuthenticationEventTimeOut());
        });
      }
    } else if (event is AuthenticationEventTimeOut) {
//      await AccountRepository.deleteToken();
      yield AuthenticationStateTimeOut();
    } else if (event is AuthenticationEventAppResumed) {
      if (_inActivityTimer != null) {
        _inActivityTimer.cancel();
        _timeoutTimer.cancel();
        _inActivityTimer = null;
        _timeoutTimer = null;
      }
    }else if (event is AppStarted) {


//      await AccountRepository.logout();
//      UserPreferences().removeUser();
      isSignedIn = false;
//      final bool hasToken = await AccountRepository.hasToken();
//      if (hasToken) {
//        print(hasToken);
//      } else {
//        yield AuthenticationUnauthenticated();
//      }
//      yield AuthenticationUnauthenticated();
//     yield AuthenticationStateShowOnBoarding();
    }
    else if (event is LoggedIn) {
//      yield AuthenticationLoading();
//      AccountModel accountModel = await AccountRepository.getCurrentUser();
//      if (accountModel != null) {
//        UserPreferences().saveUser(accountModel);
        isSignedIn = true;
        yield AuthenticationAuthenticated(userData: event.userData);
      }
//      yield AuthenticationAuthenticated();
//    } else if (event is LoggedOut) {
//      isSignedIn = false;
//      yield AuthenticationLoading();
////      await AccountRepository.logout();
////      UserPreferences().removeUser();
//      yield AuthenticationUnauthenticated();
//    }
  }
}

