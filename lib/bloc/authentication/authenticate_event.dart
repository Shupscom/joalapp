import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class AuthenticationEvent extends Equatable{
  AuthenticationEvent();
  @override
  List<Object> get props => [];
}

class AppStarted extends AuthenticationEvent{}

class LoggedIn extends AuthenticationEvent{
  final BuildContext context;
  final String token;
  final Map<String,dynamic> userData;

  LoggedIn({ @required this.token, this.context, @required this.userData});

  @override
  List<Object> get props => [token,userData];

}
class LoggedOut extends AuthenticationEvent {}

class EditProfilePressed extends AuthenticationEvent{}

class AuthenticationEventAppInactive extends AuthenticationEvent{}

class AuthenticationEventTimeOut extends AuthenticationEvent{}

class AuthenticationEventAppResumed extends AuthenticationEvent{}

class AuthenticationEventShowOnBoarding extends AuthenticationEvent {}

class AuthenticationEventOnBoardingComplete extends AuthenticationEvent {}