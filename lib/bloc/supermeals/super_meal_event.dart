import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class SuperMealEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class LoadVeganButton extends SuperMealEvent{}


class LoadByCateegory extends SuperMealEvent{
  final String group;
  final String category;

  LoadByCateegory({
    @required this.group,
    @required this.category
  });

  @override
  List<Object> get props => [group,category];
}