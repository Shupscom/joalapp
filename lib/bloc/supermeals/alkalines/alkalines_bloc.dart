import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/supermeals/alkalines/alkalines_event.dart';
import 'package:joalapp/bloc/supermeals/alkalines/alkalines_state.dart';
import 'package:joalapp/model/super_meal.dart';
import 'package:joalapp/repository/super_meal_repository.dart';

class AlkalinesBloc extends Bloc<AlkalinesEvent,AlkalinesState> {

  @override
  // TODO: implement initialState
  AlkalinesState get initialState => AlkalinesLoading();
  @override
  Stream<AlkalinesState> mapEventToState(AlkalinesEvent event) async* {
    if (event is LoadlkalinesButton) {
      try {
        var superMeals = await SuperMealRepository.getMealByAlkaline();
        var superMealData = json.decode(superMeals);
        print(superMealData);
        List<SuperMealModel> superMealValues = [];
        for (var item in superMealData['data']) {
          SuperMealModel superMealModel = SuperMealModel.fromJson(item);
          superMealValues.add(superMealModel);
        }
        if (superMealData['success'] == true) {
          yield AlkalinesInitial(superMealData: superMealValues);
        } else {
          yield AlkalinesFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield AlkalinesFailure(error: err.message);
      }
    }
    if(event is LoadAlkalinensByCateegory){
      yield AlkalinesLoading();
      try {
        var superMeals = await SuperMealRepository.getMealByCategory(event.group, event.category);
        var superMealData = json.decode(superMeals);
        print(superMealData);
        List<SuperMealModel> superMealValues = [];
        for (var item in superMealData['data']) {
          SuperMealModel superMealModel = SuperMealModel.fromJson(item);
          superMealValues.add(superMealModel);
        }
        if (superMealData['success'] == true) {
          yield AlkalinesSuccess(superMealData: superMealValues);
        } else {
          yield AlkalinesFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield AlkalinesFailure(error: err.message);
      }
    }
  }
}