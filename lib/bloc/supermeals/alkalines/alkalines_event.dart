import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class AlkalinesEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class LoadlkalinesButton extends AlkalinesEvent{}

class LoadAlkalinensByCateegory extends AlkalinesEvent{
  final String group;
  final String category;

  LoadAlkalinensByCateegory({
    @required this.group,
    @required this.category
  });

  @override
  List<Object> get props => [group,category];
}