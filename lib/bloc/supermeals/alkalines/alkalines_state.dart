import 'package:equatable/equatable.dart';
import 'package:joalapp/model/super_meal.dart';
import 'package:meta/meta.dart';

abstract class AlkalinesState extends Equatable{
  List<SuperMealModel> superMealData;

  AlkalinesState({@required this.superMealData});
  @override
  List<Object> get props => [superMealData];
}

class AlkalinesInitial extends AlkalinesState {
  List<SuperMealModel> superMealData;

  AlkalinesInitial({@required this.superMealData});

  @override
  List<Object> get props => [superMealData];

  @override
  String toString() => 'SuperMealInitial { superMealData: $superMealData }';

}

class AlkalinesLoading extends AlkalinesState {}

class  AlkalinesSuccess extends AlkalinesState {
  List<SuperMealModel> superMealData;

  AlkalinesSuccess({@required this.superMealData});

  @override
  List<Object> get props => [superMealData];

  @override
  String toString() => 'SuperMealSuccess { superMealData: $superMealData }';
}

class AlkalinesFailure extends AlkalinesState {
  final String error;

  AlkalinesFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SuperMealFailure { error: $error }';

}