import 'package:equatable/equatable.dart';
import 'package:joalapp/model/super_meal.dart';
import 'package:meta/meta.dart';

abstract class NonVeganState extends Equatable{
  List<SuperMealModel> superMealData;

  NonVeganState({@required this.superMealData});
  @override
  List<Object> get props => [superMealData];
}

class NonVeganInitial extends NonVeganState {
  List<SuperMealModel> superMealData;

  NonVeganInitial({@required this.superMealData});

  @override
  List<Object> get props => [superMealData];

  @override
  String toString() => 'SuperMealInitial { superMealData: $superMealData }';

}

class NonVeganLoading extends NonVeganState {}

class NonVeganSuccess extends NonVeganState {
  List<SuperMealModel> superMealData;

  NonVeganSuccess({@required this.superMealData});

  @override
  List<Object> get props => [superMealData];

  @override
  String toString() => 'SuperMealSuccess { superMealData: $superMealData }';
}

class NonVeganFailure extends NonVeganState {
  final String error;

  NonVeganFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SuperMealFailure { error: $error }';

}
