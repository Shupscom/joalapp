import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/supermeals/non-vegan/non-vegan_event.dart';
import 'package:joalapp/bloc/supermeals/non-vegan/non-vegan_state.dart';
import 'package:joalapp/model/super_meal.dart';
import 'package:joalapp/repository/super_meal_repository.dart';

class NonVeganBloc extends Bloc<NonVeganEvent,NonVeganState> {

  @override
  // TODO: implement initialState
  NonVeganState get initialState => NonVeganLoading();
  @override
  Stream<NonVeganState> mapEventToState(NonVeganEvent event) async* {
    if (event is LoadNonVeganButton) {
      try {
        var superMeals = await SuperMealRepository.getMealByNonVegan();
        var superMealData = json.decode(superMeals);
        print(superMealData);
        List<SuperMealModel> superMealValues = [];
        for (var item in superMealData['data']) {
          SuperMealModel superMealModel = SuperMealModel.fromJson(item);
          superMealValues.add(superMealModel);
        }
        if (superMealData['success'] == true) {
          yield NonVeganInitial(superMealData: superMealValues);
        } else {
          yield NonVeganFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield NonVeganFailure(error: err.message);
      }
    }
    if(event is LoadNonVeganByCategory){
      yield NonVeganLoading();
      try {
        var superMeals = await SuperMealRepository.getMealByCategory(event.group, event.category);
        var superMealData = json.decode(superMeals);
        print(superMealData);
        List<SuperMealModel> superMealValues = [];
        for (var item in superMealData['data']) {
          SuperMealModel superMealModel = SuperMealModel.fromJson(item);
          superMealValues.add(superMealModel);
        }
        if (superMealData['success'] == true) {
          yield NonVeganSuccess(superMealData: superMealValues);
        } else {
          yield NonVeganFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield NonVeganFailure(error: err.message);
      }
    }
  }
}