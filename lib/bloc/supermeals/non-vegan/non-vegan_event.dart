import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class NonVeganEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class LoadNonVeganButton extends NonVeganEvent{}

class LoadNonVeganByCategory extends NonVeganEvent{
  final String group;
  final String category;

  LoadNonVeganByCategory({
    @required this.group,
    @required this.category
  });

  @override
  List<Object> get props => [group,category];
}