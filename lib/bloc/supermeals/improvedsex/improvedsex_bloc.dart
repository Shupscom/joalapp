import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/supermeals/improvedsex/improvedsex_event.dart';
import 'package:joalapp/bloc/supermeals/improvedsex/improvedsex_state.dart';
import 'package:joalapp/model/super_meal.dart';
import 'package:joalapp/repository/super_meal_repository.dart';

class ImprovedSexBloc extends Bloc<ImprovedSexEvent,ImprovedSexState> {

@override
// TODO: implement initialState
  ImprovedSexState get initialState => ImprovedSexSLoading();
@override
Stream<ImprovedSexState> mapEventToState(ImprovedSexEvent event) async* {
  if (event is LoadImprovedSexButton) {
    try {
      var superMeals = await SuperMealRepository.getMealByImprovedSexLife();
      var superMealData = json.decode(superMeals);
      print(superMealData);
      List<SuperMealModel> superMealValues = [];
      for (var item in superMealData['data']) {
        SuperMealModel superMealModel = SuperMealModel.fromJson(item);
        superMealValues.add(superMealModel);
      }
      if (superMealData['success'] == true) {
        yield ImprovedSexInitial(superMealData: superMealValues);
      } else {
        yield ImprovedSexSFailure(error: 'Something very weird just happened');
      }
    } catch (err) {
      print(err);
      yield ImprovedSexSFailure(error: err.message);
    }
  }
  if (event is LoadImprovedSexByCategory) {
    yield ImprovedSexSLoading();
    try {
      var superMeals = await SuperMealRepository.getMealByCategory(event.group, event.category);
      var superMealData = json.decode(superMeals);
      print(superMealData);
      List<SuperMealModel> superMealValues = [];
      for (var item in superMealData['data']) {
        SuperMealModel superMealModel = SuperMealModel.fromJson(item);
        superMealValues.add(superMealModel);
      }
      if (superMealData['success'] == true) {
        yield ImprovedSexSSuccess(superMealData: superMealValues);
      } else {
        yield ImprovedSexSFailure(error: 'Something very weird just happened');
      }
    } catch (err) {
      print(err);
      yield ImprovedSexSFailure(error: err.message);
    }
  }

}
}