import 'package:equatable/equatable.dart';
import 'package:joalapp/model/super_meal.dart';
import 'package:meta/meta.dart';

abstract class ImprovedSexState extends Equatable{
  List<SuperMealModel> superMealData;

  ImprovedSexState({@required this.superMealData});
  @override
  List<Object> get props => [superMealData];
}

class ImprovedSexInitial extends ImprovedSexState {
  List<SuperMealModel> superMealData;

  ImprovedSexInitial({@required this.superMealData});

  @override
  List<Object> get props => [superMealData];

  @override
  String toString() => 'ImprovedSexInitial { superMealData: $superMealData }';

}

class ImprovedSexSLoading extends ImprovedSexState {}

class ImprovedSexSSuccess extends ImprovedSexState {
  List<SuperMealModel> superMealData;

  ImprovedSexSSuccess({@required this.superMealData});

  @override
  List<Object> get props => [superMealData];

  @override
  String toString() => 'ImprovedSexSSuccess { superMealData: $superMealData }';
}

class ImprovedSexSFailure extends ImprovedSexState {
  final String error;

  ImprovedSexSFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SuperMealFailure { error: $error }';

}