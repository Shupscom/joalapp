import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class ImprovedSexEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class LoadImprovedSexButton extends ImprovedSexEvent{}

class LoadImprovedSexByCategory extends ImprovedSexEvent{
  final String group;
  final String category;

  LoadImprovedSexByCategory({
    @required this.group,
    @required this.category
  });

  @override
  List<Object> get props => [group,category];
}