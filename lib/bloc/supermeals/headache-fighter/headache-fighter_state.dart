import 'package:equatable/equatable.dart';
import 'package:joalapp/model/super_meal.dart';
import 'package:meta/meta.dart';

abstract class HeadacheFighterState extends Equatable{
  List<SuperMealModel> superMealData;

  HeadacheFighterState({@required this.superMealData});
  @override
  List<Object> get props => [superMealData];
}

class HeadacheFighterInitial extends HeadacheFighterState {
  List<SuperMealModel> superMealData;

  HeadacheFighterInitial({@required this.superMealData});

  @override
  List<Object> get props => [superMealData];

  @override
  String toString() => 'SuperMealInitial { superMealData: $superMealData }';

}

class HeadacheFighterLoading extends HeadacheFighterState {}

class  HeadacheFighterSuccess extends HeadacheFighterState {
  List<SuperMealModel> superMealData;

  HeadacheFighterSuccess({@required this.superMealData});

  @override
  List<Object> get props => [superMealData];

  @override
  String toString() => 'SuperMealSuccess { superMealData: $superMealData }';
}

class HeadacheFighterFailure extends HeadacheFighterState {
  final String error;

  HeadacheFighterFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SuperMealFailure { error: $error }';

}