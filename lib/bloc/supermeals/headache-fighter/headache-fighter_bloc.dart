import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/supermeals/headache-fighter/headache-fighter_event.dart';
import 'package:joalapp/bloc/supermeals/headache-fighter/headache-fighter_state.dart';
import 'package:joalapp/model/super_meal.dart';
import 'package:joalapp/repository/super_meal_repository.dart';

class HeadacheFighterBloc extends Bloc<HeadAcheFighterEvent,HeadacheFighterState> {

  @override
  // TODO: implement initialState
  HeadacheFighterState get initialState => HeadacheFighterLoading();
  @override
  Stream<HeadacheFighterState> mapEventToState(HeadAcheFighterEvent event) async* {
    if (event is LoadHeadAcheFighterButton) {
      try {
        var superMeals = await SuperMealRepository.getMealByHeadFighter();
        var superMealData = json.decode(superMeals);
        print(superMealData);
        List<SuperMealModel> superMealValues = [];
        for (var item in superMealData['data']) {
          SuperMealModel superMealModel = SuperMealModel.fromJson(item);
          superMealValues.add(superMealModel);
        }
        if (superMealData['success'] == true) {
          yield HeadacheFighterInitial(superMealData: superMealValues);
        } else {
          yield HeadacheFighterFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield HeadacheFighterFailure(error: err.message);
      }
    }
    if(event is LoadHeadFighterByCategory){
      yield HeadacheFighterLoading();
      try {
        var superMeals = await SuperMealRepository.getMealByCategory(event.group, event.category);
        var superMealData = json.decode(superMeals);
        print(superMealData);
        List<SuperMealModel> superMealValues = [];
        for (var item in superMealData['data']) {
          SuperMealModel superMealModel = SuperMealModel.fromJson(item);
          superMealValues.add(superMealModel);
        }
        if (superMealData['success'] == true) {
          yield HeadacheFighterSuccess(superMealData: superMealValues);
        } else {
          yield HeadacheFighterFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield HeadacheFighterFailure(error: err.message);
      }
    }
  }
}