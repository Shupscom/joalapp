import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class HeadAcheFighterEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class LoadHeadAcheFighterButton extends HeadAcheFighterEvent{}


class LoadHeadFighterByCategory extends HeadAcheFighterEvent{
  final String group;
  final String category;

  LoadHeadFighterByCategory({
    @required this.group,
    @required this.category
  });

  @override
  List<Object> get props => [group,category];
}