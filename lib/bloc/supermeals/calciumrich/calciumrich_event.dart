import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class CalciumRichEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class LoadCalciumRichButton extends CalciumRichEvent{}

class LoadACalciumRichByCateegory extends CalciumRichEvent{
  final String group;
  final String category;

  LoadACalciumRichByCateegory({
    @required this.group,
    @required this.category
  });

  @override
  List<Object> get props => [group,category];
}