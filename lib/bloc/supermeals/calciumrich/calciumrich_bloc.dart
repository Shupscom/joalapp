import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/supermeals/calciumrich/calciumrich_event.dart';
import 'package:joalapp/bloc/supermeals/calciumrich/calciumrich_state.dart';
import 'package:joalapp/model/super_meal.dart';
import 'package:joalapp/repository/super_meal_repository.dart';

class CalciumRichBloc extends Bloc<CalciumRichEvent,CalciumRichState> {

  @override
  // TODO: implement initialState
  CalciumRichState get initialState => CalciumRichLoading();
  @override
  Stream<CalciumRichState> mapEventToState(CalciumRichEvent event) async* {
    if (event is LoadCalciumRichButton) {
      try {
        var superMeals = await SuperMealRepository.getMealByHeadFighter();
        var superMealData = json.decode(superMeals);
        print(superMealData);
        List<SuperMealModel> superMealValues = [];
        for (var item in superMealData['data']) {
          SuperMealModel superMealModel = SuperMealModel.fromJson(item);
          superMealValues.add(superMealModel);
        }
        if (superMealData['success'] == true) {
          yield CalciumRichInitial(superMealData: superMealValues);
        } else {
          yield CalciumRichFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield CalciumRichFailure(error: err.message);
      }
    }

    if(event is LoadACalciumRichByCateegory){
      try {
        var superMeals = await SuperMealRepository.getMealByCategory(event.group, event.category);
        var superMealData = json.decode(superMeals);
        print(superMealData);
        List<SuperMealModel> superMealValues = [];
        for (var item in superMealData['data']) {
          SuperMealModel superMealModel = SuperMealModel.fromJson(item);
          superMealValues.add(superMealModel);
        }
        if (superMealData['success'] == true) {
          yield CalciumRichSuccess(superMealData: superMealValues);
        } else {
          yield CalciumRichFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield CalciumRichFailure(error: err.message);
      }
    }
  }
}