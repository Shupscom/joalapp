import 'package:equatable/equatable.dart';
import 'package:joalapp/model/super_meal.dart';
import 'package:meta/meta.dart';

abstract class CalciumRichState extends Equatable{
  List<SuperMealModel> superMealData;

  CalciumRichState({@required this.superMealData});
  @override
  List<Object> get props => [superMealData];
}

class CalciumRichInitial extends CalciumRichState {
  List<SuperMealModel> superMealData;

  CalciumRichInitial({@required this.superMealData});

  @override
  List<Object> get props => [superMealData];

  @override
  String toString() => 'CalciumRichInitial { superMealData: $superMealData }';

}

class CalciumRichLoading extends CalciumRichState {}

class  CalciumRichSuccess extends CalciumRichState {
  List<SuperMealModel> superMealData;

  CalciumRichSuccess({@required this.superMealData});

  @override
  List<Object> get props => [superMealData];

  @override
  String toString() => 'SuperMealSuccess { superMealData: $superMealData }';
}

class CalciumRichFailure extends CalciumRichState {
  final String error;

  CalciumRichFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'CalciumRichFailure { error: $error }';

}