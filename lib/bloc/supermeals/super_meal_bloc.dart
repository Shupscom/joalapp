import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/supermeals/super_meal_event.dart';
import 'package:joalapp/bloc/supermeals/super_meal_state.dart';
import 'package:joalapp/model/super_meal.dart';
import 'package:joalapp/repository/super_meal_repository.dart';

class SuperMealBloc extends Bloc<SuperMealEvent,SuperMealState> {

  @override
  // TODO: implement initialState
  SuperMealState get initialState => SuperMealLoading();
  @override
  Stream<SuperMealState> mapEventToState(SuperMealEvent event) async* {
    if (event is LoadVeganButton) {
      try {
        var superMeals = await SuperMealRepository.getMealByVegan();
        var superMealData = json.decode(superMeals);
        print(superMealData);
        List<SuperMealModel> superMealValues = [];
        for (var item in superMealData['data']) {
          SuperMealModel superMealModel = SuperMealModel.fromJson(item);
          superMealValues.add(superMealModel);
        }
        if (superMealData['success'] == true) {
          yield SuperMealInitial(superMealData: superMealValues);
        } else {
          yield SuperMealFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield SuperMealFailure(error: err.message);
      }
    }
    if(event is LoadByCateegory){
      yield SuperMealLoading();
       try {
        var superMeals = await SuperMealRepository.getMealByCategory(event.group, event.category);
        var superMealData = json.decode(superMeals);
        print(superMealData);
        List<SuperMealModel> superMealValues = [];
        for (var item in superMealData['data']) {
          SuperMealModel superMealModel = SuperMealModel.fromJson(item);
          superMealValues.add(superMealModel);
        }
        if (superMealData['success'] == true) {
          yield SuperMealSuccess(superMealData: superMealValues);
        } else {
          yield SuperMealFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield SuperMealFailure(error: err.message);
      }
    }
  }
}