import 'package:equatable/equatable.dart';
import 'package:joalapp/model/super_meal.dart';
import 'package:meta/meta.dart';

abstract class SuperMealState extends Equatable{
  List<SuperMealModel> superMealData;

  SuperMealState({@required this.superMealData});
  @override
  List<Object> get props => [superMealData];
}

class SuperMealInitial extends SuperMealState {
  List<SuperMealModel> superMealData;

  SuperMealInitial({@required this.superMealData});

  @override
  List<Object> get props => [superMealData];

  @override
  String toString() => 'SuperMealInitial { superMealData: $superMealData }';

}

class SuperMealLoading extends SuperMealState {}

class SuperCategoryMealLoading extends SuperMealState {}

class SuperMealSuccess extends SuperMealState {
  List<SuperMealModel> superMealData;

  SuperMealSuccess({@required this.superMealData});

  @override
  List<Object> get props => [superMealData];

  @override
  String toString() => 'SuperMealSuccess { superMealData: $superMealData }';
}

class SuperMealFailure extends SuperMealState {
  final String error;

  SuperMealFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SuperMealFailure { error: $error }';

}
