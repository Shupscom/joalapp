import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class HighFiberEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class LoadHighFiberButton extends HighFiberEvent{}

class LoadHighFiberByCategory extends HighFiberEvent{
  final String group;
  final String category;

  LoadHighFiberByCategory({
    @required this.group,
    @required this.category
  });

  @override
  List<Object> get props => [group,category];
}