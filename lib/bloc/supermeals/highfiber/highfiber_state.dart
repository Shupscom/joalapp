import 'package:equatable/equatable.dart';
import 'package:joalapp/model/super_meal.dart';
import 'package:meta/meta.dart';

abstract class HighFiberState extends Equatable{
  List<SuperMealModel> superMealData;

  HighFiberState({@required this.superMealData});
  @override
  List<Object> get props => [superMealData];
}

class HighFiberInitial extends HighFiberState {
  List<SuperMealModel> superMealData;

  HighFiberInitial({@required this.superMealData});

  @override
  List<Object> get props => [superMealData];

  @override
  String toString() => 'ImprovedSexInitial { superMealData: $superMealData }';

}

class HighFiberLoading extends HighFiberState {}

class HighFiberSuccess extends HighFiberState {
  List<SuperMealModel> superMealData;

  HighFiberSuccess({@required this.superMealData});

  @override
  List<Object> get props => [superMealData];

  @override
  String toString() => 'HighFiberSuccess { superMealData: $superMealData }';
}

class HighFiberFailure extends HighFiberState {
  final String error;

  HighFiberFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'HighFiberFailure { error: $error }';

}