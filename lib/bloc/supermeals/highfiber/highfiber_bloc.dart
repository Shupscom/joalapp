
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:joalapp/bloc/supermeals/highfiber/highfiber_event.dart';
import 'package:joalapp/bloc/supermeals/highfiber/highfiber_state.dart';
import 'package:joalapp/model/super_meal.dart';
import 'package:joalapp/repository/super_meal_repository.dart';

class HighFiberBloc extends Bloc<HighFiberEvent,HighFiberState> {

  @override
// TODO: implement initialState
  HighFiberState get initialState => HighFiberLoading();
  @override
  Stream<HighFiberState> mapEventToState(HighFiberEvent event) async* {
    if (event is LoadHighFiberButton) {
      try {
        var superMeals = await SuperMealRepository.getMealByHighFiber();
        var superMealData = json.decode(superMeals);
        print(superMealData);
        List<SuperMealModel> superMealValues = [];
        for (var item in superMealData['data']) {
          SuperMealModel superMealModel = SuperMealModel.fromJson(item);
          superMealValues.add(superMealModel);
        }
        if (superMealData['success'] == true) {
          yield HighFiberInitial(superMealData: superMealValues);
        } else {
          yield HighFiberFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield HighFiberFailure(error: err.message);
      }
    }
    if(event is LoadHighFiberByCategory){
      try {
        var superMeals = await SuperMealRepository.getMealByCategory(event.group, event.category);
        var superMealData = json.decode(superMeals);
        List<SuperMealModel> superMealValues = [];
        for (var item in superMealData['data']) {
          SuperMealModel superMealModel = SuperMealModel.fromJson(item);
          superMealValues.add(superMealModel);
        }
        if (superMealData['success'] == true) {
          yield HighFiberSuccess(superMealData: superMealValues);
        } else {
          yield HighFiberFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield HighFiberFailure(error: err.message);
      }
    }
  }
}