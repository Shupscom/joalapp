import 'package:equatable/equatable.dart';
import 'package:joalapp/model/super_meal.dart';
import 'package:meta/meta.dart';

abstract class IronRichState extends Equatable{
  List<SuperMealModel> superMealData;

  IronRichState({@required this.superMealData});
  @override
  List<Object> get props => [superMealData];
}

class IronRichInitial extends IronRichState {
  List<SuperMealModel> superMealData;

  IronRichInitial({@required this.superMealData});

  @override
  List<Object> get props => [superMealData];

  @override
  String toString() => 'SuperMealInitial { superMealData: $superMealData }';

}

class IronRichLoading extends IronRichState {}

class IronRichSuccess extends IronRichState {
  List<SuperMealModel> superMealData;

  IronRichSuccess({@required this.superMealData});

  @override
  List<Object> get props => [superMealData];

  @override
  String toString() => 'SuperMealSuccess { superMealData: $superMealData }';
}

class IronRichFailure extends IronRichState {
  final String error;

  IronRichFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'SuperMealFailure { error: $error }';

}