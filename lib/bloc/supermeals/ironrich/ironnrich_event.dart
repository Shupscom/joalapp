import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class IronRichEvent extends Equatable{
  @override
  List<Object> get props => [];
}

class IronRichButton extends IronRichEvent {}

class LoadIronRichByCategory extends IronRichEvent{
  final String group;
  final String category;

  LoadIronRichByCategory({
    @required this.group,
    @required this.category
  });

  @override
  List<Object> get props => [group,category];
}