import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:joalapp/bloc/supermeals/ironrich/ironnrich_event.dart';
import 'package:joalapp/bloc/supermeals/ironrich/ironrich_state.dart';
import 'package:joalapp/model/super_meal.dart';
import 'package:joalapp/repository/super_meal_repository.dart';

class IronRichBloc extends Bloc<IronRichEvent,IronRichState> {

  @override
  // TODO: implement initialState
  IronRichState get initialState => IronRichLoading();
  @override
  Stream<IronRichState> mapEventToState(IronRichEvent event) async* {
    if (event is IronRichButton) {
      try {
        var superMeals = await SuperMealRepository.getMealByIronRich();
        var superMealData = json.decode(superMeals);
        print(superMealData);
        List<SuperMealModel> superMealValues = [];
        for (var item in superMealData['data']) {
          SuperMealModel superMealModel = SuperMealModel.fromJson(item);
          superMealValues.add(superMealModel);
        }
        if (superMealData['success'] == true) {
          yield IronRichInitial(superMealData: superMealValues);
        } else {
          yield IronRichFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield IronRichFailure(error: err.message);
      }
    }
    if(event is LoadIronRichByCategory){
      try {
        var superMeals = await SuperMealRepository.getMealByCategory(event.group, event.category);
        var superMealData = json.decode(superMeals);
        print(superMealData);
        List<SuperMealModel> superMealValues = [];
        for (var item in superMealData['data']) {
          SuperMealModel superMealModel = SuperMealModel.fromJson(item);
          superMealValues.add(superMealModel);
        }
        if (superMealData['success'] == true) {
          yield IronRichSuccess(superMealData: superMealValues);
        } else {
          yield IronRichFailure(error: 'Something very weird just happened');
        }
      } catch (err) {
        print(err);
        yield IronRichFailure(error: err.message);
      }
    }
  }
}