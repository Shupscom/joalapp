import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
class LinkedText extends StatelessWidget {

  LinkedText({@required this.ontap,this.title,this.colour});
  final Function ontap;
  final String  title;
  final Color colour;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: ontap,
      child: Text(title,
        style: TextStyle(
            color: colour,
           fontWeight: FontWeight.w500,
          fontSize: ScreenUtil().setSp(18.0)
        ),
      ),
    );
  }
}