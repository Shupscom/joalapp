import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:joalapp/components/constants.dart';
import 'package:joalapp/screens/profile/profile_screen.dart';

class MakeBottom extends StatefulWidget {
  String active;
  Map<String, dynamic> userData;

  MakeBottom({@required this.active, this.userData});

  @override
  _MakeBottomState createState() => _MakeBottomState();
}

class _MakeBottomState extends State<MakeBottom> {
  @override
  void initState() {
    // TODO: implement initState
    setState(() {
      print(widget.userData);
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setHeight(80.0),
      child: BottomAppBar(
        color: ksecondaryColour,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            InkWell(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) =>
                    ProfileScreen(userData: widget.userData) ));
              },
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SvgPicture.asset(widget.active == 'profile'
                        ? 'assets/images/profile_icon_active.svg'
                        : 'assets/images/profile_icon.svg'),
                    SizedBox(
                      height: 3,
                    ),
                    widget.active == 'profile'
                        ? Text(
                            'Profile',
                            style: TextStyle(color: kprimaryColour),
                          )
                        : SizedBox()
                  ],
                ),
              ),
            ),
            InkWell(
              onTap: () {
                Navigator.pushNamed(context, '/recommendedMealScreen');
              },
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SvgPicture.asset(widget.active == 'diet_manager'
                        ? 'assets/images/diet_manager_active.svg'
                        : 'assets/images/diet_manager.svg'),
                    SizedBox(
                      height: 3,
                    ),
                    widget.active == 'diet_manager'
                        ? Column(
                      children: [
                        Text(
                          'Diet',
                          style: TextStyle(color: kprimaryColour),
                        ),
                        Text(
                          'Manager',
                          style: TextStyle(color: kprimaryColour),
                        ),
                      ],
                    )
                        : SizedBox()
                  ],
                ),
              ),
            ),
            InkWell(
              onTap: () {
                  Navigator.pushNamed(context, '/statisticScreen');
//                Navigator.push(context, MaterialPageRoute(builder: (context) => DonationTest()));
              },
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SvgPicture.asset(widget.active == 'statistic'
                        ? 'assets/images/stats_active.svg'
                        : 'assets/images/stats.svg'),
                    SizedBox(
                      height: 3,
                    ),
                    widget.active == 'statistic'
                        ? Text(
                            'Statistic',
                            style: TextStyle(color: kprimaryColour),
                          )
                        : SizedBox()
                  ],
                ),
              ),
            ),
            InkWell(
              onTap: () {
                  Navigator.pushNamed(context, '/superMealScreen');
              },
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SvgPicture.asset(widget.active == 'supermeal'
                        ? 'assets/images/supermeal.svg'
                        : 'assets/images/subscription.svg', width: 17, height: 22,),
                    SizedBox(
                      height: 3,
                    ),
                    widget.active == 'supermeal'
                        ? Column(
                      children: [
                        Text(
                          'Super',
                          style: TextStyle(color: kprimaryColour),
                        ),
                        Text(
                          'Meal',
                          style: TextStyle(color: kprimaryColour),
                        ),
                      ],
                    )
                        : SizedBox()
                  ],
                ),
              ),
            ),
            InkWell(
              onTap: () {
                  Navigator.pushNamed(context, '/waterTrackerScreen');
              },
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SvgPicture.asset(widget.active == 'water'
                        ? 'assets/images/water_active.svg'
                        : 'assets/images/water.svg'),
                    SizedBox(
                      height: 3,
                    ),
                    widget.active == 'water'
                        ? Text(
                            'Tracker',
                            style: TextStyle(color: kprimaryColour),
                          )
                        : SizedBox()
                  ],
                ),
              ),
            ),
            InkWell(
              onTap: () {
                  Navigator.pushNamed(context, '/allNuggetScreen');
              },
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SvgPicture.asset(widget.active == 'diary'
                        ? 'assets/images/diary_active.svg'
                        : 'assets/images/diary.svg'),
                    SizedBox(
                      height: 3,
                    ),
                    widget.active == 'diary'
                        ? Column(
                          children: [
                            Text(
                                'Health',
                                style: TextStyle(color: kprimaryColour),
                              ),
                            Text(
                              'Nuggets',
                              style: TextStyle(color: kprimaryColour),
                            ),
                          ],
                        )
                        : SizedBox()
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
