import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

const kprimaryColour = Color(0xFF40853C);
const ksecondaryColour = Colors.white;
const ksecondaryTextColour = Color(0xFF666A86);
final kNow = DateTime.now();
final kFirstDay = DateTime(kNow.year, kNow.month - 3, kNow.day);
final kLastDay = DateTime(kNow.year, kNow.month + 3, kNow.day);

const kTextFieldDecoration = InputDecoration(
  contentPadding:
  EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
  border: OutlineInputBorder(
//    borderRadius: BorderRadius.all(Radius.circular(10.0)),
  ),
  enabledBorder: OutlineInputBorder(
    borderSide:
    BorderSide(color: Color(0xFFD0F1DD), width: 1.0),
//    borderRadius: BorderRadius.all(Radius.circular(10.0)),
  ),
  focusedBorder: OutlineInputBorder(
    borderSide:
    BorderSide(color: Color(0xFFD0F1DD), width: 2.0),
//    borderRadius: BorderRadius.all(Radius.circular(10.0)),
  ),

);

MaterialColor generateMaterialColor(Color color) {
  return MaterialColor(color.value, {
    50: tintColor(color, 0.9),
    100: tintColor(color, 0.8),
    200: tintColor(color, 0.6),
    300: tintColor(color, 0.4),
    400: tintColor(color, 0.2),
    500: color,
    600: shadeColor(color, 0.1),
    700: shadeColor(color, 0.2),
    800: shadeColor(color, 0.3),
    900: shadeColor(color, 0.4),
  });
}

int tintValue(int value, double factor) =>
    max(0, min((value + ((255 - value) * factor)).round(), 255));

Color tintColor(Color color, double factor) => Color.fromRGBO(
    tintValue(color.red, factor),
    tintValue(color.green, factor),
    tintValue(color.blue, factor),
    1);

int shadeValue(int value, double factor) =>
    max(0, min(value - (value * factor).round(), 255));

Color shadeColor(Color color, double factor) => Color.fromRGBO(
    shadeValue(color.red, factor),
    shadeValue(color.green, factor),
    shadeValue(color.blue, factor),
    1);
class Palette {
  static const Color primary = kprimaryColour;
}


void sizess(BuildContext context){
  double defaultScreenWidth = 400.0;
  double defaultScreenHeight = 810.0;
  double screenWidth = defaultScreenWidth;
  double screenHeight = defaultScreenHeight;

  if (screenWidth > 300 && screenWidth < 500) {
    defaultScreenWidth = 450.0;
    defaultScreenHeight = defaultScreenWidth * screenHeight / screenWidth;
  } else if (screenWidth > 500 && screenWidth < 600) {
    defaultScreenWidth = 500.0;
    defaultScreenHeight = defaultScreenWidth * screenHeight / screenWidth;
  } else if (screenWidth > 600 && screenWidth < 700) {
    defaultScreenWidth = 550.0;
    defaultScreenHeight = defaultScreenWidth * screenHeight / screenWidth;
  } else if (screenWidth > 700 && screenWidth < 1050) {
    defaultScreenWidth = 800.0;
    defaultScreenHeight = defaultScreenWidth * screenHeight / screenWidth;
  } else {
    defaultScreenWidth = screenWidth;
    defaultScreenHeight = screenHeight;
  }
  ScreenUtil.init(
    context,
    width: defaultScreenWidth,
    height: defaultScreenHeight,
    allowFontScaling: true,
  );
}