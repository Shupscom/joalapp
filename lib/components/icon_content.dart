import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:joalapp/components/constants.dart';
class IconContent extends StatelessWidget {
  IconContent({this.up,this.middle, this.middleDown, this.down});
  final String up;
  final String middle;
  final String middleDown;
  final String down;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
               Text(up,
                 style: TextStyle(
                   fontSize: ScreenUtil().setSp(16.0),
                   color: kprimaryColour,
                   fontWeight: FontWeight.bold
                 ),
               ),
               Column(
                 children: [
                   Text(middle,
                     style: TextStyle(
                         fontSize: ScreenUtil().setSp(16.0),
                         color: kprimaryColour,
                         fontWeight: FontWeight.bold
                     ),
                   ),
                   SizedBox(height: ScreenUtil().setHeight(10.0),),
                   Text(middleDown,
                     style: TextStyle(
                         fontSize: ScreenUtil().setSp(16.0),
                         color: kprimaryColour,
                         fontWeight: FontWeight.bold
                     ),
                   ),
                 ],
               ),
                Text(down,
                  style: TextStyle(
                      color: kprimaryColour,
                  ),
                )
      ],
    );
  }
}